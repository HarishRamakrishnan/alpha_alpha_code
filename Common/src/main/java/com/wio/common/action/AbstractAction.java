package com.wio.common.action;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;
/**
 * Abstract class implementing the AccessPointAction interface. This class is used to declare utility method
 * shared with all Action implementations
 */
public abstract class AbstractAction implements IRequestAction {
    /**
     * Returns an initialized Gson object with the default configuration
     * @return An initialized Gson object
     */
    protected Gson getGson() {
        return new GsonBuilder()
                //.enableComplexMapKeySerialization()
                //.serializeNulls()
                //.setDateFormat(DateFormat.LONG)
                //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        		//.registerTypeAdapter(APRadioInterfaceSettings.class, new APRadioSettingsModelInstanceCreator())
                .setPrettyPrinting()
                .create();
    }
    
    
    protected JsonObject getBodyFromRequest(JsonObject request)
    {    	
    	JsonObject body = null;
	    if (request.get("body") != null) 
	    {
	         body = request.get("body").getAsJsonObject();
	         return body;
	    }	      
	   return null;   
    }
    
 
    protected User getUserInfoFromRequest(JsonObject request) throws DAOException, BadRequestException
    {
    	User loggedUser=null;
    
    	try
    	{
    		System.out.println("Getting user info from request !");
			GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
			
			String securityToken = getBodyFromRequest(request).get("securityToken").getAsString();
			
			if(RequestValidator.isEmptyField(securityToken))
				throw new BadRequestException("Security Token cannot be empty !"+this.getClass());
			
			List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
		    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SECURITY_TOKEN, securityToken, ComparisonOperator.EQ));  
			
		    loggedUser = genDAO.getGenericObjects(User.class, filterConditions).get(0);
			
			System.out.println("Found user : "+loggedUser.getUsername());
    	}
    	catch (DAOException e1) 
    	{
 			e1.printStackTrace();
    	}	
    	
    	if(loggedUser == null)
    		throw new DAOException("No logged user details found with the Security Token: "+getBodyFromRequest(request).get("securityToken"));
	
    	return loggedUser;
    }
}
