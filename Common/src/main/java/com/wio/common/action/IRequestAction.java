package com.wio.common.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;

/**
 * AccessPointAction defines the methods called by the RequestRouter when it is invoked by a Lambda function. Implementing
 * classes should be able to receive a JsonObject containing the body of the request (taken from the "body" property)
 * of the incoming JSON, and return a String that contains valid json.
 */
public interface IRequestAction {
    /**
     * The main handler method for each requests. This method is called by the RequestRouter when invoked by a Lambda
     * function.
     *
     * @param request       Receives a JsonObject containing t
     * he body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return A string containing valid JSON to be returned to the client
     * @throws BadRequestException    This exception should be thrown whenever request parameters are not valid or improperly
     *                                formatted
     * @throws InternalErrorException This exception should be thrown if an error that is independent from user input happens
     * @throws DAOException 
     * @throws AuthorizationException 
     */
    String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException, AuthorizationException;
}
