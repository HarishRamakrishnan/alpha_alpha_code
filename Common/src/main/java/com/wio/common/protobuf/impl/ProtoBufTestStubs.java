package com.wio.common.protobuf.impl;

import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;

public class ProtoBufTestStubs {

	public static void main(String[] args) {
		ProtoBufTestStubs stub = new ProtoBufTestStubs();
		stub.testConfigProto();
		stub.testWioLogProto();
		stub.testWIOConfigProto();
	}

	private void testWioLogProto() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		WioLogProtoData proto = (WioLogProtoData) protocolType.getMesgSerializer("WIO_LOG");

		proto.messageConstructor();
		proto.setConfigUpdateDetails("log", "Tue Oct  3 11:17:27 2017", "06aa74-f801-a401-b6ytgheb7117",
				"/var/log/AP.log",
				"Tue Oct  3 11:02:47 2017 daemon.warn dnsmasq[1766]: possible DNS-rebind attack detected: infics.com");
		proto.serialize();
		System.out.println("=========>> "+ proto.serialize().getAllFields());

	}

	private void testConfigProto() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		ConfigProtoData proto = (ConfigProtoData) protocolType.getMesgSerializer("CONFIG_UPDATE");
		proto.messageConstructor();
		proto.setConfigUpdateDetails("Config", "12:20", "06aa74-f801-a401-b6ytgheb7117", "Success", "ffc3e01a-dad1-4b76-b307-fc0453a73d4e");
		proto.setConfigParamDetails("key1", "1");
		proto.setConfigParamDetails("key2", "2");
		proto.serialize();
		String output = proto.deSerializeToString(proto.getConfigUpdateByteString().toByteArray());
		
		System.out.println("De-Serialize out ==> "+output);
		System.out.println("===========>>"+proto.serialize().getAllFields());
	}
	
	private void testWIOConfigProto() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		WIOConfigData wioData = (WIOConfigData) protocolType.getMesgSerializer("WIO_CONFIG");
		wioData.messageConstructor();
		wioData.serializeOneOf(2);
		System.out.println("wioData.serialize()==>> "+wioData.serialize().getAllFields());
		wioData.serialize();
		System.out.println("Size ===>> "+wioData.wioConfigByteSize());
		wioData.deSerialize(wioData.getWIOConfigByteString().toByteArray());
	}

}
