package com.wio.common.protobuf.framework;

public interface IMessageProtocol {

	public IMessageSerializer getMessageProtocol(String protocolType);
	
}
