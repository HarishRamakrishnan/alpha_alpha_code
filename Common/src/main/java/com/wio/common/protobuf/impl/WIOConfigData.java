package com.wio.common.protobuf.impl;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Internal.EnumLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.wio.common.config.request.PublishingTopics;
import com.wio.common.protobuf.framework.IMessage;
import com.wio.common.protobuf.framework.IProtobufMessage;
import com.wio.common.protobuf.generated.WIOConfigProto;
import com.wio.common.protobuf.generated.WIOConfigProto.WIOCONFIG;
import com.wio.common.protobuf.generated.WIOConfigProto.WIOCONFIG.WIOCONFIGOneofCase;
import com.wio.common.validation.RequestValidator;


public class WIOConfigData implements IProtobufMessage {

	private WIOCONFIG.Builder wioConfig = null;
	private WIOConfigProto.COMMANDS.Builder cmd = null;
	private WIOConfigProto.CONFIG.Builder config = null;
	private WIOConfigProto.TOPICS.Builder topics = null;
	private WIOConfigProto.FIRMWARE.Builder firmware = null;
	private WIOConfigProto.WIOCONFIG.WIOCONFIGOneofCase wioConfigOneOf = null;
	
	private static IMessage wioConfigInst = new WIOConfigData();
	
	private WIOConfigData() {}
	
	public static IMessage getInstance() {
		return wioConfigInst;
	}
	
	@Override
	public void messageConstructor() {
		
		wioConfig = WIOConfigProto.WIOCONFIG.newBuilder();
		cmd = WIOConfigProto.COMMANDS.newBuilder();
		config = WIOConfigProto.CONFIG.newBuilder();
		topics = WIOConfigProto.TOPICS.newBuilder();
		firmware = WIOConfigProto.FIRMWARE.newBuilder();
	}

	@Override
	public GeneratedMessageV3 serialize() {
		// Set other required fields before setting OneOf value and then return the
		// message.
		if (wioConfigOneOf == WIOCONFIG.WIOCONFIGOneofCase._CMD) {
			wioConfig = wioConfig.setCmd(cmd);
			return wioConfig.build();
		} else if (wioConfigOneOf == WIOCONFIG.WIOCONFIGOneofCase._CONF) {
			wioConfig = wioConfig.setConf(config);
			return wioConfig.build();
		} else if (wioConfigOneOf == WIOCONFIG.WIOCONFIGOneofCase._TOPIC) {
			wioConfig = wioConfig.setTopic(topics);
			return wioConfig.build();
		} else if (wioConfigOneOf == WIOCONFIG.WIOCONFIGOneofCase._FW) {
			wioConfig = wioConfig.setFw(firmware);
			return wioConfig.build();
		} else {
			System.out.println("Nothing matches the desired WIOConfig OneOf.");
			return wioConfig.build();
		}
	}
	
	public void setFirmwareDetails(String fwUrl) {
		if(!RequestValidator.isEmptyField(fwUrl)) {
			firmware = firmware.setFwurl(fwUrl);
		}
		//wioConfig = wioConfig.setFw(firmware);
	}
	
	public void setTopicDetails(PublishingTopics pubTopics) {
		if(!RequestValidator.isEmptyField(pubTopics.getAuth())) {
			topics = topics.setAuth(pubTopics.getAuth());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getEventsArp())) {
			topics = topics.setEventsArp(pubTopics.getEventsArp());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getEventsDhcp())) {
			topics = topics.setEventsDhcp(pubTopics.getEventsDhcp());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getEventsHostapd3())) {
			topics = topics.setEventsHostapd(pubTopics.getEventsHostapd3());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getLog())) {
			topics = topics.setLog(pubTopics.getLog());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getRfMetric())) {
			topics = topics.setRfMetric(pubTopics.getRfMetric());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getRfMgmt())) {
			topics = topics.setRfMgmt(pubTopics.getRfMgmt());
		}
		if(!RequestValidator.isEmptyField(pubTopics.getConfresp())) {
			topics = topics.setConfresp(pubTopics.getConfresp());
		}
		//wioConfig = wioConfig.setTopic(topics);
	}

	public void setConfigDetails(String transID, String script) {
		if(!RequestValidator.isEmptyField(transID)) {
			config = config.setTransid(transID);
		}
		if(!RequestValidator.isEmptyField(script)) {
			config = config.setScript(script);
		}
		
		//wioConfig.setConf(config);
	}
	
	public void setCmdDetails(String command) {
		if(!RequestValidator.isEmptyField(command)) {
			cmd.setCommand(command);
		}
		//wioConfig = wioConfig.setCmd(cmd);
	}
	
	@Override
	public Object deSerialize(byte[] input) {

		WIOConfigProto.WIOCONFIG wioConfig = null;
		
		if(input.length > 0 ) {
			try {
				System.out.println("Start to de-serialize....");
				wioConfig = WIOConfigProto.WIOCONFIG.parser().parseFrom(input);
				System.out.println("De-serialize o/p ====>> "+new String(wioConfig.toByteArray()));
			} catch (InvalidProtocolBufferException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * valid values between 0-4
	 */
	@Override
	public EnumLite serializeOneOf(int value) {
		
		//validate input value
		if(!(value == 0 || value <= 4)) {
			throw new IllegalArgumentException("Accepts value between 0 through 4 only.");
		}
		
		switch(value) {
		case 1:
			wioConfigOneOf = (WIOCONFIGOneofCase) setOneOfMessage(value);
			break;
		case 2:
			wioConfigOneOf = (WIOCONFIGOneofCase) setOneOfMessage(value);
			break;
		case 3:
			wioConfigOneOf = (WIOCONFIGOneofCase) setOneOfMessage(value);
			break;
		case 4:
			wioConfigOneOf = (WIOCONFIGOneofCase) setOneOfMessage(value);
			break;
		case 0:
			wioConfigOneOf = (WIOCONFIGOneofCase) setOneOfMessage(value);
			break;
		}
		return wioConfigOneOf;
	}
	
	private EnumLite setOneOfMessage(int value) {
		return WIOCONFIG.WIOCONFIGOneofCase.forNumber(value);
	}
	
	public ByteString getWIOConfigByteString() {
		return wioConfig.build().toByteString();
	}

	public int wioConfigByteSize() {
		 return	wioConfig.build().toByteString().size();
	}
	
	@Override
	public String description() {
		return null;
	}

	@Override
	public String schema() {
		return null;
	}

}
