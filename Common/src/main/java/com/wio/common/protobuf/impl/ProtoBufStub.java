package com.wio.common.protobuf.impl;

import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;

public class ProtoBufStub {

	public static void main(String[] args) {
		
		ProtoBufStub stub = new ProtoBufStub();
		stub.invoke();
	}

	private void invoke() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		WIOConfigData wioData = (WIOConfigData) protocolType.getMesgSerializer("WIO_CONFIG");
		wioData.messageConstructor();
		wioData.serializeOneOf(2);
		System.out.println("wioData.serialize()==>> "+wioData.serialize().getAllFields());
		wioData.serialize();
		System.out.println("Size ===>> "+wioData.wioConfigByteSize());
		wioData.deSerialize(wioData.getWIOConfigByteString().toByteArray());
		
	}
	
}
