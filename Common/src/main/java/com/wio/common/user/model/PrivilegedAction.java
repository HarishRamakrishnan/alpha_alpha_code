package com.wio.common.user.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.PRIVILEGED_ACTION)
public class PrivilegedAction 
{
	private String privilege;
	private String action;
	private String inputRole;
	
	
	public PrivilegedAction()
	{
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((privilege == null) ? 0 : privilege.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrivilegedAction other = (PrivilegedAction) obj;
		if (privilege == null) {
			if (other.privilege != null)
				return false;
		} else if (!privilege.equals(other.privilege))
			return false;
		return true;
	}

	@DynamoDBHashKey(attributeName = "PRIVILEGE")
	public String getPrivilege() {
		return privilege;
	}
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	
	
	@DynamoDBAttribute(attributeName = "ACTION")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	@DynamoDBAttribute(attributeName = "INPUT_ROLE")
	public String getInputRole() {
		return inputRole;
	}
	public void setInputRole(String inputRole) {
		this.inputRole = inputRole;
	}
}
