package com.wio.common.exception;

public class InvalidArugumentException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static final String PREFIX = "Invalid_Arugument: ";
	
	public InvalidArugumentException(String mesg) {
		super(PREFIX+mesg);
	}
	
	public InvalidArugumentException(String mesg, Exception e) {
		super(PREFIX+mesg , e);
	}
	

}
