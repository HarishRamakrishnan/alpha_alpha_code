package com.wio.common.generic.dao;

import java.util.List;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;

public class QueryHelper 
{
	public static FilterCondition CreateQueryFilter(String param, String value, ComparisonOperator operator)
	{
		FilterCondition condition = new FilterCondition();
		
		condition.setParam(param);
		condition.setValue(value);
		condition.setOperator(operator);
		
		return condition;		
	}
	
	public static FilterCondition2 CreateQueryFilterV2(String param, List<AttributeValue> values, ComparisonOperator operator)
	{
		FilterCondition2 condition = new FilterCondition2();
		
		condition.setParam(param);
		condition.setValues(values);
		condition.setOperator(operator);
		
		return condition;		
	}

}
