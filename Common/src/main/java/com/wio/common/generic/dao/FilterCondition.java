package com.wio.common.generic.dao;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;

public class FilterCondition 
{
	private String param;
	private String value;
	private ComparisonOperator operator;
	
	
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public ComparisonOperator getOperator() {
		return operator;
	}
	public void setOperator(ComparisonOperator operator) {
		this.operator = operator;
	}
	
}
