package com.wio.common.generic.dao;

import java.util.List;

import com.wio.common.exception.DAOException;

public interface GenericDAO 
{
	<T> void saveGenericObjects(List<T> genericObjects) throws DAOException;
	
	<T> void saveGenericObject(T genericObject) throws DAOException;
	
	<T> void saveGenericObjectWithPrefix(T genericObject, String tableName, String prefix) throws DAOException;
	
    <T> List<T> getGenericObjects(Class<T> genericObject, List<FilterCondition> filterConditions) throws DAOException;
    
    <T> T getGenericObject(Class<T>  genericObject, String hashKey) throws DAOException;
    
    <T> T getGenericObjectWithPrefix(Class<T>  genericObject, String hashKey, String tableName, String prefix) throws DAOException;
    
    <T> T getGenericObject(Class<T>  genericObject, String hashKey, String rangeKey) throws DAOException;
    
    <T> void updateGenericObject(Object genericObject) throws DAOException;
    
    <T> void deleteGenericObject(Object genericObject) throws DAOException;
    
    <T> void deleteGenericObjects(List<T> genericObject) throws DAOException;
}
