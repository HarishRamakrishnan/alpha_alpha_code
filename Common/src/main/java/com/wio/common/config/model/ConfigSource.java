package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.CONFIG_SOURCE)
public class ConfigSource {
	
	@JsonProperty("DEVICE_ID")
	private String deviceID;
	
	@JsonProperty("DG_ID")
	private String dgID;
	
	@JsonProperty("SERVICE_NAME")
	private String serviceName;
	
	@JsonProperty("TIMESTAMP")
	private String timestamp;
	
	@JsonProperty("UPDATED_BY")
	private String updatedBy;
	
	@DynamoDBHashKey(attributeName = "DEVICE_ID")
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	@DynamoDBAttribute(attributeName = "DG_ID")
	public String getDgID() {
		return dgID;
	}
	public void setDgID(String dgID) {
		this.dgID = dgID;
	}
	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@DynamoDBAttribute(attributeName = "TIMESTAMP")
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	@DynamoDBAttribute(attributeName = "UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
