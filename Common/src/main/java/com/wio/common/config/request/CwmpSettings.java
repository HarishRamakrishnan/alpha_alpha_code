package com.wio.common.config.request;

public class CwmpSettings {
	
	private String manufacturer; // Hardware table.
	private String manufacturerOUI;
	private String productClass;
	private String serialNumber;
	private String hardwareVersion;
	private String softwareVersion;
	private String provisioningCode;
	private String specVersion;
	private String description;
	private String serviceName;
	private String acsServerUrl;// service table
	private String connectionRequestUsername;// service table
	private String connectionRequestPassword;// service table
	private String acsUsername;// service table
	private String acsPassword;// service table
	private String connectionRequestURL;// service table
	
	

}
