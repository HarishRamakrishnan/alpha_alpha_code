package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.config.request.DeviceConfigResponse;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.DEVICE_CONFIG)
public class DeviceConfig {
	
	private String tranID;
	private String deviceID;
	private String status;
	private String topic;
	private String script;
	private DeviceConfigurationRequest config;
	private DeviceConfigResponse response;
	private String creationTime;
	private String lastUpdateTime;
	
	
	public DeviceConfig() {
		
	}
	
	@DynamoDBHashKey(attributeName = "TRAN_ID")
	public String getTranID() {
		return tranID;
	}
	public void setTranID(String tranID) {
		this.tranID = tranID;
	}
	
	@DynamoDBRangeKey(attributeName = "DEVICE_ID")
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	
	@DynamoDBAttribute(attributeName = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@DynamoDBAttribute(attributeName = "TOPIC")
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	@DynamoDBAttribute(attributeName = "SCRIPT")
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	
	@DynamoDBAttribute(attributeName = "CONFIG")
	public DeviceConfigurationRequest getConfig() {
		return config;
	}
	public void setConfig(DeviceConfigurationRequest config) {
		this.config = config;
	}
	
	@DynamoDBAttribute(attributeName = "RESPONSE")
	public DeviceConfigResponse getResponse() {
		return response;
	}
	public void setResponse(DeviceConfigResponse response) {
		this.response = response;
	}
	
	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	
	@DynamoDBAttribute(attributeName = "LAST_UPDATE_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

}
