package com.wio.common.config.request;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.wio.common.config.model.MacFilterList;

@DynamoDBDocument
public class NetworkSettings 
{
	private String networkID;
    private String serviceName;
	private String networkName;
	private String networkType;
	private String ztEnable;
	private String dhcpServer;
	private String ipPoolStart;
	private String ipPoolEnd;
	private String leaseTime;
	private String iFName;
	private List<MacFilterList> macFilterList;
	
	
	@DynamoDBAttribute(attributeName = "NETWORK_ID")
	public String getNetworkID() {
		return networkID;
	}
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}
	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@DynamoDBAttribute(attributeName = "NETWORK_NAME")
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	@DynamoDBAttribute(attributeName = "NETWORK_TYPE")
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	@DynamoDBAttribute(attributeName = "ZT_ENABLE")
	public String getZtEnable() {
		return ztEnable;
	}
	public void setZtEnable(String ztEnable) {
		this.ztEnable = ztEnable;
	}
	@DynamoDBAttribute(attributeName = "DHCP_SERVER")
	public String getDhcpServer() {
		return dhcpServer;
	}
	public void setDhcpServer(String dhcpServer) {
		this.dhcpServer = dhcpServer;
	}
	@DynamoDBAttribute(attributeName = "IP_POOL_START")
	public String getIpPoolStart() {
		return ipPoolStart;
	}
	public void setIpPoolStart(String ipPoolStart) {
		this.ipPoolStart = ipPoolStart;
	}
	@DynamoDBAttribute(attributeName = "IP_POOL_END")
	public String getIpPoolEnd() {
		return ipPoolEnd;
	}
	public void setIpPoolEnd(String ipPoolEnd) {
		this.ipPoolEnd = ipPoolEnd;
	}
	@DynamoDBAttribute(attributeName = "LEASE_TIME")
	public String getLeaseTime() {
		return leaseTime;
	}
	public void setLeaseTime(String leaseTime) {
		this.leaseTime = leaseTime;
	}
	@DynamoDBAttribute(attributeName = "MAC_FILTER")
	public List<MacFilterList> getMacFilterList() {
		return macFilterList;
	}
	public void setMacFilterList(List<MacFilterList> macFilterList) {
		this.macFilterList = macFilterList;
	}
	
	@DynamoDBAttribute(attributeName = "IFNAME")
	public String getiFName() {
		return iFName;
	}
	public void setiFName(String iFName) {
		this.iFName = iFName;
	}
	
	@Override
	public String toString() {
		return "NetworkSettings [networkID=" + networkID + ", serviceName=" + serviceName + ", networkName="
				+ networkName + ", networkType=" + networkType + ", ztEnable=" + ztEnable + ", dhcpServer=" + dhcpServer
				+ ", ipPoolStart=" + ipPoolStart + ", ipPoolEnd=" + ipPoolEnd + ", leaseTime=" + leaseTime
				+ ", macFilterList=" + macFilterList + "]";
	}
	
	

}
