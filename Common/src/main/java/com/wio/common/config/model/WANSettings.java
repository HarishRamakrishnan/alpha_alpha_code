package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBDocument
public class WANSettings 
{
	private String portID;
	private String portNo; // only one 
	private String wanType;
	private String username;
	private String password;
	private String gateway;
	private String ipv4;
	private String ipv6;
	private String dnsPrimary;
	private String dnsSecondary;
	private String subnetMask;
	private String nat;
	private String ifName;
	private String name; // unique identifier
	
	private String vlanNo;
    
    public WANSettings() {

    }


    @DynamoDBHashKey(attributeName = "PORT_ID")
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}


	@DynamoDBAttribute(attributeName = "PORT_NO")
	public String getPortNo() {
		return portNo;
	}
	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}


	@DynamoDBAttribute(attributeName = "WAN_TYPE")
	public String getWanType() {
		return wanType;
	}
	public void setWanType(String wanType) {
		this.wanType = wanType;
	}


	@DynamoDBAttribute(attributeName = "USERNAME")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}


	@DynamoDBAttribute(attributeName = "PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


	@DynamoDBAttribute(attributeName = "GATEWAY")
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}


	@DynamoDBAttribute(attributeName = "IPV4")
	public String getIpv4() {
		return ipv4;
	}
	public void setIpv4(String ipv4) {
		this.ipv4 = ipv4;
	}


	@DynamoDBAttribute(attributeName = "IPV6")
	public String getIpv6() {
		return ipv6;
	}
	public void setIpv6(String ipv6) {
		this.ipv6 = ipv6;
	}


	@DynamoDBAttribute(attributeName = "DNS_PRIMARY")
	public String getDnsPrimary() {
		return dnsPrimary;
	}
	public void setDnsPrimary(String dnsPrimary) {
		this.dnsPrimary = dnsPrimary;
	}


	@DynamoDBAttribute(attributeName = "DNS_SECONDARY")
	public String getDnsSecondary() {
		return dnsSecondary;
	}
	public void setDnsSecondary(String dnsSecondary) {
		this.dnsSecondary = dnsSecondary;
	}


	@DynamoDBAttribute(attributeName = "SUBNET_MASK")
	public String getSubnetMask() {
		return subnetMask;
	}
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}
    	
	@DynamoDBAttribute(attributeName = "NAT")
	public String getNat() {
		return nat;
	}
	public void setNat(String nat) {
		this.nat = nat;
	}
    	
	@DynamoDBAttribute(attributeName = "NAME")
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@DynamoDBHashKey(attributeName = "VLAN_NO")
	public String getVlanNo() {
		return vlanNo;
	}

	public void setVlanNo(String vlanNo) {
		this.vlanNo = vlanNo;
	}

	@DynamoDBHashKey(attributeName = "IFNAME")
	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

}
