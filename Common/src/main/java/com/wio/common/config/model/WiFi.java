package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.WIFI)
public class WiFi 
{	
	private String wifiID;
	private String wifiName;
	private String description;
	private String serviceName;
	private String ssid;
	private String hiddenSSID;
	private String country;
	private String networkID;
	private String networkUser;
	private String authenticationType;
	private String encryptionType;
	private String passPhrase;
	private String enableWireless;
	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;		   
    private String eapType; // SIM or    
	private String mode; // AP or Station mode;
	private String radioName;
	private String index;
    
   
	public WiFi() {

	}


    @DynamoDBHashKey(attributeName = "WIFI_ID")
	public String getWifiID() {
		return wifiID;
	}
	public void setWifiID(String wifiID) {
		this.wifiID = wifiID;
	}


    @DynamoDBAttribute(attributeName = "WIFI_NAME")
	public String getWifiName() {
		return wifiName;
	}
	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}

	
	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	@DynamoDBAttribute(attributeName = "SSID")
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}


	@DynamoDBAttribute(attributeName = "HIDDEN_SSID")
	public String getHiddenSSID() {
		return hiddenSSID;
	}
	public void setHiddenSSID(String hiddenSSID) {
		this.hiddenSSID = hiddenSSID;
	}


	@DynamoDBAttribute(attributeName = "COUNTRY")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}


	@DynamoDBAttribute(attributeName = "NETWORK_ID")
	public String getNetworkID() {
		return networkID;
	}
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}


	@DynamoDBAttribute(attributeName = "NETWORK_USER")
	public String getNetworkUser() {
		return networkUser;
	}
	public void setNetworkUser(String networkUser) {
		this.networkUser = networkUser;
	}


	@DynamoDBAttribute(attributeName = "AUTHENTICATION_TYPE")
	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}


	@DynamoDBAttribute(attributeName = "ENCRYPTION_TYPE")
	public String getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(String encryptionType) {
		this.encryptionType = encryptionType;
	}


	@DynamoDBAttribute(attributeName = "PASS_PHRASE")
	public String getPassPhrase() {
		return passPhrase;
	}
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}


	@DynamoDBAttribute(attributeName = "ENABLE_WIRELESS")
	public String getEnableWireless() {
		return enableWireless;
	}
	public void setEnableWireless(String enableWireless) {
		this.enableWireless = enableWireless;
	}
	
	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	@DynamoDBAttribute(attributeName = "LAST_UPDATED_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}  
	
	@DynamoDBAttribute(attributeName = "EAP_TYPE")
	public String getEapType() {
		return eapType;
	}

	public void setEapType(String eapType) {
		this.eapType = eapType;
	}
	@DynamoDBAttribute(attributeName = "MODE")
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@DynamoDBAttribute(attributeName = "RADIO_NAME")
	public String getRadioName() {
		return radioName;
	}


	public void setRadioName(String radioName) {
		this.radioName = radioName;
	}

	@DynamoDBAttribute(attributeName = "INDEX")
	public String getIndex() {
		return index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	@Override
	public String toString() {
		return "WiFi [wifiID=" + wifiID + ", wifiName=" + wifiName + ", serviceName=" + serviceName + ", ssid=" + ssid
				+ ", hiddenSSID=" + hiddenSSID + ", country=" + country + ", networkID=" + networkID + ", networkUser="
				+ networkUser + ", authenticationType=" + authenticationType + ", encryptionType=" + encryptionType
				+ ", passPhrase=" + passPhrase + ", enableWireless=" + enableWireless + ", lastUpdateTime="
				+ lastUpdateTime + ", eapType=" + eapType + ", mode=" + mode + ", radioName=" + radioName + ", index="
				+ index + "]";
	}
	
	

}
