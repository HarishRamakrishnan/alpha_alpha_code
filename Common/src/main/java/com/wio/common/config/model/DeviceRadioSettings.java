package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * AP Radio Settings object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBDocument
public class DeviceRadioSettings {
	
	private String radioName;
	private String dbdcStatus;
	private String dbdcMode;
	private String radioBand;
	private String radioStatus;
	private String radioMode;
	private String bandwidth;
	private String transmitPower;
	private String channel;
	private String babel;
	private String index;

	public DeviceRadioSettings() {

    }
	
	@DynamoDBHashKey(attributeName = "RADIO_NAME")
	public String getRadioName() {
		return radioName;
	}

	public void setRadioName(String radioName) {
		this.radioName = radioName;
	}
	
	@DynamoDBAttribute(attributeName = "DBDC_STATUS")
	public String getDbdcStatus() {
		return dbdcStatus;
	}

	public void setDbdcStatus(String dbdcStatus) {
		this.dbdcStatus = dbdcStatus;
	}

	@DynamoDBAttribute(attributeName = "DBDC_MODE")
	public String getDbdcMode() {
		return dbdcMode;
	}

	public void setDbdcMode(String dbdcMode) {
		this.dbdcMode = dbdcMode;
	}
	
	@DynamoDBAttribute(attributeName = "RADIO_BAND")
	public String getRadioBand() {
		return radioBand;
	}

	public void setRadioBand(String radioBand) {
		this.radioBand = radioBand;
	}

	@DynamoDBAttribute(attributeName = "RADIO_STATUS")
	public String getRadioStatus() {
		return radioStatus;
	}

	public void setRadioStatus(String radioStatus) {
		this.radioStatus = radioStatus;
	}

	@DynamoDBAttribute(attributeName = "RADIO_MODE")
	public String getRadioMode() {
		return radioMode;
	}

	public void setRadioMode(String radioMode) {
		this.radioMode = radioMode;
	}

	@DynamoDBAttribute(attributeName = "BANDWIDTH")
	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	@DynamoDBAttribute(attributeName = "TRANSMIT_POWER")
	public String getTransmitPower() {
		return transmitPower;
	}

	public void setTransmitPower(String transmitPower) {
		this.transmitPower = transmitPower;
	}

	@DynamoDBAttribute(attributeName = "CHANNEL")
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	
	@DynamoDBAttribute(attributeName = "BABEL")
	public String getBabel() {
		return babel;
	}

	public void setBabel(String babel) {
		this.babel = babel;
	}
	
	@DynamoDBAttribute(attributeName = "INDEX")
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return "DeviceRadioSettings [radioName=" + radioName + ", dbdcStatus=" + dbdcStatus + ", dbdcMode=" + dbdcMode
				+ ", radioBand=" + radioBand + ", radioStatus=" + radioStatus + ", radioMode=" + radioMode
				+ ", bandwidth=" + bandwidth + ", transmitPower=" + transmitPower + ", channel=" + channel + ", babel="
				+ babel + ", index=" + index + "]";
	}
	

}
