package com.wio.common.config.model;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.DEVICE)
public class Device {
	private String deviceID;
	private String serviceName;
	private String deviceName;
	private String model;
	private String hpID;
	private String dgID;
	private String status;
	private Boolean provisionAuthorized;
	private String babelServices;
	private String firmware;
	private String mac;
	private String networkAdmin;
	private String defaultTopic;
	private String platform; // OpenWRT
	private String scriptAgent; // like UCI for config script execution.
	private List<LANSettings> lanSettings;
	private List<DeviceRadioSettings> radioSettings;
	private WANSettings wanSettings;
	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;
		
	
	public Device()	{
		//empty constructor
	}
	
	@DynamoDBHashKey(attributeName = "DEVICE_ID")
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@DynamoDBAttribute(attributeName = "DEVICE_NAME")
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	@DynamoDBAttribute(attributeName = "DEVICE_MODEL")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

	@DynamoDBAttribute(attributeName = "HP_ID")
	public String getHpID() {
		return hpID;
	}
	public void setHpID(String hpID) {
		this.hpID = hpID;
	}

	@DynamoDBAttribute(attributeName = "DG_ID")
	public String getDgID() {
		return dgID;
	}
	public void setDgID(String dgID) {
		this.dgID = dgID;
	}

	@DynamoDBAttribute(attributeName = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@DynamoDBAttribute(attributeName = "BABEL_SERVICES")
	public String getBabelServices() {
		return babelServices;
	}
	public void setBabelServices(String babelServices) {
		this.babelServices = babelServices;
	}
	
	@DynamoDBAttribute(attributeName = "FIRMWARE")
	public String getFirmware() {
		return firmware;
	}
	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}

	@DynamoDBAttribute(attributeName = "MAC_ADDRESS")
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}

	@DynamoDBAttribute(attributeName = "NETWORK_ADMIN")
	public String getNetworkAdmin() {
		return networkAdmin;
	}
	public void setNetworkAdmin(String networkAdmin) {
		this.networkAdmin = networkAdmin;
	}

	@DynamoDBAttribute(attributeName = "DEFAULT_TOPIC")
	public String getDefaultTopic() {
		return defaultTopic;
	}
	public void setDefaultTopic(String defaultTopic) {
		this.defaultTopic = defaultTopic;
	}

	@DynamoDBAttribute(attributeName = "PLATFORM")
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	@DynamoDBAttribute(attributeName = "SCRIPT_AGENT")
	public String getScriptAgent() {
		return scriptAgent;
	}

	public void setScriptAgent(String scriptAgent) {
		this.scriptAgent = scriptAgent;
	}

	@DynamoDBAttribute(attributeName = "LAN_SETTINGS")
	public List<LANSettings> getLanSettings() {
		return lanSettings;
	}
	public void setLanSettings(List<LANSettings> lanSettings) {
		this.lanSettings = lanSettings;
	}


	@DynamoDBAttribute(attributeName = "WAN_SETTINGS")
	public WANSettings getWanSettings() {
		return wanSettings;
	}
	public void setWanSettings(WANSettings wanSettings) {
		this.wanSettings = wanSettings;
	}


	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	@DynamoDBAttribute(attributeName = "LAST_UPDATE_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}


	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@DynamoDBAttribute(attributeName = "PROVISION_AUTHORISED")
	public Boolean getProvisionAuthorized() {
		return provisionAuthorized;
	}
	public void setProvisionAuthorized(Boolean provisionAuthorized) {
		this.provisionAuthorized = provisionAuthorized;
	}

	@DynamoDBAttribute(attributeName = "RADIO_SETTINGS")
	public List<DeviceRadioSettings> getRadioSettings() {
		return radioSettings;
	}


	public void setRadioSettings(List<DeviceRadioSettings> radioSettings) {
		this.radioSettings = radioSettings;
	}

	@Override
	public String toString() {
		return "Device [deviceID=" + deviceID + ", serviceName=" + serviceName + ", deviceName=" + deviceName
				+ ", model=" + model + ", hpID=" + hpID + ", dgID=" + dgID + ", status=" + status
				+ ", provisionAuthorized=" + provisionAuthorized + ", babelServices=" + babelServices + ", firmware="
				+ firmware + ", mac=" + mac + ", networkAdmin=" + networkAdmin + ", defaultTopic=" + defaultTopic
				+ ", platform=" + platform + ", scriptAgent=" + scriptAgent + ", lanSettings=" + lanSettings
				+ ", radioSettings=" + radioSettings + ", wanSettings=" + wanSettings + ", createdBy=" + createdBy
				+ ", creationTime=" + creationTime + ", lastUpdateTime=" + lastUpdateTime + "]";
	}

	
	
/*
	private int limit;
	
	@DynamoDBIgnore
	public int getLimit() {
		return limit;
	}

	@DynamoDBIgnore
	public void setLimit(int limit) {
		this.limit = limit;
	}*/
	
}
