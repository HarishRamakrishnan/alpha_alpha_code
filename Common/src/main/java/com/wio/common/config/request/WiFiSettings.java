package com.wio.common.config.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

@DynamoDBDocument
public class WiFiSettings 
{
	private String wifiID;
	private String wifiName;
	private String serviceName;
	private String ssid;
	private String hiddenSSID;
	private String country;
	private String networkUser;
	private String authenticationType;
	private String encryptionType;
	private String passPhrase;	
	private String enableWireless;
    private String eapType; // SIM or 
	private String mode; // AP or Station mode;
	private String radioName;
	private String index;
	private NetworkSettings networkSettings;
	
	@DynamoDBHashKey(attributeName = "WIFI_ID")
	public String getWifiID() {
		return wifiID;
	}
	public void setWifiID(String wifiID) {
		this.wifiID = wifiID;
	}
	@DynamoDBAttribute(attributeName = "WIFI_NAME")
	public String getWifiName() {
		return wifiName;
	}
	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}
	
	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@DynamoDBAttribute(attributeName = "SSID")
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	@DynamoDBAttribute(attributeName = "HIDDEN_SSID")
	public String getHiddenSSID() {
		return hiddenSSID;
	}
	public void setHiddenSSID(String hiddenSSID) {
		this.hiddenSSID = hiddenSSID;
	}
	@DynamoDBAttribute(attributeName = "COUNTRY")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@DynamoDBAttribute(attributeName = "NETWORK_USER")
	public String getNetworkUser() {
		return networkUser;
	}
	public void setNetworkUser(String networkUser) {
		this.networkUser = networkUser;
	}

	@DynamoDBAttribute(attributeName = "AUTHENTICATION_TYPE")
	public String getAuthenticationType() {
		return authenticationType;
	}
	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}
	@DynamoDBAttribute(attributeName = "ENCRYPTION_TYPE")
	public String getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(String encryptionType) {
		this.encryptionType = encryptionType;
	}
	@DynamoDBAttribute(attributeName = "PASS_PHRASE")
	public String getPassPhrase() {
		return passPhrase;
	}
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}
	@DynamoDBAttribute(attributeName = "ENABLE_WIRELESS")
	public String getEnableWireless() {
		return enableWireless;
	}
	public void setEnableWireless(String enableWireless) {
		this.enableWireless = enableWireless;
	}
	
	
	
	@DynamoDBAttribute(attributeName = "EAP_TYPE")
	public String getEapType() {
		return eapType;
	}

	public void setEapType(String eapType) {
		this.eapType = eapType;
	}
	
	@DynamoDBAttribute(attributeName = "MODE")
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@DynamoDBAttribute(attributeName = "RADIO_NAME")
	public String getRadioName() {
		return radioName;
	}

	public void setRadioName(String radioName) {
		this.radioName = radioName;
	}

	@DynamoDBAttribute(attributeName = "INDEX")
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	@DynamoDBAttribute(attributeName = "NETWORK_SETTINGS")
	public NetworkSettings getNetworkSettings() {
		return networkSettings;
	}
	
	public void setNetworkSettings(NetworkSettings networkSettings) {
		this.networkSettings = networkSettings;
	}
	@Override
	public String toString() {
		return "WiFiSettings [wifiID=" + wifiID + ", wifiName=" + wifiName + ", serviceName=" + serviceName + ", ssid="
				+ ssid + ", hiddenSSID=" + hiddenSSID + ", country=" + country + ", networkUser=" + networkUser
				+ ", authenticationType=" + authenticationType + ", encryptionType=" + encryptionType + ", passPhrase="
				+ passPhrase + ", enableWireless=" + enableWireless + ", eapType=" + eapType + ", mode=" + mode
				+ ", radioName=" + radioName + ", index=" + index + ", networkSettings=" + networkSettings + "]";
	}


}
