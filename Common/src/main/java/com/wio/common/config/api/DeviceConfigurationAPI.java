package com.wio.common.config.api;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.Gson;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.model.DGWifiMap;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.config.request.DeviceFirmware;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.firmware.Firmware;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.common.staticinfo.DynamoDBObject;
import com.wio.common.validation.RequestValidator;

public class DeviceConfigurationAPI
{
	static LambdaLogger logger = null;
	private static GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
	
	public static Hardware hardware = null;
	public static HardwareProfile hardwareProfile = null;
	public static DeviceConfigurationRequest deviceConfig = null;
	public static Device device = null;
	public static Service service = null;
	public static String deviceID = "";
	
	
	public static String fetchConfig(String deviceID, Context lambdaContext) throws InternalErrorException, BadRequestException
	{
		logger = lambdaContext.getLogger();
        Gson gson = new Gson();
        DeviceConfigurationRequest deviceConfiguration = null;
        
        if(RequestValidator.isEmptyField(deviceID))
        	throw new BadRequestException("Device ID cannot be empty !"); 
        try
        {            
            Device device = genDAO.getGenericObject(Device.class, deviceID);
            
            if(null == device)
            	throw new DAOException("Device does not Exist"); 
            
            deviceConfiguration = prepareConfiguration(device, lambdaContext);
            
            if(null == deviceConfiguration)
            	throw new DAOException("Unable to fetch the configuration, returing null !"); 
            
            return gson.toJson(deviceConfiguration, DeviceConfigurationRequest.class);
            
            //Publishing device configuration to default topic
            /*String publishStatus = MQTTPublisher_bck.publishMessage(confg, device.getDefaultTopic(), true, lambdaContext);
            
            if(publishStatus.equals("SUCCESS"))
            	logger.log("Configuration published to device topic: "+device.getDefaultTopic());
            else
            	logger.log("Unable to publish configuration to device topic, MQTT Broker issue !");*/
        } 
        catch (final Exception e) {
            logger.log("Error while fetching Device Configiration...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
	}
	
    
    public static DeviceConfigurationRequest prepareConfiguration(Device device, Context lambdaContext) throws DAOException
    {    	
    	LambdaLogger logger = lambdaContext.getLogger();
    	
    	logger.log("Fetching hardware profile !");
    	//Fetch hardware profile
    	if(RequestValidator.isEmptyField(device.getHpID()))
    		throw new DAOException("No Hardware Profile created for this device: "+device.getDeviceID());
    		
    	HardwareProfile hp = genDAO.getGenericObject(HardwareProfile.class, device.getHpID());
    	
    	if(null == hp)
    		throw new DAOException("Device hardware profile does not exist or not avilable !");
    	
    	logger.log("Fetching DeviceGroup details!");
    	//Fetch Wifi settings
    	if(RequestValidator.isEmptyField(device.getDgID()))
    		throw new DAOException("Device is not under any DeviceGroup / Provision!");
    	
    	DeviceGroup dg = genDAO.getGenericObject(DeviceGroup.class, device.getDgID());
    	
    	if(null == dg)
    		throw new DAOException("DeviceGroup does not exist or not avilable !");
    	
    	List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();    	
    	filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, dg.getDgID(), ComparisonOperator.EQ));
    	
    	List<DGWifiMap> dgwifi = genDAO.getGenericObjects(DGWifiMap.class, filterConditions);

    	List<WiFiSettings> wifiList = new ArrayList<>();
    	//WiFiSettings wifiSettings = null;
    	
    	if(dgwifi.isEmpty())
    		throw new DAOException("No WiFi settings asigned to this Device !");
    	
    	logger.log("Fetching Wifi Settings !");
    	dgwifi.forEach(mapping->{
    		try 
    		{
	    		WiFi wifi = genDAO.getGenericObject(WiFi.class, mapping.getWifiID());
	    		
	    		if(null == wifi)
					throw new DAOException("No wifi/wireless settings assigned to this Device !");					
	    		
	    		WiFiSettings wifiSetting = new WiFiSettings();
	    		
	    		wifiSetting.setWifiID(wifi.getWifiID());
	    		wifiSetting.setWifiName(wifi.getWifiName());
	    		wifiSetting.setServiceName(wifi.getServiceName());
	    		wifiSetting.setSsid(wifi.getSsid());
	    		wifiSetting.setHiddenSSID(wifi.getHiddenSSID());
	    		wifiSetting.setCountry(wifi.getCountry());
	    		wifiSetting.setNetworkUser(wifi.getNetworkUser());
	    		wifiSetting.setAuthenticationType(wifi.getAuthenticationType());
	    		wifiSetting.setEncryptionType(wifi.getEncryptionType());
	    		wifiSetting.setPassPhrase(wifi.getPassPhrase());
	    		wifiSetting.setEnableWireless(wifi.getEnableWireless());
	    		
	    		if(wifi.getNetworkID().equals("NA") || wifi.getNetworkID() == null)
	    			throw new DAOException("No network asigned to WiFi !");
	    		
	    		logger.log("Fetching Network Settings !");
	    		
	    		Network network = genDAO.getGenericObject(Network.class, wifi.getNetworkID());
	    		
	    		if(null == network)
	    			throw new DAOException("Network "+wifi.getNetworkID()+" does not exist !");    		
	    		
	    		logger.log("Preparing Network settings !");
	    		
	    		NetworkSettings networkSetting = new NetworkSettings();
	    		
	    		networkSetting.setNetworkID(network.getNetworkID());
	    		networkSetting.setServiceName(network.getServiceName());
	    		networkSetting.setNetworkName(network.getNetworkName());
	    		networkSetting.setNetworkType(network.getNetworkType());
	    		networkSetting.setZtEnable(network.getZtEnable());
	    		networkSetting.setDhcpServer(network.getDhcpServer());
	    		networkSetting.setIpPoolStart(network.getIpPoolStart());
	    		networkSetting.setIpPoolEnd(network.getIpPoolEnd());
	    		networkSetting.setLeaseTime(network.getLeaseTime());
	    		
	    		logger.log("Adding MAC Filter List");
	    		networkSetting.setMacFilterList(network.getMacFilterList());
	    		
	    		logger.log("Adding Network settings to WifiSettings !");
	    		wifiSetting.setNetworkSettings(networkSetting);
	    		
	    		wifiList.add(wifiSetting);
	    		} 
    		catch (DAOException e) 
    		{
				e.printStackTrace();
			} 		
    	});  
    	
    	logger.log("Fetching Firmware info !");
    	
    	//Fetching firmware info
    	if(device.getFirmware().equals("NA") || null == device.getFirmware())
    		throw new DAOException("No firmware asigned to this device !");
    	
    	Firmware fw = genDAO.getGenericObject(Firmware.class, device.getFirmware());
    	
    	if(null == fw)
    		throw new DAOException("Firmware does not exist or not avilable !");
    	
    	DeviceFirmware firmware = new DeviceFirmware();
    	
    	firmware.setFwID(fw.getFwID());
    	firmware.setFilename(fw.getFilename());
    	firmware.setVersion(fw.getVersion());
    	firmware.setVersionType(fw.getVersionType());
    	firmware.setChecksum(fw.getChecksum());
    	firmware.setChecksumType(fw.getChecksumType());
    	firmware.setEdgeURL(fw.getEdgeURL());
    	

    	logger.log("Preparing device configuration !");
    	
    	//Prepare device configuration
    	DeviceConfigurationRequest configuration = new DeviceConfigurationRequest();
    	
    	configuration.setDeviceID(device.getDeviceID());
    	configuration.setDeviceName(device.getDeviceName());
    	configuration.setServiceName(device.getServiceName());
    	configuration.setModel(device.getModel());
    	configuration.setMac(device.getMac());
    	configuration.setNetworkAdmin(device.getNetworkAdmin());
    	configuration.setDefaultTopic(device.getDefaultTopic());
    	
    	
    	configuration.setRadioSettings(hp.getRadioSettings());
    	configuration.setPortSettings(hp.getPortSettings());
    	
    	configuration.setLanSettings(device.getLanSettings());
    	configuration.setWanSettings(device.getWanSettings());    	  	
    	
    	configuration.setWifiSettings(wifiList);
    	
    	configuration.setFirmware(firmware);
    	
    	return configuration;
    }
    
    public static ConfiguredValuesMO queryConfigUpdatesTable(String transactionID) {
    	ConfiguredValuesMO configUpdates = null;
    	try {
			configUpdates = genDAO.getGenericObject(ConfiguredValuesMO.class, transactionID);
		} catch (DAOException e) {
			System.out.println(
					"DAOException occured while fetching configUpdates from the table CONFIG_UPDATES with transactionID = "
							+ transactionID);
			e.printStackTrace();
		}
		return configUpdates;
    	
    }
    
	public static DeviceConfiguration fetchDeviceConfiguration(String deviceID) {

		DeviceConfigurationAPI.deviceID = deviceID;
		String serviceName = null;
		String deviceGroupId = null;
		String hwProfileId = null;

		DeviceConfiguration devConfig = new DeviceConfiguration();
		System.out.println("Loading WIO Device Configuration Started... ");
		try {
			String uuid[] = null;
			if(deviceID.contains("-")){
				uuid = deviceID.split("-");
				if(uuid.length == 4 ){
					devConfig.setManufacturerOUI(uuid[0]);
					devConfig.setProductClass(uuid[1]+ "-" +uuid[2]);
					devConfig.setSerialNumber(uuid[3]);
				} else {
					System.out.println("Not a valid DeviceID format [ " + deviceID + " ]");
					return null;
				}
				device = genDAO.getGenericObject(Device.class, deviceID);
			} else {
				System.out.println("Not a valid DeviceID format [ " + deviceID + " ]");
				return null;
			}
			if(device == null){
				System.out.println("Device details doesn't exist for the DEVICE_ID = " + deviceID + " in "
						+ "DEVICE table");
				return null; 
			}
			serviceName = device.getServiceName();
			deviceGroupId = device.getDgID();
			hwProfileId = device.getHpID();
			
			//TODO Need to finalize whether validation should be done here.
			if (RequestValidator.isValidString(serviceName) && RequestValidator.isValidString(deviceID)
					&& RequestValidator.isValidString(hwProfileId)) {
				devConfig.setServiceName(serviceName);
				devConfig.setDeviceId(deviceID);
				devConfig.setHpId(hwProfileId);
			} else {
				return null;
			}

			System.out.println("Service_Name = " + serviceName + ", DG_ID = " + deviceGroupId + ", HP_ID = "
					+ hwProfileId);		
			hardwareProfile = genDAO.getGenericObject(HardwareProfile.class, hwProfileId);
			if(hardwareProfile != null) {
				hardware = genDAO.getGenericObject(Hardware.class, hardwareProfile.getHwID());
			} else {
				return null;
			}
				
			setLanSettingsInPOJO(devConfig, device);
			setWanSettingsInPOJO(devConfig, device);
			setServiceDetailsInPOJO(devConfig, service);
			setRadioSettingsInPOJO(devConfig, device);
			setWIFIAndNWSettingsInPOJO(devConfig, deviceGroupId);
			
			devConfig.setHardwareVersion("H2.0");
//			wavesParam.setProductClass("APWavesIO");
			devConfig.setProvisioningCode("OOEEFF");
			devConfig.setManufacturer("BAYONETTE");
//			wavesParam.setManufacturerOUI("0AEE1D");
//			wavesParam.setSerialNumber("ABCCCC111411");
			devConfig.setSoftwareVersion("S2.0");
			devConfig.setSpecVersion("00001");
			devConfig.setDescription("TEST LAMBDA");
			
			System.out.println("Loading WIO Device Configuration Completed... ");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception while reading tr69 params -" + e.getMessage());
		}
		return devConfig;
	}
    
	/**
	 * Get the DG_ID from device table to query DG_WIFI_MAP to get Wifi
	 * ID and get all Wifi Settings.
	 */
	private static void setWIFIAndNWSettingsInPOJO(DeviceConfiguration wavesParam, String deviceGroupId) throws DAOException {
		List<WiFiSettings> wifiList;
		List<Network> networkList;
		List<FilterCondition> filterConditions = new ArrayList<>();
		
		FilterCondition condition = new FilterCondition();
		condition.setParam(DynamoDBObjectKeys.DG_ID);
		condition.setOperator(ComparisonOperator.EQ);
		condition.setValue(deviceGroupId);
		
		condition.setParam("DEVICE_ID");
		condition.setOperator(ComparisonOperator.EQ);
		condition.setValue(deviceID);

		filterConditions.add(condition);

		try{
			List<DGWifiMap> dgWifiList = genDAO.getGenericObjects(DGWifiMap.class, filterConditions);

			System.out.println("No of WiFi's for the device = "+dgWifiList.size());
			
			wifiList = new ArrayList<>();
			networkList = new ArrayList<>();
			WiFiSettings wifiSetting = null ;
			NetworkSettings networkSetting = null;

			for (DGWifiMap dgWifi : dgWifiList) {

				WiFi wifi = null;
				wifiSetting = new WiFiSettings();
				if ((wifi = genDAO.getGenericObject(WiFi.class, dgWifi.getWifiID())) != null) {

					Network network = genDAO.getGenericObject(Network.class, wifi.getNetworkID());
					
					wifiSetting.setWifiID(wifi.getWifiID());
					wifiSetting.setWifiName(wifi.getWifiName());
					wifiSetting.setServiceName(wifi.getServiceName());
					wifiSetting.setSsid(wifi.getSsid());
					wifiSetting.setHiddenSSID(wifi.getHiddenSSID());
					wifiSetting.setCountry(wifi.getCountry());
					wifiSetting.setNetworkUser(wifi.getNetworkUser());
					wifiSetting.setAuthenticationType(wifi.getAuthenticationType());
					wifiSetting.setEncryptionType(wifi.getEncryptionType());
					wifiSetting.setPassPhrase(wifi.getPassPhrase());
					wifiSetting.setEnableWireless(wifi.getEnableWireless());
					wifiSetting.setEapType(wifi.getEapType());
					wifiSetting.setIndex(wifi.getIndex());
					wifiSetting.setMode(wifi.getMode());
					wifiSetting.setRadioName(wifi.getRadioName());

					if (network != null) {
						networkSetting = new NetworkSettings();
						networkSetting.setNetworkID(network.getNetworkID());
			    		networkSetting.setServiceName(network.getServiceName());
			    		networkSetting.setNetworkName(network.getNetworkName());
			    		networkSetting.setNetworkType(network.getNetworkType());
			    		networkSetting.setZtEnable(network.getZtEnable());
			    		networkSetting.setDhcpServer(network.getDhcpServer());
			    		networkSetting.setIpPoolStart(network.getIpPoolStart());
			    		networkSetting.setIpPoolEnd(network.getIpPoolEnd());
			    		networkSetting.setLeaseTime(network.getLeaseTime());
			    		networkSetting.setiFName(network.getIfName());
			    		networkSetting.setMacFilterList(network.getMacFilterList());
						wifiSetting.setNetworkSettings(networkSetting);
						networkList.add(network);
					}
					wifiList.add(wifiSetting);
				}
			}

			wavesParam.setWifiSettings(wifiList);
			System.out.println("WIFI Settings ==>> " + wavesParam.getWifiSettings());
		}catch (DAOException e) {
			System.out.println("DAOException occured while fetching Wifi and N/w details.");
		}
		
	}

	/**
	 * Retrieving LAN settings from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private static void setWanSettingsInPOJO( DeviceConfiguration wavesParam, Device device)
			throws Exception {
		if (device.getWanSettings() != null) {
			wavesParam.setWanSetting(device.getWanSettings());
			System.out.println("WAN Settings ==>> " + wavesParam.getWanSetting().toString());
		} else {
			System.out.println("WAN Settings doesn't exist for DEVICE_ID = " + deviceID + " in "
					+ "DEVICE table.");
			System.out.println("WAN Settings will not be available in INFORM RPC. ");
		}
	}

	/**
	 * Retrieving WAN settings from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private static void setLanSettingsInPOJO( DeviceConfiguration wavesParam, Device device)
			throws Exception {

		if (device.getLanSettings() != null) {
			wavesParam.setLanSettings(DynamoDBHelper.getInstance().getLanSetting(device.getLanSettings()));
			System.out.println("LAN Settings ==>> " + wavesParam.getLanSettings());
		} else {
			System.out.println("WAN Settings doesn't exist for the DEVICE_ID = " + deviceID + " in "
					+ "DEVICE Table");
			System.out.println("WAN Settings will not be available in INFORM RPC. ");
		}
	}

	/**
	 * Retrieving the radio settings from H/W Profile table and setting
	 * it in the DeviceConfiguration class.
	 */
	private static void setRadioSettingsInPOJO(DeviceConfiguration wavesParam, Device device)
			throws DAOException, Exception {
			
			if(device.getRadioSettings() != null){
				
				wavesParam.setRadioSettings(device.getRadioSettings());
				System.out.println("Radio Settings ==>> " + device.getRadioSettings());
				
			}else{
				System.out.println("HW Profile doesn't exist in HARDWARE_PROFILE table.");
			}

	}

	/**
	 * Retrieving Service table values from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private static void setServiceDetailsInPOJO( DeviceConfiguration wavesParam, Service service) throws DAOException {
		
		if(service != null){
			//wavesParam.setConnectionRequestPassword(service.getCpePassword());
			wavesParam.setConnectionRequestUsername(service.getCpeUsername());
			wavesParam.setConnectionRequestURL(service.getCpeGatewayURL());
			
			if(service.getCpeGatewayURL().contains("xxx")) {
				String cpeGatewayURL = service.getCpeGatewayURL().replace("xxx", deviceID);
				wavesParam.setConnectionRequestURL(cpeGatewayURL);
			}
			
			wavesParam.setAcsServerUrl(service.getAcsURL());
			wavesParam.setAcsUsername(service.getAcsUsername());
			//wavesParam.setAcsPassword(service.getAcsPassword());
		} else {
			System.out.println("Service doesn't exist for the device " +deviceID);
		}
	}

	public static <T> T getListItem(List<T> list, int index){
		String indices = String.valueOf(index);
		System.out.println("Size == "+list);
		
		for (T t: list) {
			System.out.println("SimpleName> = "+t.getClass().getSimpleName());
			
			switch (t.getClass().getSimpleName()) {
			
			case "WiFiSettings":
				WiFiSettings wifisettings = (WiFiSettings) t;
				System.out.println("Choosing WiFi based on Index.."+wifisettings.getIndex());
				System.out.println("INDEX ==> "+indices);
				if((wifisettings.getIndex().equals(indices))) {
					System.out.println("Matched wifisettings ==>> "+wifisettings);
					return t;
				}
				break;
			case "HPRadioSettings":
				DeviceRadioSettings radioSettings = (DeviceRadioSettings) t;
				System.out.println(" Choosing Radio based on Index.. "+radioSettings.getIndex());
				if((radioSettings.getIndex().equals(indices))) {
					System.out.println("Matched wifisettings ==>> "+radioSettings);
					return t;
				}
				break;
			case "LANSettings":
				LANSettings lanSettings = (LANSettings) t;
				System.out.println("Choosing LAN based on Index.."+lanSettings.getIndex());
				if(lanSettings.getIndex().equals(indices)) {
					System.out.println("Matched wifisettings ==>> "+lanSettings);
					return t;
				}
				break;
			default:
				break;
			}

		}
		return null;
	}
	
    
}
