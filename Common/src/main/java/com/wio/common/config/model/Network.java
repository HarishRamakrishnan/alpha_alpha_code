package com.wio.common.config.model;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.NETWORK)
public class Network 
{
	private String networkID;
    private String serviceName;
	private String networkName;
	private String description;
	private String networkType;
	private String ztEnable;
	private String dhcpServer;  // yes or no
	private String ipPoolStart;
	private String ipPoolEnd;
	private String ifName;
	private String leaseTime;
	private List<MacFilterList> macFilterList;
	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;	
	
 
	public Network() {

    }
    
	
    @DynamoDBHashKey(attributeName = "NETWORK_ID")
	public String getNetworkID() {
		return networkID;
	}
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}


	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	@DynamoDBAttribute(attributeName = "NETWORK_NAME")
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}


	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@DynamoDBAttribute(attributeName = "NETWORK_TYPE")
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}


	@DynamoDBAttribute(attributeName = "ZT_ENABLE")
	public String getZtEnable() {
		return ztEnable;
	}
	public void setZtEnable(String ztEnable) {
		this.ztEnable = ztEnable;
	}


	@DynamoDBAttribute(attributeName = "DHCP_SERVER")
	public String getDhcpServer() {
		return dhcpServer;
	}
	public void setDhcpServer(String dhcpServer) {
		this.dhcpServer = dhcpServer;
	}


	@DynamoDBAttribute(attributeName = "IP_POOL_START")
	public String getIpPoolStart() {
		return ipPoolStart;
	}
	public void setIpPoolStart(String ipPoolStart) {
		this.ipPoolStart = ipPoolStart;
	}


	@DynamoDBAttribute(attributeName = "IP_POOL_END")
	public String getIpPoolEnd() {
		return ipPoolEnd;
	}
	public void setIpPoolEnd(String ipPoolEnd) {
		this.ipPoolEnd = ipPoolEnd;
	}


	@DynamoDBAttribute(attributeName = "LEASE_TIME")
	public String getLeaseTime() {
		return leaseTime;
	}
	public void setLeaseTime(String leaseTime) {
		this.leaseTime = leaseTime;
	}


	@DynamoDBAttribute(attributeName = "MAC_FILTER_LIST")
	public List<MacFilterList> getMacFilterList() {
		return macFilterList;
	}
	public void setMacFilterList(List<MacFilterList> macFilterList) {
		this.macFilterList = macFilterList;
	}

	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	

	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	@DynamoDBAttribute(attributeName = "LAST_UPDATED_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}    
	  
	@DynamoDBAttribute(attributeName = "IFNAME")
	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}    

	@Override
	public String toString() {
		return "Network [networkID=" + networkID + ", serviceName=" + serviceName + ", networkName=" + networkName
				+ ", networkType=" + networkType + ", ztEnable=" + ztEnable + ", dhcpServer=" + dhcpServer
				+ ", ipPoolStart=" + ipPoolStart + ", ipPoolEnd=" + ipPoolEnd + ", leaseTime=" + leaseTime
				+ ", macFilterList=" + macFilterList + ", createdBy=" + createdBy + "]";
	}
	  
}
