package com.wio.common.config.request;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.WANSettings;


@DynamoDBDocument
public class DeviceConfiguration {


	private String manufacturer; // Hardware table.

	private String manufacturerOUI;

	private String productClass;

	private String serialNumber;

	private String hardwareVersion;

	private String softwareVersion;

	private String provisioningCode;

	private String specVersion;

	private String description;

	private String serviceName;

	private String acsServerUrl;// service table

	private String connectionRequestUsername;// service table

	private String connectionRequestPassword;// service table

	private String acsUsername;// service table

	private String acsPassword;// service table

	private String connectionRequestURL;// service table

	private String hpId;

	private String deviceId;
	
	private String deviceName;
	private String model;	
	private String mac;
	private String networkAdmin;
	private String defaultTopic;
	private Boolean generateZTKey;

	private List<LanSettings> lanSettings;
	
	private WANSettings wanSetting;
	
	private List<DeviceRadioSettings> radioSettings;
	
	private List<WiFiSettings> wifiSettings;
	
	private DeviceFirmware firmware;

	public DeviceConfiguration() {

	}

	@DynamoDBAttribute(attributeName = "MANUFACTURER")
	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@DynamoDBAttribute(attributeName = "MANUFACTURER_OUI")
	public String getManufacturerOUI() {
		return manufacturerOUI;
	}

	public void setManufacturerOUI(String manufacturerOUI) {
		this.manufacturerOUI = manufacturerOUI;
	}

	@DynamoDBAttribute(attributeName = "PRODUCT_CLASS")
	public String getProductClass() {
		return productClass;
	}

	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}

	@DynamoDBAttribute(attributeName = "SERIAL_NUMBER")
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@DynamoDBAttribute(attributeName = "HARDWARE_VERSION")
	public String getHardwareVersion() {
		return hardwareVersion;
	}

	public void setHardwareVersion(String hardwareVersion) {
		this.hardwareVersion = hardwareVersion;
	}

	@DynamoDBAttribute(attributeName = "SOFTWARE_VERSION")
	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	@DynamoDBAttribute(attributeName = "PROVISIONING_CODE")
	public String getProvisioningCode() {
		return provisioningCode;
	}

	public void setProvisioningCode(String provisioningCode) {
		this.provisioningCode = provisioningCode;
	}

	@DynamoDBAttribute(attributeName = "SPEC_VERSION")
	public String getSpecVersion() {
		return specVersion;
	}

	public void setSpecVersion(String specVersion) {
		this.specVersion = specVersion;
	}

	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@DynamoDBAttribute(attributeName = "CPE_PASSWORD")
	public String getConnectionRequestPassword() {
		return connectionRequestPassword;
	}

	public void setConnectionRequestPassword(String connectionRequestPassword) {
		this.connectionRequestPassword = connectionRequestPassword;
	}

	@DynamoDBAttribute(attributeName = "ACS_SERVER_URL")
	public String getAcsServerUrl() {
		return acsServerUrl;
	}

	public void setAcsServerUrl(String acsServerUrl) {
		this.acsServerUrl = acsServerUrl;
	}

	@DynamoDBAttribute(attributeName = "CPE_USERNAME")
	public String getConnectionRequestUsername() {
		return connectionRequestUsername;
	}

	public void setConnectionRequestUsername(String connectionRequestUsername) {
		this.connectionRequestUsername = connectionRequestUsername;
	}

	@DynamoDBAttribute(attributeName = "CPE_GATEWAY_URL")
	public String getConnectionRequestURL() {
		return connectionRequestURL;
	}

	public void setConnectionRequestURL(String connectionRequestURL) {
		this.connectionRequestURL = connectionRequestURL;
	}
	
	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	@DynamoDBAttribute(attributeName = "ACS_USERNAME")
	public String getAcsUsername() {
		return acsUsername;
	}

	public void setAcsUsername(String acsUsername) {
		this.acsUsername = acsUsername;
	}

	@DynamoDBAttribute(attributeName = "ACS_PASSWORD")
	public String getAcsPassword() {
		return acsPassword;
	}

	public void setAcsPassword(String acsPassword) {
		this.acsPassword = acsPassword;
	}
	
	@DynamoDBAttribute(attributeName = "HP_ID")
	public String getHpId() {
		return hpId;
	}

	public void setHpId(String hpId) {
		this.hpId = hpId;
	}

	@DynamoDBAttribute(attributeName = "DEVICE_ID")
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	@DynamoDBAttribute(attributeName = "DEVICE_NAME")
	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	@DynamoDBAttribute(attributeName = "MODEL")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMac() {
		return mac;
	}

	@DynamoDBAttribute(attributeName = "MAC")
	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getNetworkAdmin() {
		return networkAdmin;
	}
	
	@DynamoDBAttribute(attributeName = "NETWORK_ADMIN")
	public void setNetworkAdmin(String networkAdmin) {
		this.networkAdmin = networkAdmin;
	}

	@DynamoDBAttribute(attributeName = "DEFAULT_TOPIC")
	public String getDefaultTopic() {
		return defaultTopic;
	}

	public void setDefaultTopic(String defaultTopic) {
		this.defaultTopic = defaultTopic;
	}

	
	@DynamoDBAttribute(attributeName = "GENERATED_ZT_KEY")
	public Boolean getGenerateZTKey() {
		return generateZTKey;
	}

	public void setGenerateZTKey(Boolean generateZTKey) {
		this.generateZTKey = generateZTKey;
	}

	@DynamoDBAttribute(attributeName = "LAN_SETTINGS")
	public List<LanSettings> getLanSettings() {
		return lanSettings;
	}

	public void setLanSettings(List<LanSettings> lanSettings) {
		this.lanSettings = lanSettings;
	}

	@DynamoDBAttribute(attributeName = "WAN_SETTINGS")
	public WANSettings getWanSetting() {
		return wanSetting;
	}

	public void setWanSetting(WANSettings wanSetting) {
		this.wanSetting = wanSetting;
	}

	@DynamoDBAttribute(attributeName = "WIFI_SETTINGS")
	public List<WiFiSettings> getWifiSettings() {
		return wifiSettings;
	}

	public void setWifiSettings(List<WiFiSettings> wifiSetting) {
		this.wifiSettings = wifiSetting;
	}

	@DynamoDBAttribute(attributeName = "RADIO_SETTINGS")
	public List<DeviceRadioSettings> getRadioSettings() {
		return radioSettings;
	}

	public void setRadioSettings(List<DeviceRadioSettings> radioSetting) {
		this.radioSettings = radioSetting;
	}
	
	@DynamoDBAttribute(attributeName = "FIRMWARE")
	public DeviceFirmware getFirmware() {
		return firmware;
	}

	public void setFirmware(DeviceFirmware firmware) {
		this.firmware = firmware;
	}

	@Override
	public String toString() {
		return "DeviceConfiguration [manufacturer=" + manufacturer + ", manufacturerOUI=" + manufacturerOUI
				+ ", productClass=" + productClass + ", serialNumber=" + serialNumber + ", hardwareVersion="
				+ hardwareVersion + ", softwareVersion=" + softwareVersion + ", provisioningCode=" + provisioningCode
				+ ", specVersion=" + specVersion + ", description=" + description + ", serviceName=" + serviceName
				+ ", acsServerUrl=" + acsServerUrl + ", connectionRequestUsername=" + connectionRequestUsername
				+ ", connectionRequestPassword=" + connectionRequestPassword + ", acsUsername=" + acsUsername
				+ ", acsPassword=" + acsPassword + ", connectionRequestURL=" + connectionRequestURL + ", hpId=" + hpId
				+ ", deviceId=" + deviceId + ", deviceName=" + deviceName + ", model=" + model + ", mac=" + mac
				+ ", networkAdmin=" + networkAdmin + ", defaultTopic=" + defaultTopic + ", generateZTKey="
				+ generateZTKey + ", lanSettings=" + lanSettings + ", wanSetting=" + wanSetting + ", radioSettings="
				+ radioSettings + ", wifiSettings=" + wifiSettings + ", firmware=" + firmware + "]";
	}

	
	
	
}
