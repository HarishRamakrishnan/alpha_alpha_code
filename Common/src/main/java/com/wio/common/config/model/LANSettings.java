package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBDocument
public class LANSettings 
{
	private String portID;
	private String dhcp; 
	private String networkID;
	private String ipV4;
	private String ipV6;
	private String gateway;
	private String subnetMask;
	private String dnsPrimary;
	private String dnsSecondary;
	private String ifName;
	private String name;
	private String portNo;
	private String vlanNo;
	private String index;
	   
    public LANSettings() {
    }

	@DynamoDBHashKey(attributeName = "PORT_ID")
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	
	@DynamoDBHashKey(attributeName = "PORT_NO")
	public String getPortNo() {
		return portNo;
	}
	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}

	@DynamoDBAttribute(attributeName = "NETWORK_ID")
	public String getNetworkID() {
		return networkID;
	}
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}


	@DynamoDBAttribute(attributeName = "VLAN_NO")
	public String getVlanNo() {
		return vlanNo;
	}
	public void setVlanNo(String vlanNo) {
		this.vlanNo = vlanNo;
	}


	@DynamoDBAttribute(attributeName = "IPV4")
	public String getIpV4() {
		return ipV4;
	}
	public void setIpV4(String ipV4) {
		this.ipV4 = ipV4;
	}


	@DynamoDBAttribute(attributeName = "IPV6")
	public String getIpV6() {
		return ipV6;
	}
	public void setIpV6(String ipV6) {
		this.ipV6 = ipV6;
	}


	@DynamoDBAttribute(attributeName = "GATEWAY")
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}


	@DynamoDBAttribute(attributeName = "SUBNET_MASK")
	public String getSubnetMask() {
		return subnetMask;
	}
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}


	@DynamoDBAttribute(attributeName = "DNS_PRIMARY")
	public String getDnsPrimary() {
		return dnsPrimary;
	}
	public void setDnsPrimary(String dnsPrimary) {
		this.dnsPrimary = dnsPrimary;
	}


	@DynamoDBAttribute(attributeName = "DNS_SECONDARY")
	public String getDnsSecondary() {
		return dnsSecondary;
	}
	public void setDnsSecondary(String dnsSecondary) {
		this.dnsSecondary = dnsSecondary;
	}	
    
	@DynamoDBAttribute(attributeName = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@DynamoDBAttribute(attributeName = "IFNAME")
	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}	
	@DynamoDBAttribute(attributeName = "DHCP")
	public String getDhcp() {
		return dhcp;
	}

	public void setDhcp(String dhcp) {
		this.dhcp = dhcp;
	}

	@DynamoDBAttribute(attributeName = "INDEX")
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dhcp == null) ? 0 : dhcp.hashCode());
		result = prime * result + ((dnsPrimary == null) ? 0 : dnsPrimary.hashCode());
		result = prime * result + ((dnsSecondary == null) ? 0 : dnsSecondary.hashCode());
		result = prime * result + ((gateway == null) ? 0 : gateway.hashCode());
		result = prime * result + ((ifName == null) ? 0 : ifName.hashCode());
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		result = prime * result + ((ipV4 == null) ? 0 : ipV4.hashCode());
		result = prime * result + ((ipV6 == null) ? 0 : ipV6.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((networkID == null) ? 0 : networkID.hashCode());
		result = prime * result + ((portID == null) ? 0 : portID.hashCode());
		result = prime * result + ((portNo == null) ? 0 : portNo.hashCode());
		result = prime * result + ((subnetMask == null) ? 0 : subnetMask.hashCode());
		result = prime * result + ((vlanNo == null) ? 0 : vlanNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LANSettings other = (LANSettings) obj;
		if (dhcp == null) {
			if (other.dhcp != null)
				return false;
		} else if (!dhcp.equals(other.dhcp))
			return false;
		if (dnsPrimary == null) {
			if (other.dnsPrimary != null)
				return false;
		} else if (!dnsPrimary.equals(other.dnsPrimary))
			return false;
		if (dnsSecondary == null) {
			if (other.dnsSecondary != null)
				return false;
		} else if (!dnsSecondary.equals(other.dnsSecondary))
			return false;
		if (gateway == null) {
			if (other.gateway != null)
				return false;
		} else if (!gateway.equals(other.gateway))
			return false;
		if (ifName == null) {
			if (other.ifName != null)
				return false;
		} else if (!ifName.equals(other.ifName))
			return false;
		if (index == null) {
			if (other.index != null)
				return false;
		} else if (!index.equals(other.index))
			return false;
		if (ipV4 == null) {
			if (other.ipV4 != null)
				return false;
		} else if (!ipV4.equals(other.ipV4))
			return false;
		if (ipV6 == null) {
			if (other.ipV6 != null)
				return false;
		} else if (!ipV6.equals(other.ipV6))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (networkID == null) {
			if (other.networkID != null)
				return false;
		} else if (!networkID.equals(other.networkID))
			return false;
		if (portID == null) {
			if (other.portID != null)
				return false;
		} else if (!portID.equals(other.portID))
			return false;
		if (portNo == null) {
			if (other.portNo != null)
				return false;
		} else if (!portNo.equals(other.portNo))
			return false;
		if (subnetMask == null) {
			if (other.subnetMask != null)
				return false;
		} else if (!subnetMask.equals(other.subnetMask))
			return false;
		if (vlanNo == null) {
			if (other.vlanNo != null)
				return false;
		} else if (!vlanNo.equals(other.vlanNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LANSettings [portID=" + portID + ", dhcp=" + dhcp + ", networkID=" + networkID + ", ipV4=" + ipV4
				+ ", ipV6=" + ipV6 + ", gateway=" + gateway + ", subnetMask=" + subnetMask + ", dnsPrimary="
				+ dnsPrimary + ", dnsSecondary=" + dnsSecondary + ", ifName=" + ifName + ", name=" + name + ", portNo="
				+ portNo + ", vlanNo=" + vlanNo + ", index=" + index + "]";
	}
	
	
    
}
