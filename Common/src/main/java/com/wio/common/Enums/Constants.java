package com.wio.common.Enums;

public class Constants 
{
	public static final String NETWORK_AUTH_MODE_WEP="WPA";
	public static final String NETWORK_AUTH_MODE_WEP2="WPA2";
	public static final String AUTHORIZATION="securityToken";
	public static final String SUPPORTED="Supported";
	
	public static final String NOTSUPPORTED="Not-Supported";
	
	public static final String ENABLE="Enable";
	
	public static final String DISABLE="Disable";
	
	public static final String SU_MIMO="SU-MIMO";
	
	public static final String MU_MIMO="MU-MIMO";
	
	public static final String SINGLE="Single";
	
	public static final String DUAL="Dual";
	
	public static final String PPPOE = "PPPoE";
	public static final String PPPOA = "PPPoA";
	public static final String STATIC = "Static";
	public static final String DYNAMIC = "Dynamic";
	public static final String RJ45 = "RJ45";
	public static final String STATUS_ON="ON";
	public static final String STATUS_OFF="OFF";
	public static final String CHANNEL="36:44:48:52:56:60:64:100:104:108:112:116:120:124:128:132:136:140:149:153:157:161:165:Auto";
	
	
	public static final String RJ11 = "RJ11";
	public static final String PORT_TYPE_WAN="WAN";
	public static final String PORT_TYPE_LAN="LAN";
	
	public static final String IPV4="IPv4";
	public static final String IPV6="IPv6";
	
	//Firmware Related Constants
	public static final String FW_CHECKSUM_MD5="MD5";
	public static final String FW_CHECKSUM_SHA256="SHA-256";
	public static final String FW_VERSION_STABLE="STABLE";
	public static final String FW_VERSION_BETA="BETA";
	public static final String CHANNEL1 = "1:2:3:4:5:6:7:8:9:10:11:Auto";
	
	public static enum Source {
		 
	    CWMP("CWMP"),
	    RESTAPI ("RESTAPI") ,
		AP ("AP") ,
		UI ("UI");

	    private String source;

	    Source(String source) {
	        this.source = source;
	    }
	    
	    public String getSource() {
	        return this.source;
	    }
	    
	    public static boolean contains(String source){
	    	Source[] source1 = Source.values();
	    	
	    	for(Source mySource: source1){
	    		if(mySource.getSource().equals(source)){
	    			return true;
	    		}
	    	}
	    	return false;
	    }
	}
}
