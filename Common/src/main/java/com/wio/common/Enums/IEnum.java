package com.wio.common.Enums;

import java.util.Iterator;

public interface IEnum {
	 /**
     * Returns the ordinal value of the Enum.
     *
     * @return  the ordinal value of the Enum.
     */
    public int getOrdinal();
    

      /**
     * Returns the text description of the Enum.
     * Used by GUI to populate lists etc
     *
     * @return  the ordinal value of the Enum.
     */
    public  String getDescription();
    
    /**
     * Returns a string representation of the object.  The descrption of the Enum
     * will be used as string representation of the object.
     *
     * @return  a string representation of the object.
     */
    public  String toString();
    
     /**
     * Returns an iterator over the elements contained in this Enum that
     * exist on the switch.
     *
     * @return an {@link java.util.Iterator} over the elements contained
     * in this Enum.
     */
     public Iterator iterator();
    
    

}

