package com.wio.common.Enums;

public class DynamoDBObjectKeys
{
	//Hardware
	public static final String HW_ID = "HW_ID";
	public static final String HW_NAME="HW_NAME";
	public static final String HW_TYPE="HW_TYPE";
	public static final String HW_MANUFACTURE="MANUFACTURE";
	public static final String HW_CHIPSET_VENDOR="CHIPSET_VENDOR";
	public static final String HW_CHIPSET_TYPE="CHIPSET_TYPE";
	public static final String HW_DBDC_SUPPORTED="DBDC_SUPPORTED";
	
	
	//HardwareProfile
	public static final String HP_ID="HP_ID";
	public static final String HP_NAME="HP_NAME";
	public static final String HP_MODEL="MODEL";
	
	
	//Device
	public static final String DEVICE_NAME="DEVICE_NAME";
	public static final String DEVICE_ID="DEVICE_ID";
	public static final String DEVICE_MODEL="DEVICE_MODEL";
	public static final String DEVICE_MAC_ADDRESS="MAC_ADDRESS";
	public static final String DEVICE_FIRMWARE="FIRMWARE";
	public static final String DEVICE_STATUS="STATUS";
	public static final String DEFAULT_TOPIC="DEFAULT_TOPIC";
	
	
	//Service
	public static final String SERVICE_NAME="SERVICE_NAME";
	public static final String SERVICE_EMAIL="EMAIL";
	public static final String SERVICE_CITY="CITY";
	public static final String SERVICE_PINCODE="PINCODE";
	public static final String SERVICE_COUNTRY="COUNTRY";
	public static final String SERVICE_ACS_URL="ACS_URL";
	
	
	//Network
	public static final String NETWORK_ID="NETWORK_ID";
	public static final String NETWORK_NAME="NETWORK_NAME";
	public static final String NETWORK_TYPE="NETWORK_TYPE";
	public static final String ZT_ENABLE="ZT_ENABLE";
	public static final String DHCP_SERVER="DHCP_SERVER";
	
	
	//WIFI
	public static final String WIFI_ID="WIFI_ID";
	public static final String WIFI_NAME="WIFI_NAME";
	public static final String SSID="SSID";
	public static final String HIDDEN_SSID="HIDDEN_SSID";
	public static final String COUNTRY="COUNTRY";
	public static final String AUTHENTICATION_TYPE="AUTHENTICATION_TYPE";
	public static final String ENCRYPTION_TYPE="ENCRYPTION_TYPE";
	public static final String ENABLE_WIRELESS="ENABLE_WIRELESS";
	
	
	//Device Group
	public static final String DG_ID="DG_ID";
	public static final String DG_NAME="DG_NAME";
	public static final String DG_TYPE = "DG_TYPE";
	public static final String PARENT_DG="PARENT_DG";
	

	//User
	public static final String USER_ID="USER_ID";
	public static final String USER_NAME="USER_NAME";
	public static final String FIRST_NAME="FIRST_NAME";
	public static final String LAST_NAME="LAST_NAME";
	public static final String EMAIL="EMAIL";
	public static final String PHONE_NUMBER="PHONE_NUMBER";
	//public static final String NETWORK_ADMIN="NETWORK_ADMIN";
	public static final String NETWORK_USER="NETWORK_USER";
	public static final String ACCOUNT_STATUS="ACCOUNT_STATUS";
	
	public static final String ROLE="ROLE";
	public static final String PRIVILEGE="PRIVILEGE";
	public static final String ACTION="ACTION";
	public static final String INPUT_ROLE="INPUT_ROLE";
	
	
	//Firmware 
	public static final String FW_FILE_NAME="FILE_NAME";
	public static final String FW_VERSION="VERSION";
	public static final String FW_VERSION_TYPE="VERSION_TYPE";
	public static final String FW_STATUS="STATUS";
	
	
	//Role
	public static final String USER_ROLE="USER_ROLE";
	
	
	//Testing Tables
	public static final String TEST_SUITE_ID="SUITE_ID";
	public static final String TEST_RUN_ID="RUN_ID";
	public static final String TEST_LATEST="LATEST";

	
	//SecurityToken
	public static final String SECURITY_TOKEN="SECURITY_TOKEN";
	
}
