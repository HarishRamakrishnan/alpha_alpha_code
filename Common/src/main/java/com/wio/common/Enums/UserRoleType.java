package com.wio.common.Enums;

public class UserRoleType
{
	public static final String USER_ROLE_NONE="None";
	public static final String USER_ROLE_WAVES_ADMIN="WavesAdmin";
	//public static final String USER_ROLE_WAVES_USER="WavesUser";
	public static final String USER_ROLE_GLOBAL_USER="GlobalUser";
	public static final String USER_ROLE_SERVICE_USER="ServiceUser";
	//public static final String USER_ROLE_NETWORK_ADMIN="NetworkAdmin";
	public static final String USER_ROLE_DEVICE_USER="DeviceUser";
	public static final String USER_ROLE_NETWORK_USER="NetworkUser";
	public static final String USER_ROLE_GUEST_USER="GuestUser";
	/*
	
	
    public static final int USER_ROLE_TYPE_NONE=0;
    public static final int USER_ROLE_TYPE_WAVES_USER=1;
    public static final int USER_ROLE_TYPE_GLOBAL_USER=2;
    public static final int USER_ROLE_TYPE_SERVICE_USER=3;
    public static final int USER_ROLE_TYPE_NETWORK_USER=4;
    public static final int USER_ROLE_TYPE_AP_USER=5;
    
    private UserRoleType(int ordinal, String description)
    {
        super(ordinal, description);
    }

    public static final UserRoleType None = new UserRoleType(USER_ROLE_TYPE_NONE, "None");
    public static final UserRoleType WavesUser = new UserRoleType(USER_ROLE_TYPE_WAVES_USER, "WavesUser");
    public static final UserRoleType GlobalUser = new UserRoleType(USER_ROLE_TYPE_GLOBAL_USER, "GlobalUser");
    public static final UserRoleType ServiceUser = new UserRoleType(USER_ROLE_TYPE_SERVICE_USER, "ServiceUser");
    public static final UserRoleType NetworkUser = new UserRoleType(USER_ROLE_TYPE_NETWORK_USER, "NetworkUser");
    public static final UserRoleType APUser = new UserRoleType(USER_ROLE_TYPE_AP_USER, "APUser");
    
    private static final UserRoleType[] VALUES =
    {
    	None,
    	WavesUser,
    	GlobalUser,
    	ServiceUser,
    	NetworkUser,
    	APUser
       
    };

    protected final Object readResolve() throws java.io.ObjectStreamException
    {
        return VALUES[m_ordinal];
    }

    public final Iterator iterator()
    {
        return new Itr(None, VALUES);
    }

    public static final UserRoleType getUserRoleType(int value)
    {
        if ((value >= USER_ROLE_TYPE_NONE) && (value < VALUES.length))
        {
            return VALUES[value];
        }
        // this is an error - return the None object
        return None;
    }*/
    
    
}
