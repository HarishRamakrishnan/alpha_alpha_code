package com.wio.common.util;

import java.util.UUID;

public class UniqueIDGenerator {

	public static String getID()
	{
		String uid = null;
		try
		{
			uid = UUID.randomUUID().toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return uid;
	}

}
