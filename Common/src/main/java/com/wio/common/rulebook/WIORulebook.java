package com.wio.common.rulebook;

import com.deliveredtechnologies.rulebook.lang.RuleBuilder;
import com.deliveredtechnologies.rulebook.model.rulechain.cor.CoRRuleBook;

public class WIORulebook extends CoRRuleBook<RulebookResult> 
{
	@Override
	public void defineRules()
	{
		addRule(RuleBuilder.create().withFactType(ConfigBean.class).withResultType(RulebookResult.class)
				.when(facts -> facts.getOne().getDeviceID().equals("device123"))
			    .then((facts, result) -> result.setValue(result.getValue()))
			    .stop()
			    .build());
		
	}
}
