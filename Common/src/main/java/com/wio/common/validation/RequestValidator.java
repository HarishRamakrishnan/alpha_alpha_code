package com.wio.common.validation;

import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import com.wio.common.Enums.UserRoleType;
import com.wio.common.util.UtilProperties;

public class RequestValidator {
	
	private static Pattern emailPattern;
	private static Pattern numberPattern;
	private static Pattern strongPasswordPattern;
	private static Pattern ipAddressPattern;
	private static Pattern ipv6AddressPattern;
	private static Pattern ssidPattern;
	private static Pattern preSharedKeyPattern;
	private static Pattern versionPattern;
	
	private static Pattern macAddressPattern;
	private static Pattern alphanumaricPattern;
	private static Pattern alphanumaricPatternwithhypen;
	private static Pattern alphanumaricPatternwithspace;
	private static Pattern fourdigitnumaric;
	
	static UtilProperties utilProp= new UtilProperties();
	static Properties prop=new Properties();
	
	static
	{
		prop = utilProp.loadProperties();
		
		emailPattern = Pattern.compile(prop.getProperty("validate.email"));
		numberPattern = Pattern.compile(prop.getProperty("validate.phoneNumber"));
		strongPasswordPattern = Pattern.compile(prop.getProperty("validate.password"));
		ipAddressPattern = Pattern.compile(prop.getProperty("validate.ip"));
		ipv6AddressPattern = Pattern.compile(prop.getProperty("validate.ipv6"));
		ssidPattern = Pattern.compile(prop.getProperty("validate.SSIDChars"));
		preSharedKeyPattern = Pattern.compile(prop.getProperty("validate.preSharedKey"));
		versionPattern = Pattern.compile(prop.getProperty("validate.version"));
		
		macAddressPattern = Pattern.compile(prop.getProperty("validate.mac"));
		alphanumaricPattern = Pattern.compile(prop.getProperty("validate.alphanumaric"));
		alphanumaricPatternwithhypen = Pattern.compile(prop.getProperty("validate.alphanumaricwithhypen"));
		alphanumaricPatternwithspace = Pattern.compile(prop.getProperty("validate.alphanumaricwithspace"));
		fourdigitnumaric = Pattern.compile(prop.getProperty("validate.fourdigitnumaric"));
	}
	
	
	public static boolean isEmptyField(String input)
	{
		if(null == input || input.trim().equals("") || input.trim().length() <= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isEmptyField(Integer input)
	{
		if(null == input || input >= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static <T> boolean isEmptyCollection(List<T> inputList)
	{
		if(inputList == null || inputList.isEmpty() || inputList.size() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isValidSSID(final String ssid)
	{
		return ssidPattern.matcher(ssid).matches();
	}
	
	public static boolean isValidPresharedKey(final String presharedKey)
	{
		return preSharedKeyPattern.matcher(presharedKey).matches();
	}
	
	//returns true if valid
	public static boolean validateEmail(final String emailId)
	{
		return emailPattern.matcher(emailId).matches();
	}
	
	//returns true if valid
	public static boolean validateNumber(final String number)
	{
		return numberPattern.matcher(number).matches();
	}
	
	//returns true if valid
	public static boolean isValidNumber(final Number number)
	{
		return numberPattern.matcher(number.toString()).matches();
	}
	
	//returns true if valid
	//Valid only if password contains, 6 to 20 characters string with at least one digit, one upper case letter, one lower case letter and one special symbol (�@#$%�)
	public static boolean isStrongPassword(final String password)
	{
		return strongPasswordPattern.matcher(password).matches();
	}
	
	//returns true if valid
	public static boolean validateIPAddress(final String ip)
	{
		return ipAddressPattern.matcher(ip).matches();
	}
	
	//returns true if valid
	public static boolean validateIPV6Address(final String ipv6)
	{
		return ipv6AddressPattern.matcher(ipv6).matches();
	}
	
	//returns true if valid
	public static boolean isValidVersionFormat(final String version)
	{
		return versionPattern.matcher(version).matches();
	}
	
	
	//returns true if valid
	public static boolean isValidMACFormat(final String macAddress)
	{
		return macAddressPattern.matcher(macAddress).matches();
	}
	
	
	//returns true if valid
	public static boolean isValidAlphaNumaricFormat(final String alphanumaric)
	{
		return alphanumaricPattern.matcher(alphanumaric).matches();
	}
	
	//returns true if valid
	public static boolean isValidAlphaNumaricWithHypenFormat(final String alphanumaricwithhypen)
	{
		return alphanumaricPatternwithhypen.matcher(alphanumaricwithhypen).matches();
	}
		
	//returns true if valid
	public static boolean isValidAlphaNumaricWithSpaceFormat(final String alphanumaricwithspace)
	{
		return alphanumaricPatternwithspace.matcher(alphanumaricwithspace).matches();
	}
	
	//returns true if valid
	public static boolean isValidFourDigitNumaricFormat(final Number fourDigitNumaric)
	{
		return fourdigitnumaric.matcher(fourDigitNumaric.toString()).matches();
	}
	
	
	public static boolean isBooleanParameter(final boolean param)
	{
		if(param == true || param == false)
			return true;
		
		return false;
	}
	
	public static <T> boolean isNullObject(T obj)
	{
		if(null == obj)
			return true;
		
		return false;
	}
	
	public static boolean isNullString(String string)
	{
		if(null == string)
			return true;
		
		return false;
	}
	
//	public static String isBooleanParameter(final boolean param)
//	{
//		if(param == true || param == false)
//			return true;
//		
//		return false;
//	}
	
	public static boolean isValidRole(String role)
	{
		 switch(role)
		 {  
		    case UserRoleType.USER_ROLE_WAVES_ADMIN:
		    case UserRoleType.USER_ROLE_GLOBAL_USER:
		    case UserRoleType.USER_ROLE_SERVICE_USER:
		    case UserRoleType.USER_ROLE_NETWORK_USER:
		    	return true;  
		    default: return false;
		  }  
	}
	
	public static Boolean isValidString(String str){
		Boolean signal = Boolean.FALSE;
		
		if(null != str && !str.isEmpty()){
			signal = Boolean.TRUE;
		}
		
		return signal;
	}

}
