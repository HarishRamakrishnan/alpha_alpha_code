package com.wio.common.validation;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.DGWifiMap;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.config.model.HWServiceMap;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.Role;
import com.wio.common.user.model.RolePrivilege;
import com.wio.common.user.model.User;

public class DataValidator 
{	
	private static GenericDAO dao = GenericDAOFactory.getGenericDAO();	
	
	public static boolean isHWDependent(String hwID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, hwID, ComparisonOperator.EQ));
				
		if(!dao.getGenericObjects(HardwareProfile.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(HWServiceMap.class, filterCondition).isEmpty())
			return true;
		
		return false;
	}
	
	
	public static boolean isHPDependent(String hpID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, hpID, ComparisonOperator.EQ));
		
		List<Device> devices = dao.getGenericObjects(Device.class, filterCondition);
		
		if(!devices.isEmpty())
			return true;
		
		return false;
	}
	
	
	public static boolean isDeviceDependent(String deviceID) throws DAOException
	{
		return true;
	}
	
	
	public static boolean isDGDependent(String dgID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, dgID, ComparisonOperator.EQ));
		
		if(!dao.getGenericObjects(Device.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(DGWifiMap.class, filterCondition).isEmpty())
			return true;
		
		/*if(isParentDG(dgID))
			return true;*/
		
		return false;
	}
	
	
	public static boolean isNetworkDependent(Network network) throws DAOException
	{
		List<FilterCondition> serviceFilter = new ArrayList<FilterCondition>();		
		serviceFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, network.getServiceName(), ComparisonOperator.EQ));
						
		for (Device device : dao.getGenericObjects(Device.class, serviceFilter))
		{
			for (LANSettings lan : device.getLanSettings())
			{
				if(lan.getNetworkID().equals(network.getNetworkID()))
					return true;
			}
		}
		
		List<FilterCondition> networkFilter = new ArrayList<FilterCondition>();		
		networkFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_ID, network.getNetworkID(), ComparisonOperator.EQ));
		
		if(!dao.getGenericObjects(DeviceGroup.class, networkFilter).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(WiFi.class, networkFilter).isEmpty())
			return true;
				
		return false;
	}
	
	
	public static boolean isWiFiDependent(String wifiID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_ID, wifiID, ComparisonOperator.EQ));
				
		if(!dao.getGenericObjects(DGWifiMap.class, filterCondition).isEmpty())
			return true;
		
		return false;
	}
	
	
	public static boolean isServiceDependent(String serviceName) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, serviceName, ComparisonOperator.EQ));
				
		if(!dao.getGenericObjects(HWServiceMap.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(HardwareProfile.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(Device.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(DeviceGroup.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(Network.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(WiFi.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(User.class, filterCondition).isEmpty())
			return true;
		
		return false;
	}
	
	public static boolean isPrivilegeDependent(String privilege) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, privilege, ComparisonOperator.EQ));
				
		if(!dao.getGenericObjects(RolePrivilege.class, filterCondition).isEmpty())
			return true;
		
		return false;
	}
	
	public static boolean isRoleDependent(String role) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, role, ComparisonOperator.EQ));
				
		if(!dao.getGenericObjects(RolePrivilege.class, filterCondition).isEmpty())
			return true;
		
		if(!dao.getGenericObjects(User.class, filterCondition).isEmpty())
			return true;
		
		return false;
	}
	
		
	public static boolean isParentDG(String dgID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PARENT_DG, dgID, ComparisonOperator.EQ));
		
	    if(!dao.getGenericObjects(DeviceGroup.class, filterCondition).isEmpty())
	    	return true;
	    
		return false;
	}
	
	
	public static boolean isHardwareExist(String hwID) throws DAOException
	{    
	    if(null != dao.getGenericObject(Hardware.class, hwID))
        	return true;
		
		return false;
	}
	
	public static boolean isHardwareProfileExist(String hpID) throws DAOException
	{    
	    if(null != dao.getGenericObject(HardwareProfile.class, hpID))
        	return true;
		
		return false;
	}
	
	public static boolean isDeviceExist(String deviceID) throws DAOException
	{    
	    if(null != dao.getGenericObject(Device.class, deviceID))
        	return true;
		
		return false;
	}
	
	public static boolean isDeviceGroupExist(String serviceName, String dgName) throws DAOException
	{    
		GenericDAO dao = GenericDAOFactory.getGenericDAO();
		
		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
		
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, serviceName, ComparisonOperator.EQ));
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_NAME, dgName, ComparisonOperator.EQ));	    
				
	    if(!dao.getGenericObjects(DeviceGroup.class, filterConditions).isEmpty())
        	return true;
		
		return false;
	}	
	
	
	public static boolean isDGExist(String dgID) throws DAOException
	{ 		
	    if(null != dao.getGenericObject(DeviceGroup.class, dgID))
        	return true;
		
		return false;
	}
	
	public static boolean isServiceExist(String serviceName) throws DAOException
	{    
	    if(null != dao.getGenericObject(Service.class, serviceName))
        	return true;
		
		return false;
	}
	
	public static boolean isNetworkExist(String networkID) throws DAOException
	{    
	    if(null != dao.getGenericObject(Network.class, networkID))
        	return true;
		
		return false;
	}
	
	public static boolean isWifiExist(String wifiID) throws DAOException
	{    
	    if(null != dao.getGenericObject(WiFi.class, wifiID))
        	return true;
		
		return false;
	}
	
	public static boolean isUserExist(String userID) throws DAOException
	{    
	    if(null != dao.getGenericObject(User.class, userID))
        	return true;
		
		return false;
	}
	
	public static boolean isRoleExist(String userRole) throws DAOException
	{    
	    if(null != dao.getGenericObject(Role.class, userRole))
        	return true;
		
		return false;
	}
	
	
	public static boolean isDGWiFiMapped(String dgID, String wifiID) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, dgID, ComparisonOperator.EQ));
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_ID, wifiID, ComparisonOperator.EQ));
				
	    if(!dao.getGenericObjects(DGWifiMap.class, filterCondition).isEmpty())
        	return true;
		
		return false;
	}
	
	public static boolean isHWServiceMapped(String hwID, String serviceName) throws DAOException
	{
		List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, hwID, ComparisonOperator.EQ));
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, serviceName, ComparisonOperator.EQ));
				
	    if(!dao.getGenericObjects(HWServiceMap.class, filterCondition).isEmpty())
        	return true;
		
		return false;
	}
	
	public static boolean isPrivilegedRole(String privilege, String role) throws DAOException
	{    
		GenericDAO dao = GenericDAOFactory.getGenericDAO();
		
		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
		
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.ROLE, role, ComparisonOperator.EQ));
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, privilege, ComparisonOperator.EQ));	    
				
	    if(!dao.getGenericObjects(RolePrivilege.class, filterConditions).isEmpty())
        	return true;
		
		return false;
	}
	
}
