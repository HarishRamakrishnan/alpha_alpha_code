package com.wio.common.aws.api.sns;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.OTP_TRANSACTIONS)
public class OTPTransaction 
{
	private String messageId;
	private String awsRequestId;
	private String phoneNumber;
	private int otp;
	private int failureTransactions;
	private int maxTransactionsAllowed;
	private String status;
	private String creationTime;
	private String expireTime;
	
	
	public OTPTransaction() {
		// TODO Auto-generated constructor stub
	}
	
	
	@DynamoDBHashKey(attributeName = "MESSAGE_ID")
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
		
	
	@DynamoDBAttribute(attributeName = "AWS_REQUEST_ID")
	public String getAwsRequestId() {
		return awsRequestId;
	}
	public void setAwsRequestId(String awsRequestId) {
		this.awsRequestId = awsRequestId;
	}

	
	@DynamoDBAttribute(attributeName = "PH_NUMBER")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	@DynamoDBAttribute(attributeName = "OTP")
	public int getOtp() {
		return otp;
	}
	public void setOtp(int otp) {
		this.otp = otp;
	}
	
	
	@DynamoDBAttribute(attributeName = "FAILURE_TRANSACTIONS")
	public int getFailureTransactions() {
		return failureTransactions;
	}
	public void setFailureTransactions(int failureTransactions) {
		this.failureTransactions = failureTransactions;
	}


	@DynamoDBAttribute(attributeName = "MAX_TRANSACTIONS_ALLOWED")
	public int getMaxTransactionsAllowed() {
		return maxTransactionsAllowed;
	}
	public void setMaxTransactionsAllowed(int maxTransactionsAllowed) {
		this.maxTransactionsAllowed = maxTransactionsAllowed;
	}


	@DynamoDBAttribute(attributeName = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}	
		
	
	@DynamoDBAttribute(attributeName = "EXPIRE_TIME")
	public String getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	
}
