package com.wio.common.aws.api.sns;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.SystemConfigMessages;

public class SNSAPI 
{
	//private static AmazonSNS snsClient = AmazonSNSClientBuilder.defaultClient();  
	
	private static AmazonSNSClientBuilder snsClientBuilder = AmazonSNSClientBuilder.standard().withRegion(Regions.US_EAST_1);	 
	private static AmazonSNSClient  snsClient = (AmazonSNSClient) snsClientBuilder.build();    	
	
    private static Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
    private static GenericDAO genDAO = null;
    
    private static SimpleDateFormat sdf =  new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT);
    	
    
	public static OTPTransaction sendOTP(String phoneNumber) throws DAOException
	{
		//Generate OTP
		int otp = generateOTP();
		
		//Create OTP message
		String message = otp+" is your OTP for mobile number validation at WIO Services, Thank you for choosing WIO !";
		
		//Setting Message Preferences
		smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
		        .withStringValue("WIOService") //The sender ID shown on the device.
		        .withDataType("String"));
		
		smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
		        .withStringValue("Transactional") //Sets the type to promotional.
		        .withDataType("String"));
		
		//Publish OTP message
		PublishResult result = snsClient.publish(new PublishRequest()
                .withMessage(message)
                .withPhoneNumber(phoneNumber)
                .withMessageAttributes(smsAttributes));		
		
		System.out.println("Message sent, ID:: "+result.getMessageId());
		
		//Prepare OTP Transaction details
		OTPTransaction transaction = new OTPTransaction();
		
		transaction.setMessageId(result.getMessageId());
		transaction.setAwsRequestId(result.getSdkResponseMetadata().getRequestId());
		transaction.setPhoneNumber(phoneNumber);
		transaction.setOtp(otp);
		transaction.setStatus("OTP_SENT");
		transaction.setFailureTransactions(0);
		transaction.setMaxTransactionsAllowed(3);
		transaction.setCreationTime(sdf.format(new Date()));	    
		transaction.setExpireTime(getOTPExpireTime());
		
		//Save OTP Transaction details
		saveOTPTransaction(transaction);
		
		OTPTransaction response = new OTPTransaction();
		
		response.setMessageId(transaction.getMessageId());
		response.setStatus(transaction.getStatus());
		
		return response;
	}
	
	
	public static OutputMessage validateOTP(String messageId, int otp) throws DAOException
	{
		OutputMessage output = new OutputMessage();			
		OTPTransaction otpTransaction = genDAO.getGenericObject(OTPTransaction.class, messageId);
		
		if(null == otpTransaction)
		{
			output.setStatus(200);
			output.setMessage("Invalid messageId !");
			output.setErrorMessage("Error: Invalid messageId !");
		}		
		else if(otpTransaction.getOtp() == otp)
		{
			if(otpTransaction.getFailureTransactions() <= 3)
			{
				output.setStatus(200);
				output.setMessage("Success");
			}
			else
			{
				output.setStatus(200);
				output.setMessage("You have exeeded maximum number of failure attempts !");
				output.setErrorMessage("Error: You have exeeded maximum number of failure attempts !");
			}
		}
		else
		{
			output.setStatus(200);
			output.setMessage("Invalid OTP, Authentication Failed !");
			
			if(otpTransaction.getFailureTransactions() == 3)
				output.setErrorMessage("Error: You have exeeded maximum number of failure attempts !");
			else
				output.setErrorMessage("Error: Invalid OTP, Authentication Failed !");
			
			//Update OTP Transaction details
			otpTransaction.setFailureTransactions(otpTransaction.getFailureTransactions()+1);			
			saveOTPTransaction(otpTransaction);
		}		
		return output;
	}
	
	
	public static void saveOTPTransaction(OTPTransaction otpTransaction) throws DAOException
	{
		genDAO = GenericDAOFactory.getGenericDAO();
		genDAO.saveGenericObject(otpTransaction);
	}
	
	
	public static int generateOTP()
	{
		SecureRandom sr = null;
		try 
		{
			sr = SecureRandom.getInstance("SHA1PRNG");
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		}
		
		return sr.nextInt(9000000) + 1000000;
	}
	
	public static String getOTPExpireTime()
	{
		Date date = new Date();
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    
	    cal.add(Calendar.MINUTE, 15);
		
	    java.util.Date endDate = cal.getTime();
	    
	    return sdf.format(endDate);
	}

}
