package com.wio.common.aws.api.lambda;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;

public class LambdaClientAPI {

	public static String invoke(String functionName, String deviceJson)
	{	
		AWSLambda lambdaClient;
		lambdaClient = AWSLambdaClientBuilder.standard().withRegion(System.getenv("REGION")).build();
	    String returnDetails = null;
	    
	    try {
	        InvokeRequest invokeRequest = new InvokeRequest();
	        invokeRequest.setFunctionName(functionName);
	        invokeRequest.setPayload(deviceJson);
	
	        System.out.println("Before Invoking " +functionName);
	        
	        returnDetails = byteBufferToString(
	        		lambdaClient.invoke(invokeRequest).getPayload(),
	                Charset.forName("UTF-8"));
	        
	        System.out.println("After Invoking " +functionName);
	        System.out.println("return details =" +returnDetails);
	        
	        return "SUCCESS";
	        
	    } catch (Exception e) {
	    	System.out.println("Exception is " +e.getMessage());
	    	return "FAILURE";
	    }
	    
	}
	
		
	public static String byteBufferToString(ByteBuffer buffer, Charset s) {
	  String data = "";
	  CharsetDecoder decoder = s.newDecoder();
	  try{
	    int old_position = buffer.position();
	    data = decoder.decode(buffer).toString();
	    // reset buffer's position to its original so it is not altered:
	    buffer.position(old_position);  
	  }catch (Exception e){
	    e.printStackTrace();
	    return "";
	  }
	  return data;
	}
}
