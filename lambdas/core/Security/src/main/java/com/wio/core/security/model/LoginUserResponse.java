package com.wio.core.security.model;

public class LoginUserResponse {
	
	private String userID;
	private String securityToken;
	
	

	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	
	

}
