package com.wio.core.security.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.helper.ActionMapper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;
import com.wio.core.security.action.AuthorizeUserAction;
import com.wio.core.security.action.auth.PublishMobileOTP;
import com.wio.core.security.action.auth.ValidateMobileOTP;
import com.wio.core.security.model.LoginUserResponse;

/**
 * This class contains the main event handler for the Lambda function.
 */
public class SecurityRequestRouter {

	//private static AWSCredentials credentials = null;
	//private static AWSLambdaClient lambdaClient;

	/**
	 * The main Lambda function handler. Receives the request as an input
	 * stream, authenticate and authorize the user login user and looks for the
	 * "action" property to decide where to route the request. The request is
	 * passed to the respective Action implementation as a request body.
	 *
	 * @param request
	 *            The InputStream for the incoming event. This should contain an
	 *            "action" and "body" properties. The action property should
	 *            contain the namespaced name of the class that should handle
	 *            the invocation. The body property should contain the full
	 *            request body for the action class.
	 * @param response
	 *            An OutputStream where the response returned by the action
	 *            class is written
	 * @param context
	 *            The Lambda Context object
	 * @throws BadRequestException
	 *             This Exception is thrown whenever parameters are missing from
	 *             the request or the action class can't be found
	 * @throws InternalErrorException
	 *             This Exception is thrown when an internal error occurs, for
	 *             example when the database is not accessible
	 * @throws IOException 
	 */
	public static void handler(InputStream request, OutputStream response, Context context) throws BadRequestException, InternalErrorException, IOException 
	{
		LambdaLogger logger = context.getLogger();

		OutputMessage outputMessage = new OutputMessage();
		
		JsonParser parser = new JsonParser();
		JsonObject input;

		try 
		{
			if(RequestValidator.isNullObject(request))
			{
				outputMessage.setStatus(200);
				outputMessage.setMessage("Input Request cannot be empty / null !");
				
				IOUtils.write(getGson().toJson(outputMessage, OutputMessage.class), response);				
			}
			
			input = parser.parse(IOUtils.toString(request)).getAsJsonObject();
			//inputObj = parser.parse(IOUtils.toString(request)).getAsJsonObject();
		} 
		catch (IOException e) 
		{
			logger.log("Error while reading request\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}
		
		System.out.println("Json Object:: "+input);
		
		JsonObject inputObj = new JsonObject();;
		
		if(null == input.get("action"))
		{
			if(null != input.get("resource") && null != input.get("headers")) //Proxy integration
			{
				System.out.println("Proxy integration data::: "+input);
				
				JsonObject headers = input.getAsJsonObject("headers");
				
				JsonObject body = parser.parse(input.get("body").getAsString()).getAsJsonObject();
				
				JsonElement header = headers.get("vernemq-hook");
								
				inputObj.add("action", header);
				inputObj.add("body", body);
			}
			else
			{
				System.out.println("No action / proxy found !");
				
				outputMessage.setStatus(200);
				outputMessage.setMessage("action cannot be empty / null !");
				
				IOUtils.write(getGson().toJson(outputMessage, OutputMessage.class), response);		
			}
		}
		else
		{
			inputObj = input;
		}
		
		
		AuthorizeUserAction authorizeUserAction = new AuthorizeUserAction();
		
		String action = inputObj.get("action").getAsString();
		User validateUserResponse=null;
		
		
		if(!ActionMapper.provisionActions().contains(action))
		{
			validateUserResponse = authorizeUserAction.handle(inputObj, context);
		}
		
		/*if(!action.equalsIgnoreCase(ActionMapper.INITIAL_PROVISIONING_VALIDATE_DEVICE) ||
				!action.equalsIgnoreCase(ActionMapper.INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER) ||
				!action.equalsIgnoreCase(ActionMapper.INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_SUBSCRIBE)) 
        {
			validateUserResponse = authorizeUserAction.handle(inputObj, context);
        }*/
		
		
        if(action.equals("login")) // For Login Request
        {
        	if(validateUserResponse.getAuthenticatedUser())
        	{
        		try 
        		{
        			//System.out.println("UserName user response===>"+validateUserResponse.getUserName());
        			LoginUserResponse loginUserResponse = new LoginUserResponse();
        			loginUserResponse.setUserID(validateUserResponse.getUserID());
        			loginUserResponse.setSecurityToken(validateUserResponse.getSecurityToken());
        			
        			//getGson().toJson(loginUserResponse, LoginUserResponse.class);
    				IOUtils.write(getGson().toJson(loginUserResponse, LoginUserResponse.class), response);
    			} catch (final IOException e) {
    				logger.log("Error while writing response\n" + e.getMessage());
    				throw new InternalErrorException(e.getMessage());
    			}
        	
        	}
        	else        		
        	   throw new BadRequestException(ExceptionMessages.EX_AUTHENTICATION_FAILED);
        }
        else if(action.equals(ActionMapper.SECURITY_MGMT_GENERATE_MOBILE_OTP))
        {
        	PublishMobileOTP publishOTP = new PublishMobileOTP();
        	
        	try 
        	{
				String result = publishOTP.handle(inputObj, context);
				
				IOUtils.write(result, response);
			} 
        	catch (DAOException e) 
        	{
				e.printStackTrace();
			}
        }
        else if(action.equals(ActionMapper.SECURITY_MGMT_VALIDATE_MOBILE_OTP))
        {
        	ValidateMobileOTP validateOTP = new ValidateMobileOTP();
        	
        	try 
        	{
				String result = validateOTP.handle(inputObj, context);
				
				IOUtils.write(result, response);
			} 
        	catch (DAOException e) 
        	{
				e.printStackTrace();
			}
        }
        else
        {        	
        	// Single Handler 
    		String output=null;
    		String handlerName = ActionMapper.getInvokingLambdaFunction(action);

    		logger.log("Invoking:: "+handlerName);
    		
    		String[] classInvokeArr = handlerName.split("::");

    		String className = classInvokeArr[0];
    		String methodName = classInvokeArr[1];

    		logger.log("ClassName::: "+className);
    		logger.log("MethodName::: "+methodName);
    		
    		//String parameter
    		Class[] paramStream = {JsonObject.class,OutputStream.class,Context.class};

    		Class cls=null;
    		try {
    			cls = Class.forName(className);
    		} catch (ClassNotFoundException e2) {
    			throw new InternalErrorException(e2.getMessage());
    		}
    		Object obj=null;
    		try {
    			obj = cls.newInstance();
    			System.out.println("obj data "+cls.getName());
    		} catch (InstantiationException | IllegalAccessException e2) {
    			throw new InternalErrorException(e2.getMessage());
    		} 

    		//call the handler method
    		Method method=null;
    		try {
    			method = cls.getDeclaredMethod(methodName, paramStream);
    		} catch (NoSuchMethodException | SecurityException e1) {
    			throw new InternalErrorException(e1.getMessage());
    		}			
    		
    		/*if(!action.equalsIgnoreCase(ActionMapper.INITIAL_PROVISIONING_VALIDATE_DEVICE) &&
    				!action.equalsIgnoreCase(ActionMapper.INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER)) */
    		if(!ActionMapper.provisionActions().contains(action))
    		{
    			logger.log("Authentication: "+validateUserResponse.getAuthenticatedUser());
    			logger.log("Authorization: "+validateUserResponse.getAuthorizedUser());
    			
    			if(!(validateUserResponse.getAuthenticatedUser() && validateUserResponse.getAuthorizedUser())) 
    			{
    				throw new BadRequestException("Invalid User, Authentication/Authorization failed !");
    			}
    			else
    			{				
					try 
					{
						System.out.println("other invlokations");
						output = (String) method.invoke(obj, inputObj,response,context);
					} 
					catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
					{
						logger.log(e.getMessage());
						e.printStackTrace();
						throw new InternalErrorException(""+e.getCause());
					}
					catch (Exception e) 
					{
						logger.log(e.getMessage());
						e.printStackTrace();
						throw new InternalErrorException(""+e.getCause());
					}					
					System.out.println("Output Data in Security====>"+output);
    			}
    		}
    		else
    		{
    			logger.log("Device Provisioning action(s) invoked !");
    			
				try 
				{
					output = (String) method.invoke(obj, inputObj,response,context);
				} 
				catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					logger.log(e.getMessage());
					e.printStackTrace();
					throw new InternalErrorException(""+e.getCause());
				}
				catch (Exception e) {
					logger.log(e.getMessage());
					e.printStackTrace();
					throw new InternalErrorException(""+e.getCause());
				}
				
				System.out.println("Output Data in Security====>"+output);
    		}
        }
	}

	public static String byteBufferToString(ByteBuffer buffer, Charset charset) {
				
		byte[] bytes;
		if (buffer.hasArray()) {
			bytes = buffer.array();
		} else {
			bytes = new byte[buffer.remaining()];
			buffer.get(bytes);
		}
		return new String(bytes, charset);
	}
	
	 private static Gson getGson() {
	        return new GsonBuilder()
	                .setPrettyPrinting()
	                .create();
	    }
	 
	
}
