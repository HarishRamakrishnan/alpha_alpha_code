package com.wio.core.security.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.user.model.User;


public interface SecurityAction {
    /**
     * The main handler method for each requests. This method is called by the RequestRouter when invoked by a Lambda
     * function.
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return A string containing valid JSON to be returned to the client
     * @throws BadRequestException    This exception should be thrown whenever request parameters are not valid or improperly
     *                                formatted
     * @throws InternalErrorException This exception should be thrown if an error that is independent from user input happens
     */
	User handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException;
}
