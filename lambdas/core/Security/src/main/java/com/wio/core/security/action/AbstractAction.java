package com.wio.core.security.action;

import java.lang.reflect.Type;
import java.nio.ByteBuffer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;


/**
 * Abstract class implementing the UserAction interface. This class is used to declare utility method
 * shared with all Action implementations
 */
public abstract class AbstractAction implements SecurityAction {
    /**
     * Returns an initialized Gson object with the default configuration
     * @return An initialized Gson object
     */
    protected Gson getGson() {
        return new GsonBuilder()
                //.enableComplexMapKeySerialization()
                //.serializeNulls()
                //.setDateFormat(DateFormat.LONG)
                //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        		//.registerTypeAdapter(UserRoleType.class, new UserRoleTypeInstanceCreator())
        		.registerTypeAdapter(ByteBuffer.class, new ByteBufferInstanceCreator())
        		//.setPrettyPrinting()
                .create();
    }
    
  //  class UserRoleTypeInstanceCreator implements InstanceCreator<UserRoleType> {
    //	public UserRoleType createInstance(Type type)    {    
    	//	return UserRoleType.WavesUser; 
   // } }

    class ByteBufferInstanceCreator implements InstanceCreator<ByteBuffer> {
    	public ByteBuffer createInstance(Type type)    {    
    		return null; 
    } }

}
