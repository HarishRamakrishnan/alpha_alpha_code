package com.wio.core.config.action.hardware.profile;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeleteHardwareProfile extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        HardwareProfile input = getGson().fromJson(getBodyFromRequest(request), HardwareProfile.class);
        
        if(RequestValidator.isEmptyField(input.getHpID()))
        {
        	logger.log("HardwareProfile ID cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("HardwareProfile ID  "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        	       
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	HardwareProfile hp = genDAO.getGenericObject(HardwareProfile.class, input.getHpID());
        	
        	if(null == hp)
        		throw new BadRequestException("Hardware Profile does noy exist !");
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(hp.getServiceName()))
        			throw new BadRequestException("Not authorized to delete the HW Profile of the Service: '"+input.getServiceName()+"' !");
        	}
        	
        	if(DataValidator.isHPDependent(input.getHpID()))
        		throw new BadRequestException("Cannot delete Hardware Profile, as it has dependents !");
        	
        	genDAO.deleteGenericObject(input);
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while deleting HP\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        HardwareProfile output = new HardwareProfile();
        output.setHpID(input.getHpID());
       
        return getGson().toJson(output, HardwareProfile.class);    
    }
}
