package com.wio.core.config.action.network;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Network;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

public class ModifyNetwork extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Network input = getGson().fromJson(getBodyFromRequest(request), Network.class);
        
        if(RequestValidator.isEmptyField(input.getNetworkID()))
			throw new BadRequestException("NetworkID "+ExceptionMessages.EX_EMPTY_FIELD);
        
        Network nw = null;
        
        try 
        {
        	nw = genDAO.getGenericObject(Network.class, input.getNetworkID());
        	
        	if(null == nw)
        		throw new BadRequestException("Invalid network, does not exist !");
        	
        	User loggedUser = getUserInfoFromRequest(request);

        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(nw.getServiceName()))
        			throw new BadRequestException("Not authorized to modify the Network of other service!");
        	} 
        	
            updateModifiedNetwork(nw, input);
            
        	genDAO.updateGenericObject(nw);     
        	
        }        	
        catch ( DAOException e) 
        {
            logger.log("Error while modifying Network \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(nw, Network.class);
    }

    private void updateModifiedNetwork(Network nw, Network input)
    {    	
    	if(!RequestValidator.isEmptyField(input.getDescription()))
    		nw.setDescription(input.getDescription());
    	    	
    }
}
