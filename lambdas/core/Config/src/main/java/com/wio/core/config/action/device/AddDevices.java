package com.wio.core.config.action.device;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.MQTTClient;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.requests.DevicesRequest;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class AddDevices extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        DevicesRequest devices = getGson().fromJson(getBodyFromRequest(request), DevicesRequest.class);
        
        List<Device> newDevicesList = new ArrayList<Device>();
        List<MQTTClient> clientsList = new ArrayList<MQTTClient>();
                        
        try 
        {
        	if(devices.getDevicelist().isEmpty() || devices.getDevicelist() == null)
        	{
        		throw new BadRequestException("At least one Device should be added !");
        	}
        	
        	User loggedUser = getUserInfoFromRequest(request);     	

        	for(Device device : devices.getDevicelist())
        	{
            	DeviceValidator.validateOnAdd(device);
        		logger.log("Device "+device.getDeviceName()+" validated successfully !");
        		
            	if (DataValidator.isDeviceExist(device.getDeviceID()))
                    throw new BadRequestException("Device: "+device.getDeviceName()+" already exist");
            	
            	/*List<FilterCondition> hpFilter = new ArrayList<FilterCondition>();
            	hpFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, device.getHpID(), ComparisonOperator.EQ));
            	
            	
            	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
            	{
            		if(!device.getServiceName().equals(loggedUser.getServiceName()))
            			throw new BadRequestException("Not athorized to add device under the Service '"+device.getServiceName());
            		
            		hpFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(), ComparisonOperator.EQ));                	            		
            	}
            	
            	List<HardwareProfile> hps = genDAO.getGenericObjects(HardwareProfile.class, hpFilter);
            	
            	if(RequestValidator.isEmptyCollection(hps))
            		throw new BadRequestException("The Hardware Profile '"+device.getHpID()+"' does not mapped under your service ! ");
            	*/
            	//device.setSqsURL(SQSQueueActions.createQueue(device.getUuid()));
      	         
            	device.setHpID("NA");
            	device.setDgID("NA");
            	
            	if(RequestValidator.isEmptyField(device.getServiceName()))
            	{
            		device.setServiceName("NA");
                	device.setStatus("NotAssignedToService");
            	}
            	else
            		device.setStatus("NotAssignedToHP");
            	
            	device.setBabelServices("Disabled");
            	device.setNetworkAdmin("NA");
            	device.setDefaultTopic("NA");
            	device.setProvisionAuthorized(false);
            	device.setCreatedBy(loggedUser.getUserID());
            	device.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
            	device.setLastUpdateTime("NA");
                
            	newDevicesList.add(device);
            	
            	//Adding as MQTT Client
            	MQTTClient client = new MQTTClient();
            	
            	client.setClientID(device.getDeviceID());
            	client.setAuthorized(false);
            	client.setType("Device-WIO-AccessPoint");
            	client.setDescription("New WIO Devices added as MQTT Client... Broker Authentication required for Provision...");
            
            	clientsList.add(client);
        	}
        	
        	genDAO.saveGenericObjects(newDevicesList);
        	genDAO.saveGenericObjects(clientsList);        	        	
        } 
        catch (final Exception e) {
            logger.log("Error while adding Device\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        
        List<Device> output = new ArrayList<Device>();
        
        for(Device dev : devices.getDevicelist())
        {
        	Device d = new Device();     
        	d.setDeviceID(dev.getDeviceID());
        	d.setDeviceName(dev.getDeviceName());
        	d.setStatus("Device added successfully !");
        	output.add(d);
        }
        
        return getGson().toJson(output, List.class);
    }
}
