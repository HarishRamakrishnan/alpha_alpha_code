package com.wio.core.config.requests;

import java.util.List;

import com.wio.common.config.model.LANSettings;


public class LANSettingsRequest {
	private String securityToken;
	private List<LANSettings> lanSettings;
	private String deviceName;
    private String wifiName;
    
	 
	   
	
	
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	public List<LANSettings> getLanSettings() {
		return lanSettings;
	}
	public void setLanSettings(List<LANSettings> lanSettings) {
		this.lanSettings = lanSettings;
	}
	public String getWifiName() {
		return wifiName;
	}
	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	

}
