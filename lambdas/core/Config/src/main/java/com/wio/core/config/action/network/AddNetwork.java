package com.wio.core.config.action.network;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Network;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.util.UniqueIDGenerator;

/**
 * Action used to register a new Device Group.
 * <p/>
 * POST to /dg/
 */
public class AddNetwork extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add user action. It expects a UserRequest object in input and returns
     * a serialized UserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Network input = getGson().fromJson(getBodyFromRequest(request), Network.class);
        
        NetworkValidator.validateOnAdd(input);
        
        try 
        {
        	List<FilterCondition> queryFilter= new ArrayList<FilterCondition>();
        	queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_NAME, input.getNetworkName(), ComparisonOperator.EQ));
        	queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
        	queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_TYPE, input.getNetworkType(), ComparisonOperator.EQ));
 	       
        	if(!genDAO.getGenericObjects(Network.class, queryFilter).isEmpty())
        		throw new BadRequestException("Network "+input.getNetworkName()+" already exist !");            
        	

            input.setNetworkID("NW"+UniqueIDGenerator.getID());
            input.setCreatedBy(getUserInfoFromRequest(request).getUserID());
            input.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
            input.setLastUpdateTime("NA");
            
            genDAO.saveGenericObject(input);
        } 
        catch (final DAOException e) {
            logger.log("Error while adding Network \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        Network output = new Network();
        output.setNetworkID(input.getNetworkID());
        
        return getGson().toJson(output, Network.class);
    }
}
