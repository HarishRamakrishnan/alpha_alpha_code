package com.wio.core.config.action.device.group.topic;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.MQTTTopic;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.core.config.requests.DeviceTopicsRequest;
import com.wio.core.config.requests.DeviceTopicsResponse;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class FetchDeviceTopics extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        DeviceTopicsRequest input = getGson().fromJson(getBodyFromRequest(request), DeviceTopicsRequest.class);
        
        List<String> subTopics = null;
        List<MQTTTopic> pubTopics = null;
        
        try
        {
        	 Device device = genDAO.getGenericObject(Device.class, input.getDeviceID());
             
             if(null == device)
             	throw new BadRequestException("Invalid DeviceID, Device not exist !");
             
             if(device.getDgID().equals("NA"))
             	throw new BadRequestException("Device not assigned to any DeviceGroup !");
             
             subTopics = DeviceGroupTopic.getForceSubscriptionTopics(device.getDgID());
             pubTopics = DeviceGroupTopic.getSharedSubscriberTopics();
             
        } 
        catch (final Exception e) 
        {
            logger.log("Error while geting device topics...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        DeviceTopicsResponse output = new DeviceTopicsResponse();
        
        output.setDeviceID(input.getDeviceID());
        output.setSubscribeTopics(subTopics);
        output.setPublishTopics(pubTopics);
        
        return getGson().toJson(output, DeviceTopicsResponse.class);
    }
}
