package com.wio.core.config.action.device;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class DeleteDevices extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Device input = getGson().fromJson(getBodyFromRequest(request), Device.class);

        if(RequestValidator.isEmptyField(input.getDeviceID()))
        {
        	logger.log("Device ID cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Device ID "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
              
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	Device device = genDAO.getGenericObject(Device.class, input.getDeviceID());
        	
        	if(device == null)
        		throw new BadRequestException("Device "+input.getDeviceID()+" does not exist, Invalid Device ID !");
        	
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(device.getServiceName()))
        			throw new BadRequestException("Not authorized to delete the Device of the Service: '"+device.getServiceName()+"' !");
        	}
        	
        	
        	genDAO.deleteGenericObject(device);
        	
        	//String sqsQueueURL = devices.get(0).getSqsURL();
        	
        	//SQSQueueActions.deleteQueue(sqsQueueURL);        	
        } 
        catch (final Exception e) 
        {
            logger.log("Error while deleting Device...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        Device output = new Device();
        
        output.setDeviceID(input.getDeviceID());
        
        return getGson().toJson(output, Device.class);
    }
}
