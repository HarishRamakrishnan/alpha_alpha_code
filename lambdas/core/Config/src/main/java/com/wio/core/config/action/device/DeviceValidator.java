package com.wio.core.config.action.device;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeviceValidator 
{
	private static List<Device> devices = new ArrayList<>();
	
	public static List<Device> getDeviceList(){
		return devices;
	}
	
	public static void validateOnAdd(Device input) throws BadRequestException, DAOException
	{	
			//Empty field validation
		    if(RequestValidator.isEmptyField(input.getDeviceName()))
	        	throw new BadRequestException("Device Name "+ExceptionMessages.EX_EMPTY_FIELD);
	        
		    /*if(RequestValidator.isEmptyField(input.getServiceName()))
	        	throw new BadRequestException("Service "+ExceptionMessages.EX_EMPTY_FIELD);*/
	      
		    if(RequestValidator.isEmptyField(input.getModel()))
	        	throw new BadRequestException("Device Model "+ExceptionMessages.EX_EMPTY_FIELD);
	      
		    if(RequestValidator.isEmptyField(input.getDeviceID()))
	        	throw new BadRequestException("Device ui "+ExceptionMessages.EX_EMPTY_FIELD);
	        	        
		    if(RequestValidator.isEmptyField(input.getFirmware()))
	        	throw new BadRequestException("Device Firmware "+ExceptionMessages.EX_EMPTY_FIELD);
	       
	        if(RequestValidator.isEmptyField(input.getMac()))
	        	throw new BadRequestException("MAC Address "+ExceptionMessages.EX_EMPTY_FIELD);
	        
	    	
		    
		    //Field format validation
		    if(!RequestValidator.isValidAlphaNumaricFormat(input.getDeviceName()))
	        	throw new BadRequestException("Device Name "+ExceptionMessages.EX_INVALID_INPUT);
	        
		    if(!RequestValidator.isValidAlphaNumaricWithHypenFormat(input.getDeviceID()))  // Validate with "-"
	         	throw new BadRequestException("Device id "+ExceptionMessages.EX_INVALID_INPUT);
	        
	        if(!RequestValidator.isValidMACFormat(input.getMac()))
	        	throw new BadRequestException("MAC Address "+ExceptionMessages.EX_INVALID_INPUT);
	        
	        
	        //Dependency validation
	       /* if(!DataValidator.isHardwareProfileExist(input.getHpID()))
	        	throw new BadRequestException("Hardware Profile is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
	        */
	        
	        if(!RequestValidator.isEmptyField(input.getServiceName()))
		        if(!DataValidator.isServiceExist(input.getServiceName()))
		        	throw new BadRequestException("Service does not exist/invalid..."+ExceptionMessages.EX_INVALID_SERVICE);
	        
	       /* if(!RequestValidator.isEmptyField(input.getHncp()))
	        {
		        if ((!(input.getHncp().equals(Constants.ENABLE) || input.getHncp().equals(Constants.DISABLE))))
		        {
					throw new BadRequestException("HNCP field should be either Enable / Disable ! "+ExceptionMessages.EX_INVALID_INPUT);
				}
	        }
			
	        if(!RequestValidator.isEmptyField(input.getMesh()))
	        {
				if ((!(input.getMesh().equals(Constants.ENABLE) || input.getMesh().equals(Constants.DISABLE))))
				{
					throw new BadRequestException("Mesh field should be either Enable / Disable ! "+ExceptionMessages.EX_INVALID_INPUT);
				}
	        }*/
	        //Need to validate serial number
	        //GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
	        
	        /*List<FilterCondition> hpFilter = new ArrayList<FilterCondition>();
	        hpFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, input.getHpID(), ComparisonOperator.EQ));
	        hpFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
		       
	        //HardwareProfile hp= genDAO.getGenericObjects(HardwareProfile.class, hpFilter).get(0);
	        if(genDAO.getGenericObjects(HardwareProfile.class, hpFilter)==null)
	        {
	        	throw new BadRequestException("Hardware Profile does not belongs to the Service");
	        }*/
	        	
	        
	        /*if(!RequestValidator.isEmptyField(input.getNetworkAdmin()))
	        {
	        	
	        	List<FilterCondition> userFilter= new ArrayList<FilterCondition>();
	        	userFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_NAME, input.getNetworkAdmin(), ComparisonOperator.EQ));
	        	
		        User user= genDAO.getGenericObjects(User.class, userFilter).get(0);
		        if(user!=null)
		        {
		        	if(!user.getServiceName().equals(input.getServiceName()))
		        	{
			        	throw new BadRequestException("NetworkAdmin" + input.getNetworkAdmin() + "does not belongs to service" + input.getServiceName()+".");
	
		        	}
		        		
		        }
		        else
		        	throw new BadRequestException("NetworkAdmin" + input.getNetworkAdmin() + "does not exist.");
	        }*/	        
	    }
	
	

	public static void validateOnModify(Device input) throws BadRequestException, DAOException 
	{
		// Empty Field validation
		
		if (!RequestValidator.isEmptyField(input.getDeviceID())) {
			//if (!InputValidator.isValidAlphaNumaricFormat(input.getUuid()))
				throw new BadRequestException("Device ID or Device Mac can not modify");
		}

		if(!RequestValidator.isEmptyField(input.getServiceName()))
			if(!DataValidator.isServiceExist(input.getServiceName())) {
				throw new BadRequestException("Service does not exist/invalid..." + ExceptionMessages.EX_INVALID_SERVICE);
			}

		if (!RequestValidator.isEmptyField(input.getHpID())) 
		{
			
			if (!DataValidator.isHardwareProfileExist(input.getHpID()))
				throw new BadRequestException(
						"Hardware Profile ID is invalid or does not exist " + ExceptionMessages.EX_INVALID_INPUT);
			
			GenericDAO dao = GenericDAOFactory.getGenericDAO();
			
			List<FilterCondition> deviceFilter = new ArrayList<FilterCondition>();
			deviceFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_NAME, input.getDeviceName(), ComparisonOperator.EQ));
			deviceFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
			
			Device device = null;
			try	{
				devices = dao.getGenericObjects(Device.class, deviceFilter);
				device = devices.get(0);
				
				 if(device == null)
		          	throw new BadRequestException("Device '"+input.getDeviceName()+"' with service '"+input.getServiceName()+"' does not exist to modify.");

				List<FilterCondition> existFilter = new ArrayList<FilterCondition>();
				existFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_NAME, device.getHpID(), ComparisonOperator.EQ));
				
				List<FilterCondition> newFilter = new ArrayList<FilterCondition>();
				newFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_NAME, input.getHpID(), ComparisonOperator.EQ));
				
				HardwareProfile existHP = dao.getGenericObjects(HardwareProfile.class, existFilter).get(0);// Getting exist HP 

				HardwareProfile newHP = dao.getGenericObjects(HardwareProfile.class, newFilter).get(0);
				
			    if (!existHP.getHwID().equals(newHP.getHwID())) 
					throw new BadRequestException("Hardware Profile cannot be modified as it is not in the same hardware.");
							    
			} 
			catch (DAOException e) 
			{
				e.printStackTrace();
			}

		}
		 if(!RequestValidator.isEmptyField(input.getNetworkAdmin()))
		 {
			GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
	        List<FilterCondition> userFilter= new ArrayList<FilterCondition>();
	        userFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_NAME, input.getNetworkAdmin(), ComparisonOperator.EQ));
	        	
		    User user= genDAO.getGenericObjects(User.class, userFilter).get(0);
	        if(user!=null)
	        {
	        	if(!user.getServiceName().equals(input.getServiceName()))
		        	throw new BadRequestException("NetworkAdmin"+ input.getNetworkAdmin() +"does not belongs to service"+input.getServiceName()+".");
	        }
	        else
	        	throw new BadRequestException("NetworkAdmin"+ input.getNetworkAdmin() +"does not exist.");
	      }

      	}

	
}
