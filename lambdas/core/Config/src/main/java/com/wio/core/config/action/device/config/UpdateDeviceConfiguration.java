package com.wio.core.config.action.device.config;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.aws.api.lambda.LambdaClientAPI;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.model.Device;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.common.validation.RequestValidator;


/**
 * This class will get the modified device configuration in the request body.
 * 
 * Modified device configuration will be maintained as a unique transaction in
 * the UPDATED_CONFIG table. 
 * 
 * After successfully updating the DB, this will
 * invoke the configLambda with the deviceId to push the config changes on to
 * actual device.
 *
 */
public class UpdateDeviceConfiguration extends AbstractAction {

	private LambdaLogger logger = null;

	@Override
	public String handle(JsonObject request, Context lambdaContext)
			throws BadRequestException, InternalErrorException, DAOException, AuthorizationException {

		logger = lambdaContext.getLogger();
		DeviceConfiguration input = getGson().fromJson(getBodyFromRequest(request), DeviceConfiguration.class);
		ConfiguredValuesMO devConfigTransaction = null;
		String response = "";

		if (RequestValidator.isEmptyField(input.getDeviceId()))
			throw new BadRequestException("Device ID cannot be empty !");

		try {
			
			Device device = DDBGenericDAO.getInstance().getGenericObject(Device.class, input.getDeviceId());
			
			devConfigTransaction = DynamoDBHelper.getInstance().createConfigUpdTransaction(input, device, "Configured",
					"REST API");
			if (null == devConfigTransaction)
				throw new DAOException("Unable to create configuration in CONFIG_UPDATE table, returing null !");
			// call configLambda with deviceId.
			logger.log("Invoke configLambda with transactionId = " + devConfigTransaction.getTransactionId());
			
			String lambdaFunctionName = "ConfigLambda";
	        String source = "REST";
			String deviceJson = "{ \"params\": { \"path\": {}, \"header\": { \"calledBy\": \""
					+ source + "\" }, \"querystring\": { \"transactionId\": \"" + devConfigTransaction.getTransactionId() + "\" } } }";
	        response = LambdaClientAPI.invoke(lambdaFunctionName, deviceJson);

			if (RequestValidator.isValidString(response) && "SUCCESS".equals(response))
				logger.log("ConfigLambda invoked successully by REST API.");
			else
				logger.log("ConfigLambda invocation failed by REST API.");
			
		} catch (final Exception e) {
			logger.log("Error while fetching Device Configiration...\n" + e.getMessage());
			throw new InternalErrorException("Error while fetching Device Configiration..."+e.getMessage());
		}

		return response;

	}
	
}
