package com.wio.core.config.action.hardware;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.Constants;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWRadio;
import com.wio.common.config.model.Hardware;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.util.UniqueIDGenerator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class AddHardware extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Hardware input = getGson().fromJson(getBodyFromRequest(request), Hardware.class);

        HardwareValidator.validateOnAdd(input); 
        
        try 
        {
        	List<FilterCondition> hardwareFilter =new ArrayList<FilterCondition>();
        	
        	hardwareFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_NAME, input.getHwName(), ComparisonOperator.EQ));
	        
            if (!genDAO.getGenericObjects(Hardware.class, hardwareFilter).isEmpty())
                throw new BadRequestException("Hardware is already taken!");
            
            input.setHwID("HW"+UniqueIDGenerator.getID());
            
            for(HWRadio hwRadio: input.getHwRadio())
	        {
	        	if(hwRadio.getDbdcSupported().equals(Constants.SUPPORTED)){
	        		input.setDbdcSupported(Constants.SUPPORTED);
	        	}	        	
	        	HardwareValidator.validateOnAddHWRadio(hwRadio);	
	        }  
            
            input.setCreatedBy(getUserInfoFromRequest(request).getUserID());
            input.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
            input.setLastUpdateTime("NA");
            
            genDAO.saveGenericObject(input); 	        
        } 
       catch (final DAOException e) 
       {
            logger.log("Error while adding hardware..\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
       }
       
        Hardware hw = new Hardware();
        
        hw.setHwID(input.getHwID());
        
       return getGson().toJson(hw, Hardware.class);
    }
}
