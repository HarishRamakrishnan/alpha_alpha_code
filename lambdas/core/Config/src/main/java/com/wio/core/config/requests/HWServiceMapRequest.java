package com.wio.core.config.requests;

import java.util.List;

public class HWServiceMapRequest 
{
	private List<String> hwID;
	private String serviceName;
	private String description;
	
	public List<String> getHwID() {
		return hwID;
	}
	public void setHwID(List<String> hwID) {
		this.hwID = hwID;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

}
