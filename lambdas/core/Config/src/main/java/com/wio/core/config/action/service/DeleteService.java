package com.wio.core.config.action.service;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Service;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeleteService extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expects a RegisterServiceRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterServiceResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Service input = getGson().fromJson(getBodyFromRequest(request), Service.class);
        
        if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("Service name "+ExceptionMessages.EX_EMPTY_FIELD);
       
        Service service = null;
        try 
        {   
        	service = genDAO.getGenericObject(Service.class, input.getServiceName());
            
            if(service == null) 
                throw new BadRequestException("Service "+input.getServiceName()+" does not exist!");
            
            if(DataValidator.isServiceDependent(service.getServiceName()))
            	throw new BadRequestException("Cannot delete Service "+input.getServiceName()+", as it depedent with other objects!");
                    
            genDAO.deleteGenericObject(service);
        } 
        catch ( DAOException e) {
            logger.log("Error while deleting device group\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }       

        return getGson().toJson(service.getServiceName(), String.class);
    }
}
