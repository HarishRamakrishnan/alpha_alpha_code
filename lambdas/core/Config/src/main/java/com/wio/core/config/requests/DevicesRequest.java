package com.wio.core.config.requests;

import java.util.List;

import com.wio.common.config.model.Device;

public class DevicesRequest 
{
	private String securityToken;
	private List<Device> devicelist;
	
	
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	public List<Device> getDevicelist() {
		return devicelist;
	}
	public void setDevicelist(List<Device> devicelist) {
		this.devicelist = devicelist;
	}
	
	
}
