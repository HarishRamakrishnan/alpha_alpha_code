package com.wio.core.config.requests;
/*package com.wavesio.server.configuration.model.config;

import java.util.List;

import com.wavesio.server.configuration.model.DeviceLanMap;
import com.wavesio.server.configuration.model.DeviceWANRadioMap;
import com.wavesio.server.configuration.model.Devices;
import com.wavesio.server.configuration.model.HPRadioSettings;
import com.wavesio.server.configuration.model.LANSettings;
import com.wavesio.server.configuration.model.PortSettings;
import com.wavesio.server.configuration.model.WANSettings;

public class DeviceConfiguration 
{
	//Device Settings
	private Devices deviceSettings;
	
	//RadioSettings
	private List<HPRadioSettings> radioSettings;
	
	//LANPortSettings	
	private List<PortSettings> lanPortSettings;
	
	//LANSettings
	private List<LANSettings> lanSettings;
	
	//WANSettings
	private List<WANSettings> wanSettings;
	
	//Mapping: AP-WAN-Radio
	private List<DeviceWANRadioMap> deviceWanRadioMap;
	
	//Mapping: AP-LAN
	private List<DeviceLanMap> deviceLanMap;

	
	
	public Devices getDeviceSettings() {
		return deviceSettings;
	}

	public void setDeviceSettings(Devices deviceSettings) {
		this.deviceSettings = deviceSettings;
	}

	public List<HPRadioSettings> getRadioSettings() {
		return radioSettings;
	}

	public void setRadioSettings(List<HPRadioSettings> radioSettings) {
		this.radioSettings = radioSettings;
	}

	public List<PortSettings> getLanPortSettings() {
		return lanPortSettings;
	}

	public void setLanPortSettings(List<PortSettings> lanPortSettings) {
		this.lanPortSettings = lanPortSettings;
	}

	public List<LANSettings> getLanSettings() {
		return lanSettings;
	}

	public void setLanSettings(List<LANSettings> lanSettings) {
		this.lanSettings = lanSettings;
	}

	public List<WANSettings> getWanSettings() {
		return wanSettings;
	}

	public void setWanSettings(List<WANSettings> wanSettings) {
		this.wanSettings = wanSettings;
	}

	public List<DeviceWANRadioMap> getDeviceWanRadioMap() {
		return deviceWanRadioMap;
	}

	public void setDeviceWanRadioMap(List<DeviceWANRadioMap> deviceWanRadioMap) {
		this.deviceWanRadioMap = deviceWanRadioMap;
	}

	public List<DeviceLanMap> getDeviceLanMap() {
		return deviceLanMap;
	}

	public void setDeviceLanMap(List<DeviceLanMap> deviceLanMap) {
		this.deviceLanMap = deviceLanMap;
	}

		
}*/
