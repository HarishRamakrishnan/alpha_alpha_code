package com.wio.core.config.action.device.group;


import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.config.model.Network;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeviceGroupValidator
{	
	static GenericDAO dao = GenericDAOFactory.getGenericDAO();
	
	public static DeviceGroup validate(DeviceGroup input, User loggedUser)throws BadRequestException, InternalErrorException, DAOException
	{
		DeviceGroup parentDG = null;
				
	  	if(RequestValidator.isEmptyField(input.getDgName()))
        	throw new BadRequestException("Device Group Name "+ExceptionMessages.EX_EMPTY_FIELD);
	  	
	  	if(!RequestValidator.isEmptyField(input.getDgType()))
        {
        	if(!DGTypes.getTypes().contains(input.getDgType()))
        		throw new BadRequestException("DeviceGroup Type is invalid !");
        }
        else
        	throw new BadRequestException("Device Group Type "+ExceptionMessages.EX_EMPTY_FIELD);
	  	
	  
        if(DataValidator.isDeviceGroupExist(loggedUser.getServiceName(), input.getDgName()))
        	throw new BadRequestException("Device Group Already exist.. "+ExceptionMessages.EX_INVALID_DG);
	        
	       
        if(!RequestValidator.isEmptyField(input.getParentDG()))
    	{
        	parentDG = dao.getGenericObject(DeviceGroup.class, input.getParentDG());
        	
		    if(null == parentDG)
		    	throw new BadRequestException("Invalid parent device group, does not exist !");			
    	}   
	               
        //Field format validation
        /*if(!RequestValidator.isValidAlphaNumaricFormat(input.getDgName()))
        {
        	throw new BadRequestException("Device Group Name '"+input.getDgName()+"' format should be AlphaNumaric !"+ExceptionMessages.EX_INVALID_INPUT);
        }*/
        
        if(input.getDgName().length() <= 4 && input.getDgName().length() >=32)
        	throw new BadRequestException("Device Group Name '"+input.getDgName()+"' should contains only 4 to 32 characters !"+ExceptionMessages.EX_INVALID_INPUT);
        
       
        if(!RequestValidator.isEmptyField(input.getNetworkID()))
        {
	        Network nw= dao.getGenericObject(Network.class, input.getNetworkID());
	        if(nw==null)
	        {
	        	throw new BadRequestException("Ivalid Network "+input.getNetworkID()+" doesnot Exist");
	        }
			if(!nw.getServiceName().equals(loggedUser.getServiceName())){
				throw new BadRequestException("Network "+input.getNetworkID()+" is not associated with your Service "+input.getServiceName());	        		
			}
        }
        
        return parentDG;
	}
	
	public static void validateOnDelete(DeviceGroup input) throws DAOException, BadRequestException
	{
		if(RequestValidator.isEmptyField(input.getDgID()))
        {
        	//logger.log("DG group id cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("DG group id "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(!DataValidator.isDGExist(input.getDgID()))
        {
        	throw new BadRequestException("DeviceGroup "+input.getDgID()+" does not exist");
        }
        
        if(DataValidator.isDGDependent(input.getDgID()))
        {
        	throw new BadRequestException("DeviceGroup "+input.getDgID()+" is mapped with devices or wifi ! Please remove the dependency and try again!");
        }
        
            
	}
	
	public static void validateOnModify(DeviceGroup input) throws DAOException, BadRequestException
	{
		if(RequestValidator.isEmptyField(input.getDgName()))
        {
        	//logger.log("DG group name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("DG group name "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(RequestValidator.isEmptyField(input.getNetworkID()))
        {
        	//logger.log("Network Domain name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Network Domain name "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(!input.getParentDG().equals("NA") || !RequestValidator.isEmptyField(input.getParentDG()))
    	{	
        	List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
			
		    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
		    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_NAME, input.getParentDG(), ComparisonOperator.EQ));	    
			
		    if(dao.getGenericObjects(DeviceGroup.class, filterConditions).isEmpty())
		    	throw new BadRequestException("Invalid parent device group, does not exist !");
			
    	}
        
        if(!RequestValidator.isEmptyField(input.getNetworkID()))
        {
	        Network nw= dao.getGenericObject(Network.class, input.getNetworkID());
	        if(nw==null)
	        {
	        	throw new BadRequestException("Network "+input.getNetworkID()+" doesnot Exist");
	        }
			if(!nw.getServiceName().equals(input.getServiceName())){
				throw new BadRequestException("Network "+input.getNetworkID()+" is not associated with Service "+input.getServiceName());	        		
			}
        }
	}
}
