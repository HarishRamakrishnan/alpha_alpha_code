package com.wio.core.config.action.hardware;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWRadio;
import com.wio.common.config.model.Hardware;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.SystemConfigMessages;

public class ModifyHardware extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Hardware input = getGson().fromJson(getBodyFromRequest(request), Hardware.class);
               
        try 
        {
        	HardwareValidator.validateOnModify(input);
        	
        	Hardware hw = genDAO.getGenericObject(Hardware.class, input.getHwID());
        	
            if (hw == null)
                throw new BadRequestException("Hardware "+input.getHwID()+" does not exist");
            
            updateModifiedHWDetails(hw, input);            
       	
            input.setLastUpdateTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
            
            genDAO.updateGenericObject(hw);            
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while modifying Hardware.. \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(input.getHwID(), String.class);    
    }
    
    private void updateModifiedHWDetails(Hardware hw, Hardware input) throws BadRequestException, DAOException
    {
    
    	if(input.getManufacture() != null && input.getManufacture().trim().length() > 0 )
         	hw.setManufacture(input.getManufacture());
    	
    	if(input.getChipsetType() != null && input.getChipsetType().trim().length() > 0 )
            hw.setChipsetType(input.getChipsetType());
    	
    	if(input.getChipsetVendor() != null && input.getChipsetVendor().trim().length() > 0 )
        	hw.setChipsetVendor(input.getChipsetVendor());
  
    	if(input.getCpuCore() != null && input.getCpuCore().trim().length() > 0 )
    	    hw.setCpuCore(input.getCpuCore());
    	
    	if(input.getCpuType() != null && input.getCpuType().trim().length() > 0 )
    	    hw.setCpuType(input.getCpuType());

    	if(input.getCpuFrequency() != null && input.getCpuFrequency().trim().length() > 0 )
           hw.setCpuFrequency(input.getCpuFrequency());
    
    	if(input.getDramController() != null && input.getDramController().trim().length() > 0 )
           hw.setDramController(input.getDramController());
    	
    	if(input.getFreeMemory() != null && input.getFreeMemory().trim().length() > 0 )
            hw.setFreeMemory(input.getFreeMemory());

    	if(input.getTotalMemory() != null && input.getTotalMemory().trim().length() > 0 )
            hw.setTotalMemory(input.getTotalMemory());

    	if(input.getNatOffloading() != null && input.getNatOffloading().trim().length() > 0 )
            hw.setNatOffloading(input.getNatOffloading());

    	if(input.getConnector() != null && input.getConnector().trim().length() > 0 )
            hw.setConnector(input.getConnector());

    	if(input.getLanInterfaceSupported() != null && input.getLanInterfaceSupported().trim().length() > 0 )
            hw.setLanInterfaceSupported(input.getLanInterfaceSupported());

    	if(input.getLanPortSupported() != null  )
            hw.setLanPortSupported(input.getLanPortSupported());

    	if(input.getUsbPortSupported() != null && input.getUsbPortSupported().toString().length() > 0 )
            hw.setUsbPortSupported(input.getUsbPortSupported());

    	if(input.getUsbSpeedSupported() != null && input.getUsbSpeedSupported().trim().length() > 0 )
            hw.setUsbSpeedSupported(input.getUsbSpeedSupported());

    	if(input.getUsbTypeSupported() != null && input.getUsbTypeSupported().trim().length() > 0 )
            hw.setUsbTypeSupported(input.getUsbTypeSupported());

    	if(input.getPowerAdapter() != null && input.getPowerAdapter().trim().length() > 0 )
            hw.setPowerAdapter(input.getPowerAdapter());

    	if(input.getVisualIndicatorSupported() != null && input.getVisualIndicatorSupported().trim().length() > 0 )
            hw.setVisualIndicatorSupported(input.getVisualIndicatorSupported());
	
    	if(input.getHwRadio()!= null && input.getHwRadio().size() > 0)
          	hw.setHwRadio(updateModifiedHWRadio(hw,input));    	
    }
    


    
   private List<HWRadio> updateModifiedHWRadio(Hardware hw, Hardware input) throws BadRequestException, DAOException
   {
	   	List<HWRadio> radioSettings = new ArrayList<HWRadio>();
	   	int count=0;
	   	for(HWRadio inputRadio : input.getHwRadio())
	       {
	    	for(HWRadio radio: hw.getHwRadio())
	   		{
	   			System.out.println("input radio name"+inputRadio.getRadioName());
	   			System.out.println(" radio name"+radio.getRadioName());
	
	   			if(inputRadio.getRadioName().equals(radio.getRadioName())){
	   				
	   				count++;
	   				
	   			HardwareValidator.validateOnModifyHWRadio(inputRadio);
	   			
	   			
	   			if(inputRadio.getRadioBandSupported() != null && inputRadio.getRadioBandSupported().trim().length() > 0 )
	   	   	         	radio.setRadioBandSupported(inputRadio.getRadioBandSupported());
	   	    	
	   	    	if(inputRadio.getRadioModeSupported() != null && inputRadio.getRadioModeSupported().trim().length() > 0 )
	   		         	radio.setRadioModeSupported(inputRadio.getRadioModeSupported());
	   	    	
	   	    	if(inputRadio.getBandwidthSupported() != null && inputRadio.getBandwidthSupported().trim().length() > 0 )
	   		         	radio.setBandwidthSupported(inputRadio.getBandwidthSupported());
	   	    	
	   	    	if(inputRadio.getMimoMode() != null && inputRadio.getMimoMode().trim().length() > 0 )
	   		         	radio.setMimoMode(inputRadio.getMimoMode());
	   	    	
	   	    	if(inputRadio.getDfsMode() != null && inputRadio.getDfsMode().trim().length() > 0 )
	   		         	radio.setDfsMode(inputRadio.getDfsMode());
	   	    	
	   	    	if(inputRadio.getDbdcSupported() != null && inputRadio.getDbdcSupported().trim().length() > 0 )
	   		         	radio.setDbdcSupported(inputRadio.getDbdcSupported());
	   	    	
	   	    	if(inputRadio.getDbdcModeSupported() != null && inputRadio.getDbdcModeSupported().trim().length() > 0 )
	   		         	radio.setDbdcModeSupported(inputRadio.getDbdcModeSupported());
	   	    	
	   	    	if(inputRadio.getSpatialStream() != null && inputRadio.getSpatialStream().trim().length() > 0 )
	   	         	radio.setSpatialStream(inputRadio.getSpatialStream());
	   			}
	   	
	   		}
	   		radioSettings.add(inputRadio);
	   	 	System.out.println("count: "+count);
	   		if(count==0){
	   	   		throw new BadRequestException("radio name cannot be modified");
	   		}
	   		count=0;
	       }
	  
	   	
	   	return radioSettings;
   }
   
    
}
