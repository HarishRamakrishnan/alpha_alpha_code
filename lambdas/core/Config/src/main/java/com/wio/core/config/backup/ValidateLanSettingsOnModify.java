package com.wio.core.config.backup;
/*package com.wavesio.server.configuration.backup;

import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.model.configuration.LANSettings;
import com.wavesio.server.common.model.configuration.MacFilterList;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.DataValidator;
import com.wavesio.server.common.validation.RequestValidator;

public class ValidateLanSettingsOnModify 
{
	public static void validate(LANSettings input) throws BadRequestException, DAOException
	{
		//Mandatory filed validations
		
        if(RequestValidator.isEmptyField(input.getLanName()))
        	throw new BadRequestException("LAN name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getDeviceName()))
        	throw new BadRequestException("Device name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        
        //Field format validations
        
        if(input.getVlanId() != null)
    	{
    		if(!RequestValidator.isValidNumber(input.getVlanId()) && input.getVlanId().toString().length() > 4)
    		{
    			//logger.log("Invalid VLAN ID format, Exception at "+this.getClass().getName());
            	throw new BadRequestException("Invalid VLAN ID format "+ExceptionMessages.EX_INVALID_INPUT);
    		}
    		if(input.getVlanId().intValue() > 4094)
    		{
    			//logger.log("VLAN ID shoud be 0 to 4094, Exception at "+this.getClass().getName());
            	throw new BadRequestException("VLAN ID shoud be 0 to 4094 "+ExceptionMessages.EX_INVALID_INPUT);
    		}
    	}
    	else
    		input.setVlanId(0);
        
        if(!RequestValidator.isValidAlphaNumaricFormat(input.getLanName()))
        	throw new BadRequestException("Invalid Lan Name, Only alphanumaric characters allowed! "+ExceptionMessages.EX_INVALID_INPUT);
        
        if(!RequestValidator.isEmptyField(input.getIpAdrress()))
        {
            if(!RequestValidator.validateIPAddress(input.getIpAdrress()))
            	throw new BadRequestException("Invalid IP Address "+ExceptionMessages.EX_INVALID_IP);
        }
        
        if(!RequestValidator.isEmptyField(input.getSubnetMask()))
		{
        	if(!RequestValidator.validateIPAddress(input.getSubnetMask()))
            	throw new BadRequestException("Invalid SubNetMask IP! "+ExceptionMessages.EX_INVALID_INPUT);
		}
        
        if(!RequestValidator.isEmptyField(input.getDefaultGateway()))
		{
            if(!RequestValidator.validateIPAddress(input.getDefaultGateway()))
            	throw new BadRequestException("Invalid Gateway IP! "+ExceptionMessages.EX_INVALID_INPUT);        	
		}
        
        if(!RequestValidator.isEmptyField(input.getDnsPrimaryServer()))
		{
            if(!RequestValidator.validateIPAddress(input.getDnsPrimaryServer()))
            	throw new BadRequestException("Invalid DNS Primary Server IP! "+ExceptionMessages.EX_INVALID_INPUT);
		}
        	  
        if(input.getMacFilterList()!=null){
            if(!input.getMacFilterList().isEmpty())
            {
   	        for(MacFilterList mac : input.getMacFilterList())
   	        {
   	        	if(!RequestValidator.isValidMACFormat(mac.getMacAddress()))
   	        	{
   	        		//logger.log("Invalid MAC Address, Exception at "+this.getClass().getName());
   	            	throw new BadRequestException("Invalid MAC Address... "+ExceptionMessages.EX_INVALID_INPUT);
   	        	}
   	        }
            }
           }
                
        //Dependency validations
        if(!DataValidator.isDeviceExist(input.getDeviceName()))
        	throw new BadRequestException("Device is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        
        if(input.getWifiName()!=null && !DataValidator.isWirelessNetworkExist(input.getWifiName()))
        	throw new BadRequestException("WiFi Name is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        
                
	}

}
*/