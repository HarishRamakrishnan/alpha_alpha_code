package com.wio.core.config.action.device.group.topic;

import java.util.Arrays;
import java.util.List;

public class LambdaTest 
{
	private static final String SEPERATOR = "/";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String topic = "airtel/usa/ny/nyc/manhattan/bc123/bld-a/apt-1/61f591f2-5527-4bda-a10c-58a57e7e7edd";
		
		
		List<String> groupLevels = getSlashSeperatedValues(topic);
		
		String top = groupLevels.get(0);
		
		for(int i=1; i<groupLevels.size(); i++)
		{
			top += SEPERATOR+groupLevels.get(i);
			
			System.out.println(top);
		}
		
		/*groupLevels.forEach(item->{
			
				System.out.println(item);
			
		});*/
	}
	
	public static List<String> getSlashSeperatedValues(String string)
	{
		String [] separatedValues = string.split("/");				
		return Arrays.asList(separatedValues);
	}

}
