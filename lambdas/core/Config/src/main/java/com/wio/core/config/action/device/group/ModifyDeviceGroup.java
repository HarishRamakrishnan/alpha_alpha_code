package com.wio.core.config.action.device.group;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.user.model.User;

public class ModifyDeviceGroup extends AbstractAction {

    private LambdaLogger logger = null;
    private GenericDAO dao  = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        dao = GenericDAOFactory.getGenericDAO();
        
        DeviceGroup input = getGson().fromJson(getBodyFromRequest(request), DeviceGroup.class);
                        
        try 
        {        	
        	User loggedUser = getUserInfoFromRequest(request);

        	DeviceGroup dg = dao.getGenericObject(DeviceGroup.class, input.getDgID());

            if(dg == null)
                throw new BadRequestException("Device Group"+input.getDgID()+" does not exist");  
            
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(dg.getServiceName()))
        			throw new BadRequestException("Not authorized to modify the Device Group of other service!");
        	}   
        	
            
            updateModifiedDGDetails(dg, input);
        	dao.updateGenericObject(dg);
        	
        	
        }

        catch ( DAOException e) 
        {
            logger.log("Error while modifying Device Group\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 

        return getGson().toJson(input, DeviceGroup.class);

    }

    private void updateModifiedDGDetails(DeviceGroup dg, DeviceGroup input)
    {
    	  //dg.setDgName(input.getDgName());
    	  dg.setDgType(input.getDgType());
          dg.setNetworkID(input.getNetworkID());
          //dg.setParentDG(input.getParentDG());
    }
}
