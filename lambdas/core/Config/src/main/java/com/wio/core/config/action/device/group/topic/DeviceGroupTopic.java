package com.wio.core.config.action.device.group.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wio.common.config.model.DeviceGroup;
import com.wio.common.config.model.MQTTTopic;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;

public class DeviceGroupTopic 
{
	private static final String SEPERATOR = "/";
	
	public static String prepareTopic(DeviceGroup newDG, DeviceGroup parentDG)
	{
		try
		{
			if(newDG.getParentDG().equals("NA"))
			{
				return newDG.getServiceName()+SEPERATOR+newDG.getDgName();
			}
			else
			{
				return parentDG.getMqttTopic()+SEPERATOR+newDG.getDgName();				
			}	         
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static List<String> getForceSubscriptionTopics(String dgID) throws DAOException {
		
		GenericDAO dao = GenericDAOFactory.getGenericDAO();
		DeviceGroup dg = dao.getGenericObject(DeviceGroup.class, dgID);
		System.out.println("Going to fetch force subscription Topics..");
		if (null == dg)
			throw new DAOException("Device Group: " + dgID + " does not exist !");
		
		List<String> topics = new ArrayList<String>();

		List<String> groupLevels = getSlashSeperatedValues(dg.getMqttTopic());
		String topic = groupLevels.get(0);

		for (int i = 1; i < groupLevels.size(); i++) {
			topic += SEPERATOR + groupLevels.get(i);
			topics.add(topic);
		}

		return topics;
	}
	
	public static List<MQTTTopic> getSharedSubscriberTopics() throws DAOException {
		List<MQTTTopic> subTopics = null;
		
		GenericDAO dao = GenericDAOFactory.getGenericDAO();		
		subTopics = dao.getGenericObjects(MQTTTopic.class, null);
		
		if(null == subTopics)
			throw new DAOException("No topic subscriptions found !");
		
		return subTopics;
	}
	
	public static List<String> getSlashSeperatedValues(String string) {
		String [] separatedValues = string.split(SEPERATOR);				
		return Arrays.asList(separatedValues);
	}
}
