package com.wio.core.config.requests;

import java.util.List;

public class DeviceHPMapRequest 
{
	private List<String> deviceID;
	private String hpID;
	
	
	
	public List<String> getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(List<String> deviceID) {
		this.deviceID = deviceID;
	}
	public String getHpID() {
		return hpID;
	}
	public void setHpID(String hpID) {
		this.hpID = hpID;
	}
	
	

}
