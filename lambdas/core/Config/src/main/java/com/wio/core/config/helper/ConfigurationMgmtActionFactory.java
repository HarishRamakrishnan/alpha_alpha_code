package com.wio.core.config.helper;

import com.wio.common.action.IRequestAction;
import com.wio.common.helper.ActionMapper;
import com.wio.core.config.action.device.AddDevices;
import com.wio.core.config.action.device.DeleteDevices;
import com.wio.core.config.action.device.ModifyDevices;
import com.wio.core.config.action.device.ViewDevices;
import com.wio.core.config.action.device.config.FetchDeviceConfiguration;
import com.wio.core.config.action.device.config.UpdateDeviceConfiguration;
import com.wio.core.config.action.device.group.AddDeviceGroup;
import com.wio.core.config.action.device.group.DeleteDeviceGroup;
import com.wio.core.config.action.device.group.ModifyDeviceGroup;
import com.wio.core.config.action.device.group.ViewDeviceGroup;
import com.wio.core.config.action.device.group.map.AssignDGToWiFi;
import com.wio.core.config.action.device.group.map.AssignDeviceToGroup;
import com.wio.core.config.action.device.group.map.UnAssignDGFromWiFi;
import com.wio.core.config.action.device.group.map.UnaAsignDeviceFromGroup;
import com.wio.core.config.action.device.group.map.ViewDGWiFiMap;
import com.wio.core.config.action.device.group.topic.FetchDeviceTopics;
import com.wio.core.config.action.device.map.AssignDevicesToHP;
import com.wio.core.config.action.device.map.AssignDevicesToService;
import com.wio.core.config.action.device.map.UnAssignDevicesToService;
import com.wio.core.config.action.hardware.AddHardware;
import com.wio.core.config.action.hardware.DeleteHardware;
import com.wio.core.config.action.hardware.ModifyHardware;
import com.wio.core.config.action.hardware.ViewHardware;
import com.wio.core.config.action.hardware.map.AssignHWToService;
import com.wio.core.config.action.hardware.map.UnAssignHWToService;
import com.wio.core.config.action.hardware.map.ViewHWServiceMap;
import com.wio.core.config.action.hardware.profile.AddHardwareProfile;
import com.wio.core.config.action.hardware.profile.DeleteHardwareProfile;
import com.wio.core.config.action.hardware.profile.ModifyHardwareProfile;
import com.wio.core.config.action.hardware.profile.ViewHardwareProfile;
import com.wio.core.config.action.network.AddNetwork;
import com.wio.core.config.action.network.DeleteNetwork;
import com.wio.core.config.action.network.ModifyNetwork;
import com.wio.core.config.action.network.ViewNetwork;
import com.wio.core.config.action.service.AddService;
import com.wio.core.config.action.service.DeleteService;
import com.wio.core.config.action.service.ModifyService;
import com.wio.core.config.action.service.ViewService;
import com.wio.core.config.action.wifi.AddWiFi;
import com.wio.core.config.action.wifi.DeleteWiFi;
import com.wio.core.config.action.wifi.ModifyWiFi;
import com.wio.core.config.action.wifi.ViewWiFi;


public class ConfigurationMgmtActionFactory {

	
	/**
     * Default constructor
     */
    private ConfigurationMgmtActionFactory() {}
    
    public static IRequestAction getUserActionInstance(String action){
    	
    	switch(action)
    	{
         	case ActionMapper.CONFIGURATION_MGMT_ADD_SERVICE:
		     	return new AddService();
		    case ActionMapper.CONFIGURATION_MGMT_MODIFY_SERVICE:
			    return new ModifyService();
		    case ActionMapper.CONFIGURATION_MGMT_DELETE_SERVICE:
			    return new DeleteService();
		    case ActionMapper.CONFIGURATION_MGMT_VIEW_SERVICE:
			    return new ViewService();    
			    
		    case ActionMapper.CONFIGURATION_MGMT_ADD_WIFI:
		     	return new AddWiFi();
		    case ActionMapper.CONFIGURATION_MGMT_MODIFY_WIFI:
			    return new ModifyWiFi();
		    case ActionMapper.CONFIGURATION_MGMT_DELETE_WIFI:
			    return new DeleteWiFi();
		    case ActionMapper.CONFIGURATION_MGMT_VIEW_WIFI:
			    return new ViewWiFi();
			   
		    case ActionMapper.CONFIGURATION_MGMT_ADD_NETWORK:
		     	return new AddNetwork();
		    case ActionMapper.CONFIGURATION_MGMT_MODIFY_NETWORK:
			    return new ModifyNetwork();
		    case ActionMapper.CONFIGURATION_MGMT_DELETE_NETWORK:
			    return new DeleteNetwork();
		    case ActionMapper.CONFIGURATION_MGMT_VIEW_NETWORK:
			    return new ViewNetwork();
			    
		    case ActionMapper.CONFIGURATION_MGMT_ADD_DG:
		     	return new AddDeviceGroup();
		    case ActionMapper.CONFIGURATION_MGMT_MODIFY_DG:
			    return new ModifyDeviceGroup();
		    case ActionMapper.CONFIGURATION_MGMT_DELETE_DG:
			    return new DeleteDeviceGroup();
		    case ActionMapper.CONFIGURATION_MGMT_VIEW_DG:
			    return new ViewDeviceGroup();
			    
    	   			
    		case ActionMapper.CONFIGURATION_MGMT_ADD_HARWARE:
    			return new AddHardware();
    		case ActionMapper.CONFIGURATION_MGMT_MODIFY_HARWARE:
    			return new ModifyHardware();
    		case ActionMapper.CONFIGURATION_MGMT_DELETE_HARWARE:
    			return new DeleteHardware();
    		case ActionMapper.CONFIGURATION_MGMT_VIEW_HARWARE:
    			return new ViewHardware();
    			
    		case ActionMapper.CONFIGURATION_MGMT_ADD_HARWARE_PROFILE:
    			return new AddHardwareProfile();
    		case ActionMapper.CONFIGURATION_MGMT_MODIFY_HARWARE_PROFILE:
    			return new ModifyHardwareProfile();
    		case ActionMapper.CONFIGURATION_MGMT_DELETE_HARWARE_PROFILE:
    			return new DeleteHardwareProfile();
    		case ActionMapper.CONFIGURATION_MGMT_VIEW_HARWARE_PROFILE:
    			return new ViewHardwareProfile();
    			
    		case ActionMapper.CONFIGURATION_MGMT_ADD_DEVICE:
    			return new AddDevices();
    		case ActionMapper.CONFIGURATION_MGMT_MODIFY_DEVICE:
    			return new ModifyDevices();
    		case ActionMapper.CONFIGURATION_MGMT_DELETE_DEVICE:
    			return new DeleteDevices();
    		case ActionMapper.CONFIGURATION_MGMT_VIEW_DEVICE:
    			return new ViewDevices();
    					
    					
    		case ActionMapper.CONFIGURATION_MGMT_DEVICE_DG_MAP:
    			return new AssignDeviceToGroup();
    		case ActionMapper.CONFIGURATION_MGMT_DEVICE_DG_UNMAP:
    			return new UnaAsignDeviceFromGroup();
    		
    		case ActionMapper.CONFIGURATION_MGMT_HARDWARE_SERVICE_MAP:
    			return new AssignHWToService();
    		case ActionMapper.CONFIGURATION_MGMT_HARDWARE_SERVICE_UNMAP:
    			return new UnAssignHWToService();
    		case ActionMapper.CONFIGURATION_MGMT_VIEW_HARDWARE_SERVICE_MAP:
    			return new ViewHWServiceMap();
    			
    		case ActionMapper.CONFIG_DEVICE_SERVICE_MAP:
    			return new AssignDevicesToService();
    			
    		case ActionMapper.CONFIG_DEVICE_SERVICE_UNMAP:
    			return new UnAssignDevicesToService();
    			
    		case ActionMapper.CONFIG_DEVICE_HP_MAP:
    			return new AssignDevicesToHP();
    			
    		case ActionMapper.CONFIGURATION_MGMT_DG_WIFI_MAP:
    			return new AssignDGToWiFi();
    		case ActionMapper.CONFIGURATION_MGMT_DG_WIFI_UNMAP:
    			return new UnAssignDGFromWiFi();
    		case ActionMapper.CONFIGURATION_MGMT_VIEW_DG_WIFI_MAP:
    			return new ViewDGWiFiMap();
    			
    		
    		case ActionMapper.CONFIGURATION_MGMT_GET_DEVICE_CONFIGURATION:
    			return new FetchDeviceConfiguration();
    			
    		case ActionMapper.CONFIGURATION_MGMT_GET_DEVICE_TOPICS:
    			return new FetchDeviceTopics();
    		case ActionMapper.CONFIGURATION_MGMT_ADD_CONFIG:
    		case ActionMapper.CONFIGURATION_MGMT_MODIFY_CONFIG:
    		case ActionMapper.CONFIGURATION_MGMT_DELETE_CONFIG:
    			return new UpdateDeviceConfiguration();
    			
    		/*case Formatter.CONFIGURATION_MGMT_DEVICE_CONFIGURATION:
    			return new PushDeviceConfigurationAction();    			
    		case Formatter.CONFIGURATION_MGMT_WIRELESS_CONFIGURATION:
    			return new PushWirelessConfigurationAction();
    		*/	
    		default:
    			return null;	
    			
    	}
    	
    }


    
}
