package com.wio.core.config.action.wifi;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to get all Device Group.
 * <p/>
 * POST to /dg/all
 */
public class ViewWiFi extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();

        WiFi input = getGson().fromJson(getBodyFromRequest(request), WiFi.class);        
        
        List<WiFi> wifis = null;
        
        List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();         
	
        if(!RequestValidator.isEmptyField(input.getWifiID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_ID, input.getWifiID(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getNetworkUser()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_USER, input.getNetworkUser(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getWifiName()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_NAME, input.getWifiName(),ComparisonOperator.EQ)); 
        
        if(!RequestValidator.isEmptyField(input.getNetworkID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_ID, input.getNetworkID(),ComparisonOperator.EQ));
        
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            	{
            		if(loggedUser.getServiceName().equals(input.getServiceName()))
                		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
            		else
            			throw new BadRequestException("Not Authorized to view the Wifi of service '"+input.getServiceName()+"' !");
            	}
            	else
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
            }
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
        	{
        		 if(!RequestValidator.isEmptyField(input.getServiceName()))
            	 {
            		 if(loggedUser.getServiceName().equals(input.getServiceName()))
                 		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
             		else
             			throw new BadRequestException("Not Authorized to view the Wifi of service '"+input.getServiceName()+"' !");
             	
            	 }
            	 if(!RequestValidator.isEmptyField(input.getNetworkUser()))
            	 {
            		 if(loggedUser.getUserID().equals(input.getNetworkUser()))
            			 queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_USER, loggedUser.getUserID(),ComparisonOperator.EQ));
            		 else
            			 throw new BadRequestException("Not Authorized to view the Wifi of NetworkAdmin '"+input.getNetworkUser()+"' !");
            	
            	 }
            	 else
            		 queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_USER, loggedUser.getUserID(),ComparisonOperator.EQ));
        	}
            else
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
            }
            
        	wifis = genDAO.getGenericObjects(WiFi.class, queryFilters);
        	
	        if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
	        	if(wifis.isEmpty())
	        		throw new InternalErrorException("Wifi does not exist or Not mapped to Service "+loggedUser.getServiceName());
        	
        	if(wifis.isEmpty())
        		throw new DAOException("Wifi does not exist.");
        } 
        catch (final Exception e) {
            logger.log("Error while viewing Wifi...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(wifis, List.class);
     }
}

