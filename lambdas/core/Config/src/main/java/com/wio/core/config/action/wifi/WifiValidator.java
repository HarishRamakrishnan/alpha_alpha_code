package com.wio.core.config.action.wifi;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class WifiValidator 
{
	public static void validateOnAdd(WiFi input) throws BadRequestException, DAOException
	{
		if(RequestValidator.isEmptyField(input.getWifiName()))
			throw new BadRequestException("WIFI Name field"+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getServiceName()))
			throw new BadRequestException("Service Name field"+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getSsid()))
        	throw new BadRequestException("SSID field"+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getHiddenSSID()))
			throw new BadRequestException("Hidden SSID field"+ExceptionMessages.EX_EMPTY_FIELD);
		else if(!(input.getHiddenSSID().equals("YES") || input.getHiddenSSID().equals("NO")))
			throw new BadRequestException("Invalid value, Hidden SSID should be eaither YES or NO !");
		
		if(RequestValidator.isEmptyField(input.getEnableWireless()))
			throw new BadRequestException("Enable Wireless Filed"+ExceptionMessages.EX_EMPTY_FIELD);
		else if(!(input.getEnableWireless().equals("YES") || input.getEnableWireless().equals("NO")))
			throw new BadRequestException("Invalid value, EnableWireless should be eaither YES or NO !");
		
		if(RequestValidator.isEmptyField(input.getCountry()))
			throw new BadRequestException("Country field "+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getAuthenticationType()))
			throw new BadRequestException("Authentication type "+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getNetworkID()))
			throw new BadRequestException("Network ID "+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(input.getNetworkUser()))
			throw new BadRequestException("Network Admin "+ExceptionMessages.EX_EMPTY_FIELD);
		
		
		if(input.getAuthenticationType().equals("WPA2-PSK"))
		{
			if(RequestValidator.isEmptyField(input.getEncryptionType()))
				throw new BadRequestException("Encryption type "+ExceptionMessages.EX_EMPTY_FIELD);
			
			if(RequestValidator.isEmptyField(input.getPassPhrase()))
				throw new BadRequestException("PassPhrase field "+ExceptionMessages.EX_EMPTY_FIELD);
		}
		else if(input.getAuthenticationType().equals("OPEN"))
		{
			if(!RequestValidator.isEmptyField(input.getEncryptionType()) || !RequestValidator.isEmptyField(input.getPassPhrase()) )
				throw new BadRequestException("For OPEN authentication Type PassPhrase and Encryption Type is not needed");
		}
		
        
        //Field format validation
        if(!RequestValidator.isValidAlphaNumaricFormat(input.getWifiName()))
        	throw new BadRequestException("WIFI Name '"+input.getWifiName()+"' format should be AlphaNumaric !"+ExceptionMessages.EX_INVALID_INPUT);
        		        
        if(!RequestValidator.isValidSSID(input.getSsid()))
        	throw new BadRequestException("Not a valid SSID, Only alphanumaric, special characters(_ - . and space) with max lenght 32 char allowed "+ExceptionMessages.EX_INVALID_SSID);
       
        if(!(input.getAuthenticationType().equals("OPEN") || input.getAuthenticationType().equals("WPA2-PSK")))
        	throw new BadRequestException("Invalid Authentication Type... Only OPEN/WPA2-PSK types allowed !"+ExceptionMessages.EX_INVALID_INPUT);
        
		if(input.getAuthenticationType().equals("WPA2-PSK"))
		{
			if(!(input.getEncryptionType().equals("TKIP") || input.getEncryptionType().equals("AES")))
				throw new BadRequestException("Invalid Encryption type.. Only TKIP / AES types allowed ! "+ExceptionMessages.EX_INVALID_INPUT);
			
			if(!RequestValidator.isValidPresharedKey(input.getPassPhrase()))
	        	//logger.log("Preshared Key may conatain A-Z a-z 0-9 and - _ space only with 8-63 length, Exception at "+this.getClass().getName());
	        	throw new BadRequestException("Preshared Key may conatain A-Z a-z 0-9 and - _ space only with 8-63 length, "+ExceptionMessages.EX_INVALID_PRESHAREDKEY);
	    }
		
		//Dependency validation
		GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
		
		if(!DataValidator.isServiceExist(input.getServiceName()))
			throw new BadRequestException("Service does not exist... "+ExceptionMessages.EX_INVALID_SERVICE);
		
		List<FilterCondition> filter= new ArrayList<FilterCondition>();
		filter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ID, input.getNetworkUser(), ComparisonOperator.EQ));
		User user = genDAO.getGenericObjects(User.class, filter).get(0);
		
		if(!DataValidator.isUserExist(input.getNetworkUser()))
			throw new DAOException("Network Admin "+input.getNetworkUser()+" does not exist.."+ExceptionMessages.EX_INVALID_USERNAME);
		
		if(!user.getServiceName().equalsIgnoreCase(input.getServiceName()))
			throw new BadRequestException("Network admin's Service is not matching !");
		
		if(!user.getUserRole().equalsIgnoreCase(UserRoleType.USER_ROLE_NETWORK_USER))
			throw new BadRequestException("The user "+user.getUserID()+" is not a Network user! Only network users can be mapped to Network!");
		
	}
	
	public static void validateOnModify(WiFi input) throws BadRequestException, DAOException
	{
		if(RequestValidator.isEmptyField(input.getWifiID()))
		{
			throw new BadRequestException("WIFI ID field"+ExceptionMessages.EX_EMPTY_FIELD);
		}
		
		if(!RequestValidator.isEmptyField(input.getAuthenticationType()) && input.getAuthenticationType().equalsIgnoreCase("WPA2-PSK"))
		{
			if(RequestValidator.isEmptyField(input.getEncryptionType()))
			{
				throw new BadRequestException("Encryption type "+ExceptionMessages.EX_EMPTY_FIELD);
			}
			
			if(RequestValidator.isEmptyField(input.getPassPhrase()))
			{
				throw new BadRequestException("PassPhrase field "+ExceptionMessages.EX_EMPTY_FIELD);
			}
		}
		
        
        //Field format validation
        if(!RequestValidator.isValidAlphaNumaricFormat(input.getWifiName()))
        {
        	throw new BadRequestException("WIFI Name '"+input.getWifiName()+"' format should be AlphaNumaric !"+ExceptionMessages.EX_INVALID_INPUT);
        }
        
		
		if(!RequestValidator.isEmptyField(input.getEnableWireless().toString()))
		{
			if(input.getEnableWireless().toString().equalsIgnoreCase("true") || input.getEnableWireless().toString().equalsIgnoreCase("false"))
			{
				
			}
			else			
				throw new BadRequestException("Enable Wireless field should be either true / false ! "+ExceptionMessages.EX_INVALID_INPUT);
		}
		
		if(!RequestValidator.isEmptyField(input.getHiddenSSID().toString()))
		{
			if(input.getHiddenSSID().toString().equalsIgnoreCase("true") || input.getHiddenSSID().toString().equalsIgnoreCase("false"))
			{
				
			}
			else
				throw new BadRequestException("Hidden SSID field should be either true / false ! "+ExceptionMessages.EX_INVALID_INPUT);
		}
        
        if(!RequestValidator.isEmptyField(input.getSsid()) && !RequestValidator.isValidSSID(input.getSsid()))
        {
        	//logger.log("Not a valid SSID, Only alphanumaric, special characters(_ - . and space) with max lenght 32 char allowed, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Not a valid SSID, Only alphanumaric, special characters(_ - . and space) with max lenght 32 char allowed "+ExceptionMessages.EX_INVALID_SSID);
        }
        
        if(!RequestValidator.isEmptyField(input.getAuthenticationType())){
        if(!(input.getAuthenticationType().equalsIgnoreCase("Open") || input.getAuthenticationType().equalsIgnoreCase("WPA2-PSK")))
        {
        	throw new BadRequestException("Invalid Authentication Type... Only Open/WPA2-PSK types allowed !"+ExceptionMessages.EX_INVALID_INPUT);
        }
        
		if(input.getAuthenticationType().equalsIgnoreCase("WPA2-PSK"))
		{
			if(!(input.getEncryptionType().equalsIgnoreCase("TKIP") || input.getEncryptionType().equalsIgnoreCase("AES")))
			{
				throw new BadRequestException("Invalid Encryption type.. Only TKIP / AES types allowed ! "+ExceptionMessages.EX_INVALID_INPUT);
			}
			
			if(!RequestValidator.isValidPresharedKey(input.getPassPhrase()))
	        {
	        	//logger.log("Preshared Key may conatain A-Z a-z 0-9 and - _ space only with 8-63 length, Exception at "+this.getClass().getName());
	        	throw new BadRequestException("Preshared Key may conatain A-Z a-z 0-9 and - _ space only with 8-63 length, "+ExceptionMessages.EX_INVALID_PRESHAREDKEY);
	        }
		}
        }
		
		//Dependency validation
		
		GenericDAO genDAO =GenericDAOFactory.getGenericDAO();
		
		List<FilterCondition> serviceFilter = new ArrayList<FilterCondition>();
		serviceFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME	, input.getServiceName(), ComparisonOperator.EQ));
		
		if(null == genDAO.getGenericObjects(Service.class, serviceFilter))
		{
			throw new BadRequestException("Service does not exist... "+ExceptionMessages.EX_INVALID_SERVICE);
		}
		
		if(!RequestValidator.isEmptyField(input.getNetworkUser())){
			
			List<FilterCondition> filter = new ArrayList<FilterCondition>();
			filter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME	, input.getServiceName(), ComparisonOperator.EQ));
			
			User user = genDAO.getGenericObjects(User.class, filter).get(0);
		
			if(null == user)
				throw new DAOException("Network Admin "+input.getNetworkUser()+" does not exist.."+ExceptionMessages.EX_INVALID_USERNAME);
		
			if(!user.getServiceName().equalsIgnoreCase(input.getServiceName()))
				throw new BadRequestException("Network admin's Service is not matching !");
		
			if(!user.getUserRole().equalsIgnoreCase(UserRoleType.USER_ROLE_NETWORK_USER))
				throw new BadRequestException("The user "+user.getUserID()+" is not a Network user! Only network users can be mapped to Network!");
		}
	
	}
	
		  

}
