package com.wio.core.config.action.wifi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.util.UniqueIDGenerator;

/**
 * Action used to register a new Device Group.
 * <p/>
 * POST to /dg/
 */
public class AddWiFi extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add user action. It expects a UserRequest object in input and returns
     * a serialized UserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();        
       
        WiFi input = getGson().fromJson(getBodyFromRequest(request), WiFi.class);
	    
        try 
	    {
        	WifiValidator.validateOnAdd(input);
		    
		    List<FilterCondition> wifiFilter =new ArrayList<FilterCondition>();
		    
		    wifiFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_NAME, input.getWifiName(), ComparisonOperator.EQ));
		    wifiFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
		       
		    if (genDAO.getGenericObjects(WiFi.class, wifiFilter).isEmpty())
		    {
		    	input.setWifiID("WIFI"+UniqueIDGenerator.getID());
		    	input.setCreatedBy(getUserInfoFromRequest(request).getUserID());
	            input.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
	            input.setLastUpdateTime("NA");
		    	
	        	genDAO.saveGenericObject(input);
		    }
	        else
	        	throw new BadRequestException("Wifi Name "+input.getWifiName()+" under service "+input.getServiceName()+" is already exists.");	        
	    } 
        catch (final Exception e) 
        {
            logger.log("Error while saving wifi \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        WiFi output = new WiFi();
        
        output.setWifiID(input.getWifiID());

        return getGson().toJson(output, WiFi.class);
    }
}
