package com.wio.core.config.action.hardware.map;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWServiceMap;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.validation.RequestValidator;


public class ViewHWServiceMap extends AbstractAction
{	
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	@Override
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{		
        logger = lambdaContext.getLogger();     
        genDAO = GenericDAOFactory.getGenericDAO();   
        
        HWServiceMap input = getGson().fromJson(getBodyFromRequest(request), HWServiceMap.class); 
        
        List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
        
        if(!RequestValidator.isEmptyField(input.getHwID()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, input.getHwID(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getServiceName()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
                
	    List<HWServiceMap> hwservice = new ArrayList<>();
	    
  	    try 
        {   
  	    	hwservice = genDAO.getGenericObjects(HWServiceMap.class, queryFilters);
        } 
  	    catch ( DAOException e) 
  	    {
            logger.log("Error while viewing Unassigned HP's \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
  		
        if(hwservice.isEmpty())
          	 throw new InternalErrorException("No Hardware to Service map found");	
             
        return getGson().toJson(hwservice, List.class);
    }
}

