package com.wio.core.config.action.hardware.profile;

import java.util.Arrays;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.Constants;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.HWRadio;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.PortSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class ModifyHardwareProfile extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	   
    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        HardwareProfile input = getGson().fromJson(getBodyFromRequest(request), HardwareProfile.class);
        
        HardwareProfile hp = null;
        		
        try 
        {
        	validate(input);
        	
        	User loggedUser = getUserInfoFromRequest(request);  
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(input.getServiceName()))
        			throw new BadRequestException("Not authorized to modify other service HW Profile!");
        	}        	
        	        	
        	hp = genDAO.getGenericObject(HardwareProfile.class, input.getHpID());
        	
        	if (null == hp) 
                throw new BadRequestException("Hardware Profile "+input.getHpID()+" does not exist");            
            
            patchHardwareProfile(hp, input);
            
            genDAO.updateGenericObject(hp);          
            
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while modifying user\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(hp, HardwareProfile.class);    
    }
    
    private void patchHardwareProfile(HardwareProfile hwp, HardwareProfile input) throws DAOException, BadRequestException
    {    	
    	System.out.println("In patchHardwareProfile");
    	if(input.getRadioSettings() != null && input.getRadioSettings().size() > 0)
    		hwp.setRadioSettings(patchHWRadioSettings(hwp, input));
    	
    	if(input.getPortSettings() != null && input.getPortSettings().size() > 0)
    		hwp.setPortSettings(patchHPLanPortSettings(hwp, input));    	
    }   

    
    private List<HPRadioSettings> patchHWRadioSettings(HardwareProfile hwProfile, HardwareProfile input) throws DAOException, BadRequestException
    {
    	System.out.println("In patchHWRadioSettings");
    	
    	Hardware hw = genDAO.getGenericObject(Hardware.class, input.getHwID());
    	
    	if(hw== null)
    		throw new BadRequestException("given Hardware "+input.getHwID()+" does not exist");
    	
    	for(HPRadioSettings inputRadio: input.getRadioSettings())
        {
    		System.out.println("In input radios settings loop");
    		if(RequestValidator.isEmptyField(inputRadio.getRadioName()))
    			throw new BadRequestException("Radio Name "+ExceptionMessages.EX_EMPTY_FIELD);
    		
    		boolean dbdcModeFlag = true;
			boolean radioBandFlag = true;
			boolean radioModeFlag = true;
			boolean bandwidthFlag = true;
			
    		for(HPRadioSettings hwpRadio: hwProfile.getRadioSettings())
    		{
        		System.out.println("In hwp radios settings loop");
    			if(hwpRadio.getRadioName().equals(inputRadio.getRadioName()))
    			{
    				System.out.println("radio name matched");
    				if(!RequestValidator.isEmptyField(inputRadio.getDbdcMode()))
    				{	
    					System.out.println("dbdc ");
    					for(HWRadio hwradio : hw.getHwRadio())
    		        	{
    						for(String dbdcMode : getColonSeperatedValueList(hwradio.getDbdcModeSupported()))
    		    			{
    		        			if(dbdcMode.trim().equalsIgnoreCase(inputRadio.getDbdcMode().trim()))
    		        			{
    		        				hwpRadio.setDbdcMode(inputRadio.getDbdcMode());
    		        				
    		        				dbdcModeFlag = true;    		        				
    		        				break;
    		        			}
    		        			else
    		        				dbdcModeFlag = false;
    		    			}
        					if(dbdcModeFlag)
        						break;
    		        	}
    				}
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getRadioBand()))
    				{
        				System.out.println("validating radio band: "+inputRadio.getRadioBand());	
    					for(HWRadio hwradio : hw.getHwRadio())
    		        	{
    						System.out.println("Supported Radios: "+hwradio.getRadioBandSupported());
    						for(String radioBand: getColonSeperatedValueList(hwradio.getRadioBandSupported()))
                    		{  
    							System.out.println("HW Radio Band:"+radioBand);
    							System.out.println("HWP Radio Band: "+inputRadio.getRadioBand());
    							
                    			if(radioBand.trim().equals(inputRadio.getRadioBand().trim()))
                    			{
                    				hwpRadio.setRadioBand(inputRadio.getRadioBand());
                    				
                    				radioBandFlag = true;
                    				break;
                    			}
                    			else
                    				radioBandFlag = false;
                    		}
    						
    						if(radioBandFlag)
    							break;
    		        	}
    					
    				}
    				
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getRadioMode()))
    				{
    					System.out.println("validating radio mode: "+inputRadio.getRadioMode());	
    					for(HWRadio hwradio : hw.getHwRadio())
    		        	{
    						for(String radioMode : getColonSeperatedValueList(hwradio.getRadioModeSupported()))
    	        			{
    	        				if(radioMode.trim().equalsIgnoreCase(inputRadio.getRadioMode().trim()))
    	        				{
    	        					hwpRadio.setRadioMode(inputRadio.getRadioMode());
    	        					
    	        					radioModeFlag = true;
    	        					break;
    	        				}
    	        				else
    	        					radioModeFlag = false;
    	        			}  
    						
    						if(radioModeFlag)
    							break;
    		        	}
    				}
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getBandwidth()))
    				{
    					System.out.println("validating bandwidth: "+inputRadio.getBandwidth());	
    					for(HWRadio hwradio : hw.getHwRadio())
    		        	{
    						for(String bandwidth : getColonSeperatedValueList(hwradio.getBandwidthSupported()))
    	        			{
    	        				if(bandwidth.trim().equalsIgnoreCase(inputRadio.getBandwidth().trim()))
    	        				{
    	        					hwpRadio.setBandwidth(inputRadio.getBandwidth());
    	        					
    	        					bandwidthFlag = true;
    	        					break;
    	        				}
    	        				else
    	        					bandwidthFlag = false;
    	        			}
    						
    						if(bandwidthFlag)
    							break;
    		        	}
    				}
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getChannel()))
    				{
    					hwpRadio.setChannel(inputRadio.getChannel());
    				}
    				
    				System.out.println("checking dbdc "+inputRadio.getDbdcStatus());
    				if(!RequestValidator.isEmptyField(inputRadio.getDbdcStatus()))
    				{
    					System.out.println("DBDC status from input: "+inputRadio.getDbdcStatus());
    					System.out.println("DBDC status from HWP: "+hwpRadio.getDbdcStatus());
    					
    					if(inputRadio.getDbdcStatus().equalsIgnoreCase(Constants.ENABLE) || inputRadio.getDbdcStatus().equalsIgnoreCase(Constants.DISABLE))
    						hwpRadio.setDbdcStatus(inputRadio.getDbdcStatus());
    					else
    	        			throw new BadRequestException("Invalid DBDC Status... Status should be enable or disable !");
    				}
    				
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getRadioStatus()))
    				{
    					hwpRadio.setRadioStatus(inputRadio.getRadioStatus());
    				}
    				
    				if(!RequestValidator.isEmptyField(inputRadio.getTransmitPower()))
    				{
    					hwpRadio.setTransmitPower(inputRadio.getTransmitPower());
    				}
    				
    			}
    		}
    		
    		if(!dbdcModeFlag)
            	throw new BadRequestException("Invalid DBDC mode: "+inputRadio.getDbdcMode());
            
            if(!radioBandFlag)
            	throw new BadRequestException("Invalid Radio Band: "+inputRadio.getRadioBand());
            
            if(!radioModeFlag)
            	throw new BadRequestException("Invalid Radio Mode: "+inputRadio.getRadioMode());
            
            if(!bandwidthFlag)
            	throw new BadRequestException("Invalid Bandwidth: "+inputRadio.getBandwidth());
        }
    	
    	return hwProfile.getRadioSettings();
    }
    
    
    private List<PortSettings> patchHPLanPortSettings(HardwareProfile hwProfile, HardwareProfile input)
    {    	
    	for(PortSettings inputPortSetting : input.getPortSettings())
    	{
    		for(PortSettings portSetting : hwProfile.getPortSettings())
    		{
        		if(portSetting.getLanInterface().equals(inputPortSetting.getLanInterface()))
        		{
        			if(!RequestValidator.isEmptyField(inputPortSetting.getPortNo()))
        			{
        				portSetting.setPortNo(inputPortSetting.getPortNo());
        			}
        		}
    			
    		}
    		
    	}
    	
		return hwProfile.getPortSettings();
    	
    }
    
    
    public static List<String> getColonSeperatedValueList(String fullString)
	{
		String [] separatedValues = fullString.split(":");				
		return Arrays.asList(separatedValues);
	}
    
    public static void validate(HardwareProfile input) throws BadRequestException, DAOException
    {
    	//Empty field validation
		
		if(RequestValidator.isEmptyField(input.getHwID()))
        	throw new BadRequestException("Hardware Name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getHpName()))
        	throw new BadRequestException("Hardware Profile Name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("Service Name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        
        //Dependency validation
        
        if(!DataValidator.isHardwareExist(input.getHwID()))
        	throw new BadRequestException("Hardware Name is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        
        if(!DataValidator.isServiceExist(input.getServiceName()))
        	throw new BadRequestException("Service is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
    	        
    }
}
