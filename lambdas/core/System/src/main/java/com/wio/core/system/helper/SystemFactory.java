package com.wio.core.system.helper;

import com.wio.common.action.IRequestAction;
import com.wio.common.helper.ActionMapper;
import com.wio.core.system.action.FetchSystemEnvStatus;

public class SystemFactory {

	
	/**
     * Default constructor
     */
    private SystemFactory() {}
    
    public static IRequestAction getUserActionInstance(String action){
    	
    	switch(action)
    	{
    		case ActionMapper.FETCH_ENV_STATUS:
    			return new FetchSystemEnvStatus();
    		default:
    			return null;	    			
    	}
    	
    }


    
}
