package com.wio.core.system.action;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sample {

	public static void main(String[] args) 
	{
		List<String> startTime = new ArrayList<>();
		
		startTime.add("19-10-2017 11:12:03 AM");
		startTime.add("19-09-2017 11:12:03 AM");
		startTime.add("19-11-2017 11:12:03 AM");
		startTime.add("19-10-2017 11:10:03 AM");
		startTime.add("19-10-2017 11:10:03 PM");
		
		
		startTime.forEach(item -> {
			System.out.println(item);
		});

		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss a");
        Collections.sort(startTime, (s1, s2) -> LocalDateTime.parse(s1, formatter).compareTo(LocalDateTime.parse(s2, formatter)));
    	
        System.out.println("After sorting.."+startTime.get(startTime.size()-1));
        
        startTime.forEach(item -> {
			System.out.println(item);
		});
	}

}
