package com.wio.core.system.handler;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.IRequestAction;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.core.system.helper.SystemFactory;

/**
 * This class contains the main event handler for the Lambda function.
 */
public class SystemRequestRouter {
	
    public static String handler(JsonObject inputObj, OutputStream response, Context context) throws BadRequestException, InternalErrorException, DAOException, AuthorizationException {
        LambdaLogger logger = context.getLogger();

        if (inputObj == null) {
            logger.log("Invald inputObj, could not find action parameter");
            throw new BadRequestException("Could not find action value in request");
        }
        
        System.out.println("json object in SS handler:: "+inputObj);
        String actionClass = inputObj.get("action").getAsString();
      
        if (actionClass == null) {
            logger.log("Action class is null");
            throw new BadRequestException("Invalid user action "+actionClass);
        }
        
        IRequestAction action;
        
        action = IRequestAction.class.cast(SystemFactory.getUserActionInstance(actionClass));

        if (action == null) {
            logger.log("Action class is null");
            throw new BadRequestException("Invalid user action ");
        }

        JsonObject body = null;
        if (inputObj.get("body") != null) {
            body = inputObj.get("body").getAsJsonObject();
        }
        
        String output = action.handle(body, context);

		try {
			IOUtils.write(output, response);
		} catch (final IOException e) {
			logger.log("Error while writing response\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}
		
		return output;
 }
}
