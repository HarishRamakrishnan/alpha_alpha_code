package com.wio.core.firmware.s3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.firmware.Firmware;
import com.wio.core.firmware.request.firmware.FirmwareUploadS3Response;

/**
 * The table in DynamoDB should be created with an Hash Key called deviceId.
 */
public class WavesioS3Bucket 
{
	private static String bucketName = System.getenv("WIO_S3_BUCKET");	
	private static String FolderName = "wio-firmware/";
	
	//Upload the firmware to S3 buckets
	public static FirmwareUploadS3Response uploadFirmware(Firmware fw) throws BadRequestException, MalformedURLException
	{
		FirmwareUploadS3Response s3Info = new FirmwareUploadS3Response();
		OutputMessage output = new OutputMessage();
		File file = null;
		URL url = new URL(fw.getSource());		
		
		String fileName = FilenameUtils.getName(url.getPath());
		String fileExtension = FilenameUtils.getExtension(url.getPath());
		String keyName = FolderName+fileName;

		AmazonS3 s3client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());	
		
		try
		{
			System.out.println("Validating source url..."); 
			if(!isValidSourceURL(fw.getSource()))
			{
        		output.setStatus(HttpURLConnection.HTTP_BAD_REQUEST);
        		output.setErrorMessage("Invalid Source URL or File not exist at: "+fw.getSource());        		
        		s3Info.setOutput(output);        		
            	
            	return  s3Info;
        	}
			      
			InputStream inputStream = url.openStream();
						
			if(isValidFirmwareFile(url.openStream(), fw.getChecksumType(), fw.getChecksum(), fileExtension))
			{
				System.out.println("file validated successfully...");
				file = File.createTempFile(fileName, "."+fileExtension);
				FileUtils.copyInputStreamToFile(inputStream, file); 
				PutObjectRequest objectRequest = new PutObjectRequest(bucketName, keyName, file);				
				objectRequest.setCannedAcl(CannedAccessControlList.PublicRead);		
				
				System.out.println("Uploading to S3 bucket..."); 
				s3client.putObject(objectRequest);
				System.out.println("uploaded to S3...");
			}
			else
			{
				output.setStatus(HttpURLConnection.HTTP_BAD_REQUEST);
        		output.setErrorMessage("Invalid Firmware:: Firmware file verification failed !");        		
        		s3Info.setOutput(output);        		
            	
            	return  s3Info;
			}
		    		    
		    s3Info.setFileName(fileName);
		    s3Info.setBucketName(bucketName);
		    s3Info.setKeyName(keyName);
		    s3Info.setS3URL(s3client.getUrl(bucketName, keyName).toString());
		}
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		    if(file.exists()) {
		        file.delete();
		    }
		}
		
		return s3Info;
	}
	
	public static boolean isValidFirmwareFile(InputStream inputStream, String checksumType, String checksum, String fileFormat) throws BadRequestException
	{
		System.out.println("Validating file..."); 
		try 
		{
			String fileChecksum = null;
			
			if(checksumType.equals("MD5"))
			{
				System.out.println("file checksum:: "+fileChecksum);
				fileChecksum = DigestUtils.md5Hex(inputStream);
				System.out.println("file checksum:: "+fileChecksum);
			}
			else if(checksumType.equals("SHA-256"))
				fileChecksum = DigestUtils.sha256Hex(inputStream);
			

			System.out.println("validating file format...");
			if(!(fileFormat.equalsIgnoreCase("bin") || fileFormat.equalsIgnoreCase("ipk")))
					throw new BadRequestException("Invalid File Format: Only .bin and .ipk file formats are valid !");
			
			if(fileChecksum.equalsIgnoreCase(checksum))
				return true;
			else
				throw new BadRequestException("Invalid file checksum !");
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean isValidSourceURL(String sourceURL)
	{
		try
		{
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(sourceURL).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return false;
	}
		
}
