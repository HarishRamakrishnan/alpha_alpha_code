package com.wio.core.firmware.action;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.firmware.Firmware;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.validation.RequestValidator;

public class ViewFirmwareAction extends AbstractAction
{	
	  private LambdaLogger logger = null;
	  private GenericDAO genDAO = null;
	    /**
	     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
	     * a serialized APResponse object
	     *
	     * @param request       Receives a JsonObject containing the body content
	     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
	     * @return Returns the new AddAPResponse object
	     * @throws BadRequestException
	     * @throws InternalErrorException
	     */
	    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	    {
	        logger = lambdaContext.getLogger();
	        genDAO = GenericDAOFactory.getGenericDAO();
	        
	        Firmware input = getGson().fromJson(getBodyFromRequest(request), Firmware.class);	        
	        
	        List<Firmware> fw = null;
	        
	        List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
	        
	        if(!RequestValidator.isEmptyField(input.getFilename()))
	    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_FILE_NAME, input.getFilename(),ComparisonOperator.EQ));
	        
	        if(!RequestValidator.isEmptyField(input.getHpID()))
	    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, input.getHpID(),ComparisonOperator.EQ));
	        
	        if(!RequestValidator.isEmptyField(input.getVersion()))
	    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_VERSION, input.getVersion(),ComparisonOperator.EQ));
	        
	        if(!RequestValidator.isEmptyField(input.getVersionType()))
	    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_VERSION_TYPE, input.getVersionType(),ComparisonOperator.EQ));
	        
	        if(!RequestValidator.isEmptyField(input.getStatus()))
	    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_STATUS, input.getStatus(),ComparisonOperator.EQ));
	        
	        	        
	        try 
	        {
	        	fw = genDAO.getGenericObjects(Firmware.class, queryFilters);
	        	
	        	if(fw == null || fw.isEmpty())
	        	{
	        		OutputMessage output = new OutputMessage();
	        		
	        		output.setStatus(HttpURLConnection.HTTP_BAD_REQUEST);
	        		output.setErrorMessage("No Firmware avilable !");
	            	
	            	return getGson().toJson(output, OutputMessage.class);  
	        	}	           
	        } 
	        catch (final Exception e) 
	        {
	            logger.log("Error while fetching firmware details..\n" + e.getMessage());
	            throw new InternalErrorException(e.getMessage());
	        } 

	        return getGson().toJson(fw, List.class);
	    }	    
}

