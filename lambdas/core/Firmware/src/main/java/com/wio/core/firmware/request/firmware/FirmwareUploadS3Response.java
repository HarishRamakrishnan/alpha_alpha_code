package com.wio.core.firmware.request.firmware;

import com.wio.common.exception.OutputMessage;

/**
 * Bean for the ap registration response
 */
public class FirmwareUploadS3Response {
    
	private String s3URL;
	private String keyName;
	private String bucketName;
	private String fileName;
	
	private OutputMessage output;
   

    public FirmwareUploadS3Response() {

    }


	public String getS3URL() {
		return s3URL;
	}


	public void setS3URL(String s3url) {
		s3URL = s3url;
	}


	public String getKeyName() {
		return keyName;
	}


	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}


	public String getBucketName() {
		return bucketName;
	}


	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public OutputMessage getOutput() {
		return output;
	}


	public void setOutput(OutputMessage output) {
		this.output = output;
	}


  
}
