package com.wio.core.firmware.action;

import com.wio.common.Enums.Constants;
import com.wio.common.exception.BadRequestException;
import com.wio.common.firmware.Firmware;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.RequestValidator;

public class ValidateFirmware 
{
	public static void validate(Firmware input) throws BadRequestException
	{
		//Mandatory field validation
		if(RequestValidator.isEmptyField(input.getVersion()))
        	throw new BadRequestException("Version "+ExceptionMessages.EX_EMPTY_FIELD);
                
        if(RequestValidator.isEmptyField(input.getHpID()))
        	throw new BadRequestException("HP "+ExceptionMessages.EX_EMPTY_FIELD);
                
        if(RequestValidator.isEmptyField(input.getSource()))
        	throw new BadRequestException("Firmware Source "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getVersionType()))
        	throw new BadRequestException("Firmware Version Type "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getChecksumType()))
        	throw new BadRequestException("Checksum Type "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getChecksum()))
        	throw new BadRequestException("Checksum "+ExceptionMessages.EX_EMPTY_FIELD);
        
        
        //Field format validation
        if(!RequestValidator.isValidVersionFormat(input.getVersion()))
        	throw new BadRequestException("Invalid version format "+ExceptionMessages.EX_INVALID_INPUT);        
        
        if(!(input.getVersionType().equals(Constants.FW_VERSION_STABLE) || input.getVersionType().equals(Constants.FW_VERSION_BETA)))
        	throw new BadRequestException("Version type should be either STABLE or BETA "+ExceptionMessages.EX_INVALID_INPUT);
        
        if(!(input.getChecksumType().equals(Constants.FW_CHECKSUM_MD5) || input.getChecksumType().equals(Constants.FW_CHECKSUM_SHA256)))
        	throw new BadRequestException("Invalid Checksum type: Checksum should be either MD5 or SHA-256 !"+ExceptionMessages.EX_INVALID_INPUT);
        
        
	}

}
