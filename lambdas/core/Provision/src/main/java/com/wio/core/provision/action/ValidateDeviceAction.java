package com.wio.core.provision.action;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.RequestValidator;
import com.wio.core.provision.model.ValidateDeviceRequest;
import com.wio.core.provision.model.ValidateDeviceResponse;

/**
 * Action used to zero provisioning a new AP.
 * <p/>
 * POST to /ap/
 */
public class ValidateDeviceAction extends AbstractAction {

    private LambdaLogger logger = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        
        ValidateDeviceRequest input = getGson().fromJson(request, ValidateDeviceRequest.class);

        if(input == null)
        	throw new BadRequestException("Empty input Request! "+ExceptionMessages.EX_INVALID_INPUT);
        
        if(RequestValidator.isEmptyField(input.getDeviceID()))
        	throw new BadRequestException("Device ID "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getMac()))
        	throw new BadRequestException("Device MAC Address "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getModel()))
        	throw new BadRequestException("Device Model "+ExceptionMessages.EX_EMPTY_FIELD);
        
        
        
        List<Device> devices = new ArrayList<Device>();
        
        GenericDAO dao = GenericDAOFactory.getGenericDAO();        

	    List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();      
	    
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_ID, input.getDeviceID(), ComparisonOperator.EQ));        
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_MAC_ADDRESS, input.getMac(), ComparisonOperator.EQ)); 
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_MODEL, input.getModel(), ComparisonOperator.EQ)); 
	    	  
	    ValidateDeviceResponse response = new ValidateDeviceResponse();
	    
        try 
        {        	
        	devices = dao.getGenericObjects(Device.class, filterConditions);
        	
            if(!devices.isEmpty())
            {   
            	System.out.println("Device validtaed, prepraing response...");
            	
            	if(devices.get(0).getStatus().equals("ACTIVE"))
            	{
            		System.out.println("Device "+devices.get(0).getDeviceID()+"is already connected to WavesIO !");            	
            		
            		response.setStatus("FAILURE, Duplicate Device Provisioning will not be allowed !");
            	}
            	else if(devices.get(0).getStatus().equals("READY_FOR_PROVISION"))
            	{
            		response.setStatus("SUCCESS");
            		
            		if(null == System.getenv("MQTT_BROKER_IP"))
            			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_IP");
            		
            		if(null == System.getenv("MQTT_BROKER_PORT"))
            			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_PORT");
            		
            		if(null == System.getenv("MQTT_BROKER_USERNAME"))
            			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_USERNAME");
            		
            		if(null == System.getenv("MQTT_BROKER_PASSWORD"))
            			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_PASSWORD");
            		
            		response.setBrokerIP(System.getenv("MQTT_BROKER_IP"));
            		response.setPort(System.getenv("MQTT_BROKER_PORT"));
            		response.setUsername(System.getenv("MQTT_BROKER_USERNAME"));
            		response.setPassword(System.getenv("MQTT_BROKER_PASSWORD"));
            		
            		response.setDefaultTopic(devices.get(0).getDefaultTopic());            		
            	}
            	else
            	{
            		response.setStatus("Device not ready for Provision / Not assigned to proper DeviceGroup !");
            	}
            }
            else
            {
            	response.setStatus("FAILURE");
            	System.out.println("Device is not registered with Waves.io");
            }
            
            return getGson().toJson(response, ValidateDeviceResponse.class);
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while validating Device \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }         
    }
}
