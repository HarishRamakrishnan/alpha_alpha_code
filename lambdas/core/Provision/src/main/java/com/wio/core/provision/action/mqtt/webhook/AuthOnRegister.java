package com.wio.core.provision.action.mqtt.webhook;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.MQTTClient;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.validation.RequestValidator;
import com.wio.core.provision.request.AuthOnRegisterRequest;
import com.wio.core.provision.request.AuthOnRegisterResponse;
import com.wio.core.provision.request.ProxyResponse;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class AuthOnRegister extends AbstractAction 
{
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
    	LambdaLogger logger = lambdaContext.getLogger();
    	GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
    	
    	AuthOnRegisterResponse resBody = new AuthOnRegisterResponse();
		ProxyResponse res = new ProxyResponse();  
        
        logger.log("Webhook invoked auth_on_register...");
        
        if(request == null)
        	logger.log("Webhook Request is Empty !");
    	   
        logger.log("request:: "+request);
        
        AuthOnRegisterRequest input = getGson().fromJson(request, AuthOnRegisterRequest.class);
        logger.log("request prepared..");   
        logger.log("request :: "+getGson().toJson(input, AuthOnRegister.class));
        
        if(RequestValidator.isEmptyField(input.getUsername()) || RequestValidator.isEmptyField(input.getPassword()))
        {
        	resBody.setError("Broker username/password cannot be empty!");
        	
        	res.setBase64Encoded(false);
	        res.setStatusCode(200);
	        //res.setBody("{'errorMessage': 'Broker username/password cannot be empty!'}");
	        res.setBody(getGson().toJson(resBody, AuthOnRegisterResponse.class));
        	
        	return getGson().toJson(res, ProxyResponse.class);  
        }
        
        if(RequestValidator.isEmptyField(input.getClient_id()))
        {
        	resBody.setError("client_id cannot be empty !");
        	
        	res.setBase64Encoded(false);
	        res.setStatusCode(200);
	        //res.setBody("{'errorMessage': 'client_id cannot be empty !'}");
	        
	        res.setBody(getGson().toJson(resBody, AuthOnRegisterResponse.class));
        	
        	return getGson().toJson(res, ProxyResponse.class);  
        }
        
        try
        {
        	MQTTClient client = genDAO.getGenericObject(MQTTClient.class, input.getClient_id());
        	
        	if(null == client)
        	{        		
        		resBody.setError("Invalid client (ID) !");
        		
        		res.setBase64Encoded(false);
    	        res.setStatusCode(200);
    	        //res.setBody("{'errorMessage': 'Invalid client (ID) !'}");
    	        
    	        res.setBody(getGson().toJson(resBody, AuthOnRegisterResponse.class));
            	
            	return getGson().toJson(res, ProxyResponse.class);  
        	}
        	
        	if(null == System.getenv("MQTT_BROKER_USERNAME"))
    			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_USERNAME");
        	
        	if(null == System.getenv("MQTT_BROKER_PASSWORD"))
    			throw new InternalErrorException("No Environment variable found for: MQTT_BROKER_PASSWORD");
        	
        	if(input.getUsername().equals(System.getenv("MQTT_BROKER_USERNAME")) && input.getPassword().equals(System.getenv("MQTT_BROKER_PASSWORD")))
        	{
        		client.setAuthorized(true);
        		
        		genDAO.updateGenericObject(client);
        		
        		logger.log("validated:: True...");
        		resBody.setResult("ok");
        		
    	        res.setBase64Encoded(false);
    	        res.setStatusCode(200);
    	        //res.setBody("{'result': 'ok'}");
    	        
    	        res.setBody(getGson().toJson(resBody, AuthOnRegisterResponse.class));
        	}
        	else
        	{   
        		resBody.setError("not allowed");
        		
    	        res.setBase64Encoded(false);
    	        res.setStatusCode(200);
    	        //res.setBody("{'error': 'not allowed'}");
    	        
    	        res.setBody(getGson().toJson(resBody, AuthOnRegisterResponse.class));
        	}        	
        } 
        catch (final InternalErrorException e) 
        {
            logger.log("Error while validating broker via webhook...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        System.out.println("Proxy response::: "+res);        
        return getGson().toJson(res, ProxyResponse.class);        
    }
}
