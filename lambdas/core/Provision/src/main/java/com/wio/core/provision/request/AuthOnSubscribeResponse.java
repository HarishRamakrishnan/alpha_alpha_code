package com.wio.core.provision.request;

import java.util.List;

public class AuthOnSubscribeResponse 
{
	private String result;
	private String error;
	private List<DeviceTopics> topics;

	public List<DeviceTopics> getTopics() {
		return topics;
	}

	public void setTopics(List<DeviceTopics> topics) {
		this.topics = topics;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
}
