package com.wio.core.provision.action.mqtt.webhook;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.MQTTClient;
import com.wio.common.config.model.MQTTTopic;
import com.wio.common.config.request.PublishingTopics;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.InvalidArugumentException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.common.mqtt.MQTTConnection;
import com.wio.common.mqtt.MQTTPublisher;
import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.WIOConfigData;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.action.device.group.topic.DeviceGroupTopic;
import com.wio.core.provision.request.AuthOnRegisterResponse;
import com.wio.core.provision.request.AuthOnSubscribeRequest;
import com.wio.core.provision.request.AuthOnSubscribeResponse;
import com.wio.core.provision.request.DeviceTopics;
import com.wio.core.provision.request.ProxyResponse;
import com.wio.core.provision.request.PublishingTopicsResponse;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class AuthOnSubscribe extends AbstractAction 
{
	LambdaLogger logger = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
    	//LambdaLogger logger = lambdaContext.getLogger();
    	GenericDAO genDAO = GenericDAOFactory.getGenericDAO();    	
    	Device device = null;
		ProxyResponse proxyResp = new ProxyResponse();  
		AuthOnRegisterResponse authOnRegResp = new AuthOnRegisterResponse();
		
		String devDefaultTopic = "";
		
        System.out.println("Webhook invoked auth_on_subscribe...");
        
        if(request == null) {
        	System.out.println("Webhook Request is Empty !");
			return getGson().toJson(
					getProxyResponse(400, Boolean.FALSE, "Request is empty/null", "", proxyResp, authOnRegResp),
					ProxyResponse.class); 
        }
        System.out.println("request:: "+request);
        
        AuthOnSubscribeRequest input = getGson().fromJson(request, AuthOnSubscribeRequest.class);
        System.out.println("AuthOnSubscribeRequest :: "+getGson().toJson(input, AuthOnSubscribeRequest.class));
        
        if(RequestValidator.isEmptyField(input.getClient_id())) {
			return getGson().toJson(
					getProxyResponse(400, Boolean.FALSE, "client_id cannot be empty!", "", proxyResp, authOnRegResp),
					ProxyResponse.class); 
        }
        
        try {
        	MQTTClient client = genDAO.getGenericObject(MQTTClient.class, input.getClient_id());
        	
        	if(null == client) {
        		
				return getGson().toJson(
						getProxyResponse(400, Boolean.FALSE, "Invalid client!", "", proxyResp, authOnRegResp),
						ProxyResponse.class); 
        	} else {
				if (client.getAuthorized()) {
					System.out.println("Client is validated and authorised to provision !");
					AuthOnSubscribeResponse authOnSubResp = new AuthOnSubscribeResponse();

					System.out.println(client.getType());
					
					if (client.getType().equals("Device-WIO-AccessPoint")) {
						System.out.println("Client is Device-WIO-AccessPoint");
						MQTTTopic _topic = DynamoDBHelper.getInstance().getSubscriptionTopics("defaultConfig");
						device = genDAO.getGenericObject(Device.class, client.getClientID());

						if (device == null) {
							return getGson().toJson(getProxyResponse(400, Boolean.FALSE,
									"Client (Device) does not found in Devices !", "", proxyResp, authOnRegResp),
									ProxyResponse.class);
						}
						
						devDefaultTopic = device.getDefaultTopic();

						List<String> subTopics = DeviceGroupTopic.getForceSubscriptionTopics(device.getDgID());

						List<DeviceTopics> topicsList = new ArrayList<>();
						subTopics.forEach(topic -> {
							DeviceTopics top = new DeviceTopics();
							top.setTopic(topic);
							top.setQos(_topic.getMqttQoS());

							topicsList.add(top);
						});

						DeviceTopics defaultTopic = new DeviceTopics();

						defaultTopic.setTopic(device.getDefaultTopic());
						defaultTopic.setQos(_topic.getMqttQoS());

						topicsList.add(defaultTopic);
						System.out.println("Force Subscription Topics:- "+topicsList);
						
						// Publishing shared subscriber topics to device default topic
						List<MQTTTopic> pubTopics = DeviceGroupTopic.getSharedSubscriberTopics();

						PublishingTopics topics = new PublishingTopics();

						pubTopics.forEach(topic -> {
							System.out.println("Topic:: " + topic.getTopic() + " and Path:: " + topic.getPath());

							String workerTopic = topic.getTopic().trim();
							switch (workerTopic) {
							case "rfMetric":
								topics.setEventsArp(topic.getPath());
								break;
							case "rfMgmt":
								topics.setRfMgmt(topic.getPath());
								break;
							case "auth":
								topics.setAuth(topic.getPath());
								break;
							
							case "log":
								topics.setLog(topic.getPath());
								break;
							case "eventsArp":
								topics.setEventsArp(topic.getPath());
								break;
							case "eventsDhcp":
								topics.setEventsDhcp(topic.getPath());
								break;
							case "config":
								topics.setConfig(topic.getPath());
								break;
							case "eventsHostapd3":
								topics.setEventsHostapd3(topic.getPath());
								break;
							}
							 
						});
						System.out.println("Shared subscription topics:- "+topics.toString());
						PublishingTopicsResponse publishingTopics = new PublishingTopicsResponse();

						List<PublishingTopics> pubTop = new ArrayList<>();
						pubTop.add(topics);

						publishingTopics.setTopics(pubTop);

						System.out.println("Going to publishing shared subscriber topics:: "
								+ getGson().toJson(publishingTopics, PublishingTopicsResponse.class) +" to Device Default topic:- "+devDefaultTopic);
						// Construct wioconfig protobuf mesg to be published in default topic per
						// device.
						boolean status = publishSubscribingTopicsToDevice(genDAO, devDefaultTopic, _topic, topics);

						if (status)
							System.out.println("Topics published to device default topic..");
						else
							System.out.println("Unable to publish topics to default topic !");

						
						return getGson().toJson(
								getProxyResponse(200, Boolean.FALSE, "", "Ok", proxyResp, authOnSubResp , topicsList),
								ProxyResponse.class); 
						/*
						 * logger.log("Preparing Default configiuration to Device...");
						 * 
						 * String config = DeviceConfigurationAPI.fetchConfig(device.getDeviceID(),
						 * lambdaContext); String publishStatus =
						 * MQTTPublisher.publishMessage(connection, config, device.getDefaultTopic(),
						 * Integer.parseInt(System.getenv("MQTT_TOPIC_DEFAULT_QOS")), true);
						 * 
						 * if(publishStatus.equals("SUCCESS"))
						 * logger.log("Configuration published to device topic: "+device.getDefaultTopic
						 * ()); else logger.
						 * log("Unable to publish configuration to device topic, MQTT Broker issue !");
						 * 
						 * connection.disconnect(); logger.log("Broker disconnected !");
						 */
					} else {
						//Other than Device, when topic is subscribed by someone.						
						return getGson().toJson(
								getProxyResponse(200, Boolean.FALSE, "", "Ok", proxyResp, authOnRegResp),
								ProxyResponse.class); 
					}
				} else {
        			
        	        return getGson().toJson(
							getProxyResponse(401, Boolean.FALSE, "Client authorisation failed!", "", proxyResp, authOnRegResp),
							ProxyResponse.class);
        		}
        	}      	
        } catch (final InvalidArugumentException e) 
        {
            System.out.println("Error while validating broker via webhook...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }        
    }

	private boolean publishSubscribingTopicsToDevice(GenericDAO genDAO, String devDefaultTopic, MQTTTopic _topic,
			PublishingTopics topics) throws DAOException {

		MQTTClient mqttClient = genDAO.getGenericObject(MQTTClient.class, "AuthOnSub");
		if (null == mqttClient) {
			createMQTTClient(mqttClient, "AuthOnSub");
		}

		MQTTConnection conn = new MQTTConnection();
		MqttClient client = conn.getConnection(mqttClient.getClientID(), 0, true, false);

		boolean status = MQTTPublisher.publishMessage(client, constructProtoMsg(topics), devDefaultTopic,
				_topic.getMqttQoS(), false);
		return status;
	}

	private byte[] constructProtoMsg(PublishingTopics topics) {
		
		System.out.println("Publishing topics to be converted to protobuf format:- "+topics.toString());
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		WIOConfigData wioData = (WIOConfigData) protocolType.getMesgSerializer("WIO_CONFIG");
		wioData.messageConstructor();
		wioData.setTopicDetails(topics);
		//Choosing TOPICS proto.
		wioData.serializeOneOf(1);
		wioData.serialize();
		return wioData.getWIOConfigByteString().toByteArray();
	}

	private void createMQTTClient(MQTTClient mqttClient, String clientId) {
		mqttClient.setClientID(clientId);
		mqttClient.setAuthorized(Boolean.FALSE);
		mqttClient.setType("PC_STANDALONE");
		mqttClient.setDescription("CoreLambda.");
	}

	private ProxyResponse getProxyResponse(int statusCode, Boolean isBase64Encoded, String errorMesg, String result, ProxyResponse proxyResp,
			AuthOnRegisterResponse authOnRegResp) {
		
		authOnRegResp.setError(errorMesg);
		authOnRegResp.setResult(result);
		
		proxyResp.setBase64Encoded(isBase64Encoded);
		proxyResp.setStatusCode(statusCode);
		proxyResp.setBody(getGson().toJson(authOnRegResp, AuthOnRegisterResponse.class));
		
		return proxyResp;
	}
	
	private ProxyResponse getProxyResponse(int statusCode, Boolean isBase64Encoded, String errorMesg, String result, ProxyResponse proxyResp,
			AuthOnSubscribeResponse authOnSubResp, List<DeviceTopics> topicsList) {
		
		System.out.println("Subscription topics:: " + getGson().toJson(authOnSubResp, AuthOnSubscribeResponse.class));
		
		authOnSubResp.setError(errorMesg);
		authOnSubResp.setResult(result);
		authOnSubResp.setTopics(topicsList);
		
		proxyResp.setBase64Encoded(isBase64Encoded);
		proxyResp.setStatusCode(statusCode);
		proxyResp.setBody(getGson().toJson(authOnSubResp, AuthOnSubscribeResponse.class));
		
		return proxyResp;
	}
	
	public static void main (String[] args) {
		AuthOnSubscribe sub = new AuthOnSubscribe();
		JsonObject json = new JsonObject();
		Gson gson = new Gson();
		String strJson = "{ \"username\": \"wioadmin\", \"mountpoint\": \"/test\", \"client_id\": \"06aa74-f801-a401-b6ytgheb7117\", \"topics\": [ { \"topic\": \"airtel/usa/ny/nyc/manhattan/bc123/bld-a/apt-a/06aa74-f801-a401-b6ytgheb7117\", \"qos\": 1 } ] }";
		
		json = gson.fromJson(strJson, JsonObject.class);
		try {
			sub.handle(json, null);
		} catch (BadRequestException | InternalErrorException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}