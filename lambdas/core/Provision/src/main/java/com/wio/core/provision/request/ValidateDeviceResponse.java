package com.wio.core.provision.request;

public class ValidateDeviceResponse 
{
	private String status;
	private String brokerIP;
	private String port;
	private String username;
	private String password;	
	
	private String defaultTopic;
	
	
	
	
	public String getBrokerIP() {
		return brokerIP;
	}
	public void setBrokerIP(String brokerIP) {
		this.brokerIP = brokerIP;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDefaultTopic() {
		return defaultTopic;
	}
	public void setDefaultTopic(String defaultTopic) {
		this.defaultTopic = defaultTopic;
	}

		
}
