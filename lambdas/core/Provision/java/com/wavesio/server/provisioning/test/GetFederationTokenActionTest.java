package com.wavesio.server.provisioning.test;

import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.wavesio.server.common.configuration.SystemConfigMessages;

public class GetFederationTokenActionTest {

	public static void main(String[] args) 
	{
		AmazonSQS sqs = null;
		String queueUrl = null;
		List<Message> messages = null;
		try
		{
			BasicSessionCredentials cred = new BasicSessionCredentials("ASIAI2B252DEZ2XSNJAQ", 
					"NwxXTWGLUQDxbQMgRsU3a1BtnDnJtacsNAZraCLl", 
					"FQoDYXdzEOn//////////wEaDMtGtfc2lGTx1hEA3iK+A91p5x/IKq80jr5FYbZTMt6LRXEsG++z490/9EEQDfgx65N6hDuM5ByqtaCZ6JqVfMz/HvMZ1wMUjyMyFLu/o5YdOMBC/f9T8SfhdIyc7PRch/PX7tSCgcu8ChCJD77aUTMDQoYSlAULaKLccQ6JnJfM7PLzAtWHchxlPgKjJPTA2jcSFeVkHZKDPufjyGg01df9he/sH7L3QZYmCB1DmyKsXRv8ReEnp6GK6xh5P/t9KRGod3mUBNVCvE3SpCc0ftxq1dg1W8w2zjmHoOHVccQko+NKeQNm1u1RkBTtyhblvUBBSYn+Kh/5xzcJmXyPdKNS5jF3tw586IIXrOkDk1W4O7ppygct+1OtS/xz3UY9QHb7FHD5PkPW1OYmIGt6NXiqxfrnEx4MRIGrO/kUn5Wn8K+7Odb42L1oeDk14vFXF7Z5UJANiMhogxoedCAHqwhbc/zYuaQesj/VSiccGST9ri788B/+RoogQLaVHCswrhi3oKzpUqXNjPUYIanIQv29yv5ldJSquTvEQ8oJp3h4MVCYIA2JggD2E6WILEEJPqkwslILsSlFrWj7p75QKEJivTktZDdRUk0ra0uUKLSfg8MF");
            
			sqs = new AmazonSQSClient(cred);
            sqs.setRegion(Region.getRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)));
            
            //CreateQueueRequest createQueueRequest = new CreateQueueRequest("TestQ123");
            //queueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
            
            System.out.println("Qurl::: "+queueUrl);
           
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest("https://sqs.us-east-1.amazonaws.com/528719264515/MyQueue");
	        messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
	        for (Message message : messages) {
	            System.out.println("  Message");
	            System.out.println("    MessageId:     " + message.getMessageId());
	            System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
	            System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
	            System.out.println("    Body:          " + message.getBody());
	            for (Entry<String, String> entry : message.getAttributes().entrySet()) {
	                System.out.println("  Attribute");
	                System.out.println("    Name:  " + entry.getKey());
	                System.out.println("    Value: " + entry.getValue());
	            }
	        }
		}
		 catch (AmazonServiceException ase) 
		{
	            System.out.println("Caught an AmazonServiceException, which means your request made it " +
	                    "to Amazon SQS, but was rejected with an error response for some reason.");
	            System.out.println("Error Message:    " + ase.getMessage());
	            System.out.println("HTTP Status Code: " + ase.getStatusCode());
	            System.out.println("AWS Error Code:   " + ase.getErrorCode());
	            System.out.println("Error Type:       " + ase.getErrorType());
	            System.out.println("Request ID:       " + ase.getRequestId());
	    } 
		catch (AmazonClientException ace) 
		{
	            System.out.println("Caught an AmazonClientException, which means the client encountered " +
	                    "a serious internal problem while trying to communicate with SQS, such as not " +
	                    "being able to access the network.");
	            System.out.println("Error Message: " + ace.getMessage());
	    }
		catch(Exception e)
		{
			throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
		}
		
	}

}
