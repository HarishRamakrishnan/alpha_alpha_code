package com.wavesio.server.provisioning.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSSessionCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.auth.SessionCredentialsProviderFactory;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.wavesio.server.common.configuration.SystemConfigMessages;

public class SQSQueueActions 
{
	//private static AWSCredentials credentials = null;
	private static AmazonSQS sqs = null;
	private static String queueUrl = null;
	static
	{
        try 
        {
        	BasicSessionCredentials cred = new BasicSessionCredentials("ASIAJHBSCY7EWSI2ARWQ", "zS6GG5WARPrr1FwmolWP538xKsRKE8Vvqxja8qR/", "FQoDYXdzEOn//////////wEaDFGQRghvauXQLc0IlCK+A/k3fmdPnP4CS9Mib1RMNS0ZFacY+gmrn5o5a02fnKAts3QyvintNlKrsIX1I+Yq3cKWwaGXVeSMnB5Ox/VkXdkdI9YKBxu2qVg/3QJ7UHMEvrbW+IcTR+jOp/OxNyI/OY0eBuwUubSvtaCsb4hMXN0eUUj9Mel7gus+gFBtaOhX+OyL0N4zpCv3FPezfhevh7WipYICvmO0uXkP1tMxt4tNq/zCi4Dl+JxGtFXVAgYotoAvShvaVRT8oZ8TLz7F33qVzCmnLHaYcMiv6VyhPonCeeDm0FRsvxPu9MTyOP9BfoizQyYyShdsprqXpIo946UuioWn8Zbxqjc77ufVNSqIc3apKqtql8s1rU6zbc4lvcd1ZfG8ftqYmzKY4jcvLIgjKXovaDrXYl2KPcvN/OIRfN3YCfKLOgKvuCs4uam16MOnbLi8s4M8CAr514/T/xbeGxgQ6ZcH/NOnOqXSwVG/hydeoXCGrFO+DNG1h7oAj29XXUkav292KmJGI7qqkH8aw/LxCDdi6B3VvDjj40iOjVtRNOpqYClldMqO9OSDjys6iJfRExaIXVOBhZqdny1F0jJqfBZ+m4FuxTuOKOyPg8MF");
            //credentials = new SessionCredentialsProviderFactory().g
        	//SessionCredentialsProviderFactory
        	
            sqs = new AmazonSQSClient(cred);
            sqs.setRegion(Region.getRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)));
        } 
        catch (Exception e) 
        {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
	}
	
	public static String createQueue(String queueName)
	{
        try 
        {
            // Create a queue
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName);
            queueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
		return queueUrl;
	}
	
	public static List<String> fetchAllQueues()
	{
		ArrayList<String> queueList = new ArrayList<String>(); 
		try
		{
			for (String queueUrl : sqs.listQueues().getQueueUrls()) 
			{
				queueList.add(queueUrl);
	        }
		} 
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
		
		return queueList;
	}
	
	public static void sendMessage(String queueURL, String queueMessage)
	{
		try
		{
			sqs.sendMessage(queueURL, queueMessage);
		} 
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
	}
	
	public static List<Message> receiveMessages(String queueURL)
	{
		List<Message> messages = null;
		try
		{
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueURL);
	        messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
	        for (Message message : messages) {
	            System.out.println("  Message");
	            System.out.println("    MessageId:     " + message.getMessageId());
	            System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
	            System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
	            System.out.println("    Body:          " + message.getBody());
	            for (Entry<String, String> entry : message.getAttributes().entrySet()) {
	                System.out.println("  Attribute");
	                System.out.println("    Name:  " + entry.getKey());
	                System.out.println("    Value: " + entry.getValue());
	            }
	        }
		} 
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        
        return messages;
	}
	
	public static void deleteMessage(String queueURL)
	{
		try
		{
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueURL);
			List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
			String messageReceiptHandle = messages.get(0).getReceiptHandle();
	        sqs.deleteMessage(new DeleteMessageRequest(queueURL, messageReceiptHandle));
		} 
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
	}
	
	public static void deleteQueue(String queueURL)
	{
		try
		{
			sqs.deleteQueue(new DeleteQueueRequest(queueURL));
		} 
		catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
	}
}
