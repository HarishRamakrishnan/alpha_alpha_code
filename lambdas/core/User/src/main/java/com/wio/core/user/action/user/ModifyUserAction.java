package com.wio.core.user.action.user;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;
import com.wio.core.user.helper.PasswordHelper;

public class ModifyUserAction extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        JsonObject body = getBodyFromRequest(request);
        
        User input = getGson().fromJson(body, User.class);
        
        User user = null;
        
        try 
        {
			UserValidator.validateCommonValues(input);
        	        	
        	user = genDAO.getGenericObject(User.class, input.getUserID());
        				
	        if(user == null)
	            throw new DAOException("User does not exist... ");
	        	        
			//User Details Modification
	        if(RequestValidator.isEmptyField(input.getNewPassword()))
	        {        	
	        	UserValidator.validateOnModify(input);	        	
	        	updateModifiedUserDetails(user, input);
	        }
	        else //Change password
	        {
	        	UserValidator.validateToChangePassword(input);	        	
	        	updateModifiedPassword(user, input);	        	
	        }	        
	        genDAO.updateGenericObject(user);
		} 
        catch (DAOException e) {
        	logger.log("Error while modifying user\n" + e.getMessage());
        	throw new InternalErrorException(e.getMessage());
		}        
      
        User u = new User();
        u.setUserID(user.getUserID());
        
        return getGson().toJson(u, User.class);        
    }

    private void updateModifiedUserDetails(User user, User input)
    {    	
    	if(!RequestValidator.isEmptyField(input.getFirstName()) )
    	{
    		user.setFirstName(input.getFirstName());
    	}
    	if(!RequestValidator.isEmptyField(input.getLastName()) )
    	{
    		user.setLastName(input.getLastName());
    	}

    	if(!RequestValidator.isEmptyField(input.getPhoneNumber()) )
    	{
    		user.setPhoneNumber(input.getPhoneNumber());
    	}

    	if(!RequestValidator.isEmptyField(input.getEmail()) )
    	{
    		user.setEmail(input.getEmail());
    	}
    }
    
    private void updateModifiedPassword(User user, User input) throws InternalErrorException
    {
    	try 
    	{
			if (!PasswordHelper.authenticate(input.getCurrentPassword(), user.getPasswordBytes(), user.getSaltBytes())) {
				throw new InternalErrorException(ExceptionMessages.EX_INVALID_PASSWORD);
			}
		} 
    	catch (final NoSuchAlgorithmException e) 
    	{
			logger.log("No algrithm found for password encryption\n" + e.getMessage());
			throw new InternalErrorException(ExceptionMessages.EX_PWD_SALT);
		} 
    	catch (final InvalidKeySpecException e) 
    	{
			logger.log("No KeySpec found for password encryption\n" + e.getMessage());
			throw new InternalErrorException(ExceptionMessages.EX_PWD_ENCRYPT);
		}
    	
    	// if we reach this point we assume that the user is authenticated...	
    	// now, update the user password...
    	
    	try
		{
			byte[] salt = PasswordHelper.generateSalt();
            byte[] encryptedPassword = PasswordHelper.getEncryptedPassword(input.getNewPassword(), salt);
            user.setPassword(ByteBuffer.wrap(encryptedPassword));
            user.setSalt(ByteBuffer.wrap(salt));
		}
		catch (final NoSuchAlgorithmException e) {
            logger.log("No algrithm found for password encryption\n" + e.getMessage());
            throw new InternalErrorException(ExceptionMessages.EX_PWD_SALT);
        } 
		catch (final InvalidKeySpecException e) {
            logger.log("No KeySpec found for password encryption\n" + e.getMessage());
            throw new InternalErrorException(ExceptionMessages.EX_PWD_ENCRYPT);
        }
    }
}
