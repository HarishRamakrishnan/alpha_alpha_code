package com.wio.core.user.request;

import java.util.List;

import com.wio.common.user.model.RolePrivilege;

public class RolePrivilegeRequest 
{
	private String securityToken;
	private List<RolePrivilege> rolePrivileges;
	
	
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	public List<RolePrivilege> getRolePrivileges() {
		return rolePrivileges;
	}
	public void setRolePrivileges(List<RolePrivilege> rolePrivileges) {
		this.rolePrivileges = rolePrivileges;
	}
	
	
}
