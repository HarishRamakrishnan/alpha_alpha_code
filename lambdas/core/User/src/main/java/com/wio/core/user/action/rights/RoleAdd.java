package com.wio.core.user.action.rights;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.Role;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

/**
 * @author mtambour Action used to add a new privilege.
 *         <p/>
 *         POST to /privilege/
 */
public class RoleAdd extends AbstractAction 
{
	private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	/**
	 * Handler implementation for the add privilege action. It expects a request
	 * object in input and returns a serialized response object
	 *
	 * @param request
	 *            Receives a JsonObject containing the body content
	 * @param lambdaContext
	 *            The Lambda context passed by the AWS Lambda environment
	 * @return Returns the privilege and a set of temporary AWS credentials as a
	 *         RegisterUserResponse object
	 * @throws BadRequestException
	 * @throws InternalErrorException
	 * @throws DAOException 
	 */
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
	{
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();

		JsonObject body = getBodyFromRequest(request);

		Role input = getGson().fromJson(body, Role.class);
		
		if(RequestValidator.isEmptyField(input.getUserRole()))
        	throw new BadRequestException("user role "+ExceptionMessages.EX_EMPTY_FIELD);
			
		if(DataValidator.isRoleExist(input.getUserRole()))
		{
			OutputMessage response = new OutputMessage();
        	
        	response.setStatus(200);
        	response.setErrorMessage("User role already exist !");
        	
        	return getGson().toJson(response, OutputMessage.class);
		}
		
		try
		{			
			input.setSystemUser(0);
			genDAO.saveGenericObject(input);
		} 
		catch (final DAOException e)
		{
			logger.log("Error while adding role...\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}

		return getGson().toJson(input, Role.class);
	}
}