package com.wio.core.user.action.rights;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.user.model.PrivilegedAction;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;


/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class PrivilegedActionDelete extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();        
        
        JsonObject body = getBodyFromRequest(request);
		
        PrivilegedAction input = getGson().fromJson(body, PrivilegedAction.class);
             
        try 
        {
        	validate(input);        	
        	
        	genDAO.deleteGenericObject(input);
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while deleting Privileged actions \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(input, PrivilegedAction.class);
    }
    
    public void validate(PrivilegedAction input) throws DAOException, BadRequestException
    {
    	genDAO = GenericDAOFactory.getGenericDAO();
    	
    	if(RequestValidator.isEmptyField(input.getPrivilege()))
    		throw new BadRequestException("Privilege cannot be empty !");
    	
    	List<PrivilegedAction> privileges = genDAO.getGenericObjects(PrivilegedAction.class, null);
    		
		if(!privileges.contains(input))        		
    		throw new DAOException("Privilege "+input.getPrivilege()+" does not exist, cannot delete !");
		
		if(DataValidator.isPrivilegeDependent(input.getPrivilege()))
			throw new DAOException("Privilege "+input.getPrivilege()+" is dependent, cannot delete !");    	
    }
}
