package com.wio.core.user.action.rights;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.user.model.Role;


public class RoleView extends AbstractAction
{	
	 //private LambdaLogger logger = null;
	 private GenericDAO genDAO = null;
	 
	 public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	 {
        //logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        List<Role> roleDetails = new ArrayList<Role>();

        try
        {
        	roleDetails = genDAO.getGenericObjects(Role.class, null);
        	if(roleDetails == null)
	        	throw new InternalErrorException("Role is not available");
        }
        catch (DAOException e) 
    	{
			e.printStackTrace();
		}	        
        return getGson().toJson(roleDetails, List.class);	       
	 }
}
