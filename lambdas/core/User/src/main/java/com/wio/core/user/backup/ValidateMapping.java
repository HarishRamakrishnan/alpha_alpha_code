package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.model.user.ActionPrivilegeMap;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.RequestValidator;
import com.wavesio.server.user.validation.Validator;

public class ValidateMapping 
{
	public static void validateMapping(ActionPrivilegeMap input, LambdaLogger logger) throws BadRequestException, DAOException
	{
		if(RequestValidator.isEmptyField(input.getUserAction()))
        {
        	//logger.log("AP settings Name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Action "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(RequestValidator.isEmptyField(input.getUserPrivilege()))
        {
        	//logger.log("LAN Name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Privilege "+ExceptionMessages.EX_EMPTY_FIELD);
        }
       
        
        if(!Validator.isUserPrivilegeExist(input.getUserPrivilege()))
        {
        	//logger.log("Lan Name is invalid or does not exist, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Privilege is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        }
        
        if(!Validator.isUserActionExist(input.getUserAction()))
        {
        	//logger.log("Lan Name is invalid or does not exist, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Action is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        }
		
	}

}
*/