package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wavesio.server.common.action.AbstractAction;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.exception.InternalErrorException;
import com.wavesio.server.common.generics.dao.FilterCondition;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.generics.dao.QueryHelper;
import com.wavesio.server.common.model.user.ActionPrivilegeMap;
import com.wavesio.server.common.model.user.UserPrivilege;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.RequestValidator;
import com.wavesio.server.user.validation.Validator;
import com.wavesio.shared.common.Enums.DynamoDBObjectKeys;

*//**
 * @author mtambour Action used to add a new privilege.
 *         <p/>
 *         POST to /privilege/
 *//*
public class DeletePrivilegeAction extends AbstractAction 
{

	private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	*//**
	 * Handler implementation for the add privilege action. It expects a request
	 * object in input and returns a serialized response object
	 *
	 * @param request
	 *            Receives a JsonObject containing the body content
	 * @param lambdaContext
	 *            The Lambda context passed by the AWS Lambda environment
	 * @return Returns the privilege and a set of temporary AWS credentials as a
	 *         RegisterUserResponse object
	 * @throws BadRequestException
	 * @throws InternalErrorException
	 * @throws DAOException 
	 *//*
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
	{
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();
		
		JsonObject body = getBodyFromRequest(request);

		UserPrivilege input = getGson().fromJson(body, UserPrivilege.class);

		if(RequestValidator.isEmptyField(input.getPrivilege()))
        {
        	logger.log("user privilege / description cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("user privilege / description "+ExceptionMessages.EX_EMPTY_FIELD);
        }
		
		if(Validator.isUserPrivilegeDependent(input.getPrivilege()))
		{
			logger.log("Privilege cannot be deleted as it already mapped to User Role, Exception at "+this.getClass().getName());
        	throw new BadRequestException(ExceptionMessages.EX_REL_DEPENDENCY_PRIVILEGE);
		}
		
		UserPrivilege newPrivilege = new UserPrivilege();
		newPrivilege.setPrivilege(input.getPrivilege());
		
		List<FilterCondition> privilegeFilter = new ArrayList<FilterCondition>();
		privilegeFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, input.getPrivilege(), ComparisonOperator.EQ));

		List<UserPrivilege> privilege = new ArrayList<UserPrivilege>();
		List<ActionPrivilegeMap> actionMapping =new ArrayList<ActionPrivilegeMap>();
		
		try {
			
			actionMapping = genDAO.getGenericObjects(ActionPrivilegeMap.class, privilegeFilter);
			if(!actionMapping.isEmpty())
			{
				throw new BadRequestException("Given Privilege is mapped with userRole/userAction.....so this userPrivilege cannot be deleted");
			}
			
			privilege=genDAO.getGenericObjects(UserPrivilege.class, privilegeFilter);
			if(privilege.isEmpty())
			{
				throw new BadRequestException("Given Privilege is not exist.....so Check the value of userPrivilege");
			}
			
			
			genDAO.deleteGenericObject(newPrivilege);
		} catch (final DAOException e) {
			logger.log("Error while deleting privilege\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}

		//UserPrivilegeDetails output = new UserPrivilegeDetails();

		//output.setPrivilege(privilege);

		return getGson().toJson(privilege, UserPrivilege.class);
	}
}*/