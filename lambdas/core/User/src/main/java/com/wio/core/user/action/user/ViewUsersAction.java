package com.wio.core.user.action.user;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class ViewUsersAction extends AbstractAction 
{
    //private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        //logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        OutputMessage message = new OutputMessage();
        
        JsonObject body = getBodyFromRequest(request);
        
        User input = getGson().fromJson(body, User.class);

        List<User> output = new ArrayList<>();
        
        try
        {
        	System.out.println("Getting logged user info...");
        	User loggedUser = getUserInfoFromRequest(request);
    	   
        	List<FilterCondition> queryFilter = new ArrayList<FilterCondition>();
        	
        	System.out.println("Input body::: "+body.toString());
        	System.out.println("user id::::"+input.getUserID());
        	
        	if(!RequestValidator.isEmptyField(input.getUserID()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ID, input.getUserID(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getUsername()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_NAME, input.getUsername(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getFirstName()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FIRST_NAME, input.getFirstName(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getLastName()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.LAST_NAME, input.getLastName(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getEmail()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.EMAIL, input.getEmail(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getPhoneNumber()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PHONE_NUMBER, input.getPhoneNumber(), ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getAccountStatus()))
        		queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.ACCOUNT_STATUS, input.getAccountStatus(), ComparisonOperator.EQ));
        	   
        	if(!RequestValidator.isEmptyField(input.getUserRole()))
        	{	
        		if(RequestValidator.isValidRole(input.getUserRole()))
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, input.getUserRole(), ComparisonOperator.EQ));        		
        		else
        		{
        			message.setStatus(200);
                	message.setMessage("Invalid user role applied !");
                	
                	return getGson().toJson(message, OutputMessage.class);
        		}
        		
        	}
        	
        	if(!RequestValidator.isEmptyField(input.getServiceName()))
        	{
        		if(DataValidator.isServiceExist(input.getServiceName()))
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
        		else
        		{
        			message.setStatus(200);
                	message.setMessage("Invalid service applied !");
                	
                	return getGson().toJson(message, OutputMessage.class);
        		}
        	}
        		
        	
        	/*if(!RequestValidator.isEmptyField(input.getServiceName()))
        	{
        		if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_GLOBAL_USER) || loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_GLOBAL_USER))
        		{
    				queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));        			
        		}
        		else if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        		{
        			if(loggedUser.getServiceName().equals(input.getServiceName()))
        				queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ));
        			
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, UserRoleType.USER_ROLE_NETWORK_USER, ComparisonOperator.EQ));        			        			
        		}
        		  
			    if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER) )
					throw new BadRequestException("Invalid User, Authentication/Authorization failed");    		  	  		   
        	}
        	else
        	{
        		if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        		{
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(), ComparisonOperator.EQ));
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, UserRoleType.USER_ROLE_NETWORK_USER, ComparisonOperator.EQ));
        		}    		  
        		else if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
        		{
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(), ComparisonOperator.EQ));
        			queryFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, UserRoleType.USER_ROLE_DEVICE_USER, ComparisonOperator.EQ));
        		}
        	}*/
        	
        	List<User> users = genDAO.getGenericObjects(User.class, queryFilter);  //all the users info as per the filters
        	
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_GLOBAL_USER))
        	{
        		users.forEach(user->{
        			if(user.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER) || user.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
        			{
                    	output.add(prepareResponse(user));
        			}
        			
        			if(loggedUser.getUserID().equals(user.getUserID())) //self user info
    				{
            			output.add(prepareResponse(user));
    				}
        		});
        	}
        	else if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		users.forEach(user->{
        			if(user.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER) && user.getServiceName().equals(loggedUser.getServiceName()))
        			{
                    	output.add(prepareResponse(user));
        			}
        			
        			if(loggedUser.getUserID().equals(user.getUserID())) //self user info
    				{
            			output.add(prepareResponse(user));
    				}
        		});
        	}
        	else if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
        	{
        		users.forEach(user->{
        			if(loggedUser.getUserID().equals(user.getUserID())) //self user info
    				{
            			output.add(prepareResponse(user));
    				}
        		});        		
        	}
        	else //waves admin
        	{
        		users.forEach(user->{
            		output.add(prepareResponse(user));
        		});
        	}
        	
        	
        	if(output.isEmpty())
            {
            	message.setStatus(200);
            	message.setMessage("User(s) does not exist or Not authorized to view requested user info !");
            	
            	return getGson().toJson(message, OutputMessage.class);
            }
        	
        	System.out.print("Users::: " +output);
        }
        catch(DAOException e)
        {
        	throw new InternalErrorException("Error while retrieving the User.");
        }        
        
        return getGson().toJson(output, List.class);    	
    }
    
    
    public User prepareResponse(User user)
    {
    	User u = new User();
    	
    	u.setUserID(user.getUserID());
    	u.setUsername(user.getUsername());
    	u.setFirstName(user.getFirstName());
    	u.setLastName(user.getLastName());
    	u.setEmail(user.getEmail());
    	u.setPhoneNumber(user.getPhoneNumber());
    	u.setUserRole(user.getUserRole());
    	u.setServiceName(user.getServiceName());
    	u.setAccountStatus(user.getAccountStatus());
    	u.setLastLogin(user.getLastLogin());
    	u.setCurrentLogin(user.getCurrentLogin());
    	u.setCreatedBy(user.getCreatedBy());
    	u.setCreationTime(user.getCreationTime());
    	u.setLastUpdatedTime(user.getLastUpdatedTime());
    	
    	return u;
    }
}
