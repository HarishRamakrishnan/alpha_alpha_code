package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wavesio.server.common.action.AbstractAction;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.exception.InternalErrorException;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.model.user.RolePrivilegeMap;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.RequestValidator;


*//**
 * @author mtambour Action used to add a new privilege.
 *         <p/>
 *         POST to /privilege/
 *//*
public class AssignPrivilegeAction extends AbstractAction 
{
	private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	*//**
	 * Handler implementation for the add privilege action. It expects a request
	 * object in input and returns a serialized response object
	 *
	 * @param request
	 *            Receives a JsonObject containing the body content
	 * @param lambdaContext
	 *            The Lambda context passed by the AWS Lambda environment
	 * @return Returns the privilege and a set of temporary AWS credentials as a
	 *         RegisterUserResponse object
	 * @throws BadRequestException
	 * @throws InternalErrorException
	 * @throws DAOException 
	 *//*
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
	{
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();
		
		JsonObject body = getBodyFromRequest(request);

		RolePrivilegeMap input = getGson().fromJson(body, RolePrivilegeMap.class);

		if(RequestValidator.isEmptyField(input.getPrivilege()) || RequestValidator.isEmptyField(input.getUserRole()))
        {
        	logger.log("user privilege / role cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("user privilege / role "+ExceptionMessages.EX_EMPTY_FIELD);
        }
		
		if(!RolePrivilegeDBValidation.isPrivilegeExist(input.getPrivilege()))
			throw new BadRequestException("Privilege not exist. "+ExceptionMessages.EX_INVALID_PRIVILEGE);
		
		if(!RolePrivilegeDBValidation.isRoleExist(input.getUserRole()))
			throw new BadRequestException("User role not exist. "+ExceptionMessages.EX_INVALID_ROLE);
		
		if(RolePrivilegeDBValidation.isRoleAndPrivilegeMapped(input.getUserRole(), input.getPrivilege()))
			throw new BadRequestException("Role & Privilege already assigned/mapped. "+ExceptionMessages.EX_PRIVILEGE_ROLE_MAPPED);
		
		RolePrivilegeMap newPrivilege = new RolePrivilegeMap();
		newPrivilege.setPrivilege(input.getPrivilege());
		newPrivilege.setUserRole(input.getUserRole());
		
		List<RolePrivilegeMap> privileges = new ArrayList<RolePrivilegeMap>();
		
		privileges.add(newPrivilege);
		
		try 
		{
			genDAO.saveGenericObjects(privileges);
		} 
		catch (final DAOException e) 
		{
			logger.log("Error while assigning privilege to action\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}

		//UserRolePrivilegeDetails output = new UserRolePrivilegeDetails();

		//output.setPrivilege(privilege);

		return getGson().toJson(privileges, RolePrivilegeMap.class);
	}
	
	
}*/