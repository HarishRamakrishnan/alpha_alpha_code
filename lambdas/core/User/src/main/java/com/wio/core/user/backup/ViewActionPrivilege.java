package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wavesio.server.common.action.AbstractAction;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.exception.InternalErrorException;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.model.user.ActionPrivilegeMap;


public class ViewActionPrivilege extends AbstractAction 
{
	//private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{
        //logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        		
		List<ActionPrivilegeMap> actionprivilege= new ArrayList<ActionPrivilegeMap>();
		try
		{
			actionprivilege = genDAO.getGenericObjects(ActionPrivilegeMap.class	, null);
			if(actionprivilege == null)
			{
				throw new InternalErrorException("PrivilegeAction map is not available");
			}
		}
		catch (DAOException e) 
    	{
			e.printStackTrace();
		}
		
		return getGson().toJson(actionprivilege , List.class); 	
	 }

}
*/