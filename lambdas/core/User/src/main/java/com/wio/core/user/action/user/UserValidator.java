package com.wio.core.user.action.user;

import com.wio.common.Enums.UserRoleType;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.user.request.UserObject;

public class UserValidator 
{
	public static void validateOnAdd(UserObject user) throws BadRequestException, DAOException
	{
		if(RequestValidator.isEmptyField(user.toString()))
        {
        	//logger.log("Invalid input passd to "+this.getClass().getName());
        	throw new BadRequestException("Input data "+ExceptionMessages.EX_EMPTY_FIELD);
        }
    	
    	if(RequestValidator.isEmptyField(user.getUsername()) || RequestValidator.isEmptyField(user.getPassword()) || RequestValidator.isEmptyField(user.getUserRole()))
        {
        	//logger.log("username / password / role cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("For all users, username / password / role "+ExceptionMessages.EX_EMPTY_FIELD);
        }
    	
    	if(RequestValidator.isEmptyField(user.getEmail()))
        {
        	 //logger.log("Invalid email id passed to " + this.getClass().getName());
             throw new BadRequestException("For user: "+user.getUsername()+", Email ID "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(RequestValidator.isEmptyField(user.getPhoneNumber()))
        {
        	 //logger.log("Invalid phone number passed to " + this.getClass().getName());
             throw new BadRequestException("For user: "+user.getUsername()+", Phone number "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(!DataValidator.isRoleExist(user.getUserRole()))
        {
        	//logger.log("Invalid user role... " + this.getClass().getName());
            throw new BadRequestException("For user: "+user.getUsername()+", "+ExceptionMessages.EX_INVALID_ROLE);
        }
        
        if(UserRoleType.USER_ROLE_WAVES_ADMIN.equalsIgnoreCase(user.getUserRole())){
        	//logger.log("Role WavesUser cannot be added, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Waves Admin cannot be created !"+ExceptionMessages.EX_INVALID_INPUT);	
        }
        
        
        if(!RequestValidator.isEmptyField(user.getServiceName()))
        {        	
        	if(UserRoleType.USER_ROLE_GLOBAL_USER.equals(user.getUserRole()))
        		throw new BadRequestException("For User: "+user.getUsername()+", Global user should not have/aquire any service.."+ExceptionMessages.EX_INVALID_SERVICE);
       
        	
        	if(!DataValidator.isServiceExist(user.getServiceName()))
        		throw new BadRequestException("For User: "+user.getUsername()+", Service does not exist !"+ExceptionMessages.EX_INVALID_SERVICE);
        	
        }
        else
        {
        	if(UserRoleType.USER_ROLE_SERVICE_USER.equals(user.getUserRole()))
            	throw new BadRequestException("For User: "+user.getUsername()+", service "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        
        
        //Field format validation
        if(!RequestValidator.isStrongPassword(user.getPassword().toString()))
        {
        	throw new BadRequestException("For User: "+user.getUsername()+", Password should contains, 6 to 20 length, at least one number, one upper case letter, one lower case letter and one special symbol (�@#$%�) ! , "+ExceptionMessages.EX_INVALID_PASSWORD);
        }
        
        if(!RequestValidator.validateEmail(user.getEmail()))
        {
        	 //logger.log("Invalid email id passed to " + this.getClass().getName());
             throw new BadRequestException("For User: "+user.getUsername()+", "+ExceptionMessages.EX_INVALID_EMAIL);
        }
        
        if(!RequestValidator.validateNumber(user.getPhoneNumber()))
        {
        	 //logger.log("Invalid phone number passed to " + this.getClass().getName());
             throw new BadRequestException("For User: "+user.getUsername()+", "+ExceptionMessages.EX_INVALID_NUMBER);
        }
	}
	
	public static void validateOnModify(User user) throws BadRequestException
	{
		if(!RequestValidator.isEmptyField(user.getUserRole()) || !RequestValidator.isEmptyField(user.getServiceName()))
        	throw new BadRequestException("User Role or Service cannot be modified.");        
		
    	if(!RequestValidator.isEmptyField(user.getEmail()) && !RequestValidator.validateEmail(user.getEmail()))
             throw new BadRequestException(ExceptionMessages.EX_INVALID_EMAIL);
        
    	if(!RequestValidator.isEmptyField(user.getPhoneNumber()) && !RequestValidator.validateNumber(user.getPhoneNumber()))
             throw new BadRequestException(ExceptionMessages.EX_INVALID_NUMBER);
        
	}
	
	public static void validateToChangePassword(User user) throws BadRequestException
	{	
		if(RequestValidator.isEmptyField(user.getCurrentPassword()))
			throw new BadRequestException("Current Password "+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(RequestValidator.isEmptyField(user.getNewPassword()))
			throw new BadRequestException("New Password "+ExceptionMessages.EX_EMPTY_FIELD);
		
		if(user.getCurrentPassword().equals(user.getNewPassword()))
			throw new BadRequestException("Current Password and New Password cannot be same !"+ExceptionMessages.EX_INVALID_INPUT);
		
		if(!RequestValidator.isStrongPassword(user.getNewPassword()))
        	throw new BadRequestException("Password should contains, 6 to 20 length, at least one number, one upper case letter, one lower case letter and one special symbol (�@#$%�) ! , "+ExceptionMessages.EX_INVALID_PASSWORD);
       
	}
	
	public static void validateCommonValues(User input) throws BadRequestException
	{
		if(RequestValidator.isEmptyField(input.toString()))
        	throw new BadRequestException("Input data "+ExceptionMessages.EX_EMPTY_FIELD);
        
		if(RequestValidator.isEmptyField(input.getUserID()))
			throw new BadRequestException("UserID "+ExceptionMessages.EX_EMPTY_FIELD);
        
	}

}
