package com.wio.cwmp.model;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;


@DynamoDBTable(tableName = "TR69_PARAM_PROPERTIES")
public class TR69ServiceToWIOParameterMapping {
	
	private String serviceName;
	private String parameters;
	private List<TR69ServiceParameter> params;
	
	public TR69ServiceToWIOParameterMapping(){
		
	}
	
	@DynamoDBHashKey(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@DynamoDBAttribute(attributeName = "PARAMETERS")
	public String getParameters() {
		return parameters;
	}
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	
	@DynamoDBAttribute(attributeName = "PARAMS")
	public List<TR69ServiceParameter> getParams() {
		return params;
	}
	public void setParams(List<TR69ServiceParameter> params) {
		this.params = params;
	}

}
