/**
 * Product Name : Modus TR-069 Orange
 *
 * Copyright c 2014 Orange
 *
 * This software is distributed under the Apache License, Version 2.0
 * (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 or see the "license.txt" file for
 * more details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Olivier Beyler - Orange
 */

package com.wio.cwmp.rpc.getparam.values;

import org.apache.log4j.Logger;

import com.wio.cwmp.RPCMethodMng;
import com.wio.cwmp.api.Session;

/**
 * The Class Activator.
 */
public final class Activator {

	/** The rpc mng. */
	private RPCMethodMng rpcMng;
	
	private static final Logger log = Logger.getLogger(Activator.class);

	
	/**
	 * 
	 * 
	 * @param session
	 * @throws Exception
	 */
	public void start(final Session session) throws Exception {
		if (session != null) {
			rpcMng =RPCMethodMng.getInstance();
			rpcMng.registerRPCMethod("GetParameterValues");
			rpcMng.registerRPCEncoder("GetParameterValuesResponse", new GetParameterValuesResponseEncoder());
			rpcMng.registerRPCDecoder("GetParameterValues", new GetParameterValuesDecoder());
			log.info("Start RPC Method GetParameterNames");
		} else {
			log.error("Unable to start GetParameterValues: " + "RPCMethodMngService is missing");
		}
	}

	/**
	 * UnRegister GetParameterValues Method, Encoder and Decoder.
	 * 
	 */
	public void stop(final Session session) throws Exception {
		if (rpcMng != null) {
			rpcMng.unregisterRPCMethod("GetParameterValues");
			rpcMng.unregisterRPCEncoder("GetParameterValuesResponse");
			rpcMng.unregisterRPCDecoder("GetParameterValues");
		}
	}
}
