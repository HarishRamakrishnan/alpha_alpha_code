package com.wio.cwmp.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import org.apache.http.ParseException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;
import com.wio.common.config.model.ConfigSource;
import com.wio.common.config.model.Device;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.cwmp.auth.CWMPAuthenticator;
import com.wio.cwmp.dao.TR69ParametersDAO;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.util.RequestValidator;


/**
 * This class contains the main event handler for the Lambda function.
 */
public class ConnectionRequestHandler  implements RequestStreamHandler {

	static LambdaLogger logger = null;
	
	InputStream in = null;
	OutputStream out = null;

	CWMPAuthenticator cwmpAuth = null;
	/**
	 * Should identify the caller to this lambda. If its from API Gateway then,
	 * read the HTTP header for credentials and authenticate the credentials. 
	 * 
	 * @param args
	 */
   
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
		
		in = inputStream;
		out = outputStream;

		logger = context.getLogger();
		logger.log("Loading Java Lambda handler of InputStream..");

		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		JsonStreamParser parser = new JsonStreamParser(reader);

		JsonObject responseJson = new JsonObject();

		String deviceId = "";
		String userName = "";
		String password = "";
		String caller = "";

		try {

			JsonObject event = new JsonObject();

			while (parser.hasNext()) {
				event = parser.next().getAsJsonObject();
			}

			logger.log("Event String = " + event.toString());

			JsonObject params = event.get("params").getAsJsonObject();
			logger.log("params = " + params.toString());

			JsonObject querystring = params.get("querystring").getAsJsonObject();
			logger.log("querystring = " + querystring.toString());

			deviceId = querystring.get("deviceID").getAsString();
			logger.log("deviceId = " + deviceId);

			JsonObject header = params.get("header").getAsJsonObject();
			logger.log("header = " + header.toString());
			
			

			try {
				
				if(deviceId != null){
					logger.log("============Before DAO call ================");
					WIOtoTR69ParamMapper.device = DDBGenericDAO.getInstance().getGenericObject(Device.class, deviceId);
					logger.log("===============After Dao call=============");
					//Check is required, because before ACS triggered, RESTAPI should have made its entry for the deviceID in this CONFIG_SOURCE table.
					// If in case ACS performs updates without this deviceID entry in the table, AP/UI will be outofSync.
					if(WIOtoTR69ParamMapper.device != null){
						
						/*WIOtoTR69ParamMapper.configSource = DDBGenericDAO.getInstance().getGenericObject(ConfigSource.class, WIOtoTR69ParamMapper.device.getDeviceID());
						if(WIOtoTR69ParamMapper.configSource == null){
							logger.log("No record found in CONFIG_SOURCE table for device ID = "+WIOtoTR69ParamMapper.device.getDeviceID());
							System.exit(0);
						}*/
						
						logger.log("Device Record found. "+WIOtoTR69ParamMapper.device.getDeviceName());
					}
					
					
				}else{
					logger.log("No device found in DEVICE table.");
					System.exit(0);
				}
				
			} catch (DAOException e) {
				e.printStackTrace();
				logger.log("DAO Exception while getting device details for the device id = "+deviceId);
			}

			cwmpAuth = new CWMPAuthenticator();
			
			if(header.has("calledBy"))
				caller = header.get("calledBy").getAsString();

			//Check the device status and authenticate the credentials supplied.
			if(RequestValidator.isValidString(caller)) {
				ACSHandler acsStart = new ACSHandler();
		        acsStart.startTR69(deviceId);
			} else {
				
				userName = header.get("userName").getAsString();
				logger.log("userName = " + userName);
				
				password = header.get("password").getAsString();
				logger.log("password = " + password);
				
				if (cwmpAuth.authenticateACS(deviceId, userName, password)) {
					returnResponse("Succesfully Authenticated",200, responseJson, event);
					ACSHandler acsStart = new ACSHandler();
			        acsStart.startTR69(deviceId);
				} else {
					String mesg = WIOtoTR69ParamMapper.errorString;
					WIOtoTR69ParamMapper.errorString = null;
					
					returnResponse(mesg,401, responseJson, event);
				}
			}
			
		} catch (ParseException pex) {
			responseJson.addProperty("statusCode", "400");
			responseJson.addProperty("exception", pex.toString());

		}

		
	}

	private void returnResponse(String message, int statusCode, JsonObject responseJson, JsonObject event)
			throws UnsupportedEncodingException, IOException {
		JsonObject responseBody = new JsonObject();
		//responseBody.addProperty("input", event.toString());
		responseBody.addProperty("message", message);

		JsonObject headerJson = new JsonObject();
		headerJson.addProperty("Response-header", "Auth Response");

		responseJson.addProperty("statusCode", statusCode);
		//responseJson.addProperty("headers", headerJson.toString());
		responseJson.addProperty("body", responseBody.toString());

		logger.log(responseJson.toString());
		OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
		writer.write(responseJson.toString());
		writer.close();
	}
    
    public static void main(String args[]){
    	//logger = context.getLogger();
        // data.setEventFile(context.getDataFile("Events.data"));
 	    String deviceID = "06aa74-f801-a401-b6ytCWMP7117";
         ACSHandler acsStart = new ACSHandler();
         //acsStart.startTR69("06aa74-f801-a401-b6ytgheb7117");
         System.out.println("============Before DAO call ================");
			try {
				WIOtoTR69ParamMapper.device = DDBGenericDAO.getInstance().getGenericObject(Device.class, deviceID);
			} catch (DAOException e) {
				e.printStackTrace();
			}
			System.out.println("===============After Dao call=============");
			if(WIOtoTR69ParamMapper.device != null){
				TR69ParametersDAO.getInstance().scanCWMPDetails(deviceID);
				System.out.println("Device Record found. "+WIOtoTR69ParamMapper.device.getDeviceName());
			}
         
         
         acsStart.startTR69(deviceID);
    }
    
}
