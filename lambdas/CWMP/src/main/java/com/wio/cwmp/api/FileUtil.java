/*--------------------------------------------------------
 * Product Name : modus TR-069
 * Version : 1.1
 * Module Name : TR69ClientAPI
 *
 * Copyright © 2011 France Telecom
 *
 * This software is distributed under the Apache License, Version 2.0
 * (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 or see the "license.txt" file for
 * more details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 
package com.wio.cwmp.api;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
/**
 * The Class FileUtil.
 */
public class FileUtil {
    /** The FILEDEF. */
    public static String DEF_CONFIG_FILE ="defineFile.cfg";
    public static String CONFIG_FILE ="config.cfg";
    public static String USINE_FILE = "usine.txt";
    public static final String CONFIG = "1 Vendor Configuration File";
    public static final String LOG = "2 Vendor Log File";
    public static final String DATAMODEL = "X ORANGE DataModel File";
    public static final String USINE = "X ORANGE Usine File";
    public static final String SAVE = "X ORANGE Data Save File";    
   
    static Logger log = Logger.getLogger(Thread.currentThread().getClass().getName());
    
    public static Logger log1 = Logger.getLogger(FileUtil.class.getName());

    private static Properties props = null;
    /**
     * Gets the File by his short name.
     * @param shortName the short filename
     * @return the file if known
     */
    public static File getFileFromShortName(String shortName) {
        File result = null;
        log.info("short name ==> "+shortName);
        File conf = readConfigFile(DEF_CONFIG_FILE);
        if (!conf.exists()) {
            log.error("Base config file not exist :\"" + DEF_CONFIG_FILE + "\"");
            return null;
        }
        FileReader fr = null;
        BufferedReader buff = null;
        try {
            fr = new FileReader(conf);
            buff = new BufferedReader(fr);
            String line;
            while ((line = buff.readLine()) != null && result == null) {
                String[] tokens = parseLine(line);
                if (tokens != null && tokens.length == 2
                        && shortName.equals(tokens[0])) {
                    result = new File(tokens[1]);
                    log.info("tokens[0] = "+tokens[0]);
                    log.info("tokens[1] = "+tokens[1]);
                }
            }
        } catch (IOException e) {
            log.error("failed to initialize file config"+e);
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                log.error("failed to close fr"+ e);
            }
            try {
                if (buff != null) {
                    buff.close();
                }
            } catch (IOException e) {
                log.error("failed to close buff"+ e);
            }
        }
        return result;
    }
    /**
     * Parse a string to extract the properties and there value (the string is
     * ignored if it starts with a # symbol).
     * @param chaine the chaine
     * @return String array
     */
    public static String[] parseLine(final String chaine) {
        String[] tokens = null;
        if (chaine != null && !chaine.startsWith("#")
                && !"".equals(chaine.trim())) {
            try {
                int pos = chaine.indexOf('=');
                if (pos < 0) {
                    log.error("bad parsing" + chaine);
                    tokens = null;
                } else {
                    tokens = new String[2];
                    tokens[0] = chaine.substring(0, pos);
                    tokens[1] = chaine.substring(pos + 1);
                }
            } catch (Exception e) {
                log.error("bad parsing" + chaine);
                tokens = null;
            }
        }
        return tokens;
    }
    
    /**
     * This method is to read file from resources dir.
     */
    public static File readConfigFile(String fileName){
    	File newFile = null;
    	ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	if(classLoader == null){
    		return null;
    	}
    	log.info("File name ==> "+fileName);
    	//newFile = new File(classLoader.getResource(fileName).getFile());
    	
    	newFile = new File(ClassLoader.getSystemResource(fileName).getFile());
    	
    	return newFile;
    }
    
    public static Boolean isFileExist(String fileName){
    	File nFile = new File(fileName); 
    	
    	if(nFile.exists()){
    		return Boolean.TRUE;
    	}
    	return Boolean.FALSE;
    }
    
    public Properties loadProperties(){
    	
    	ClassLoader classLoader = getClass().getClassLoader();
    	
    	Properties defProp = new Properties();
    	
    	Properties confProp = new Properties();
    	
    	
    	if(props == null){
    		props = new Properties();
    	}
    	try {
    		defProp.load(classLoader.getResourceAsStream(FileUtil.DEF_CONFIG_FILE));
    		confProp.load(classLoader.getResourceAsStream(FileUtil.CONFIG_FILE));
    		//usineProp.load(ClassLoader.getSystemResourceAsStream(USINE_FILE));
    		
			props.putAll(defProp);
			props.putAll(confProp);
			//props.putAll(usineProp);
		} catch (IOException e) {
			log.error("Could not able to locate the filename"+e.getMessage());
		}
    	return props;
    }
    
    public static InputStream getFileInputStream(String fileName){
	 
    		return ClassLoader.getSystemResourceAsStream(fileName);
		
    }
    
}
