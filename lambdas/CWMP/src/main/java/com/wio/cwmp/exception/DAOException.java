package com.wio.cwmp.exception;

/**
 * This exception should not be exposed to Lambda or the client application. It's used internally to identify a DAO issue
 */
public class DAOException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DAOException(String s, Exception e) {
        super(s, e);
    }

    public DAOException(String s) {
        super(s);
    }
}
