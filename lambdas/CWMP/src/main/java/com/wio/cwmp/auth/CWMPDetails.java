package com.wio.cwmp.auth;

public class CWMPDetails {
	
	private String serviceName;
	
	private String userName;
	
	private String password;
	
	private String deviceStatus;

	public CWMPDetails(String userName, String password) {
		this.password= password;
		this.userName = userName;
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	
	
}
