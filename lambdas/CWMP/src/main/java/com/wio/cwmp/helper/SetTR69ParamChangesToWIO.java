package com.wio.cwmp.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.cwmp.dao.TR69ParametersDAO;
import com.wio.cwmp.model.TR69ServiceParameter;
import com.wio.cwmp.rpc.setparam.values.SetParameterValues;
import com.wio.cwmp.soap.Fault;
import com.wio.cwmp.soap.FaultUtil;
import com.wio.cwmp.soap.SetParamValuesFault;
import com.wio.cwmp.util.RequestValidator;
import com.wio.cwmp.util.ValidateRadioSettings;
import com.wio.cwmp.util.WIOConstants;

public class SetTR69ParamChangesToWIO {
	
	
	private Device configuredDevice = new Device();
	private DeviceConfiguration deltaConfig = new DeviceConfiguration();
	
	private List<DeviceRadioSettings> runningRadioList = null;
	private List<WiFiSettings> runningWifiList = WIOtoTR69ParamMapper.wavesParameters.getWifiSetting();
	private List<DeviceRadioSettings> configuredRadioList =  new ArrayList<>();
	private List<WiFiSettings> configWifiList = new ArrayList<>();
	private List<LANSettings> configuredLANList = new ArrayList<>();
	private boolean radioValidationFlag = false;
	
	public static String transID = "";
	
	private static final Logger log = Logger.getLogger(SetTR69ParamChangesToWIO.class);
	
	public DeviceConfiguration updateConfigSetting(TR69ServiceParameter param, Boolean isDBCall) throws Fault {

		TR69ParametersDAO dao = TR69ParametersDAO.getInstance();
		
		WIOtoTR69ParamMapper.deviceConfig = new DeviceConfigurationRequest();

		if (param != null) {
			switch (param.getSettingType()) {

			case WIOConstants.LAN_STRING:
				validateAndSetLanSettingToDB(param, isDBCall, dao );
				break;
			case WIOConstants.WAN_STRING:
				validateAndSetWanSettingToDB(param, isDBCall, dao);
				break;
			case WIOConstants.RADIO_STRING:
				setRadioSettingToDB(param, isDBCall, dao);
				break;
			case WIOConstants.WIFI_STRING:
				validateAndSetWifiSettingToDB(param, isDBCall );
				break;
			case WIOConstants.NETWORK_STRING:
				validateAndSetNWSettingToDB(param, isDBCall );
				break;
			default:
				String value = null;
				switch (param.getWavesParamName()) {

				case "MANUFACTURER":
					if (!isDBCall)
						RequestValidator.isValidString64(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setManufacturer(param.getServiceParamValue());
						deltaConfig.setManufacturer(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getManufacturer();
					}
					break;
				case "MANUFACTURER_OUI":
					if (!isDBCall)
						RequestValidator.isValidManufacturerOUI(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setManufacturerOUI(param.getServiceParamValue());
						deltaConfig.setManufacturerOUI(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getManufacturerOUI();
					}
					break;
				case "PRODUCT_CLASS":
					if (!isDBCall)
						RequestValidator.isValidString64(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setProductClass(param.getServiceParamValue());
						deltaConfig.setProductClass(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getProductClass();
					}
					break;
				case "SERIAL_NUMBER":
					if (!isDBCall)
						RequestValidator.isValidString64(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setSerialNumber(param.getServiceParamValue());
						deltaConfig.setSerialNumber(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getSerialNumber();
					}
					break;
				case "HARDWARE_VERSION":
					if (!isDBCall)
						RequestValidator.isValidAlphaNumaricFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setHardwareVersion(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getHardwareVersion();
					}
					break;
				case "SOFTWARE_VERSION":
					if (!isDBCall)
						RequestValidator.isValidAlphaNumaricFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setSoftwareVersion(param.getServiceParamValue());
						deltaConfig.setSoftwareVersion(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getSoftwareVersion();
					}
					break;
				case "PROVISIONING_CODE":
					if (!isDBCall)
						//validateGatewayValue()
						;
					else{
						WIOtoTR69ParamMapper.wavesParameters.setProvisioningCode(param.getServiceParamValue());
						deltaConfig.setProvisioningCode(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getProvisioningCode();
					}
					break;
				case "SPEC_VERSION":
					if (!isDBCall)
						RequestValidator.isValidAlphaNumaricFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setSpecVersion(param.getServiceParamValue());
						deltaConfig.setSpecVersion(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getSpecVersion();
					}
					break;
				case "DESCRIPTION":
					if (!isDBCall)
						RequestValidator.isValidString(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setDescription(param.getServiceParamValue());
						deltaConfig.setDescription(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getDescription();
					}
					break;
				case "ACS_SERVER_URL":
					if (!isDBCall)
						RequestValidator.isValidURLFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setAcsServerUrl(param.getServiceParamValue());
						deltaConfig.setAcsServerUrl(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getAcsServerUrl();
					}
					break;
				case "ACS_PASSWORD":
					if (!isDBCall)
						RequestValidator.isValidPassword(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setAcsPassword(param.getServiceParamValue());
						deltaConfig.setAcsPassword(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getAcsPassword();
					}
					break;
				case "ACS_USERNAME":
					if (!isDBCall)
						RequestValidator.isValidAlphaNumaricFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setAcsUsername(param.getServiceParamValue());
						deltaConfig.setAcsUsername(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getAcsUsername();
					}
					break;
				case "CPE_USERNAME":
					if (!isDBCall)
						RequestValidator.isValidAlphaNumaricFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setConnectionRequestUsername(param.getServiceParamValue());
						deltaConfig.setConnectionRequestUsername(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getConnectionRequestUsername();
					}
					break;
				case "CPE_PASSWORD":
					if (!isDBCall)
						RequestValidator.isValidPassword(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setConnectionRequestPassword(param.getServiceParamValue());
						deltaConfig.setConnectionRequestPassword(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getConnectionRequestPassword();
					}
					break;
				case "CPE_GATEWAY_URL":
					if (!isDBCall)
						RequestValidator.isValidURLFormat(param.getServiceParamValue());
					else{
						WIOtoTR69ParamMapper.wavesParameters.setConnectionRequestURL(param.getServiceParamValue());
						deltaConfig.setConnectionRequestURL(param.getServiceParamValue());
						value = WIOtoTR69ParamMapper.wavesParameters.getConnectionRequestURL();
					}
					break;
				default:
					break;

				}

				log.info("For the param " + param.getServiceParamName() + " , value to be updated in DB is " + value);
				if(isDBCall){
					dao.updateDBItem(param.getTableName(), WIOConstants.SERVICE_NAME,
							WIOtoTR69ParamMapper.wavesParameters.getHpId(), param.getSettingType(), value);
				}
				break;
			}
		}
		return deltaConfig;
	}

	private void validateAndSetNWSettingToDB(TR69ServiceParameter param, Boolean isSetCall) throws Fault {
		
		WiFiSettings wifiSetting = null;
		NetworkSettings networkSettings = null;
		WiFiSettings runningWifiSettings = getListItem(runningWifiList, (param.getIndex()));
		int configWifiListSize = configWifiList.size();
		if(configWifiListSize > 0 && param.getIndex() <= configWifiListSize) { 
			//wifiSetting = configWifiList.get(param.getIndex());
			wifiSetting = getListItem(configWifiList, (param.getIndex()));
			networkSettings = wifiSetting.getNetworkSettings();
		}else {
			wifiSetting = new WiFiSettings();
			wifiSetting.setRadioName(runningWifiSettings.getRadioName());
			wifiSetting.setWifiID(runningWifiSettings.getWifiID());
			wifiSetting.setWifiName(runningWifiSettings.getWifiName());
			wifiSetting.setIndex(runningWifiSettings.getIndex());
			
			networkSettings = wifiSetting.getNetworkSettings();
			networkSettings.setNetworkID(runningWifiSettings.getNetworkSettings().getNetworkID());
			networkSettings.setNetworkName(runningWifiSettings.getNetworkSettings().getNetworkName());
			wifiSetting.setNetworkSettings(networkSettings);
			configWifiList.add((param.getIndex()-1), wifiSetting);
		}
		
		log.info("Network setting .. " + param.toString());

		switch (param.getWavesParamName()) {

		case "DHCP_SERVER":
			if (!isSetCall)
				RequestValidator.isValidEnable(param.getServiceParamValue());
			else
				networkSettings.setDhcpServer(param.getServiceParamValue());
			break;
		case "IP_POOL_START":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				networkSettings.setIpPoolStart(param.getServiceParamValue());
			break;
		case "IP_POOL_END":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				networkSettings.setIpPoolEnd(param.getServiceParamValue());
			break;
		case "LEASE_TIME":
			if (!isSetCall)
				RequestValidator.isValidLeaseTime(param.getServiceParamValue());
			else
				networkSettings.setLeaseTime(param.getServiceParamValue());
			break;

		default:
			break;

		}
		
		wifiSetting.setNetworkSettings(networkSettings);
		log.info("********* WIFI Setting *********"+ wifiSetting);
		configWifiList.set((param.getIndex()-1), wifiSetting);
		deltaConfig.setWifiSetting(configWifiList);

		log.debug("NW list = " + networkSettings);
		
	}

	private void validateAndSetWifiSettingToDB(TR69ServiceParameter param, Boolean isSetCall) throws Fault {
		
		WiFiSettings wifiSetting = null;
		NetworkSettings networkSettings = null;
		log.info("Param index = "+param.getIndex());
		WiFiSettings runningWifiSettings = getListItem(runningWifiList, (param.getIndex()));
		int configWifiListSize = configWifiList.size();
		if(configWifiListSize > 0 && param.getIndex() <= configWifiListSize) { 
			//wifiSetting = configWifiList.get(param.getIndex());
			wifiSetting = getListItem(configWifiList, (param.getIndex()));
			networkSettings = wifiSetting.getNetworkSettings();
		}else {
			log.info("WiFiSettings  ===>> "+runningWifiSettings);
			wifiSetting = new WiFiSettings();
			wifiSetting.setRadioName(runningWifiSettings.getRadioName());
			wifiSetting.setWifiID(runningWifiSettings.getWifiID());
			wifiSetting.setWifiName(runningWifiSettings.getWifiName());
			wifiSetting.setIndex(runningWifiSettings.getIndex());
			
			networkSettings = new NetworkSettings();
			networkSettings.setNetworkID(runningWifiSettings.getNetworkSettings().getNetworkID());
			networkSettings.setNetworkName(runningWifiSettings.getNetworkSettings().getNetworkName());
			wifiSetting.setNetworkSettings(networkSettings);
			configWifiList.add((param.getIndex()-1), wifiSetting);
		}
		log.info("Wifi setting.. " + param.toString());

		switch (param.getWavesParamName()) {
		case "SSID":
			if (!isSetCall)
				RequestValidator.isValidSSID(param.getServiceParamValue());
			else
				wifiSetting.setSsid(param.getServiceParamValue());
			break;
		case "AUTHENTICATION_TYPE":
			if (!isSetCall)
				// validateGatewayValue()
				;
			else
				wifiSetting.setAuthenticationType(param.getServiceParamValue());
			break;
		case "ENCRYPTION_TYPE":
			if (!isSetCall)
				// validateGatewayValue()
				;
			else
				wifiSetting.setEncryptionType(param.getServiceParamValue());
			break;
		case "PASS_PHRASE":
			if (!isSetCall)
				RequestValidator.isValidPassPhrase(param.getServiceParamValue());
			else
				wifiSetting.setPassPhrase(param.getServiceParamValue());
			break;
		case "ENABLE_WIRELESS":
			if (!isSetCall)
				RequestValidator.isValidEnable(param.getServiceParamValue());
			else
				wifiSetting.setEnableWireless(param.getServiceParamValue());
			break;
		case "HIDDEN_SSID":
			if (!isSetCall) {
				RequestValidator.isValidEnable(param.getServiceParamValue());
			} else {
				wifiSetting.setHiddenSSID(param.getServiceParamValue());
			}

		default:
			break;

		}
		/*wifiList.set(param.getIndex(), wifiSetting); 
		TR69Helper.wavesParameters.setWifiSetting(wifiList);*/
		configWifiList.set((param.getIndex()-1), wifiSetting);
		deltaConfig.setWifiSetting(configWifiList);
		log.debug("WIFI list size = " + configWifiList.size());
		log.info("WIFI list = " + configWifiList);
		
	}

	private void setRadioSettingToDB(TR69ServiceParameter param, Boolean isSetCall, TR69ParametersDAO dao) throws Fault {

		radioValidationFlag = true;
		runningRadioList = WIOtoTR69ParamMapper.wavesParameters.getRadioSetting();
		DeviceRadioSettings runningRadioSetting = getListItem(runningRadioList, (param.getIndex()));
		DeviceRadioSettings radioSetting = null;
		int configRadioSize = configuredRadioList.size();
	
		if (configRadioSize > 0 && param.getIndex() <= configRadioSize) {
			//radioSetting = configuredRadioList.get(param.getIndex()-1);
			radioSetting = getListItem(configuredRadioList, (param.getIndex()));
		}else {
			radioSetting = new DeviceRadioSettings();
			radioSetting.setIndex(runningRadioSetting.getIndex());
			radioSetting.setRadioName(runningRadioSetting.getRadioName());
			configuredRadioList.add((param.getIndex()-1), radioSetting);
			//configHardwareProfile.setRadioSettings(configuredRadioList);
		}
		
		Map<String,String> tempMap = WIOtoTR69ParamMapper.radioParamMap.get((param.getIndex()-1));
		if(tempMap == null){
			tempMap = new HashMap<>();
		}
		log.info("RADIO setting .. " + param.toString());
		//HPRadioSettings radioSetting = radioList.get(param.getIndex());
		if(!isSetCall){
			switch (param.getWavesParamName()) {

			case "BANDWIDTH":
				radioSetting.setBandwidth(param.getServiceParamValue());
				break;
			case "CHANNEL":
				radioSetting.setChannel(param.getServiceParamValue());
				break;
			case "RADIO_NAME":
				if (RequestValidator.isValidString(param.getServiceParamValue())){
					radioSetting.setRadioName(param.getServiceParamValue());
				}
				break;
			case "RADIO_STATUS":
				if (RequestValidator.isValidEnable(param.getServiceParamValue()))
					radioSetting.setRadioStatus(param.getServiceParamValue());
				break;
			case "RADIO_BAND":
				if (RequestValidator.isValidString(param.getServiceParamValue()))
					radioSetting.setRadioBand(param.getServiceParamValue());
				break;
			case "RADIO_MODE":
				if (RequestValidator.isValidString(param.getServiceParamValue()))
					radioSetting.setRadioMode(param.getServiceParamValue());
				break;
			case "TRANSMIT_POWER":
				if (RequestValidator.isValidString(param.getServiceParamValue()))
					radioSetting.setTransmitPower(param.getServiceParamValue());
				break;
			case "BABEL":
				if(RequestValidator.isValidString(param.getServiceParamValue())){
					radioSetting.setBabel(param.getServiceParamValue());
				}
				break;
				
			default:
				break;

			}
			
			runningRadioList.set((param.getIndex()-1), radioSetting);
			configuredRadioList.set((param.getIndex()-1), radioSetting);
			deltaConfig.setRadioSetting(configuredRadioList);
			tempMap.put(param.getWavesParamName(), param.getServiceParamName());
			// Map<Index, Map<WPN, SPN >>
			WIOtoTR69ParamMapper.radioParamMap.put(param.getIndex(),tempMap);
		}
		log.debug("RADIO list size = " + runningRadioList.size());
		log.info("Configured RADIO list = " + configuredRadioList);
		
		//WIOtoTR69ParamMapper.wavesParameters.setRadioSetting(runningRadioList);
		WIOtoTR69ParamMapper.device.setRadioSettings(configuredRadioList);
		configuredDevice.setRadioSettings(configuredRadioList);

		// DB update item.
		if(isSetCall){
			/*dao.updateDBItem(param.getTableName(), WIOConstants.HP_ID, TR69Helper.wavesParameters.getHpId(),
					param.getSettingType(), radioList);*/
			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateDeviceModel(param, configuredDevice), WIOConstants.HARDWARE_PROFILE_TABLE, "CONFIGURED_");
				//DDBGenericDAO.getInstance().saveGenericObject(configHardwareProfile);
			} catch (DAOException e) {
				e.printStackTrace();
				log.error("Exception occured while saving HardwareProfile.");
			}
		}
	}

	private void validateAndSetWanSettingToDB(TR69ServiceParameter param, Boolean isSetCall, TR69ParametersDAO dao) throws Fault {
		
		WANSettings wanSetting = configuredDevice.getWanSettings();
		if(wanSetting == null) {
			wanSetting = new WANSettings();
			wanSetting.setIfName(WIOtoTR69ParamMapper.wavesParameters.getWanSetting().getIfName());
			wanSetting.setName(WIOtoTR69ParamMapper.wavesParameters.getWanSetting().getName());
			wanSetting.setVlanNo(WIOtoTR69ParamMapper.wavesParameters.getWanSetting().getVlanNo());
		}
		log.info("WAN setting .. " + param.toString());
		switch (param.getWavesParamName()) {

		case "GATEWAY":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				wanSetting.setGateway(param.getServiceParamValue());
			break;
		case "SUBNET_MASK":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				wanSetting.setSubnetMask(param.getServiceParamValue());
			break;
		case "IPV6":
			if (!isSetCall){
				
				RequestValidator.isValidIPV6Address(param.getServiceParamValue());
			}else{
				wanSetting.setIpv6(param.getServiceParamValue());
			}
			break;
		case "DNS_PRIMARY":
			if (!isSetCall){
				RequestValidator.isValidDNS(param.getServiceParamValue());
			}else{
				wanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				wanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			}
			break;
		case "IPV4":
			if (!isSetCall)
				RequestValidator.isValidIPV4Address(param.getServiceParamValue());
			else
				wanSetting.setIpv4(param.getServiceParamValue());
			break;
		case "DNS_SECONDARY":
			if (!isSetCall){
				RequestValidator.isValidDNS(param.getServiceParamValue());
			}else{
				wanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				wanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			}
			break;
		case "USERNAME":
			if (!isSetCall)
				RequestValidator.isValidString(param.getServiceParamValue());
			else
				wanSetting.setUsername(param.getServiceParamValue());
			break;
		case "PASSWORD":
			if (!isSetCall)
				RequestValidator.isEmptyField(param.getServiceParamValue());
			else
				wanSetting.setPassword(param.getServiceParamValue());
			break;
		default:
			break;

		}

		WIOtoTR69ParamMapper.wavesParameters.setWanSetting(wanSetting);
		WIOtoTR69ParamMapper.device.setWanSettings(wanSetting);
		configuredDevice.setWanSettings(wanSetting);
		deltaConfig.setWanSetting(wanSetting);
		// DB update item.
		if(isSetCall){
			/*dao.updateDBItem(param.getTableName(), WIOConstants.DEVICE_ID_PK,
					TR69Helper.wavesParameters.getDeviceId(), param.getSettingType(), wanSetting);*/
			
			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateDeviceModel(param,configuredDevice), WIOConstants.DEVICE_TABLE, "CONFIGURED_");
			} catch (DAOException e) {
				log.error("Exception while saving wan setting on configured device table.");
				e.printStackTrace();
			}
		}
	}

	private void validateAndSetLanSettingToDB(TR69ServiceParameter param, Boolean isSetCall, TR69ParametersDAO dao) throws Fault {
		List<LANSettings> runningLANList = WIOtoTR69ParamMapper.wavesParameters.getLanSetting();
		LANSettings runningLANSettings = getListItem(runningLANList, (param.getIndex()));
		LANSettings lanSetting = null;
		int configLANListSize = configuredLANList.size();
		
		if (configLANListSize > 0 && param.getIndex() <= (configLANListSize)) {
			lanSetting = getListItem(configuredLANList, (param.getIndex()));
			//lanSetting = configuredLANList.get(param.getIndex()-1);
		}else {
			lanSetting = new LANSettings();
			lanSetting.setIfName(runningLANSettings.getIfName());
			lanSetting.setDhcp(runningLANSettings.getDhcp());
			lanSetting.setIndex(runningLANSettings.getIndex());
			lanSetting.setName(runningLANSettings.getName());
			lanSetting.setVlanNo(runningLANSettings.getVlanNo());
			configuredLANList.add((param.getIndex()-1), lanSetting);
			configuredDevice.setLanSettings(configuredLANList);
		}
		log.info("LAN setting .. " + param.toString());

		switch (param.getWavesParamName()) {

		case "GATEWAY":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				lanSetting.setGateway(param.getServiceParamValue());
			break;
		case "SUBNET_MASK":
			if (!isSetCall)
				RequestValidator.isvalidIPFormat(param.getServiceParamValue());
			else
				lanSetting.setSubnetMask(param.getServiceParamValue());
			break;
		case "IPV6":
			if (!isSetCall)
				RequestValidator.isValidIPV6Address(param.getServiceParamValue());
			else
				lanSetting.setIpV6(param.getServiceParamValue());
			break;
		case "DNS_PRIMARY":
			if (!isSetCall){
				RequestValidator.isValidDNS(param.getServiceParamValue());
			}else{
				lanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				lanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			}
			break;
		case "IPV4":
			if (!isSetCall)
				RequestValidator.isValidIPV4Address(param.getServiceParamValue());
			else
				lanSetting.setIpV4(param.getServiceParamValue());
			break;
		case "DNS_SECONDARY":
			if (!isSetCall){
				RequestValidator.isValidDNS(param.getServiceParamValue());
			}else{
				lanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				lanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			}
			break;

		default:
			break;
		}
		configuredLANList.set((param.getIndex()-1), lanSetting);
		// update local cache..
		WIOtoTR69ParamMapper.wavesParameters.setLanSetting(runningLANList);
		WIOtoTR69ParamMapper.device.setLanSettings(runningLANList);
		//local instance referencing from cache.
		configuredDevice.setLanSettings(configuredLANList);
		deltaConfig.setLanSetting(configuredLANList);
		
		log.debug("LAN list Size = " + runningLANList.size());
		log.info("LAN list  = " + runningLANList);
		// DB update item.
		if(isSetCall){
			/*dao.updateDBItem(param.getTableName(), WIOConstants.DEVICE_ID_PK,
					TR69Helper.wavesParameters.getDeviceId(), param.getSettingType(), lanList);*/
			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateDeviceModel(param,configuredDevice), WIOConstants.DEVICE_TABLE,"CONFIGURED_");
			} catch (DAOException e) {
				log.error("Exception while saving Lan Settings on configured device table.");
				e.printStackTrace();
			}
		}
	}
	
	private Device populateDeviceModel(TR69ServiceParameter param, Device configDevice) {
		configuredDevice.setDeviceID(WIOtoTR69ParamMapper.device.getDeviceID());
		configuredDevice.setHpID(WIOtoTR69ParamMapper.device.getHpID());
		configuredDevice.setDgID(WIOtoTR69ParamMapper.device.getDgID());
		configuredDevice.setServiceName(WIOtoTR69ParamMapper.device.getServiceName());
		return configuredDevice;
	}
	
	private Network populateNetwork(TR69ServiceParameter param, Network nwSettings) {
		Network network = new Network();
		network.setCreatedBy("CWMP"); 
		network.setDhcpServer(nwSettings.getDhcpServer());
		//network.setNetworkID(WIOtoTR69ParamMapper.wavesParameters.getWifiSetting().get(param.getIndex()).getNetwork().getNetworkID());
		network.setServiceName(nwSettings.getServiceName());
		//network.setNetworkName(WIOtoTR69ParamMapper.wavesParameters.getWifiSetting().get(param.getIndex()).getNetwork().getNetworkName());
		network.setNetworkType(nwSettings.getNetworkType());
		network.setZtEnable(nwSettings.getZtEnable());
		network.setDhcpServer(nwSettings.getDhcpServer());
		network.setIpPoolStart(nwSettings.getIpPoolStart());
		network.setIpPoolEnd(nwSettings.getIpPoolEnd());
		network.setLeaseTime(nwSettings.getLeaseTime());	
		network.setMacFilterList(network.getMacFilterList());
		return network;
	}
	
	private WiFi populateWifiSetting(TR69ServiceParameter param, WiFi wifi) {
		
		WiFiSettings wifiSettings = getListItem(WIOtoTR69ParamMapper.wavesParameters.getWifiSetting(), (param.getIndex()));
		WiFi updWifi = new WiFi();
		updWifi.setWifiID(wifiSettings.getWifiID());
		//wifi.setNetworkID(TR69Helper.wavesParameters.getWifiSetting().get(param.getIndex()).getWifi().getNetworkSettings().getNetworkID());
		updWifi.setWifiName(wifiSettings.getWifiName());
		updWifi.setServiceName(wifi.getServiceName());
		updWifi.setSsid(wifi.getSsid());
		updWifi.setHiddenSSID(wifi.getHiddenSSID());
		updWifi.setCountry(wifi.getCountry());
		updWifi.setNetworkUser(wifi.getNetworkUser());
		updWifi.setAuthenticationType(wifi.getAuthenticationType());
		updWifi.setEncryptionType(wifi.getEncryptionType());
		updWifi.setPassPhrase(wifi.getPassPhrase());
		updWifi.setEnableWireless(wifi.getEnableWireless());
		return updWifi;
	}

	private static <T> T getListItem(List<T> list, int index){
		String indices = String.valueOf(index);
		System.out.println("Size == "+list);
		
		for (T t: list) {
			System.out.println("SimpleName> = "+t.getClass().getSimpleName());
			
			switch (t.getClass().getSimpleName()) {
			
			case "WiFiSettings":
				WiFiSettings wifisettings = (WiFiSettings) t;
				System.out.println("Choosing WiFi based on Index.."+wifisettings.getIndex());
				System.out.println("INDEX ==> "+indices);
				if((wifisettings.getIndex().equals(indices))) {
					System.out.println(" matched wifisettings ===>> "+wifisettings);
					return t;
				}
				break;
			case "HPRadioSettings":
				DeviceRadioSettings radioSettings = (DeviceRadioSettings) t;
				System.out.println(" Choosing Radio based on Index.. "+radioSettings.getIndex());
				if((radioSettings.getIndex().equals(indices))) {
					return t;
				}
				break;
			case "LANSettings":
				LANSettings lanSettings = (LANSettings) t;
				System.out.println("Choosing LAN based on Index.."+lanSettings.getIndex());
				if(lanSettings.getIndex().equals(indices)) {
					return t;
				}
				break;
			default:
				break;
			}

		}
		return null;
	}
	
	public void validateRadioSettings(List lsFault) {
		if(radioValidationFlag ){
			try{
				ValidateRadioSettings.validateValues(runningRadioList);
			}catch (Fault e){
				StringBuffer error;
				error = new StringBuffer(FaultUtil.STR_FAULT_9003);
				error.append(": Unable to find this parameter " + "in the data model");
				lsFault.add(new SetParamValuesFault(e.getFaultParamName(), e.getFaultcode(), e.getFaultstring()));
			}
			
		}
	}


}
