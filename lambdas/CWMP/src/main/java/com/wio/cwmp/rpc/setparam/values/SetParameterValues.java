/**
 * Product Name : Modus TR-069 Orange
 *
 * Copyright c 2014 Orange
 *
 * This software is distributed under the Apache License, Version 2.0
 * (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 or see the "license.txt" file for
 * more details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Olivier Beyler - Orange
 */

package com.wio.cwmp.rpc.setparam.values;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.cwmp.api.RPCMethod;
import com.wio.cwmp.api.Session;
import com.wio.cwmp.dao.TR69ParametersDAO;
import com.wio.cwmp.helper.SetTR69ParamChangesToWIO;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.model.IParameterData;
import com.wio.cwmp.model.Parameter;
import com.wio.cwmp.model.ParameterValueStruct;
import com.wio.cwmp.model.TR69ServiceParameter;
import com.wio.cwmp.soap.Fault;
import com.wio.cwmp.soap.FaultUtil;
import com.wio.cwmp.soap.SetParamValuesFault;
import com.wio.cwmp.util.RequestValidator;
import com.wio.cwmp.util.ValidateRadioSettings;
import com.wio.cwmp.util.WIOConstants;


/**
 * The Class SetParameterValues.
 */
public final class SetParameterValues implements RPCMethod {
	/**
	 * The Constructor.
	 * 
	 * @param pParameterList
	 *            the parameter list
	 * @param pParameterKey
	 *            the parameter key
	 */
	public SetParameterValues(final ParameterValueStruct[] pParameterList, final String pParameterKey) {
		super();
		this.parameterList = pParameterList;
		this.parameterKey = pParameterKey;
	}

	/** The Constant name. */
	private static final String NAME = "SetParameterValues";
	/** The parameter list. */
	private final ParameterValueStruct[] parameterList;
	/** The parameter key. */
	private final String parameterKey;
	
	private DeviceConfiguration deltaConfig = new DeviceConfiguration();

	private static final Logger log = Logger.getLogger(SetParameterValues.class);

	/**
	 * Gets the parameter list.
	 * 
	 * @return the parameter list
	 */
	public ParameterValueStruct[] getParameterList() {
		return parameterList;
	}

	/**
	 * Gets the parameter key.
	 * 
	 * @return the parameter key
	 */
	public String getParameterKey() {
		return parameterKey;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 * @see com.wio.cwmp.api.RPCMethod#getName()
	 */
	public String getName() {
		return NAME;
	}

	/**
	 * Perform.
	 * 
	 * @param pSession - the session
	 * @throws Fault exception
	 */
	public void perform(final Session pSession) throws Fault {
		RPCMethod response;
		List lsFault = new ArrayList();
		IParameterData parameterData = pSession.getParameterData();
		SetTR69ParamChangesToWIO saveChanges = new SetTR69ParamChangesToWIO();
		
		for (int i = 0; i < parameterList.length; i++) {
			try {
				ParameterValueStruct pvs = parameterList[i];
				for (int u = 0; u < i; u++) {
					if (pvs.getName().equals(parameterList[u].getName())) {
						StringBuffer error;
						error = new StringBuffer(FaultUtil.STR_FAULT_9003);
						error.append(": is present more than one time " + "and it's forbidden");
						throw new SetParamValuesFault(pvs.getName(), FaultUtil.FAULT_9003, error.toString());
					}
				}
				TR69ServiceParameter param = parameterData.getTR69Parameter(pvs.getName());
				// unable to find the parameter
				if (param == null) {
					StringBuffer error;
					error = new StringBuffer(FaultUtil.STR_FAULT_9005);
					error.append(": Unable to find this parameter " + "in the data model");
					throw new SetParamValuesFault(pvs.getName(), FaultUtil.FAULT_9005, error.toString());
				}
				//To-Do :- commenting because in GetParameterValues RPC call, isWritable flag will be sent to ACS for a TR69parameter. 
				//checkIfSetIsPossible(param);
				//checkIfSetIsApplicable(param, pvs.getValue());
			} catch (SetParamValuesFault fault) {
				lsFault.add(fault);
			}
		}
		if (!lsFault.isEmpty()) {
			// create a global fault with all detected fault
			throw new Fault(FaultUtil.FAULT_9003, FaultUtil.STR_FAULT_9003, lsFault);
		}
		boolean isAllApplied = true;
		String value;
		String name;
		boolean rebootFlag = false;
		TR69ServiceParameter param;
		Object[] oldValues = storeInitialValue(parameterList, parameterData);
		for (int i = 0; i < parameterList.length; i++) {
			name = parameterList[i].getName();
			value = parameterList[i].getValue();
			param = parameterData.getTR69Parameter(name);
			try {
				if (param == null) {
					throw new Fault(FaultUtil.FAULT_9005, "Attempt to set a invalid parameter.");
				}
				if (!rebootFlag) {
					if (param.getRebootRequired()) {
						rebootFlag = true;
					}
				}
				param.setServiceParamValue(value);
				// Validate the parameter using basic data type validation.
				// SetParameterValues must fail if it would put the CPE in an
				// invalid configuration state
				log.info("SET-PARAM-RPC: Validation started for the parameter - " + param.toString());
				saveChanges.updateConfigSetting(param, Boolean.FALSE);
			} catch (Fault e) {
				StringBuffer error;
				error = new StringBuffer(FaultUtil.STR_FAULT_9003);
				error.append(": Unable to find this parameter " + "in the data model");
				lsFault.add(new SetParamValuesFault(name, e.getFaultcode(), e.getFaultstring()));
			}
			//param.notifyObservers("ACS");
			//isAllApplied &= param.isImmediateChanges();
		}
		
		saveChanges.validateRadioSettings(lsFault);
		
		if (!lsFault.isEmpty()) {
			restoreInitialValue(parameterList, oldValues, parameterData);
			// create a global fault with all detected fault
			throw new Fault(FaultUtil.FAULT_9003, FaultUtil.STR_FAULT_9003, lsFault);
		}else{
			
			for (int i = 0; i < parameterList.length; i++) {
				name = parameterList[i].getName();
				value = parameterList[i].getValue();
				param = parameterData.getTR69Parameter(name);
				param.setServiceParamValue(value);
				// Update the set params to DB.
				deltaConfig = saveChanges.updateConfigSetting(param, Boolean.TRUE);
			}
			/*
			 * should be called only after successfully updating all the parameters.
			 */
			deltaConfig.setDeviceId(WIOtoTR69ParamMapper.wavesParameters.getDeviceId());
			String transId = TR69ParametersDAO.getInstance().createConfigUpdTransaction(deltaConfig);
		}
		
		// add reboot condition along with isAllApplied in order to send the setParamValuesResponse.
		if (!rebootFlag) {
			response = new SetParameterValuesResponse(0);
		} else {
			response = new SetParameterValuesResponse(1);
		}
		parameterData.setParameterKey(parameterKey);
		pSession.doASoapResponse(response);
		
		// Call lambda-lambda client api.
		
	}

	/**
	 * Store initial value.
	 * 
	 * @param values
	 *            the values
	 * @param parameterData
	 *            the parameter data
	 * @return the object[]
	 */
	private Object[] storeInitialValue(final ParameterValueStruct[] values, final IParameterData parameterData) {
		Object[] result = new Object[values.length];
		TR69ServiceParameter param;
		for (int i = 0; i < parameterList.length; i++) {
			param = parameterData.getTR69Parameter(parameterList[i].getName());
			result[i] = param.getServiceParamValue();
		}
		return result;
	}

	/**
	 * Restore initial value.
	 * 
	 * @param values
	 *            the values
	 * @param oldValues
	 *            the old values
	 * @param parameterData
	 *            the parameter data
	 * @throws Fault 
	 */
	private void restoreInitialValue(final ParameterValueStruct[] values, final Object[] oldValues,
			final IParameterData parameterData) throws Fault {
		TR69ServiceParameter param;
		for (int i = 0; i < parameterList.length; i++) {
			param = parameterData.getTR69Parameter(parameterList[i].getName());
			try {
				param.setServiceParamValue(oldValues[i].toString());
			} catch (Fault e) {
				List<SetParamValuesFault> lsFault = null;
				lsFault .add(new SetParamValuesFault(param.getServiceParamName(), e.getFaultcode(), e.getFaultstring()));
				throw new Fault(FaultUtil.FAULT_9003, FaultUtil.STR_FAULT_9003, lsFault);
			}
		}
	}

	/**
	 * Check if set is possible.
	 * 
	 * @param param
	 *            the parameter
	 * @throws SetParamValuesFault
	 *             the set parameter values fault
	 */
	private void checkIfSetIsPossible(final TR69ServiceParameter param) throws SetParamValuesFault {
		if (!param.getWritable()) {
			throw new SetParamValuesFault(param.getServiceParamName(), FaultUtil.FAULT_9008, FaultUtil.STR_FAULT_9008);
		}
	}

	/**
	 * Check if the parameter is compliance with the request.
	 * 
	 * @param param
	 *            parameter
	 * @param value
	 *            new value
	 * @throws SetParamValuesFault
	 *             in case of error.
	 */
	private void checkIfSetIsApplicable(final Parameter param, final String value) throws SetParamValuesFault {
		try {
			param.check(value);
		} catch (Fault exp) {
			throw new SetParamValuesFault(param.getName(), exp.getFaultcode(), exp.getFaultstring(), exp);
		}
	}

	/**
	 * id of the RPCMethod Request by ACS.
	 */
	private String id = null;

	/**
	 * Gets the id.
	 * 
	 * @return the Id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter the Id.
	 */
	public void setId(String id) {
		this.id = id;
	}
}
