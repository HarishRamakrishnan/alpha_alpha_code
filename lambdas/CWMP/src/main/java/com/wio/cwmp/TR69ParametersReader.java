/*
 * Copyright 2011 FranceTelecom.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wio.cwmp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.config.model.DGWifiMap;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.cwmp.dao.TR69ParametersDAO;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.model.IParameterData;
import com.wio.cwmp.model.TR69ServiceParameter;
import com.wio.cwmp.model.TR69ServiceToWIOParameterMapping;
import com.wio.cwmp.util.RequestValidator;
import com.wio.cwmp.util.WIOConstants;

/**
 * The Class PropertiesReader.
 */
public final class TR69ParametersReader {
    
	public static final Logger log = Logger.getLogger(TR69ParametersReader.class);
	
	/** The parameter data. */
    private final IParameterData parameterData;
	
	private Map<String,TR69ServiceParameter> tr69ServiceParamMap = new HashMap<>();
	private TR69ParametersDAO dao = TR69ParametersDAO.getInstance();
	private DDBGenericDAO genDao = DDBGenericDAO.getInstance();
	private String deviceId = "";
    
    /**
     * Instantiates a new properties reader.
     * @param parameterData the parameter data
     */
    public TR69ParametersReader(IParameterData pParameterData) {
        parameterData = pParameterData;
    }

	
	/**
	 * This method will collect all the details about the device. 
	 * Persist all the data retrieved from DB to TR69WavesParameter POJO.
	 * 
	 */
	
	public DeviceConfiguration readWIOParamsFromDB(String deviceId) {

		this.deviceId = deviceId;
		String serviceName = null;
		String deviceGroupId = null;
		String hwProfileId = null;

		DeviceConfiguration wavesParam = new DeviceConfiguration();
		log.info("Loading WIO Device Configuration Started... ");
		try {
			
			String uuid[] = null;
			if(deviceId.contains("-")){
				uuid = deviceId.split("-");
				if(uuid.length == 4 ){
					
					wavesParam.setManufacturerOUI(uuid[0]);
					wavesParam.setProductClass(uuid[1]+ "-" +uuid[2]);
					wavesParam.setSerialNumber(uuid[3]);
				} else {
					log.info("Not a valid DeviceID format [ " + deviceId + " ]");
					return null;
				}
			} else {
				log.info("Not a valid DeviceID format [ " + deviceId + " ]");
				return null;
			}
			
			if(WIOtoTR69ParamMapper.device == null){
				log.error("Device details doesn't exist for the DEVICE_ID = " + deviceId + " in "
						+ WIOConstants.DEVICE_TABLE);

				log.error("INFORM RPC will not be sent to ACS.");
				return null; 
			}
			
			serviceName = WIOtoTR69ParamMapper.device.getServiceName();
			deviceGroupId = WIOtoTR69ParamMapper.device.getDgID();
			hwProfileId = WIOtoTR69ParamMapper.device.getHpID();
			
			//TODO Need to finalize whether validation should be done here.
			if(RequestValidator.isValidString(serviceName) && RequestValidator.isValidString(deviceId) && RequestValidator.isValidString(hwProfileId)){
				wavesParam.setServiceName(serviceName);
				wavesParam.setDeviceId(deviceId);
				wavesParam.setHpId(hwProfileId);
			}else{
				return null;
			}

			log.info("Service_Name = " + serviceName + ", DG_ID = " + deviceGroupId + ", HP_ID = "
					+ hwProfileId);
			
			WIOtoTR69ParamMapper.hardwareProfile = genDao.getGenericObject(HardwareProfile.class, hwProfileId);
			
			if(WIOtoTR69ParamMapper.hardwareProfile != null) {
				WIOtoTR69ParamMapper.hardware = genDao.getGenericObject(Hardware.class, WIOtoTR69ParamMapper.hardwareProfile.getHwID());
			} else {
				return null;
			}
				
			setLanSettingsInPOJO(deviceId, wavesParam, WIOtoTR69ParamMapper.device);
			setWanSettingsInPOJO(deviceId, wavesParam, WIOtoTR69ParamMapper.device);
			setServiceDetailsInPOJO(deviceId, wavesParam, WIOtoTR69ParamMapper.service);
			setRadioSettingsInPOJO(wavesParam, WIOtoTR69ParamMapper.device);
			setWIFIAndNWSettingsInPOJO(wavesParam, deviceGroupId);
			
			wavesParam.setHardwareVersion("H2.0");
//			wavesParam.setProductClass("APWavesIO");
			wavesParam.setProvisioningCode("OOEEFF");
			wavesParam.setManufacturer("BAYONETTE");
//			wavesParam.setManufacturerOUI("0AEE1D");
//			wavesParam.setSerialNumber("ABCCCC111411");
			wavesParam.setSoftwareVersion("S2.0");
			wavesParam.setSpecVersion("00001");
			wavesParam.setDescription("TEST LAMBDA");
			
			WIOtoTR69ParamMapper.convertPOJOToJson(wavesParam);
			log.info("Loading WIO Device Configuration Completed... ");
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception while reading tr69 params -" + e.getMessage());
		}
		return wavesParam;
	}

	/**
	 * Get the DG_ID from device table to query DG_WIFI_MAP to get Wifi
	 * ID and get all Wifi Settings.
	 */
	private void setWIFIAndNWSettingsInPOJO(DeviceConfiguration wavesParam, String deviceGroupId) throws DAOException {
		List<WiFiSettings> wifiList;
		List<Network> networkList;
		List<FilterCondition> filterConditions = new ArrayList<>();
		
		FilterCondition condition = new FilterCondition();
		condition.setParam(WIOConstants.DG_ID);
		condition.setOperator(ComparisonOperator.EQ);
		condition.setValue(deviceGroupId);
		
		condition.setParam(WIOConstants.DEVICE_ID);
		condition.setOperator(ComparisonOperator.EQ);
		condition.setValue(deviceId);

		filterConditions.add(condition);

		try{
			List<DGWifiMap> dgWifiList = genDao.getGenericObjects(DGWifiMap.class, filterConditions);

			log.info("No of WiFi's for the device = "+dgWifiList.size());
			
			wifiList = new ArrayList<>();
			networkList = new ArrayList<>();
			WiFiSettings wifiSetting = null ;
			NetworkSettings networkSetting = null;

			for (DGWifiMap dgWifi : dgWifiList) {

				WiFi wifi = null;
				wifiSetting = new WiFiSettings();
				if ((wifi = genDao.getGenericObject(WiFi.class, dgWifi.getWifiID())) != null) {

					Network network = genDao.getGenericObject(Network.class, wifi.getNetworkID());
					
					wifiSetting.setWifiID(wifi.getWifiID());
					wifiSetting.setWifiName(wifi.getWifiName());
					wifiSetting.setServiceName(wifi.getServiceName());
					wifiSetting.setSsid(wifi.getSsid());
					wifiSetting.setHiddenSSID(wifi.getHiddenSSID());
					wifiSetting.setCountry(wifi.getCountry());
					wifiSetting.setNetworkUser(wifi.getNetworkUser());
					wifiSetting.setAuthenticationType(wifi.getAuthenticationType());
					wifiSetting.setEncryptionType(wifi.getEncryptionType());
					wifiSetting.setPassPhrase(wifi.getPassPhrase());
					wifiSetting.setEnableWireless(wifi.getEnableWireless());
					wifiSetting.setEapType(wifi.getEapType());
					wifiSetting.setIndex(wifi.getIndex());
					wifiSetting.setMode(wifi.getMode());
					wifiSetting.setRadioName(wifi.getRadioName());

					if (network != null) {
						networkSetting = new NetworkSettings();
						networkSetting.setNetworkID(network.getNetworkID());
			    		networkSetting.setServiceName(network.getServiceName());
			    		networkSetting.setNetworkName(network.getNetworkName());
			    		networkSetting.setNetworkType(network.getNetworkType());
			    		networkSetting.setZtEnable(network.getZtEnable());
			    		networkSetting.setDhcpServer(network.getDhcpServer());
			    		networkSetting.setIpPoolStart(network.getIpPoolStart());
			    		networkSetting.setIpPoolEnd(network.getIpPoolEnd());
			    		networkSetting.setLeaseTime(network.getLeaseTime());
			    		networkSetting.setiFName(network.getIfName());
			    		networkSetting.setMacFilterList(network.getMacFilterList());
						wifiSetting.setNetworkSettings(networkSetting);
						networkList.add(network);
					}
					wifiList.add(wifiSetting);
				}
			}

			wavesParam.setWifiSetting(wifiList);
			log.info("WIFI Settings ==>> " + wavesParam.getWifiSetting());
		}catch (DAOException e) {
			log.error("DAOException occured while fetching Wifi and N/w details.");
		}
		
	}

	/**
	 * Retrieving LAN settings from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private void setWanSettingsInPOJO(String deviceId, DeviceConfiguration wavesParam, Device device)
			throws Exception {
		if (device.getWanSettings() != null) {
			wavesParam.setWanSetting(device.getWanSettings());
			log.info("WAN Settings ==>> " + wavesParam.getWanSetting().toString());
		} else {
			log.error("WAN Settings doesn't exist for the DEVICE_ID = " + deviceId + " in "
					+ WIOConstants.DEVICE_TABLE);
			log.error("WAN Settings will not be available in INFORM RPC. ");
		}
	}

	/**
	 * Retrieving WAN settings from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private void setLanSettingsInPOJO(String deviceId, DeviceConfiguration wavesParam, Device device)
			throws Exception {

		if (device.getLanSettings() != null) {
			wavesParam.setLanSetting(device.getLanSettings());
			log.info("LAN Settings ==>> " + wavesParam.getLanSetting());
		} else {
			log.error("WAN Settings doesn't exist for the DEVICE_ID = " + deviceId + " in "
					+ WIOConstants.DEVICE_TABLE);
			log.error("WAN Settings will not be available in INFORM RPC. ");
		}
	}

	/**
	 * Retrieving the radio settings from H/W Profile table and setting
	 * it in the DeviceConfiguration class.
	 */
	private void setRadioSettingsInPOJO(DeviceConfiguration wavesParam, Device device)
			throws DAOException, Exception {
			
			if(device.getRadioSettings() != null){
				
				wavesParam.setRadioSetting(device.getRadioSettings());
				log.info("Radio Settings ==>> " + device.getRadioSettings());
				
			}else{
				log.error("HW Profile doesn't exist in "+WIOConstants.HARDWARE_PROFILE_TABLE);
				log.error("Radio Settings will not be available in INFORM RPC. ");
			}

	}

	/**
	 * Retrieving Service table values from DB and setting it in the
	 * DeviceConfiguration class.
	 */
	private void setServiceDetailsInPOJO(String deviceId, DeviceConfiguration wavesParam, Service service) throws DAOException {
		
		if(service != null){
			wavesParam.setConnectionRequestPassword(service.getCpePassword());
			wavesParam.setConnectionRequestUsername(service.getCpeUsername());
			wavesParam.setConnectionRequestURL(service.getCpeGatewayURL());
			
			if(service.getCpeGatewayURL().contains("xxx")) {
				String cpeGatewayURL = service.getCpeGatewayURL().replace("xxx", deviceId);
				wavesParam.setConnectionRequestURL(cpeGatewayURL);
			}
			
			wavesParam.setAcsServerUrl(service.getAcsURL());
			wavesParam.setAcsUsername(service.getAcsUsername());
			wavesParam.setAcsPassword(service.getAcsPassword());
		} else {
			log.error("Service doesn't exist for the device " +deviceId);
			log.error("Service details will not be available in INFORM RPC.");
		}
	}

	
	public void populateParameterData(DeviceConfiguration wavesParameters) {
		
		String serviceName = null;
		TR69ServiceToWIOParameterMapping paramMapping = null;
		
		if (wavesParameters != null && wavesParameters.getServiceName() != null) {
			serviceName = wavesParameters.getServiceName();
		} else {
			log.error("Could not find service name mapped for the device." );
			return;
		}
		/** 
		 * Query for the Service Provider parameter from mapping table
		 *	for the given serviceProviderName.
		 */
		log.info("Started to populate parameter data for the service provider "+serviceName);
		paramMapping = dao.readParamPropertiesMapping(serviceName);
		
		if(paramMapping == null){
			log.error("No Parameter mapping record found for service name = "+serviceName);
			return;
		}
		tr69ServiceParamMap = WIOtoTR69ParamMapper.convertTR69ParamPropToMap(paramMapping, wavesParameters);
		log.debug("tr69ServeParamMap size ==>> " + tr69ServiceParamMap.size());
		Set<String> keys = tr69ServiceParamMap.keySet();

		log.info("Service Param key set ==>> " + keys.toString());

		for (String key : keys) {
			parameterData.addTR69Parameter(tr69ServiceParamMap.get(key));
		}

		log.info("Parameter Data list size ==>> " + parameterData.getParametersArray().length);

	}
	
//	private List<?> getConfigSettings(Item docItem, String settingsType) throws JSONException, Exception {
//
//		List<LANSettings> lanSettingList = null;
//		List<HPRadioSettings> radioSettingList = null;
//		JSONArray arr = null;
//
//		if (docItem != null) {
//			JSONObject obj = new JSONObject(docItem.toJSON());
//			switch (settingsType) {
//
//			case WIOConstants.LAN_STRING:
//				lanSettingList = new ArrayList<>();
//				arr = obj.getJSONArray(WIOConstants.LAN_SETTINGS);
//				log.debug("obj.getJSONArray(LAN_SETTINGS" + arr);
//
//				for (int i = 0; i < arr.length(); i++) {
//					LANSettings lanSett = TR69Helper.convertToLanSetting(arr.getJSONObject(i));
//					lanSettingList.add(lanSett);
//				}
//
//				return lanSettingList;
//
//			case WIOConstants.RADIO_STRING:
//				radioSettingList = new ArrayList<>();
//				arr = obj.getJSONArray(WIOConstants.RADIO_SETTINGS);
//				for (int i = 0; i < arr.length(); i++) {
//					HPRadioSettings radioSett = TR69Helper.convertToRadioSetting(arr.getJSONObject(i));
//					radioSettingList.add(radioSett);
//				}
//				return radioSettingList;
//
//			default:
//				break;
//			}
//
//		}
//		log.error("DB Item is not available to construct LAN/RADIO settings.");
//		return null;
//	}
    
}    

