/*--------------------------------------------------------
 * Product Name : modus TR-069
 * Version : 1.1
 * Module Name : TR69ClientAPI
 *
 * Copyright © 2011 France Telecom
 *
 * This software is distributed under the Apache License, Version 2.0
 * (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 or see the "license.txt" file for
 * more details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 
package com.wio.cwmp.helper;
/*--------------------------------------------------------
 * Version : 1.1
 * Module Name : TR69ClientAPI
 *
 * Copyright © 2011 France Telecom
 *
 * This software is distributed under the Apache License, Version 2.0
 * (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 or see the "license.txt" file for
 * more details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */ 
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

import com.wio.cwmp.api.EventCode;
import com.wio.cwmp.api.Getter;
import com.wio.cwmp.api.ICom;
import com.wio.cwmp.api.RPCMethodMngService;
import com.wio.cwmp.model.EventStruct;
import com.wio.cwmp.model.IParameterData;
import com.wio.cwmp.model.TR69ServiceParameter;
import com.wio.cwmp.soap.Fault;

public final class Com implements Observer, ICom {
    /** The sem. */
    private Semaphore sem = new Semaphore();

    /** The Constant STATE_INIT. */
    static final int STATE_INIT = 0;
    /** The Constant STATE_STARTED. */
    static final int STATE_STARTED = 1;
    /** The state. */
    private int state = STATE_INIT;
   
    /** The Constant CONNECTION_URL. */
    private static final String CONNECTION_URL = "ManagementServer.ConnectionRequestURL";
    /** The Constant URL. */
    private static final String URL = "ManagementServer.URL";
    /** The param url. */
    private TR69ServiceParameter paramURL;
    /** The parameter data. */
    private IParameterData parameterData;
    /** The rpc mng. */
    private RPCMethodMngService rpcMng;
    
    public static final Logger log = Logger.getLogger(Com.class);
    /**
     * Instantiates a new com.
     */
    public Com() { 
    }
    public void setRPCMng(final RPCMethodMngService pRpcMng) {
        this.rpcMng = pRpcMng;
    }
    public void setParameterData(final IParameterData pParameterData) {        
        parameterData = pParameterData;       
    }
    
    /**
     * Request new session.
     */
    public void requestNewSessionByHTTPConnection() {
        parameterData.addEvent(new EventStruct(EventCode.CONNECTION_REQUEST,
                ""));
        sem.release();
    }
    /**
     * Request new session.
     */
    public void requestNewSession() {
        sem.release();
    }
    
	public void startCOM() {
		log.info("COM is Invoked ...");
		try {
			// set com
			parameterData.setCom(this);
			parameterData.addObserver(this);
			paramURL = parameterData.createOrRetrieveParameter(parameterData.getRoot() + URL);

		} catch (Exception e1) {
			log.error("Exception while create Com" + e1);
		}

		TR69ServiceParameter param;
		TR69Session session;
		int retry = 0;
		getterUrl getter = new getterUrl();
		try {
			session = new TR69Session(parameterData, rpcMng, retry);
			session.startACSSession();
			retry = 0;
		} catch (Fault e) {
			log.error("Pb :" + e.getFaultstring() + e);

		}
	}
    class getterUrl implements Getter {
        public  Object get(String sessionId){
        	log.info("IN class getterUrl with session ID - "+sessionId);
            return getURL();
        }
        
        public String getURL() {
			return null;
			}
    }
    /** The old random. */
    private static long oldRandom = 0;
    /**
     * Gets the sleeping time before retry.
     * @param retry the retry
     * @return the sleeping time(unit ms) before retry
     */
    protected static long getSleepingTimeBeforeRetry(int retry) {
        long result;  
        double tmp = Math.random();
        switch (retry) {
        case 1:
            result = 5 ;
            break;
        case 2:
            result = 10;
            break;
        case 3:
            result = 20 ;
            break;
        case 4:
            result = 40;
            break;
        case 5:
            result = 80;
            break;
        case 6:
            result = 160;
            break;
        case 7:
            result = 320;
            break;
        case 8:
            result = 640;
            break;
        case 9:
            result = 1280;
            break;
        default:
            result = 2560;
            break;
        }
        result = Math.round(((tmp* result) + result) * 1000);
        if (result == oldRandom) {
        	result = getSleepingTimeBeforeRetry(retry);
        }
        oldRandom = result;
        return result;
    }
    /**
     * Update.
     * @param o the observable
     * @param arg the argument
     * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
     */
    public void update(final Observable o, final Object arg) {
        if (o.equals(paramURL) && state != STATE_INIT) {
            onURLUpdate();
        }
    }
    /**
     * <p>
     * Set the STUNclient.
     * </p>
     * <p>
     * This method is also used to unset the STUN client if it is not more
     * available (stunClient parameter set to null).
     * </p>
     * @param stunClient stun client object or null to unset
     */
   /* public void setSTUNClient(final ISTUNCLient stunClient) {
    	
        this.udpServer.setSTUNClient(stunClient);
    }*/
    /**
     * On url update.
     */
    private void onURLUpdate() {
      
        parameterData.addEvent(new EventStruct(EventCode.BOOTSTRAP, ""));
        sem.release();
    }
    public Object get(String sessionId) {
        // TODO Auto-generated method stub
        return null;
    }
	@Override
	public void setRunning(boolean b) {
		// TODO Auto-generated method stub
		
	}
}
