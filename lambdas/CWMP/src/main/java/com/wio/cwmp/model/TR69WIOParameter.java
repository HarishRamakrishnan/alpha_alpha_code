package com.wio.cwmp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.request.WiFiSettings;



public class TR69WIOParameter {

	@JsonProperty("MANUFACTURER")
	private String manufacturer; // Hardware table.
	@JsonProperty("MANUFACTURER_OUI")
	private String manufacturerOUI;
	@JsonProperty("PRODUCT_CLASS")
	private String productClass;
	@JsonProperty("SERIAL_NUMBER")
	private String serialNumber;
	@JsonProperty("HARDWARE_VERSION")
	private String hardwareVersion;
	@JsonProperty("SOFTWARE_VERSION")
	private String softwareVersion;
	@JsonProperty("PROVISIONING_CODE")
	private String provisioningCode;
	@JsonProperty("SPEC_VERSION")
	private String specVersion;
	@JsonProperty("DESCRIPTION")
	private String description;
	@JsonProperty("SERVICE_NAME")
	private String serviceName;
	@JsonProperty("ACS_SERVER_URL")
	private String acsServerUrl;// service table
	@JsonProperty("CPE_USERNAME")
	private String connectionRequestUsername;// service table
	@JsonProperty("CPE_PASSWORD")
	private String connectionRequestPassword;// service table
	@JsonProperty("ACS_USERNAME")
	private String acsUsername;// service table
	@JsonProperty("ACS_PASSWORD")
	private String acsPassword;// service table
	@JsonProperty("CPE_GATEWAY_URL")
	private String connectionRequestURL;// service table
	@JsonProperty("HP_ID")
	private String hpId;
	@JsonProperty("DEVICE_ID")
	private String deviceId;

	@JsonProperty("LAN_SETTINGS")
	private List<LANSettings> lanSetting;
	@JsonProperty("WAN_SETTINGS")
	private WANSettings wanSetting;
	@JsonProperty("RADIO_SETTINGS")
	private List<HPRadioSettings> radioSetting;
	@JsonProperty("WIFI_SETTINGS")
	private List<WiFiSettings> wifiSetting;
	@JsonProperty("NETWORK")
	private List<Network> networkSetting;


	public TR69WIOParameter() {

	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getManufacturerOUI() {
		return manufacturerOUI;
	}

	public void setManufacturerOUI(String manufacturerOUI) {
		this.manufacturerOUI = manufacturerOUI;
	}

	public String getProductClass() {
		return productClass;
	}

	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getHardwareVersion() {
		return hardwareVersion;
	}

	public void setHardwareVersion(String hardwareVersion) {
		this.hardwareVersion = hardwareVersion;
	}

	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	public String getProvisioningCode() {
		return provisioningCode;
	}

	public void setProvisioningCode(String provisioningCode) {
		this.provisioningCode = provisioningCode;
	}

	public String getSpecVersion() {
		return specVersion;
	}

	public void setSpecVersion(String specVersion) {
		this.specVersion = specVersion;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConnectionRequestPassword() {
		return connectionRequestPassword;
	}

	public void setConnectionRequestPassword(String connectionRequestPassword) {
		this.connectionRequestPassword = connectionRequestPassword;
	}

	public String getAcsServerUrl() {
		return acsServerUrl;
	}

	public void setAcsServerUrl(String acsServerUrl) {
		this.acsServerUrl = acsServerUrl;
	}

	public String getConnectionRequestUsername() {
		return connectionRequestUsername;
	}

	public void setConnectionRequestUsername(String connectionRequestUsername) {
		this.connectionRequestUsername = connectionRequestUsername;
	}

	public String getConnectionRequestURL() {
		return connectionRequestURL;
	}

	public void setConnectionRequestURL(String connectionRequestURL) {
		this.connectionRequestURL = connectionRequestURL;
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public String getAcsUsername() {
		return acsUsername;
	}

	public void setAcsUsername(String acsUsername) {
		this.acsUsername = acsUsername;
	}

	public String getAcsPassword() {
		return acsPassword;
	}

	public void setAcsPassword(String acsPassword) {
		this.acsPassword = acsPassword;
	}
	

	public String getHpId() {
		return hpId;
	}

	public void setHpId(String hpId) {
		this.hpId = hpId;
	}

	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public List<LANSettings> getLanSetting() {
		return lanSetting;
	}

	public void setLanSetting(List<LANSettings> lanSetting) {
		this.lanSetting = lanSetting;
	}

	public WANSettings getWanSetting() {
		return wanSetting;
	}

	public void setWanSetting(WANSettings wanSetting) {
		this.wanSetting = wanSetting;
	}

	public List<WiFiSettings> getWifiSetting() {
		return wifiSetting;
	}

	public void setWifiSetting(List<WiFiSettings> wifiSetting) {
		this.wifiSetting = wifiSetting;
	}

	public List<HPRadioSettings> getRadioSetting() {
		return radioSetting;
	}

	public void setRadioSetting(List<HPRadioSettings> radioSetting) {
		this.radioSetting = radioSetting;
	}
	
	

	public List<Network> getNetworkSetting() {
		return networkSetting;
	}

	public void setNetworkSetting(List<Network> networkSetting) {
		this.networkSetting = networkSetting;
	}

	@Override
	public String toString() {
		return "TR69WavesParameter [manufacturer=" + manufacturer + ", manufacturerOUI=" + manufacturerOUI
				+ ", productClass=" + productClass + ", serialNumber=" + serialNumber + ", hardwareVersion="
				+ hardwareVersion + ", softwareVersion=" + softwareVersion + ", provisioningCode=" + provisioningCode
				+ ", specVersion=" + specVersion + ", description=" + description + ", serviceName=" + serviceName
				+ ", acsServerUrl=" + acsServerUrl + ", connectionRequestUsername=" + connectionRequestUsername
				+ ", connectionRequestPassword=" + connectionRequestPassword + ", acsUsername=" + acsUsername
				+ ", acsPassword=" + acsPassword + ", connectionRequestURL=" + connectionRequestURL + ", hpId=" + hpId
				+ ", deviceId=" + deviceId + ", lanSetting=" + lanSetting + ", wanSetting=" + wanSetting
				+ ", radioSetting=" + radioSetting + ", wifiSetting=" + wifiSetting + ", networkSetting="
				+ networkSetting + "]";
	}
	
}
