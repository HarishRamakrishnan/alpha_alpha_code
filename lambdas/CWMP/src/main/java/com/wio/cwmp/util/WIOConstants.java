package com.wio.cwmp.util;

public class WIOConstants {

	/** The Constant PATH. */
	public static final String PATH = "DeviceInfo.";

	public static final String SESSIONID = "session";

	public static final String CWMPPREFIX = "cwmp";

	public static final String SOAPENVPREFIX = "soap_env";

	public static final String SOAPENCPREFIX = "soap_enc";

	public static final String CWMPNAMESPACE = "urn:dslforum-org:cwmp-1-2";

	public static final String COOKIE = "Set-Cookie";

	public static final String DEVICE = "InternetGatewayDevice.";

	public static final String DEVICEINFO = "DeviceInfo.";

	public static final String MANAGEMENTSERVER = "ManagementServer.";

	public static final String DBERRORCODE_TABLE = "100";

	public static final String DBERRORCODE_ITEM = "101";
	
	public static final String DEVICE_TABLE = "DEVICE";

	public static final String HARDWARE_PROFILE_TABLE = "HARDWARE_PROFILE";

	public static final String SERVICE_TABLE = "SERVICE";

	public static final String LAN_STRING = "LAN";

	public static final String WAN_STRING = "WAN";

	public static final String WIFI_STRING = "WIFI";
	
	public static final String NETWORK_STRING = "NETWORK";

	public static final String RADIO_STRING = "RADIO";
	
	public static final String HP_ID = "HP_ID";

	public static final String DG_ID = "DG_ID";

	public static final String SERVICE_NAME = "SERVICE_NAME";

	public static final String DEVICE_ID = "DEVICE_ID";

	public static final String TABLE_MAPPING_COLUMN = "parameters";
	
	public static final String RADIO_SETTINGS = "RADIO_SETTINGS";

	public static final String LAN_SETTINGS = "LAN_SETTINGS";

	public static final String WAN_SETTINGS = "WAN_SETTINGS";

	public static final String CPE_PASSWORD = "CPE_PASSWORD";

	public static final String CPE_USERNAME = "CPE_USERNAME";

	public static final String CPE_GATEWAY_URL = "CPE_GATEWAY_URL";

	public static final String ACS_SERVER_URL = "ACS_SERVER_URL";

	public static final String ACS_PASSWORD = "ACS_PASSWORD";

	public static final String ACS_USERNAME = "ACS_USERNAME";
	
	public static final String CHANNEL1 = "1,2,3,4,5,6,7,8,9,10,11,Auto";
	
	public static final String CHANNEL2 = "36,44,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140,149,153,157,161,165, Auto";

}
