package com.wio.cwmp.auth;

import org.apache.log4j.Logger;

import com.wio.cwmp.dao.TR69ParametersDAO;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.util.RequestValidator;

public class CWMPAuthenticator implements Authentication{
	
	private static final Logger log = Logger.getLogger(CWMPAuthenticator.class);
	
	public CWMPAuthenticator() {}
	
	/**
	 * 1. Query DB 
	 * 		a. Get the service provider name based on Device ID.
	 * 
	 *  	b. Get the CWMP credentials based on service provider name.
	 * 
	 * 2. Authenticate CWMP credentials with the credentials supplied by ACS.
	 * 
	 */

	private TR69ParametersDAO inst = TR69ParametersDAO.getInstance();
	
	public boolean authenticateACS(String deviceID, String userName, String password){
		
		boolean flag = false;
		
		log.debug("Authenticating ACS");
		CWMPDetails credentials = inst.scanCWMPDetails(deviceID);
		//Helper method to validate string.
		
		if( credentials != null && credentials.getUserName() != null && credentials.getPassword() != null) {
						
			if ( userName.equals(credentials.getUserName()) && password.equals(credentials.getPassword())){
				flag = true;
				log.debug("Authenticated = "+flag);
			}else{
				if(!RequestValidator.isValidString(WIOtoTR69ParamMapper.errorString ) ){
					log.info("Invalid CPE UserName or Password");
					
					WIOtoTR69ParamMapper.errorString = "Invalid CPE UserName or Password";
				}
				
			}
		}
		
		return flag;
	}

	
}
