<#compress>

<#assign modifiedradios = [] />

#!/bin/bash


ucihandle()                                         
{
if "$@"; then
   echo "uci command is executed successfully is $@"
else
   echo "uci command error is $@"
fi
}

<#list network.lans as lan>

    <#assign i = lan.index >
            
    <#if lan.type??>        ucihandle uci set network.lan${i}.type='${lan.type}'</#if>
        
    <#if lan.ifname??>        ucihandle uci set network.lan${i}.ifname='${lan.ifname}'</#if>
        
    <#if lan.proto??>        ucihandle uci set network.lan${i}.proto='${lan.proto}'</#if>
        
    <#if lan.ipaddr??>        ucihandle uci set network.lan${i}.ipaddr='${lan.ipaddr}'</#if>
        
    <#if lan.netmask??>        ucihandle uci set network.lan${i}.netmask='${lan.netmask}'</#if>
        
    <#if lan.ip6assign??>    ucihandle uci set network.lan${i}.ip6assign='${lan.ip6assign}'</#if>
  
</#list>

<#list network.wans as wan>

    <#assign i = wan.index >
            
    <#if wan.type??>    ucihandle uci set network.wan${i}.ifname='${wan.ifname}'</#if>
        
    <#if wan.ifname??>    ucihandle uci set network.wan${i}.proto='${wan.proto}'</#if>
    
</#list>

<#list network.vlans as vlan>

    <#assign i = vlan.index >
    <#if vlan.device??>    ucihandle uci set network.@switch_vlan[${i}].device='switch0'</#if>
    <#if vlan.vlan??>    ucihandle uci set network.@switch_vlan[${i}].vlan='${vlan.vlan}'</#if>
    <#if vlan.ports??>    ucihandle uci set network.@switch_vlan[${i}].ports='${vlan.ports}'</#if>

</#list>

<#list wireless.radios as radio>

    <#assign i = radio.index >
        
    <#if radio.type??>        ucihandle uci set wireless.radio${i}.type='${radio.type}'</#if>
    
    <#if radio.channel??>    ucihandle uci set wireless.radio${i}.channel='${radio.channel}'</#if>
    
    <#if radio.hwmode??>    ucihandle uci set wireless.radio${i}.hwmode='${radio.hwmode}'</#if>
    
    <#if radio.htmode??>    ucihandle uci set wireless.radio${i}.htmode='${radio.htmode}'</#if>
    
    <#if radio.disabled??>    ucihandle uci set wireless.radio${i}.disabled='${radio.disabled}'</#if>

</#list>

<#list wireless.interfaces as iface>

    <#assign i = iface.index >
        
    <#if iface.device??>        ucihandle uci set wireless.default_radio${i}.device='${iface.device}'</#if>
    
    <#if iface.network??>        ucihandle uci set wireless.default_radio${i}.network='${iface.network}'</#if>
    
    <#if iface.mode??>            ucihandle uci set wireless.default_radio${i}.mode='${iface.mode}'</#if>
    
    <#if iface.ssid??>            ucihandle uci set wireless.default_radio${i}.ssid='${iface.ssid}'</#if>
    
    <#if iface.encryption??>    ucihandle uci set wireless.default_radio${i}.encryption='${iface.encryption}'</#if>
    
    <#if iface.eap_type??>        ucihandle uci set wireless.default_radio${i}.eap_type='${iface.eap_type}'</#if>

   
    
</#list>


<#list dhcp.dhcpLans as lan>

    <#assign i = lan.index >
            
    <#if lan.interface??>    ucihandle uci set dhcp.lan${i}.interface='${lan.iface}'</#if>
        
    <#if lan.dhcpv6??>        ucihandle uci set dhcp.lan${i}.dhcpv6='${lan.dhcpv6}'</#if>
        
    <#if lan.ra??>            ucihandle uci set dhcp.lan${i}.ra='${lan.ra}'</#if>
        
    <#if lan.leasetime??>    ucihandle uci set dhcp.lan${i}.leasetime='${lan.leasetime}'</#if>
        
    <#if lan.start??>        ucihandle uci set dhcp.lan${i}.start='${lan.start}'</#if>
        
    <#if lan.limit??>        ucihandle uci set dhcp.lan${i}.limit='${lan.limit}'</#if>
  
</#list>


<#list dhcp.dhcpWans as wan>

    <#assign i = wan.index >
            
    <#if wan.interface??>    ucihandle uci set network.wan${i}.interface='${wan.iface}'</#if>
        
    <#if wan.ignore??>        ucihandle uci set network.wan${i}.ignore='${wan.ignore}'</#if>
    
</#list>

<#list firewall.defaults as default>

    <#assign i = default.index >
    
    <#if default.syn_flood??>    ucihandle uci set firewall.@defaults[${i}].syn_flood='${default.syn_flood}'</#if>
    
    <#if default.input??>        ucihandle uci set firewall.@defaults[${i}].input='${default.input}'</#if>
    
    <#if default.output??>        ucihandle uci set firewall.@defaults[${i}].output='${default.output}'</#if>
    
    <#if default.forward??>        ucihandle uci set firewall.@defaults[${i}].forward='${default.forward}'</#if>
    
</#list>

<#list firewall.zones as zone>

    <#assign i = zone.index >
    
    <#if zone.name??>        ucihandle uci set firewall.@zone[${i}].name='${zone.name}'</#if>
    
    <#if zone.network??>    ucihandle uci set firewall.@zone[${i}].network='${zone.network}'</#if>
    
    <#if zone.input??>        ucihandle uci set firewall.@zone[${i}].input='${zone.input}'</#if>
    
    <#if zone.output??>        ucihandle uci set firewall.@zone[${i}].output='${zone.output}'</#if>
    
    <#if zone.forward??>    ucihandle uci set firewall.@zone[${i}].forward='${zone.forward}'</#if>
    
</#list>

<#list firewall.forwardings as forwarding>

    <#assign i = forwarding.index >
    
    <#if forwarding.src??>    ucihandle uci set firewall.@forwarding[${i}].src='${forwarding.src}'</#if>
    
    <#if forwarding.dest??>    ucihandle uci set firewall.@forwarding[${i}].dest='${forwarding.dest}'</#if>

</#list>

ucihandle uci commit

</#compress>
