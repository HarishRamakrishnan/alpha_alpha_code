package com.wio.server.config.handler;

import java.util.List;

public class Device {

	private String deviceId;
	List<Network> network;
	
	class Network {
		private String dhcp;
		private String dnsPrimary;
		private String dnsSecondary;
		private String gateway;
		private String ifname;
		private String ipv4;;
		private String ipv6;
	}

}
