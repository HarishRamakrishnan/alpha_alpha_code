package com.wio.template.mqtt;

public class PublisherRunner implements Runnable{

	private String taskName;
	private String topicName;
	
	public PublisherRunner(String taskName, String topicName) {
		this.taskName = taskName;
		this.topicName = topicName;
		Thread.currentThread().setName(taskName);
	}
	
	public void run() {

		System.out.println(taskName+ " Task executed..");
		try {
			JMSPublisher pub = new JMSPublisher(topicName);
			System.out.println(taskName +" = is going to publish the message. ");
			pub.mesgPublisher();
			
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(taskName+ " Task executed..");
	
	}

}
