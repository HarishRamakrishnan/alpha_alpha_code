package com.wio.template.mo;

import java.util.ArrayList;
import java.util.List;

public class WirelessMO {

	public List<RadioMO> radios = new ArrayList<RadioMO>();
	public List<InterfaceMO> interfaces = new ArrayList<InterfaceMO>();
	
	public List<RadioMO> getRadios() {
		return radios;
	}
	public void setRadios(List<RadioMO> radios) {
		this.radios = radios;
	}
	public List<InterfaceMO> getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(List<InterfaceMO> interfaces) {
		this.interfaces = interfaces;
	}
	
	
}
