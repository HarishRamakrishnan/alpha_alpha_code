package com.wio.template.mo;

public class DhcpLanMO {

	public String iface;
	public String dhcpv6;
	public String ra;
	public String leasetime;
	public String start;
	public String limit;
	public String index;
	
	public String getIface() {
		return iface;
	}
	public void setIface(String iface) {
		this.iface = iface;
	}
	public String getDhcpv6() {
		return dhcpv6;
	}
	public void setDhcpv6(String dhcpv6) {
		this.dhcpv6 = dhcpv6;
	}
	public String getRa() {
		return ra;
	}
	public void setRa(String ra) {
		this.ra = ra;
	}
	public String getLeasetime() {
		return leasetime;
	}
	public void setLeasetime(String leasetime) {
		this.leasetime = leasetime;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	
	

}
