package com.wio.template.mqtt;

import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.WIOConfigData;

public class JMSMessageHandler {
	
	public static WIOConfigData constructProtoBufMesg(String transactionId, String script, String actionType) {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		WIOConfigData proto = (WIOConfigData) protocolType.getMesgSerializer("WIO_CONFIG");
		proto.messageConstructor();
		proto.serializeOneOf(3);
		proto.setCmdDetails("Config Settings.");
		proto.setConfigDetails(transactionId, script, actionType);
        System.out.println("wioData.serialize()==>> "+proto.serialize().getAllFields());
		proto.serialize();
		return proto;
	}
	
}
