package com.wio.template.mo;

import java.util.ArrayList;
import java.util.List;

public class NetworkMO {

	public List<LanMO> lans = new ArrayList<LanMO>();
	public List<WanMO> wans = new ArrayList<WanMO>();
	public List<VlanMO> Vlans = new ArrayList<VlanMO>();

	public List<LanMO> getLans() {
		return lans;
	}

	public void setLans(List<LanMO> lans) {
		this.lans = lans;
	}

	public List<WanMO> getWans() {
		return wans;
	}

	public void setWans(List<WanMO> wans) {
		this.wans = wans;
	}

	public List<VlanMO> getVlans() {
		return Vlans;
	}

	public void setVlans(List<VlanMO> vlans) {
		Vlans = vlans;
	}
}