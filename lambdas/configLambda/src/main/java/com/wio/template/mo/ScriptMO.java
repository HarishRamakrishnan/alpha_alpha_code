package com.wio.template.mo;

public class ScriptMO {

	public NetworkMO network = new NetworkMO();
	public WirelessMO wireless = new WirelessMO();
	public DhcpMO dhcp = new DhcpMO();
	public FirewallMO firewall = new FirewallMO();
	
	public NetworkMO getNetwork() {
		return network;
	}
	public void setNetwork(NetworkMO network) {
		this.network = network;
	}
	public WirelessMO getWireless() {
		return wireless;
	}
	public void setWireless(WirelessMO wireless) {
		this.wireless = wireless;
	}
	public DhcpMO getDhcp() {
		return dhcp;
	}
	public void setDhcp(DhcpMO dhcp) {
		this.dhcp = dhcp;
	}
	public FirewallMO getFirewall() {
		return firewall;
	}
	public void setFirewall(FirewallMO firewall) {
		this.firewall = firewall;
	}
}
