package com.wio.template.mo;

import java.util.ArrayList;
import java.util.List;

public class DhcpMO {

	public List<DhcpLanMO> dhcpLans = new ArrayList<DhcpLanMO>();
	public List<DhcpWanMO> dhcpWans = new ArrayList<DhcpWanMO>();
	
	public List<DhcpLanMO> getDhcpLans() {
		return dhcpLans;
	}
	public void setDhcpLans(List<DhcpLanMO> dhcpLans) {
		this.dhcpLans = dhcpLans;
	}
	public List<DhcpWanMO> getDhcpWans() {
		return dhcpWans;
	}
	public void setDhcpWans(List<DhcpWanMO> dhcpWans) {
		this.dhcpWans = dhcpWans;
	}
	
	
	
}
