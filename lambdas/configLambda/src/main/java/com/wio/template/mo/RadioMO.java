package com.wio.template.mo;

public class RadioMO {

	public String type;
	public String channel;
	public String hwmode;
	public String htmode;
	public String disabled;
	public String index;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getHwmode() {
		return hwmode;
	}
	public void setHwmode(String hwmode) {
		this.hwmode = hwmode;
	}
	public String getHtmode() {
		return htmode;
	}
	public void setHtmode(String htmode) {
		this.htmode = htmode;
	}
	public String getDisabled() {
		return disabled;
	}
	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	
	
}
