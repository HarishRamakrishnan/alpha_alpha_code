package com.wio.server.config.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class MQTTPublisher 
{
	private static LambdaLogger logger = null;
	private static String ServerURI = "tcp://"+System.getenv("MQTT_BROKER_IP")+":"+System.getenv("MQTT_BROKER_PORT");
	private static String username = System.getenv("MQTT_BROKER_USERNAME");
	private static String password = System.getenv("MQTT_BROKER_PASSWORD");	
	
	private static int qos = 2;
	private static String clientId = "RestAPILambda";
	
	public static String publishMessage(String messageContent, String topic, Context lambdaContext)
	{
		logger = lambdaContext.getLogger();
		MemoryPersistence persistence = new MemoryPersistence();
		
		try 
		{
            MqttClient messagingClient = new MqttClient(ServerURI, clientId, persistence);
            MqttConnectOptions connectOptions = new MqttConnectOptions();
            
            connectOptions.setCleanSession(true);
            connectOptions.setUserName(username);
            connectOptions.setPassword(password.toCharArray());

            logger.log("Connecting to broker: "+ServerURI);
            
            messagingClient.connect(connectOptions);
            
            logger.log("Connected");
            logger.log("Publishing message: "+messageContent);
            
            MqttMessage message = new MqttMessage(messageContent.getBytes());
            message.setQos(qos);
            
            messagingClient.publish(topic, message);
            
            logger.log("Message published");
            
            messagingClient.disconnect();
            
            logger.log("Disconnected");
            return "SUCCESS";
            //System.exit(0);
        } 
		catch(MqttException me) 
		{
            logger.log("reason "+me.getReasonCode());
            logger.log("msg "+me.getMessage());
            logger.log("loc "+me.getLocalizedMessage());
            logger.log("cause "+me.getCause());
            logger.log("excep "+me);
            me.printStackTrace();
        }		
		return "FAILURE";
	}	
}
