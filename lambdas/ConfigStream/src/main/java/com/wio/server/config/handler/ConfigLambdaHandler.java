package com.wio.server.config.handler;

import java.io.IOException;

import org.json.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.wio.server.config.cwmp.CWMPClient;
import com.wio.server.config.elasticsearch.ElasticSearch;
import com.wio.server.config.mqtt.MQTTPublisher;

public class ConfigLambdaHandler implements RequestHandler<DynamodbEvent, Integer> 
{
	private static LambdaLogger logger = null;
	
    @Override
	public Integer handleRequest(DynamodbEvent event, Context context) 
    {
    	logger = context.getLogger();
    	
		JSONObject newImage = null;
		
		for(DynamodbStreamRecord record : event.getRecords()) 
		{
			try 
			{
				newImage = new JSONObject(record.getDynamodb().getNewImage());
			} 
			catch (Exception e) 
			{
				logger.log("Catch block - ConfigLambda - handleRequest() - Parse Error\n" + e);
			}

			logger.log("Plblishing to Broker...");			
			MQTTPublisher.publishMessage(newImage.toString(), "ConfigLambda", context);
			
			
			logger.log("Updating to ElasticSearch...");	
			try 
			{
				ElasticSearch.update(newImage);
			} 
			catch (IOException e) 
			{
				logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - IOException\n" + e);
			}
			
			logger.log("Invoking CWMP Client...");			
			CWMPClient.invoke("DeviceID");
			
		}
		
		return event.getRecords().size();
	}    
}