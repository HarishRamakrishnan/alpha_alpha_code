package com.wio.worker.log.handler;

import com.wio.worker.log.mqtt.MQTTSubscriber;

public class StandAloneInvokation 
{
	public static void main(String[] args) 
	{
		MQTTSubscriber sub = new MQTTSubscriber();		
		sub.connect();
	}
}
