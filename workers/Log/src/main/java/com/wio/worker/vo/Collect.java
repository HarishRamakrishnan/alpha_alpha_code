package com.wio.worker.vo;

public class Collect {

	private String timestamp;
	private String module;
	private String loglevel;	
	private String logsource;
	private String program;
	private String pid;
	private String message;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getLoglevel() {
		return loglevel;
	}
	public void setLoglevel(String loglevel) {
		this.loglevel = loglevel;
	}
	public String getLogsource() {
		return logsource;
	}
	public void setLogsource(String logsource) {
		this.logsource = logsource;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Collect [timestamp=" + timestamp + ", module=" + module + ", loglevel=" + loglevel + ", logsource="
				+ logsource + ", program=" + program + ", pid=" + pid + ", message=" + message + "]";
	}
	
	
	
}
