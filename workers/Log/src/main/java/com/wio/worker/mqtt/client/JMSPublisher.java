package com.wio.worker.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.json.JSONException;

import com.wio.common.mqtt.MQTTConnection;
import com.wio.common.mqtt.MQTTPublisher;
import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.WioLogProtoData;

public class JMSPublisher {
	MqttClient mqttClient = null;
	
	// Topic filter the client will subscribe to
	final String subTopic = "logtopic";
	//String[] messages = {"Rajesh","Mani","Balaji","Sibkkki","ppppp","iuidh","Arve","Rajesh","Sibkkki","ppppp","iuidh","Pritha","Magesh","Arve",
	//		"Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve","Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve","Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve",
		//	"jkj","Mani","jji","Sibkkki","ppppp","iuidh","dkjkfd",};
	//String[] messages = {"Rajesh","Mani","ppppp","iuidh","dkjkfd"};
	
	/*public void mesgPublisher(){
		String[] messages=createMsg();

		for(int i = 0; i< messages.length; i++){
			
			MQTTConnection conn = new MQTTConnection();
			mqttClient = conn.getConnection("WioTestClient", 10, true, false);
			String response = MQTTPublisher.publishMessage(mqttClient, messages[i], subTopic, 2, false);
			System.out.println("===========>> "+ response);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}*/
	
	public void mesgPublisher(){
		WioLogProtoData messages=createMsg();

		//for(int i = 0; i< messages.length; i++){
			
			MQTTConnection conn = new MQTTConnection();
			mqttClient = conn.getConnection("WioTestClient", 10, true, false);
			boolean response = MQTTPublisher.publishMessage(mqttClient, messages.getWioLogByteString().toByteArray(), subTopic, 2, false);
			System.out.println("===========>> "+ response);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		//}
	}

	private WioLogProtoData createMsg() {
		String[] arr = null;
		WioLogProtoData proto = null;
		try {
			IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");

			proto = (WioLogProtoData) protocolType.getMesgSerializer("WIO_LOG");

			proto.messageConstructor();

			proto.setConfigUpdateDetails("log", "Tue Oct 3 11:17:27 2017", "06aa74-f801-a401-b6ytgheb7117",

					"/var/log/AP.log",

					"Wed Jun 14 10:19:37 2017 daemon.warn dnsmasq-dhcp[1506]: DHCP packet received on wlan0 which has no address");

			proto.serialize();

			System.out.println("=========>> " + proto.serialize().getAllFields());
			
			/*
			 * JSONArray array=new JSONArray();
			 * 
			 * JSONObject jObjd=new JSONObject(); jObjd.put("serviceName", "Airtel");
			 * jObjd.put("deviceId", "96aa74-f801-a401-b6ytgheb7117"); jObjd.put("action",
			 * "collect"); jObjd.put("collect",
			 * "Wed Jun 14 10:19:36 2017 daemon.info hostapd: wlan0: STA c4:0b:cb:54:47:2f WPA: pairwise key handshake completed (WPA)\n Wed Jun 14 10:19:37 2017 daemon.warn dnsmasq-dhcp[1506]: DHCP packet received on wlan0 which has no address"
			 * );
			 * 
			 * array.put(jObjd); JSONObject jObjd2=new JSONObject();
			 * jObjd2.put("serviceName", "Test"); jObjd2.put("action", "collect");
			 * jObjd2.put("deviceId", "06aa74-f801-a401-b6ytgheb7117");
			 * jObjd2.put("collect",
			 * "Wed Jun 14 10:19:36 2017 daemon.info hostapd: wlan0: STA c4:priti WPA: LogMessages"
			 * );
			 * 
			 * array.put(jObjd2);
			 * 
			 * JSONObject jObj3=new JSONObject(); jObj3.put("serviceName", "Voda");
			 * jObj3.put("deviceId", "06aa74-f801-a401-b6ytgheb711"); jObj3.put("collect",
			 * "Wed Jun 14 10:19:36 2017 daemon.info hostapd: wlan0: STA c4:0b:cb:54:47:2f WPA: pairwise key handshake completed (WPA)"
			 * );
			 * 
			 * array.put(jObj3); JSONObject jObjd4=new JSONObject();
			 * jObjd4.put("serviceName", "Test4s"); jObjd4.put("deviceId",
			 * "06aa74-f801-a401-b6ytgheb711"); jObjd4.put("collect",
			 * "Wed Jun 14 10:19:36 2017 daemon.info hostapd: wlan0: STA c4:0b:cb:54:47:2f WPA: pairwise key handshake completed (WPA)"
			 * );
			 * 
			 * array.put(jObjd4);
			 * 
			 * 
			 * 
			 * JSONObject jObjd3=new JSONObject(); jObjd3.put("Name", "sibi");
			 * jObjd3.put("surName", "chak"); array.put(jObjd3);
			 * 
			 * arr=new String[array.length()]; for(int i=0; i<arr.length; i++) {
			 * arr[i]=array.optString(i); } return arr;
			 */
		} catch (JSONException ex) {

		}
		return proto;
	}
}
