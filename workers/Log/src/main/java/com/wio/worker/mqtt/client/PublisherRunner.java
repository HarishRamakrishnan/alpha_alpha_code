package com.wio.worker.mqtt.client;

public class PublisherRunner implements Runnable{

	private String taskName;
	
	public PublisherRunner(String taskName) {
		this.taskName = taskName;
		Thread.currentThread().setName(taskName);
	}
	
	@Override
	public void run() {

		System.out.println(taskName+ " Task executed..");
		try {
			JMSPublisher pub = new JMSPublisher();
			System.out.println(taskName +" = is going to publish the message. ");
			pub.mesgPublisher();
			
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(taskName+ " Task executed..");
	
	}

}
