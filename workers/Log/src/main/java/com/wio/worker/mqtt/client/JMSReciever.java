package com.wio.worker.mqtt.client;

import java.util.concurrent.CountDownLatch;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.wio.common.mqtt.MQTTConnection;

public class JMSReciever {

	MqttClient mqttClient = null;
	// Latch used for synchronizing b/w threads
	CountDownLatch latch = null;
	// Topic filter the client will subscribe to
	final String subTopic = "$share/wiolog/logtopic";
	byte[] currentMessage ;
	
	public JMSReciever(int countDown){
		latch = new CountDownLatch(countDown);
		
	}
	
	public byte[] mesgRecieved(String clientID){
		
		//mqttClient = MQTTConnection.getConnection("Test", 50, true, false);
		MQTTConnection conn = new MQTTConnection();
		
		mqttClient = conn.getConnection(clientID, 50, true, false);
		try {
			mqttClient.setCallback(new MqttCallback() {
				
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					
					currentMessage = message.getPayload();
			        System.out.println(Thread.currentThread().getName()+ "\tReceived a Message!" +
			            "\tTime:    " + System.currentTimeMillis() +
			            "\tTopic:   " + topic + 
			            "\tMessage: " + currentMessage +
			            "\tQoS:     " + message.getQos() + "\n");
			        
			        latch.countDown();
				}
				
				public void deliveryComplete(IMqttDeliveryToken arg0) {
					
				}
				
				public void connectionLost(Throwable cause) {
					System.out.println(Thread.currentThread().getName()+" Connection to MQTT broker lost!" + cause.getMessage());
					latch.countDown();						
				}
			});
			
			// Subscribe client to the topic filter and a QoS level of 0
            System.out.println(Thread.currentThread().getName()+" Subscribing client to topic: " + subTopic);
            mqttClient.subscribe(subTopic, 1);

            // Wait for the message to be received
            try {
                latch.await(); 
            } catch (InterruptedException e) {
                System.out.println("someone awaked me!!");
            }
            
            if(mqttClient.isConnected()){
            	// Disconnect the client
            	mqttClient.disconnect();
            }
            
            System.out.println(Thread.currentThread().getName()+" Exiting");
            return currentMessage;
            //System.exit(0);
		} catch (MqttException e) {
			try {
				mqttClient.disconnect();
			} catch (MqttException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return currentMessage;
	}

}
