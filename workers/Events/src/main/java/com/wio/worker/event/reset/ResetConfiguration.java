package com.wio.worker.event.reset;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public class ResetConfiguration {
	
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	static DynamoDBMapper mapper = new DynamoDBMapper(client);
	
	public static void fetchConfiguration() {
		Table defaultConfiguration = dynamoDB.getTable("DEFAULT_CONFIGURATION");
		defaultConfiguration.getItem("HP_ID", "");
	}
}
