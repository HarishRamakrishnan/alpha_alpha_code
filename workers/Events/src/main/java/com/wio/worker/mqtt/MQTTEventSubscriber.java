package com.wio.worker.mqtt;

import java.io.ByteArrayInputStream;

import javax.json.Json;
import javax.json.JsonReader;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.wio.common.mqtt.MQTTPublisher;
import com.wio.worker.elasticsearch.UpdateElasticSearch;



public class MQTTEventSubscriber implements MqttCallback {

	private  String topic  ;
	private  int qos;
	private String broker;
	private  String clientId;
	private  String userName;
	private   String password;
	public final static Logger logger = Logger.getLogger(MQTTPublisher.class);


	public MQTTEventSubscriber(String topic,int qos,String broker,String clientId,String userName,String password){
		this.topic=topic;
		this.qos=qos;
		this.broker=broker;
		this.clientId=clientId;		 
		this.userName=userName;
		this.password=password;
	}

	MqttClient client;
	MqttConnectOptions connOpt;
	MemoryPersistence persistence = new MemoryPersistence();

	public void connect(){
		try 
		{	   		 
			logger.info("Intiating Broker:: "+broker);
			client = new MqttClient(broker, clientId, persistence);

			logger.info("Setting up connection properties...");
			logger.info("Broker is:: "+broker +"Client id"+clientId+"topic is :"+topic);


			this.connOpt = new MqttConnectOptions();

			// MQTT Configurations
			//this.connOpt.setMqttVersion(4);
			this.connOpt.setCleanSession(true);
			//this.connOpt.setKeepAliveInterval(0);
			this.connOpt.setAutomaticReconnect(true);
			this.connOpt.setUserName(userName);
			this.connOpt.setPassword(password.toCharArray());

			logger.info("Specifying callback...");
			this.client.setCallback(this);

			logger.info("Connecting...");
			this.client.connect(connOpt);

			logger.info("Connected ! Subscribing to topic: "+topic);
			this.client.subscribe(topic, 1);

			logger.info("Subscription completed !");
		}
		catch(Exception error)
		{
			logger.info("Exception occured while connecting to broker"+error);
		}
	}
	/**
	 * 
	 * deliveryComplete
	 * This callback is invoked when a message published by this client
	 * is successfully received by the broker.
	 * 
	 */
	public void deliveryComplete(IMqttDeliveryToken token) {
		//logger.info("Pub complete" + new String(token.getMessage().getPayload()));
		
	}
	
	//@Override
	public void connectComplete(boolean reconnect, java.lang.String serverURI){
		logger.info("Re-Connection Attempt " + reconnect);
		if(reconnect) {
			try {
				this.client.subscribe(topic, 1);
			} catch (MqttException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * messageArrived
	 * This callback is invoked when a message is received on a subscribed topic.
	 * 
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		
		logger.info("-------------------------------------------------");
		logger.info("| Topic:" + topic);
		logger.info("| Message: " + new String(message.getPayload()));
		logger.info("-------------------------------------------------");
		ByteArrayInputStream bais = new ByteArrayInputStream(message.getPayload());
        JsonReader payload = Json.createReader(bais);
        UpdateElasticSearch.processMessage(payload.readObject(),topic );

      //  LOGGER.debug("After Parsing MQTT : JSON is "+payload);
			
		//UpdateElasticSearch.processMessage(new JSONObject(message.getPayload()), topic);
		
		
	}
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

}
