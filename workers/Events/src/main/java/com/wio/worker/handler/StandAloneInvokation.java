package com.wio.worker.handler;

import com.wio.worker.mqtt.MQTTSubscriber;

public class StandAloneInvokation 
{
	public static void main(String[] args) 
	{
		MQTTSubscriber sub = new MQTTSubscriber();		
		sub.connect();
	}
}
