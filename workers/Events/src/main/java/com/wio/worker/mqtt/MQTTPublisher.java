package com.wio.worker.mqtt;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class MQTTPublisher extends AbstractMqttClient
{
	final static Logger logger = Logger.getLogger(MQTTPublisher.class);
	//private static LambdaLogger logger = null;
	/*private static String ServerURI = "tcp://52.23.182.90:1883";
	private static String username = "wioadmin";
	private static String password = "wioadmin";
	
	private static int qos = 2;
	private static String clientId = "Worker";*/
	
	public static String publishMessage(String messageContent, String topic)
	{
		//logger = lambdaContext.getLogger();
		MemoryPersistence persistence = new MemoryPersistence();
		
		try 
		{
            MqttClient messagingClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connectOptions = new MqttConnectOptions();
            
            connectOptions.setCleanSession(true);
            connectOptions.setUserName(userName);
            connectOptions.setPassword(password.toCharArray());

            logger.info("Connecting to broker: "+broker);
            
            messagingClient.connect(connectOptions);
            
            logger.info("Connected");
            logger.info("Publishing message: "+messageContent);
            
            MqttMessage message = new MqttMessage(messageContent.getBytes());
            message.setQos(qos);
            
            messagingClient.publish(topic, message);
            
            logger.info("Message published");
            
            messagingClient.disconnect();
            
            logger.info("Disconnected");
            return "SUCCESS";
            //System.exit(0);
        } 
		catch(MqttException me) 
		{
            logger.info("reason "+me.getReasonCode());
            logger.info("msg "+me.getMessage());
            logger.info("loc "+me.getLocalizedMessage());
            logger.info("cause "+me.getCause());
            logger.info("excep "+me);
            me.printStackTrace();
        }		
		return "FAILURE";
	}	
}
