package com.wio.worker.elasticsearch;

import java.io.IOException;
import java.util.TreeMap;

import javax.json.JsonObject;

import org.apache.http.client.ClientProtocolException;

import com.wio.common.config.model.Device;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;


public class UpdateElasticSearch {

	public static void  processMessage(JsonObject jsonObject ,String query) throws ClientProtocolException, IOException, DAOException {

		if(jsonObject!=null){

			String host = null;
			String deviceName = null;
			TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
			String deviceId = jsonObject.getString("deviceId");
			String payload=jsonObject.toString();

			GenericDAO  genDAO = GenericDAOFactory.getGenericDAO();

			if(deviceId!=null ){	
				Device device = genDAO.getGenericObject(Device.class, deviceId);
				if(device!=null){
					String serviceName = device.getServiceName();
					deviceName = device.getDeviceName();
					host=HttpClientUtil.getEndpoint(serviceName);		
					awsHeaders.put("host", host);	
					String elasticSearchEndpoint = "https://" + host + query +deviceName;
						HttpClientUtil.postRequest(payload, elasticSearchEndpoint, awsHeaders,query+deviceName);
					
				}

			}
		}

	}

/*
	private static void saveDataToES(String deviceId, String payload, String query) throws DAOException, ClientProtocolException, IOException {

		GenericDAO  genDAO = GenericDAOFactory.getGenericDAO();
		if(deviceId!=null ){	
			Device device = genDAO.getGenericObject(Device.class, deviceId);
			if(device!=null){

				String serviceName = device.getServiceName();
				String deviceName = device.getDeviceName();
				String host=HttpClientUtil.getEndpoint(serviceName);
				String elasticSearchEndpoint = "https://" + host + query +deviceName;

				TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
				awsHeaders.put("host", host);	

				HttpClientUtil.postRequest(payload, elasticSearchEndpoint, awsHeaders,query+deviceName);

			}

		}
	}*/


}
