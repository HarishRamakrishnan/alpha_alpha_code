package com.wio.worker.event.reset;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

public class ResetHandler {
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
								   .withCredentials(new InstanceProfileCredentialsProvider(false)).build();

}
