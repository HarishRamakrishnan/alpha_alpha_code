package com.wio.worker.radio.service;

import java.sql.Connection;
import java.util.concurrent.atomic.AtomicLong;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.gson.Gson;
import com.wio.common.mqtt.MQTTConnection;
import com.wio.worker.radio.redshift.WIOAmazonRedshiftAPI;
import com.wio.worker.radio.redshift.dbutil.ConnectionFactory;
import com.wio.worker.radio.request.RFMetrics;

public class RadioService implements MqttCallback
{
	public static String clientID = "WIO-RADIO-SERVICE";
	public static String topic = "wio/radio";
	public static AtomicLong count = new AtomicLong(); 
	
	Connection connection = null;
	MqttClient client;

	public RadioService()
	{		
		super();
	}	
	
	public void subscribe()
	{
		MQTTConnection conn = new MQTTConnection();
		client = conn.getConnection(clientID, 0, true, false);
		
		System.out.println("Setting callback..");
		client.setCallback(this);
		
		try 
		{
			System.out.println("subscribing...");
			client.subscribe(topic);
			
			System.out.println("subscription completed !");
			
			System.out.println("Connecting to Redshift...");
		    connection = ConnectionFactory.getConnection();
		    System.out.println("Connection success !");
		} 
		catch (MqttException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void processMessage(String message)
	{
		Gson g = new Gson(); 
		
		RFMetrics metrics = g.fromJson(message, RFMetrics.class);
		
		System.out.println("Writing to Redshit...");		
		WIOAmazonRedshiftAPI redshift = new WIOAmazonRedshiftAPI();
		
		try 
		{
			redshift.save(metrics, connection);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void connectionLost(Throwable cause) 
	{
		// TODO Auto-generated method stub
		System.out.println("Connection lost!");
		System.out.println(cause.getStackTrace());		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception 
	{
		// TODO Auto-generated method stub
		
		System.out.println("-------------------------------------------------");
		System.out.println("| Topic:" + topic);
		System.out.println("| Message: " + new String(message.getPayload()));
		System.out.println("-------------------------------------------------");
				
		String messageString = new String(message.getPayload());
		
		processMessage(messageString);
		
		count.incrementAndGet();	
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) 
	{
		// TODO Auto-generated method stub
		/*try 
		{
			System.out.println("Pub complete" + new String(token.getMessage().getPayload()));
		} catch (MqttException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public void connectComplete(boolean reconnect, java.lang.String serverURI){
		System.out.println("Re-Connection Attempt " + reconnect);
		if(reconnect) {
			try {
				this.client.subscribe(topic, 1);
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
}
