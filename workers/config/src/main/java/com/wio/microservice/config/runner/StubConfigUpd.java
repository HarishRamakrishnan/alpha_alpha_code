package com.wio.microservice.config.runner;

import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.ConfigProtoData;
import com.wio.microservice.util.WIOConstants;


public class StubConfigUpd {

	public static void main(String[] args) {
		StubConfigUpd stub = new StubConfigUpd();
		stub.test();
		
	}

	private void test() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		ConfigProtoData proto = (ConfigProtoData) protocolType.getMesgSerializer(WIOConstants.CONFIG_UPDATE_PROTO);
		proto.messageConstructor();
		proto.setConfigUpdateDetails("Config", "12:20", "06aa74-f801-a401-b6ytCWMP7117", "Success", "ffc3e01a-dad1-4b76-b307-fc0453a73d4e");
		proto.setConfigParamDetails("key1", "1");
		proto.setConfigParamDetails("key2", "2");
		proto.serialize();
		String output = proto.deSerializeToString(proto.getConfigUpdateByteString().toByteArray());
		System.out.println("De-Serialize out ==> "+output);
		System.out.println("===========>>"+proto.serialize().getAllFields());
	}

}
