package com.wio.microservice.config.runner;

import com.wio.common.config.api.DeviceConfigurationAPI;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.common.protobuf.generated.ConfigUpdatesProto.ConfigUpdates;

public class ConfigFailureRespHandler {

	private ConfiguredValuesMO configUpdates = null;
	private DeviceConfiguration updatedConfig = null;
	
	private DDBGenericDAO genDao = DDBGenericDAO.getInstance();
	
	public ConfigFailureRespHandler() {
		
	}
	
	public Boolean processFailureResp(ConfigUpdates configUpdateMsg) {
		
		getUpdatedConfiguration(configUpdateMsg.getTransid());
		configUpdates.setStatus("Failed");
		configUpdates.setConfigAck(configUpdateMsg.getStatusBytes().toString());
		try {
			genDao.saveGenericObject(configUpdates);
		} catch (DAOException e) {
			//e.printStackTrace();
			System.out.println("Exception occured while saving updated ConfiguredValuesMO entity.");
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * This method will get the updated configuration for the transactionID
	 * from CONFIG_UPDATE table.
	 * @param transactionID
	 */
	private void getUpdatedConfiguration(String transactionID) {
		// fetch the configuration.
		configUpdates = DeviceConfigurationAPI.queryConfigUpdatesTable(transactionID);
		updatedConfig = configUpdates.getConfiguration();
		if(updatedConfig == null) {
			System.out.println("DeviceConfiguration is null.");
			return ;
		}
	}
	
	
}
