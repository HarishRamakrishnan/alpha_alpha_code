package com.wio.microservice.config.runner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import com.wio.common.exception.DAOException;
import com.wio.common.exception.InvalidArugumentException;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.microservice.framework.mqtt.client.JMSMessageHandler;
import com.wio.microservice.framework.mqtt.client.JMSPublisher;
import com.wio.microservice.framework.mqtt.client.RecieverRunner;
import com.wio.microservice.framework.threadpool.WIOExecutorService;
import com.wio.microservice.framework.threadpool.WIOExecutorServiceCreation;


public class ExecutorServiceStub {
	
	private String topicName = "";
	private static final String _$share = "$share";
	JMSMessageHandler jmsHandler = new JMSMessageHandler();
	
	JMSPublisher pub = null;
	
	public static void main(String args[]){
		ExecutorServiceStub stub = new ExecutorServiceStub();
		stub.testExecService();
	}

	private void testExecService() {
		
		//JMSPublisher pub = new JMSPublisher();
		
		WIOExecutorServiceCreation create = WIOExecutorServiceCreation.getInst();
		ExecutorService ex = create.createNewFixedThreadPool(5);
		//ExecutorService ex = create.createNewCatchedThreadPool();
		WIOExecutorService service = new WIOExecutorService(ex);
		try {
			topicName = DynamoDBHelper.getInstance().getSubscriptionTopics("config").getPath();
			topicName = _$share + topicName;
		} catch (DAOException | InvalidArugumentException e1) {
			System.out.println("Unable to get topic name for config worker. "+e1.getLocalizedMessage());
			e1.printStackTrace();
		}
		
		/*service.execute(new Runnable() {
			
			@Override
			public void run() {
				IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
				ConfigProtoData proto = (ConfigProtoData) protocolType.getMesgSerializer(WIOConstants.CONFIG_UPDATE_PROTO);
				proto.messageConstructor();
				proto.setConfigUpdateDetails("Config", "12:20", "DEVTT77778v987986sd897", "Success");
				proto.setConfigParamDetails("key1", "val1");
				proto.setConfigParamDetails("key2", "val2");
				proto.serialize();
				String output = proto.deSerializeToString(proto.getConfigUpdateByteString().toByteArray());
				//System.out.println("De-Serialize out ==> "+output);
				//System.out.println("===========>>"+proto.serialize().getAllFields());
				
				MqttMessage mesg = new MqttMessage();
				mesg.setPayload(proto.getConfigUpdateByteString().toByteArray());
				System.out.println("byte length = "+mesg.getPayload().length);
				pub.setMessage(mesg);
				pub.publishProtobufMesg();
			}
		});*/
		
		service.execute(new RecieverRunner("CMS-1",topicName,1));
		service.execute(new RecieverRunner("CMS-2",topicName,1));
		service.execute(new RecieverRunner("CMS-3",topicName,1));
		service.execute(new RecieverRunner("CMS-4",topicName,1));
		service.execute(new RecieverRunner("CMS-5",topicName,1));
		
		
		
		//System.out.println("service is going to shutdown..");
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			
			System.out.println("Executor service got interupted..");
			e.printStackTrace();
		}
		
	}

}
