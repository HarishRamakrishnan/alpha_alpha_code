package com.wio.microservice.config.runner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.wio.common.config.api.DeviceConfigurationAPI;
import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.PortSettings;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.microservice.util.RequestValidator;
import com.wio.microservice.util.WIOConstants;

public class UpdateRunningValues {
	
	
	private List<HPRadioSettings> radioSettings;
	private List<PortSettings> portSettings;
	private List<LANSettings> lanSettings;
	private WANSettings wanSettings;	
	private List<WiFiSettings> wifiSettings;
	
	private DeviceConfigurationRequest runningConfig = null;
	
	private DeviceConfigurationRequest updatedConfig = null;
	
	
	
	
	public boolean processSuccessResp(String deviceId) {
		
		getRunningConfiguration(deviceId);
		getUpdatedConfiguration(deviceId);
		populateRunningSettings();
		return false;
	}

	private void getUpdatedConfiguration(String deviceId) {
		// fetch the configuration.
		Gson g = new Gson();
		try {
			String deviceConfigurationRequest = DeviceConfigurationAPI.fetchConfig(deviceId, null);
			updatedConfig = g.fromJson(deviceConfigurationRequest, DeviceConfigurationRequest.class);

		} catch (InternalErrorException | BadRequestException e) {
			e.printStackTrace();
		}
	}

	public void getRunningConfiguration(String deviceId) {
		// fetch the configuration.
		Gson g = new Gson();
		try {
			String deviceConfigurationRequest = DeviceConfigurationAPI.fetchConfig(deviceId, null);
			runningConfig = g.fromJson(deviceConfigurationRequest, DeviceConfigurationRequest.class);
			
		} catch (InternalErrorException | BadRequestException e) {
			e.printStackTrace();
		}
		
	}

	private void populateRunningSettings() {
		radioSettings = runningConfig.getRadioSettings();
		lanSettings = runningConfig.getLanSettings();
		wanSettings = runningConfig.getWanSettings();
		wifiSettings = runningConfig.getWifiSettings();
	}
/*
	private void updateConfigSetting(DeviceConfigurationRequest deltaConfig, String settingType) throws Exception {

		radioSettings = deltaConfig.getRadioSettings();
		
		if (param != null) {
			switch (settingType) {

			case WIOConstants.LAN_STRING:
				validateAndSetLanSettingToDB();
				break;
			case WIOConstants.WAN_STRING:
				validateAndSetWanSettingToDB(param);
				break;
			case WIOConstants.RADIO_STRING:
				setRadioSettingToDB(radioSettings);
				break;
			case WIOConstants.WIFI_STRING:
				validateAndSetWifiSettingToDB(param);
				break;
			case WIOConstants.NETWORK_STRING:
				validateAndSetNWSettingToDB(param);
				break;
			default:
				String value = null;
				switch ("") {

				case "MANUFACTURER":
					break;
				case "MANUFACTURER_OUI":
					break;
				case "PRODUCT_CLASS":
					break;
				case "SERIAL_NUMBER":
					break;
				case "HARDWARE_VERSION":
					break;
				case "SOFTWARE_VERSION":
					break;
				case "PROVISIONING_CODE":
					break;
				case "SPEC_VERSION":
					break;
				case "DESCRIPTION":
					break;
				case "ACS_SERVER_URL":
					break;
				case "ACS_PASSWORD":
					break;
				case "ACS_USERNAME":
					break;
				case "CPE_USERNAME":
					break;
				case "CPE_PASSWORD":
					break;
				case "CPE_GATEWAY_URL":
					break;
				default:
					break;

				}

					dao.updateDBItem(param.getTableName(), WIOConstants.SERVICE_NAME,
							TR69Helper.wavesParameters.getHpId(), param.getSettingType(), value);
			
				break;
			}
		}
	}

	private void validateAndSetNWSettingToDB() throws Exception {

		
		*//**
		 * 1. get runningConfig NW setting and updatedConfig NW setting 
		 * 2. compare the runningConfig NWID = updatedConfig NWID and 
		 * 		if matches get the updatedConfig values and set in the corresponding setter parameter in the runningConfig.
		 * 3. if doesn't match with any NWID in the runningConfig then, skip 
		 * that updatedConfig which is not for this device (Very rare scenario).  
		 * 
		 *//*
		
		
		WiFiSettings wifiSetting = null;
		NetworkSettings networkSetting = null;
		
		
		

		int configWifiListSize = configWifiList.size();
		if (configWifiListSize > 0 && param.getIndex() <= (configWifiListSize - 1)) {
			wifiSetting = configWifiList.get(param.getIndex());
			networkSetting = wifiSetting.getNetworkSettings();
		} else {
			wifiSetting = new WiFiSettings();
			networkSetting = new NetworkSettings();
			wifiSetting.setNetworkSettings(networkSetting);
			configWifiList.add(param.getIndex(), wifiSetting);
		}

		System.out.println("Network setting .. " + param.toString());

		switch (param.getWavesParamName()) {

		case "DHCP_SERVER":
			networkSetting.setDhcpServer(param.getServiceParamValue());
			break;
		case "IP_POOL_START":
			networkSetting.setIpPoolStart(param.getServiceParamValue());
			break;
		case "IP_POOL_END":
			networkSetting.setIpPoolEnd(param.getServiceParamValue());
			break;
		case "LEASE_TIME":
			networkSetting.setLeaseTime(param.getServiceParamValue());
			break;
		case "MAC_ADDRESS":
			// do nothing
			break;
		default:
			break;
		}

		wifiSetting.setNetworkSettings(networkSetting);
		System.out.println("********* WIFI Setting *********" + wifiSetting);
		configWifiList.set(param.getIndex(), wifiSetting);

		System.out.println("NW list = " + networkSetting);
		try {
			if (isSetCall) {

				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateNetwork(param, networkSetting),
						WIOConstants.NETWORK_STRING, "CONFIGURED_");
				// DDBGenericDAO.getInstance().saveGenericObject(TR69Helper.deviceConfig.getWifiSettings().get(param.getIndex()));
			}

		} catch (DAOException e) {
			e.printStackTrace();
			System.out.println("Error while updating WIFI or Network settings :- " + e.getMessage());
		}
	}

	private void validateAndSetWifiSettingToDB() throws Fault {

		WiFiSettings wifiSetting = null;
		NetworkSettings networkSetting = null;
		int configWifiListSize = configWifiList.size();
		if (configWifiListSize > 0 && param.getIndex() >= (configWifiListSize - 1)) {
			wifiSetting = configWifiList.get(param.getIndex());

		} else {
			wifiSetting = new WiFiSettings();
			networkSetting = new NetworkSettings();
			wifiSetting.setNetworkSettings(networkSetting);
			configWifiList.add(param.getIndex(), wifiSetting);
		}
		System.out.println("Wifi setting.. " + param.toString());

		switch (param.getWavesParamName()) {
		case "SSID":
			wifiSetting.setSsid(param.getServiceParamValue());
			break;
		case "AUTHENTICATION_TYPE":
			wifiSetting.setAuthenticationType(param.getServiceParamValue());
			break;
		case "ENCRYPTION_TYPE":
			wifiSetting.setEncryptionType(param.getServiceParamValue());
			break;
		case "PASS_PHRASE":
			wifiSetting.setPassPhrase(param.getServiceParamValue());
			break;
		case "ENABLE_WIRELESS":
			wifiSetting.setEnableWireless(param.getServiceParamValue());
			break;
		case "HIDDEN_SSID":
			wifiSetting.setHiddenSSID(param.getServiceParamValue());

		default:
			break;

		}
		
		 * wifiList.set(param.getIndex(), wifiSetting);
		 * TR69Helper.wavesParameters.setWifiSetting(wifiList);
		 
		configWifiList.set(param.getIndex(), wifiSetting);

		log.debug("WIFI list size = " + configWifiList.size());
		System.out.println("WIFI list = " + configWifiList);
		try {
			if (isSetCall) {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateWifiSetting(param, wifiSetting),
						WIOConstants.WIFI_STRING, "CONFIGURED_");
				// DDBGenericDAO.getInstance().saveGenericObject(TR69Helper.deviceConfig.getWifiSettings().get(param.getIndex()));
				// DDBGenericDAO.getInstance().saveGenericObject(wifiSetting);
			}

		} catch (DAOException e) {
			e.printStackTrace();
			System.out.println("Error while updating WIFI or Network settings :- " + e.getMessage());
		}
	}

	private void setRadioSettingToDB(List<HPRadioSettings> radioSettings)
			throws Exception {

		radioValidationFlag = true;
		runningRadioList = TR69Helper.wavesParameters.getRadioSetting();
		HPRadioSettings radioSetting = null;
		int configRadioSize = configuredRadioList.size();

		if (configRadioSize > 0 && param.getIndex() <= (configRadioSize - 1)) {
			radioSetting = configuredRadioList.get(param.getIndex());
		} else {
			radioSetting = new HPRadioSettings();
			configuredRadioList.add(param.getIndex(), radioSetting);
			configHardwareProfile.setRadioSettings(configuredRadioList);
		}

		Map<String, String> tempMap = TR69Helper.radioParamMap.get(param.getIndex());
		if (tempMap == null) {
			tempMap = new HashMap<>();
		}
		System.out.println("RADIO setting .. " + param.toString());
		// HPRadioSettings radioSetting = radioList.get(param.getIndex());
		if (!isSetCall) {
			switch (param.getWavesParamName()) {

			case "BANDWIDTH":
				radioSetting.setBandwidth(param.getServiceParamValue());
				break;
			case "CHANNEL":
				radioSetting.setChannel(param.getServiceParamValue());
				break;
			case "RADIO_NAME":
					radioSetting.setRadioName(param.getServiceParamValue());
				break;
			case "RADIO_STATUS":
					radioSetting.setRadioStatus(param.getServiceParamValue());
				break;
			case "RADIO_BAND":
					radioSetting.setRadioBand(param.getServiceParamValue());
				break;
			case "RADIO_MODE":
					radioSetting.setRadioMode(param.getServiceParamValue());
				break;
			case "TRANSMIT_POWER":
					radioSetting.setTransmitPower(param.getServiceParamValue());
				break;
			case "BABEL":
					radioSetting.setBabel(param.getServiceParamValue());
				break;

			default:
				break;

			}

			runningRadioList.set(param.getIndex(), radioSetting);
			configuredRadioList.set(param.getIndex(), radioSetting);
			tempMap.put(param.getWavesParamName(), param.getServiceParamName());
			// Map<Index, Map<WPN, SPN >>
			TR69Helper.radioParamMap.put(param.getIndex(), tempMap);
		}
		log.debug("RADIO list size = " + runningRadioList.size());
		System.out.println("Configured RADIO list = " + configuredRadioList);

		TR69Helper.wavesParameters.setRadioSetting(runningRadioList);
		TR69Helper.hardwareProfile.setRadioSettings(runningRadioList);
		configHardwareProfile.setRadioSettings(configuredRadioList);

		// DB update item.
		if (isSetCall) {
			
			 * dao.updateDBItem(param.getTableName(), WIOConstants.HP_ID,
			 * TR69Helper.wavesParameters.getHpId(), param.getSettingType(), radioList);
			 
			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(configHardwareProfile,
						WIOConstants.HARDWARE_PROFILE_TABLE, "CONFIGURED_");
				// DDBGenericDAO.getInstance().saveGenericObject(configHardwareProfile);
			} catch (DAOException e) {
				e.printStackTrace();
				System.out.println("Exception occured while saving HardwareProfile.");
			}
		}
	}

	private void validateAndSetWanSettingToDB()
			throws Exception {

		WANSettings wanSetting = configuredDevice.getWanSettings();
		if (wanSetting == null) {
			wanSetting = new WANSettings();
		}
		System.out.println("WAN setting .. " + param.toString());
		switch (param.getWavesParamName()) {

		case "GATEWAY":
				wanSetting.setGateway(param.getServiceParamValue());
			break;
		case "SUBNET_MASK":
				wanSetting.setSubnetMask(param.getServiceParamValue());
			break;
		case "IPV6":
				wanSetting.setIpv6(param.getServiceParamValue());
			break;
		case "DNS_PRIMARY":
				wanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				wanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			break;
		case "IPV4":
				wanSetting.setIpv4(param.getServiceParamValue());
			break;
		case "DNS_SECONDARY":
				wanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				wanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			break;
		case "USERNAME":
				wanSetting.setUsername(param.getServiceParamValue());
			break;
		case "PASSWORD":
				wanSetting.setPassword(param.getServiceParamValue());
			break;
		default:
			break;

		}

		TR69Helper.wavesParameters.setWanSetting(wanSetting);
		TR69Helper.device.setWanSettings(wanSetting);
		configuredDevice.setWanSettings(wanSetting);

		// DB update item.
		if (isSetCall) {
			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateWANSetting(param, configuredDevice),
						WIOConstants.DEVICE_TABLE, "CONFIGURED_");
			} catch (DAOException e) {
				System.out.println("Exception while saving wan setting on configured device table.");
				e.printStackTrace();
			}
		}
	}

	private void validateAndSetLanSettingToDB()
			throws Exception {
		List<LANSettings> lanList = runningConfig.getLanSettings();
		LANSettings lanSetting = null;
		int configLANListSize = configuredLANList.size();

		if (configLANListSize > 0 && param.getIndex() <= (configLANListSize - 1)) {
			lanSetting = configuredLANList.get(param.getIndex());
		} else {
			lanSetting = new LANSettings();
			configuredLANList.add(param.getIndex(), lanSetting);
			configuredDevice.setLanSettings(configuredLANList);
		}
		System.out.println("LAN setting .. " + param.toString());

		switch (param.getWavesParamName()) {

		case "GATEWAY":
				lanSetting.setGateway(param.getServiceParamValue());
			break;
		case "SUBNET_MASK":
				lanSetting.setSubnetMask(param.getServiceParamValue());
			break;
		case "STATIC_IPV6":
				lanSetting.setStaticIPv6(param.getServiceParamValue());
			break;
		case "DNS_PRIMARY":
				lanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				lanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			break;
		case "STATIC_IPV4":
				lanSetting.setStaticIPv4(param.getServiceParamValue());
			break;
		case "DNS_SECONDARY":
				lanSetting.setDnsPrimary(param.getServiceParamValue().split(",")[0]);
				lanSetting.setDnsSecondary(param.getServiceParamValue().split(",")[1]);
			break;

		default:
			break;

		}

		configuredLANList.set(param.getIndex(), lanSetting);
		// update local cache..
		TR69Helper.wavesParameters.setLanSetting(lanList);
		TR69Helper.device.setLanSettings(lanList);
		// local instance referencing from cache.
		configuredDevice.setLanSettings(configuredLANList);

		System.out.println("LAN list  = " + lanList);

		// DB update item.
		if (isSetCall) {
			
			 * dao.updateDBItem(param.getTableName(), WIOConstants.DEVICE_ID_PK,
			 * TR69Helper.wavesParameters.getDeviceId(), param.getSettingType(), lanList);
			 

			try {
				DDBGenericDAO.getInstance().saveGenericObjectWithPrefix(populateWANSetting(param, configuredDevice),
						WIOConstants.DEVICE_TABLE, "CONFIGURED_");
			} catch (DAOException e) {
				System.out.println("Exception while saving Lan Settings on configured device table.");
				e.printStackTrace();
			}

		}
	}
*/
}
