package com.wio.microservice.framework.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.wio.microservice.config.runner.MessageProcessor;

public class RecieverRunner implements Runnable {
	
	private String threadName;
	private String shareTopic;
	private int latchCount;
	
	public RecieverRunner(String threadName,String shareTopic, int latchCount) {
		this.threadName = threadName;
		this.shareTopic = shareTopic;
		this.latchCount = latchCount;
	}
	
	public void run() {
		MqttMessage mesg = new MqttMessage();
		Thread.currentThread().setName(threadName);
		System.out.println(Thread.currentThread().getName()+"|"+threadName + " will be reading the messages from topic:- "+shareTopic);
		while (true) {
			/*
			 * 1. Need to call the protobuf message de-serialisation to get the message.
			 * 2. get the deviceId
			 * 3. get the message content, it can be success or failure.
			 * 4. if Success, then inform ACS or ISP on applying the config
			 * 5. if Failure, then inform ACS or ISP and also the user about the CPE fault.
			 * 6. log the message.
			 * 
			 **/
			
			try {
				JMSReciever recv = new JMSReciever(shareTopic, latchCount, threadName);
				System.out.println(Thread.currentThread().getName()+"|"+threadName + " is going to process for the message. ");
				recv.mesgRecieved(threadName);
				String s = recv.getStringMessage();
				mesg = recv.getMqttMessage();
				if(mesg == null) {
					System.out.println(Thread.currentThread().getName()+"|"+threadName +"Will wait until a message arrives.. ");
					Thread.sleep(1000);
					continue;
				}
				System.out.println(Thread.currentThread().getName()+"|"+threadName +"In RecieveRunner mqtt mesg = "+mesg.getPayload().length);
				new MessageProcessor(threadName).startMessageProcessing(mesg);
				System.out.println(Thread.currentThread().getName()+"|"+threadName + " Task executed and running in background..");
				Thread.sleep(10000);
				continue;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}