package com.wio.microservice.framework.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WIOExecutorServiceCreation {
	
	private int queueSize = 0;
	
	private ThreadPoolExecutor tpExecutor = null;
	private BlockingQueue<Runnable> worksQueue = null;
	private RejectedExecutionHandler rejectionHandler = null;
	private ExecutorService executor = null;
	
	private static WIOExecutorServiceCreation tpExecCreation = new WIOExecutorServiceCreation();

	private WIOExecutorServiceCreation(){
	}
	
	public static WIOExecutorServiceCreation getInst(){
		return tpExecCreation;
	}
	
	public ThreadPoolExecutor createThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime,
			TimeUnit unit, int quequeSize) {

		rejectionHandler = new RejectedExecutionHandlerImpl();
		worksQueue = new ArrayBlockingQueue<Runnable>(queueSize);
		tpExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, worksQueue,
				rejectionHandler);
		return tpExecutor;
	}
	
	public ExecutorService getExecutorSErvice(){
		return executor;
	}
	
	public ExecutorService createNewFixedThreadPool(int poolSize){
		executor = Executors.newFixedThreadPool(poolSize);
		return executor;
	}
	
	public ExecutorService createNewFixedThreadPool(int poolSize, ThreadFactory thFactory){
		return executor = Executors.newFixedThreadPool(poolSize, thFactory);
	}
	
	public ExecutorService createNewCatchedThreadPool(){
		return executor = Executors.newCachedThreadPool();
	}
	
	public ExecutorService createNewCatchedThreadPool(ThreadFactory thFactory){
		return executor = Executors.newCachedThreadPool(thFactory);
	}
	
	public ExecutorService createNewSingleThreadPool(){
		return executor = Executors.newSingleThreadExecutor();
	}
	
}
