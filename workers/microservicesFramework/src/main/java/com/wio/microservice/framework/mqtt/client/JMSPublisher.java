package com.wio.microservice.framework.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.wio.common.mqtt.MQTTConnection;
import com.wio.common.mqtt.MQTTPublisher;

public class JMSPublisher {
	MqttClient mqttClient = null;
	
	// Topic filter the client will subscribe to
	String subTopic = "configTopic";
	//String[] messages = {"Rajesh","Mani","Balaji","Sibkkki","ppppp","iuidh","Arve","Rajesh","Sibkkki","ppppp","iuidh","Pritha","Magesh","Arve",
	//		"Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve","Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve","Rajesh","Mani","Balaji","Sibi","Pritha","Magesh","Arve",
		//	"jkj","Mani","jji","Sibkkki","ppppp","iuidh","dkjkfd"};
	String[] messages = {"Rajesh","Mani","ppppp","iuidh","dkjkfd"};
	
	private MqttMessage mqttMesg = null;
	
	public JMSPublisher(String topicName) {	
		this.subTopic = topicName;
	
	}
	
	public void setMessage(String... mesg) {
		this.messages = mesg;
	}
	
	public void setMessage(MqttMessage mqttMesg) {
		this.mqttMesg = mqttMesg;
	}
	
	public String publishProtobufMesg() {
		MQTTConnection conn = new MQTTConnection();
		mqttClient = conn.getConnection("CMS_P", 10, true, false);
		System.out.println("Published message on topic = "+subTopic);
		String response = MQTTPublisher.publishMessage(mqttClient, mqttMesg, subTopic, 2, false);
		System.out.println("===========>> "+ response);
		
		return response;
	}
	
	public void mesgPublisher(){
		
		for(int i = 0; i< messages.length; i++){
			
			MQTTConnection conn = new MQTTConnection();
			mqttClient = conn.getConnection("11", 10, true, false);
			String response = MQTTPublisher.publishMessage(mqttClient, messages[i], subTopic, 2, false);
			System.out.println("===========>> "+ response);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
