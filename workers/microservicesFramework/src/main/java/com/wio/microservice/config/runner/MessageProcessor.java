package com.wio.microservice.config.runner;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.common.protobuf.framework.IMessage;
import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.generated.ConfigUpdatesProto.ConfigUpdates;
import com.wio.common.protobuf.generated.ConfigUpdatesProto.ConfigUpdates.ConfigParam;
import com.wio.microservice.framework.threadpool.WIOExecutorService;
import com.wio.microservice.framework.threadpool.WIOExecutorServiceCreation;
import com.wio.microservice.util.WIOConstants;


public class MessageProcessor {
	
	private WIOExecutorService service = null;
	private IMessage proto = null;
	private Map<FieldDescriptor, Object> fieldMap = null;
	private List<ConfigParam> configParam = null;
	private String threadName = "";
	//private DDBGenericDAO genDao = DDBGenericDAO.getInstance();
	
	public MessageProcessor(String threadName) {
		this.threadName = threadName;
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		proto = (IMessage) protocolType.getMesgSerializer(WIOConstants.CONFIG_UPDATE_PROTO);
		System.out.println(Thread.currentThread().getName() +" Recieved "+proto.schema() + " message.");
		createExecutorService();
	}
	
	private void createExecutorService() {
		WIOExecutorServiceCreation create = WIOExecutorServiceCreation.getInst();
		//ExecutorService ex = create.createNewFixedThreadPool(5);
		ExecutorService ex = create.createNewSingleThreadPool();
		service = new WIOExecutorService(ex);
	}
	
	public void startMessageProcessing(MqttMessage mesg) {
		System.out.println(Thread.currentThread().getName() +" ======== START MQTT Message Processing ==========");
		service.execute(new MessageProcessRunner(mesg));
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
			System.out.println(Thread.currentThread().getName() +" Single thread will terminate now..");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private final class MessageProcessRunner implements Runnable {
		private MqttMessage mesg;

		private MessageProcessRunner(MqttMessage mesg) {
			this.mesg = mesg;
			Thread.currentThread().setName(threadName);
			System.out.println(Thread.currentThread().getName() +" mesg len = "+mesg.getPayload().length);
			System.out.println(Thread.currentThread().getName() +" Initializing MessageProcessRunner..");
		}

		@Override
		public void run() {
			Thread.currentThread().setName(threadName);
			proto.messageConstructor();
			ConfigUpdates msgUpdates = (ConfigUpdates) proto.deSerialize(mesg.getPayload());
			fieldMap = msgUpdates.getAllFields();
			System.out.println(Thread.currentThread().getName()+" Size of the decrypted Proto message = "+fieldMap.size());
			configParam = msgUpdates.getMapFieldList();
			process(msgUpdates);
			System.out.println(Thread.currentThread().getName()+" ======== END MQTT Message Processing ==========");
		}
		
		public void process(ConfigUpdates lconfigUpds) {
			System.out.println(Thread.currentThread().getName() +" Recieved message for the device Id = "+lconfigUpds.getDeviceid() + " Status = "+lconfigUpds.getConfigStatus());
			if(lconfigUpds.getConfigStatus().equals("Success")) {
				System.out.println(Thread.currentThread().getName() +" Configuration applied succesfully on device = "+lconfigUpds.getDeviceid());
				
				
				
				
			}else if(lconfigUpds.getConfigStatus().equals("Failure")){
				System.out.println(Thread.currentThread().getName() +" Configuration failed to apply on device = "+lconfigUpds.getDeviceid());
				System.out.println(Thread.currentThread().getName() +" Failed to apply parameter(s) = "+configParam);
			}
		}
	}

}
