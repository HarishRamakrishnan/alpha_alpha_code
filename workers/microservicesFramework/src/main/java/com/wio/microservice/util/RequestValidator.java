package com.wio.microservice.util;

import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import com.wio.common.util.UtilProperties;


public class RequestValidator {

	private static Pattern emailPattern;
	private static Pattern numberPattern;
	private static Pattern strongPasswordPattern;
	private static Pattern ipAddressPattern;
	private static Pattern ipv6AddressPattern;
	private static Pattern ssidPattern;
	private static Pattern preSharedKeyPattern;
	private static Pattern versionPattern;

	private static Pattern macAddressPattern;
	private static Pattern alphanumaricPattern;
	private static Pattern alphanumaricPattern64;
	private static Pattern alphanumaricPatternwithhypen;
	private static Pattern alphanumaricPatternwithspace;
	private static Pattern alphanumaricSplPattern64;
	private static Pattern fourdigitnumeric;
	private static Pattern urlPattern;
	private static Pattern sixHexaDecimalPattern;
	private static Pattern passwordPattern;
	private static Pattern leaseTimePattern;
	
	
	
	
	static UtilProperties utilProp = new UtilProperties();
	static Properties prop = new Properties();

	static {
		prop = utilProp.loadProperties();

		emailPattern = Pattern.compile(prop.getProperty("validate.email"));
		numberPattern = Pattern.compile(prop.getProperty("validate.phoneNumber"));
		strongPasswordPattern = Pattern.compile(prop.getProperty("validate.password"));
		ipAddressPattern = Pattern.compile(prop.getProperty("validate.ip"));
		ipv6AddressPattern = Pattern.compile(prop.getProperty("validate.ipv6"));
		ssidPattern = Pattern.compile(prop.getProperty("validate.SSIDChars"));
		preSharedKeyPattern = Pattern.compile(prop.getProperty("validate.preSharedKey"));
		versionPattern = Pattern.compile(prop.getProperty("validate.version"));

		macAddressPattern = Pattern.compile(prop.getProperty("validate.mac"));
		alphanumaricPattern = Pattern.compile(prop.getProperty("validate.alphanumaric"));
		alphanumaricPatternwithhypen = Pattern.compile(prop.getProperty("validate.alphanumaricwithhypen"));
		alphanumaricPatternwithspace = Pattern.compile(prop.getProperty("validate.alphanumaricwithspace"));
		fourdigitnumeric = Pattern.compile(prop.getProperty("validate.fourdigitnumaric"));
		urlPattern = Pattern.compile(prop.getProperty("validate.url"));
		passwordPattern = Pattern.compile(prop.getProperty("validate.password"));
		sixHexaDecimalPattern = Pattern.compile(prop.getProperty("validate.sixdigit.hexadecimal"));
		leaseTimePattern = Pattern.compile(prop.getProperty("validate.leaseTime"));
		alphanumaricSplPattern64 = Pattern.compile(prop.getProperty("validate.alphanumaric.spl64"));
		
	}

	public static boolean isValidSSID(final String ssid) {
		return ssidPattern.matcher(ssid).matches();
	}

	public static boolean isValidPresharedKey(final String presharedKey) {
		return preSharedKeyPattern.matcher(presharedKey).matches();
	}

	// returns true if valid
	public static boolean validateEmail(final String emailId) {
		return emailPattern.matcher(emailId).matches();
	}

	// returns true if valid
	public static boolean validateNumber(final String number) {
		return numberPattern.matcher(number).matches();
	}

	// returns true if valid
	public static boolean isValidNumber(final Number number) {
		return numberPattern.matcher(number.toString()).matches();
	}

	// returns true if valid
	// Valid only if password contains, 6 to 20 characters string with at least
	// one digit, one upper case letter, one lower case letter and one special
	// symbol (�@#$%�)
	public static boolean isStrongPassword(final String password) {
		return strongPasswordPattern.matcher(password).matches();
	}

	// returns true if valid
	public static boolean validateIPAddress(final String ip) {
		return ipAddressPattern.matcher(ip).matches();
	}

	// returns true if valid
	public static boolean validateIPV6Address(final String ipv6) throws Exception{
		return ipv6AddressPattern.matcher(ipv6).matches();
	}

	// returns true if valid
	public static boolean isValidVersionFormat(final String version) throws Exception{
		return versionPattern.matcher(version).matches();
	}

	// returns true if valid
	public static boolean isValidMACFormat(final String macAddress) throws Exception {
		return macAddressPattern.matcher(macAddress).matches();
	}

	// returns true if valid
	public static boolean isValidAlphaNumaricFormat(final String alphanumaric) throws Exception {
		return alphanumaricPattern.matcher(alphanumaric).matches();
	}

	// returns true if valid
	public static boolean isValidAlphaNumaricWithHypenFormat(final String alphanumaricwithhypen) throws Exception {
		return alphanumaricPatternwithhypen.matcher(alphanumaricwithhypen).matches();
	}

	// returns true if valid
	public static boolean isValidAlphaNumaricWithSpaceFormat(final String alphanumaricwithspace) throws Exception {
		return alphanumaricPatternwithspace.matcher(alphanumaricwithspace).matches();
	}

	// returns true if valid
	public static boolean isValidFourDigitNumaricFormat(final Number fourDigitNumeric) {
		return fourdigitnumeric.matcher(fourDigitNumeric.toString()).matches();
	}

	public static boolean isBooleanParameter(final boolean param) {
		if (param == true || param == false)
			return true;

		return false;
	}
	
	public static boolean isValidURLFormat(final String url) throws Exception {
		return urlPattern.matcher(url).matches();
	}
	
	public static boolean isValidPasswordFormat(final String password) throws Exception{
		return passwordPattern.matcher(password).matches();
	}
	
	public static boolean isValidIPV4Address(final String ipStr) throws Exception {
		return ipAddressPattern.matcher(ipStr).matches();
	}
	
	public static boolean isValidIPV6Address(final String ipStr) throws Exception {
		return ipv6AddressPattern.matcher(ipStr).matches();
	}
	
	public static boolean isValidManufacturerOUIFormat(final String manufacturerOUI) throws Exception {
		return sixHexaDecimalPattern.matcher(manufacturerOUI).matches();
	}
	
	public static boolean isValidString64Format(final String value)  throws Exception{
		return alphanumaricSplPattern64.matcher(value).matches();
	}
	
	public static boolean isValidLeaseTimeFormat(final String value)  throws Exception{
		return leaseTimePattern.matcher(value).matches();
	}
	
	public enum Mode {
	 
	    YES("YES"),  
	    NO ("NO") ; 


	    private final String mode;

	    Mode(String mode) {
	        this.mode = mode;
	    }
	    
	    public String getLevelCode() {
	        return this.mode;
	    }
	    
	    public boolean contains(String mode){
	    	Mode[] mode1 = Mode.values();
	    	
	    	for(Mode myMode: mode1){
	    		if(myMode.equals(mode)){
	    			return true;
	    		}
	    		
	    	}
	    	
	    	return false;
	    }
	    
	}
	
	public enum Switch {
		 
	    ON("ON"),  
	    OFF ("OFF") ; 


	    private final String mode;

	    Switch(String mode) {
	        this.mode = mode;
	    }
	    
	    public String getLevelCode() {
	        return this.mode;
	    }
	    
	    public boolean contains(String mode){
	    	Switch[] mode1 = Switch.values();
	    	
	    	for(Switch myMode: mode1){
	    		if(myMode.equals(mode)){
	    			return true;
	    		}
	    		
	    	}
	    	
	    	return false;
	    }
	    
	}


	public static <E extends Enum<E>> boolean isInEnum(String value, Class<E> enumClass) {
		for (E e : enumClass.getEnumConstants()) {
			if (e.name().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isValidString64(String input) {
		if (null != input && input.trim().length() > 0 && input.trim().length() <= 64) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isEmptyField(String input) {
		if (null == input || input.trim().equals("") || input.trim().length() <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Boolean isValidString(String str){
		Boolean signal = Boolean.FALSE;
		
		if(null != str && !str.isEmpty()){
			signal = Boolean.TRUE;
		}
		
		return signal;
	}

	public static boolean isEmptyField(Integer input) {
		if (null == input || input.equals(" ")) {
			return true;
		} else {
			return false;
		}
	}

	public static <T> boolean isEmptyCollection(List<T> inputList) {
		if (inputList == null || inputList.isEmpty() || inputList.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * validates for both IPV4 and IPV6
	 * @param value
	 * @return
	 */
	public static boolean isvalidIPFormat(final String value) throws Exception{
		if(!isEmptyField(value)){
			
			if(isValidIPV4Address(value) || isValidIPV6Address(value)){
				return true;
			}else{
				throw new Exception( "Attempt to set a invalid value. Value should either be in IPV4 or IPV6 format.");
			}
		}else{
			throw new Exception("Attempt to set a invalid value. Value should not be null.");
		}
		
	}

	public static void isValidPassPhrase(final String value) throws Exception{
		if(!isEmptyField(value)){
			
			if(value.length() >= 8 && value.length() <= 16 ){
				return ;
			}else{
				throw new Exception( "Attempt to set a invalid value. Value should be Minimum 8 characters & Max 16 characters.");
			}
			
		}else{
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
	}

	public static boolean isValidEnable(String value) throws Exception {
		if(!isEmptyField(value)){
			if(isInEnum(value, Mode.class)){
				return true;
			}else{
				throw new Exception( "Attempt to set a invalid value. Value should be YES or NO.");
			}
			
		}else{
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
	}

	public static void isValidManufacturerOUI(String value) throws Exception {
		
		if (!isEmptyField(value)) {			
			if (isValidManufacturerOUIFormat(value)) {				
				return;
			} else {
				throw new Exception( "Attempt to set a invalid value. "
						+ "Value should be a six hexadecimal-digit value using all upper-case letters and including leading zeros");			
			}
			
		} else {
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
		
	}
	
	public static void isValidPassword(String value) throws Exception {
		if(!isEmptyField(value)){
			if(isValidPasswordFormat(value)){
				return;
			}else{
				throw new Exception("Attempt to set a invalid value. Value should be YES or NO.");
			}
			
		}else{
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
	}
	
	
	public static void isValidLeaseTime(String value) throws Exception {
		if(!isEmptyField(value)){
			if(isValidLeaseTimeFormat(value)){
				return;
			}else{
				throw new Exception( "Attempt to set a invalid value. Value should be positive integer or -1.");
			}
			
		}else{
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
	}
	
	public static void isValidSwitch(String value) throws Exception {
		if(!isEmptyField(value)){
			if(isInEnum(value, Switch.class)){
				return;
			}else{
				throw new Exception( "Attempt to set a invalid value. Value should be YES or NO.");
			}
			
		}else{
			throw new Exception( "Attempt to set a invalid value. Value should not be null.");
		}
	}
	
	public static void isValidDNS(String value) throws Exception{
		if(value.contains(",")){
			RequestValidator.isvalidIPFormat(value.split(",")[0]);
			RequestValidator.isvalidIPFormat(value.split(",")[1]);
		}else{
			throw new Exception( "Attempt to set a invalid value. Should have minimum two DNS server IP's seperated by comma. ");
		}
	}
	
	
}
